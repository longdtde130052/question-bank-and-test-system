<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html dir="ltr" lang="en" xml:lang="en" class="yui3-js-enabled"><head>
        <title>My Home Page</title>
        <link rel="shortcut icon" href="https://cacbank.moodlecloud.com/theme/image.php/boost/theme/1569995285/favicon">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="keywords" content="moodle, My new Moodle site">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cacbank.moodlecloud.com/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.css"><script async="" src="https://www.google-analytics.com/analytics.js"></script><script charset="utf-8" id="yui_3_17_2_1_1572873373103_8" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?m/1569995285/core/event/event-min.js&amp;m/1569995285/filter_mathjaxloader/loader/loader-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1572873373103_19" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?3.17.2/event-mousewheel/event-mousewheel-min.js&amp;3.17.2/event-resize/event-resize-min.js&amp;3.17.2/event-hover/event-hover-min.js&amp;3.17.2/event-touch/event-touch-min.js&amp;3.17.2/event-move/event-move-min.js&amp;3.17.2/event-flick/event-flick-min.js&amp;3.17.2/event-valuechange/event-valuechange-min.js&amp;3.17.2/event-tap/event-tap-min.js" async=""></script><script id="firstthemesheet" type="text/css">/** Required in order to fix style inclusion problems in IE with YUI **/</script><link rel="stylesheet" type="text/css" href="https://cacbank.moodlecloud.com/theme/styles.php/boost/1569995285_1569995291/all">
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="core/first" src="https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/core/first.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="core/str" src="https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/core/str.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="jquery" src="https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/jquery/jquery-3.2.1.min.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="core/config" src="https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/core/config.js"></script><script type="text/x-mathjax-config">
            MathJax.Hub.Config({
            config: ["Accessible.js", "Safe.js"],
            errorSettings: { message: ["!"] },
            skipStartupTypeset: true,
            messageStyle: "none"
            });
        </script></head>
    <body id="page-site-index" class="format-site course path-site chrome dir-ltr lang-en yui-skin-sam yui3-skin-sam cacbank-moodlecloud-com pagelayout-frontpage course-1 context-2 drawer-open-left jsenabled">

        <div id="page-wrapper">

            <div>
                <a class="sr-only sr-only-focusable" href="#maincontent">Skip to main content</a>
            </div><script type="text/javascript" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.js"></script><script type="text/javascript" src="https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/javascript-static.js"></script>



            <nav class="fixed-top navbar navbar-light bg-white navbar-expand moodle-has-zindex" aria-label="Site navigation">

                <div data-region="drawer-toggle" class="d-inline-block mr-3">
                    <button aria-expanded="true" aria-controls="nav-drawer" type="button" class="btn nav-link float-sm-left mr-1 btn-light bg-gray" data-action="toggle-drawer" data-side="left" data-preference="drawer-open-nav"><i class="fas fa-bars"></i><span class="sr-only">Side panel</span></button>
                </div>

                <a href="homeLec_1.jsp" class="navbar-brand 
                   d-none d-md-block
                   ">
                    <span class="site-name d-none d-md-inline">Question and Test Bank</span>
                </a>
                <ul class="navbar-nav d-none d-md-flex">
                    <!-- custom_menu -->
                    <li class="dropdown nav-item">
                        <a class="dropdown-toggle nav-link" id="drop-down-5dc0249d7928b5dc0249d773cf4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" aria-controls="drop-down-menu-5dc0249d7928b5dc0249d773cf4">
                            English ‎(en)‎
                        </a>
                    </li>

                </ul>
                <ul class="nav navbar-nav ml-auto">
                    [                    <div class="d-none d-lg-block">

                    </div>
                    <!-- navbar_plugin_output -->
                    <li class="nav-item">
                        <div class="float-right popover-region collapsed">
                            <a id="message-drawer-toggle-5dc0249d7e1065dc0249d773cf5" class="nav-link d-inline-block popover-region-toggle position-relative" href="profile" role="button">
                                <i class="fas fa-user"></i>
                                <div class="count-container hidden" data-region="count-container" aria-label="There are 0 unread conversations">0</div>
                            </a>
                        </div><div class="popover-region collapsed popover-region-notifications" id="nav-notification-popover-container" data-userid="2" data-region="popover-region">

                            <div id="popover-region-container-5dc0249d80d125dc0249d773cf6" class="popover-region-container" data-region="popover-region-container" aria-expanded="false" aria-hidden="true" aria-label="Notification window" role="region">
                                <div class="popover-region-header-container">
                                    <h3 class="popover-region-header-text" data-region="popover-region-header-text">Notifications</h3>
                                    <div class="popover-region-header-actions" data-region="popover-region-header-actions">        <a class="mark-all-read-button" href="#" title="Mark all as read" data-action="mark-all-read" role="button">
                                            <span class="normal-icon"><i class="icon fa fa-check fa-fw " title="Mark all as read" aria-label="Mark all as read"></i></span>
                                            <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </a>
                                        <a href="#" title="Notification preferences">
                                            <i class="fas fa-cog"></i>
                                            <ul>
                                                <li>
                                                    <a href="changepass.jsp">Change Password</a>
                                                    <a href="editprofile.jsp">Edit Profile</a>
                                                </li>
                                            </ul>
                                        </a>
                                    </div>
                                </div>
                                <div class="popover-region-content-container" data-region="popover-region-content-container">
                                    <div class="popover-region-content" data-region="popover-region-content">
                                        <div class="all-notifications" data-region="all-notifications" role="log" aria-busy="false" aria-atomic="false" aria-relevant="additions"></div>
                                        <div class="empty-message" tabindex="0" data-region="empty-message">You have no notifications</div>

                                    </div>
                                    <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                </div>
                                <a class="see-all-link" href="#">
                                    <div class="popover-region-footer-container">
                                        <div class="popover-region-seeall-text">See all</div>
                                    </div>
                                </a>
                            </div>
                        </div><div class="popover-region collapsed popover-region-quotas boost" id="nav-moodlecloud-quota-popover-container" data-userid="" data-region="popover-region">

                            <div id="popover-region-container-5dc0249d826255dc0249d773cf7" class="popover-region-container" data-region="popover-region-container" aria-expanded="false" aria-hidden="true" aria-label="Notification window" role="region">
                                <div class="popover-region-header-container">
                                    <h3 class="popover-region-header-text" data-region="popover-region-header-text">Quotas</h3>
                                    <div class="popover-region-header-actions" data-region="popover-region-header-actions"></div>
                                </div>
                                <div class="popover-region-content-container" data-region="popover-region-content-container">
                                    <div class="popover-region-content" data-region="popover-region-content">
                                        <div class="items">
                                            <div class="item users">
                                                <h2>2/50</h2>
                                                <h3>Users</h3>
                                                <svg width="160" height="160" xmlns="http://www.w3.org/2000/svg">
                                                <g>
                                                <circle id="circle" style="stroke-dashoffset: 0;" class="circle_animation" r="69.85699" cy="81" cx="81" stroke-width="20" stroke="#dddddd" fill="none"></circle>
                                                <circle id="circle" style="stroke-dashoffset: 422.4;" class="circle_animation" r="69.85699" cy="81" cx="81" stroke-width="19" stroke="#ff7518" fill="none"></circle>
                                                </g>
                                                </svg>
                                            </div>

                                            <div class="item storage">
                                                <h2>0/200</h2>
                                                <h3>MB used</h3>
                                                <svg width="160" height="160" xmlns="http://www.w3.org/2000/svg">
                                                <g>
                                                <circle id="circle" style="stroke-dashoffset: 0;" class="circle_animation" r="69.85699" cy="81" cx="81" stroke-width="20" stroke="#dddddd" fill="none"></circle>
                                                <circle id="circle" style="stroke-dashoffset: 439.79790000916;" class="circle_animation" r="69.85699" cy="81" cx="81" stroke-width="19" stroke="#ff7518" fill="none"></circle>
                                                </g>
                                                </svg>
                                            </div>
                                        </div>

                                    </div>
                                    <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                </div>
                                <a class="see-all-link" href="#">
                                    <div class="popover-region-footer-container">
                                        <div class="popover-region-seeall-text">See all</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <!-- user_menu -->
                    <li class="nav-item d-flex align-items-center">
                        <div class="usermenu"><div class="action-menu moodle-actionmenu nowrap-items d-inline" id="action-menu-1" data-enhance="moodle-core-actionmenu">

                                <div class="menubar d-flex " id="action-menu-1-menubar" role="menubar">
                                    <div class="action-menu-trigger">
                                        <div class="dropdown">
                                            <a href="#" tabindex="0" class=" dropdown-toggle icon-no-margin" id="dropdown-1" aria-label="User menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" aria-controls="action-menu-1-menu">
                                                <span class="userbutton"><span class="usertext mr-1">${USER.name}</span><span class="avatars"><span class="avatar current"></span></span></span>

                                                <b class="caret"></b>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right menu  align-tr-br" id="action-menu-1-menu" data-rel="menu-content" aria-labelledby="action-menu-toggle-1" role="menu" data-align="tr-br">
                                                <div class="dropdown-divider" role="presentation"><span class="filler">&nbsp;</span></div>
                                                <a href="profile.jsp" class="dropdown-item menu-action" role="menuitem" data-title="profile,moodle" aria-labelledby="actionmenuaction-2">
                                                    <i class="icon fa fa-user fa-fw " aria-hidden="true"></i>
                                                    <span class="menu-action-text" id="actionmenuaction-2">
                                                        Profile
                                                    </span>
                                                </a>
                                                <a href="#" class="dropdown-item menu-action" role="menuitem" data-title="grades,grades" aria-labelledby="actionmenuaction-3">
                                                    <i class="icon fa fa-table fa-fw " aria-hidden="true"></i>
                                                    <span class="menu-action-text" id="actionmenuaction-3">
                                                        Grades
                                                    </span>
                                                </a>
                                                <a href="#" class="dropdown-item menu-action" role="menuitem" data-title="messages,message" aria-labelledby="actionmenuaction-4">
                                                    <i class="icon fa fa-comment fa-fw " aria-hidden="true"></i>
                                                    <span class="menu-action-text" id="actionmenuaction-4">
                                                        Messages
                                                    </span>
                                                </a>
                                                <a href="#" class="dropdown-item menu-action" role="menuitem" data-title="preferences,moodle" aria-labelledby="actionmenuaction-5">
                                                    <i class="icon fa fa-wrench fa-fw " aria-hidden="true"></i>
                                                    <span class="menu-action-text" id="actionmenuaction-5">
                                                        Preferences
                                                    </span>
                                                </a>
                                                <a href="logout" class="dropdown-item menu-action" role="menuitem" data-title="logout,moodle" aria-labelledby="actionmenuaction-6">
                                                    <i class="icon fa fa-sign-out fa-fw " aria-hidden="true"></i>
                                                    <span class="menu-action-text" id="actionmenuaction-6">
                                                        Log out
                                                    </span>
                                                </a>
                                                <div class="dropdown-divider" role="presentation"><span class="filler">&nbsp;</span></div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div></div>
                    </li>
                </ul>
                <!-- search_box -->
            </nav>
            <div id="page" class="container-fluid">
                <header id="page-header" class="row">
                    <div class="col-12 pt-3 pb-3">
                        <div class="card ">
                            <div class="card-body ">
                                <div class="d-flex">
                                    <div class="mr-auto">
                                        <div class="page-context-header"><div class="page-header-headings"><h1>All Course</h1></div></div>
                                    </div>

                                    <div class="context-header-settings-menu">
                                        <div class="action-menu moodle-actionmenu d-inline" id="action-menu-2" data-enhance="moodle-core-actionmenu">
                                            <div class="menubar d-flex " id="action-menu-2-menubar" role="menubar">


                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex flex-wrap">
                                    <div class="ml-auto d-flex">

                                    </div>
                                    <div id="course-header">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <div id="page-content" class="row pb-3">
                    <div id="region-main-box" class="col-12">
                        <section id="region-main" aria-label="Content">
                            <c:forEach items="${listCourse}" var="c">
                                <li><a style="font-size: 25px;" href="view?cCode=${c.cCode}"> ${c.cName} </a> <br>
                                    <label>Teacher:</label> <a href="#">${USER.name}</a>
                                    <hr>
                                </c:forEach>
                        </section>
                    </div>
                </div>
            </div>

            <div id="nav-drawer" data-region="drawer" class="d-print-none moodle-has-zindex " aria-hidden="false" tabindex="-1">
                <nav class="list-group" aria-label="Site">
                    <a class="list-group-item list-group-item-action active" href="homeLec_1.jsp" data-key="home" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="1" data-nodetype="1" data-collapse="0" data-forceopen="1" data-isactive="1" data-hidden="0" data-preceedwithhr="0">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body font-weight-bold">Home</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="questionBank" data-key="myhome" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="70" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="home">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body ">Question Bank</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="createTest.jsp" data-key="myhome" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="70" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="home">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body ">Create Test</span>
                            </div>
                        </div>
                    </a>
                    <div class="list-group-item" data-key="mycourses" data-isexpandable="1" data-indent="0" data-showdivider="0" data-type="0" data-nodetype="1" data-collapse="0" data-forceopen="1" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="home">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body">My courses</span>
                            </div>
                        </div>
                    </div>
                    <c:forEach items="${listCourse}" var="c">
                        <a class="list-group-item list-group-item-action " href="#" data-key="2" data-isexpandable="1" data-indent="1" data-showdivider="0" data-type="20" data-nodetype="1" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="1" data-preceedwithhr="0" data-parent-key="mycourses">
                            <div class="ml-1">
                                <div class="media">
                                    <span class="media-body ">${c.cName}</span>
                                </div>
                            </div>
                        </a>
                    </c:forEach>
                </nav>
            </div>
            <footer id="page-footer" class="py-3 bg-dark text-light">
                <div class="container">
                    <div id="course-footer"></div>
                    <div class="logininfo">You are logged in as <a href="profile.jsp" title="View profile">${USER.name}</a> (<a href="logout">Log out</a>)</div>
                    <div class="tool_usertours-resettourcontainer"></div>
                    <div class="sitelink"><a title="Moodle" href="http://moodle.org/"><img src="https://cacbank.moodlecloud.com/theme/image.php/boost/core/1569995285/moodlelogo_grayhat" alt="Moodle logo"></a></div>
                    <nav class="nav navbar-nav d-md-none" aria-label="Custom menu">

                    </nav>
                    <div class="tool_dataprivacy"><a href="https://cacbank.moodlecloud.com/admin/tool/dataprivacy/summary.php">Data retention summary</a></div><a href="https://download.moodle.org/mobile?version=2019052002&amp;lang=en&amp;iosappid=633359593&amp;androidappid=com.moodle.moodlemobile">Get the mobile app</a><div class="policiesfooter"><a href="https://cacbank.moodlecloud.com/admin/tool/policy/viewall.php?returnurl=https%3A%2F%2Fcacbank.moodlecloud.com%2F">Policies</a></div>

                </div>  
            </footer>
        </div><div id="yui3-css-stamp" style="position: absolute !important; visibility: hidden !important" class=""></div>


    </body></html>