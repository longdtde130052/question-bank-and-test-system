<%@page import="model.Competition"%>
<%@page import="model.Chapter"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Course"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<html dir="ltr" lang="en" xml:lang="en" class="yui3-js-enabled"><head>
        <title>My Home Page</title>
        <link rel="shortcut icon" href="https://cacbank.moodlecloud.com/theme/image.php/boost/theme/1569995285/favicon">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="keywords" content="moodle, My new Moodle site">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cacbank.moodlecloud.com/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.css"><script async="" src="https://www.google-analytics.com/analytics.js"></script><script charset="utf-8" id="yui_3_17_2_1_1572873373103_8" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?m/1569995285/core/event/event-min.js&amp;m/1569995285/filter_mathjaxloader/loader/loader-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1572873373103_19" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?3.17.2/event-mousewheel/event-mousewheel-min.js&amp;3.17.2/event-resize/event-resize-min.js&amp;3.17.2/event-hover/event-hover-min.js&amp;3.17.2/event-touch/event-touch-min.js&amp;3.17.2/event-move/event-move-min.js&amp;3.17.2/event-flick/event-flick-min.js&amp;3.17.2/event-valuechange/event-valuechange-min.js&amp;3.17.2/event-tap/event-tap-min.js" async=""></script><script id="firstthemesheet" type="text/css">/** Required in order to fix style inclusion problems in IE with YUI **/</script><link rel="stylesheet" type="text/css" href="https://cacbank.moodlecloud.com/theme/styles.php/boost/1569995285_1569995291/all">
        <script type="text/javascript">
            //<![CDATA[
            var M = {};
            M.yui = {};
            M.pageloadstarttime = new Date();
            M.cfg = {"wwwroot": "https:\/\/cacbank.moodlecloud.com", "sesskey": "fHCfTyJ6FE", "themerev": "1569995285", "slasharguments": 1, "theme": "boost", "iconsystemmodule": "core\/icon_system_fontawesome", "jsrev": "1569995285", "admin": "admin", "svgicons": true, "usertimezone": "Asia\/Ho_Chi_Minh", "contextid": 2};
            var yui1ConfigFn = function (me) {
                if (/-skin|reset|fonts|grids|base/.test(me.name)) {
                    me.type = 'css';
                    me.path = me.path.replace(/\.js/, '.css');
                    me.path = me.path.replace(/\/yui2-skin/, '/assets/skins/sam/yui2-skin')
                }
            };
            var yui2ConfigFn = function (me) {
                var parts = me.name.replace(/^moodle-/, '').split('-'), component = parts.shift(), module = parts[0], min = '-min';
                if (/-(skin|core)$/.test(me.name)) {
                    parts.pop();
                    me.type = 'css';
                    min = ''
                }
                if (module) {
                    var filename = parts.join('-');
                    me.path = component + '/' + module + '/' + filename + min + '.' + me.type
                } else {
                    me.path = component + '/' + component + '.' + me.type
                }
            };
            YUI_config = {"debug": false, "base": "https:\/\/cacbank.moodlecloud.com\/lib\/yuilib\/3.17.2\/", "comboBase": "https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?", "combine": true, "filter": null, "insertBefore": "firstthemesheet", "groups": {"yui2": {"base": "https:\/\/cacbank.moodlecloud.com\/lib\/yuilib\/2in3\/2.9.0\/build\/", "comboBase": "https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?", "combine": true, "ext": false, "root": "2in3\/2.9.0\/build\/", "patterns": {"yui2-": {"group": "yui2", "configFn": yui1ConfigFn}}}, "moodle": {"name": "moodle", "base": "https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?m\/1569995285\/", "combine": true, "comboBase": "https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?", "ext": false, "root": "m\/1569995285\/", "patterns": {"moodle-": {"group": "moodle", "configFn": yui2ConfigFn}}, "filter": null, "modules": {"moodle-core-popuphelp": {"requires": ["moodle-core-tooltip"]}, "moodle-core-lockscroll": {"requires": ["plugin", "base-build"]}, "moodle-core-tooltip": {"requires": ["base", "node", "io-base", "moodle-core-notification-dialogue", "json-parse", "widget-position", "widget-position-align", "event-outside", "cache-base"]}, "moodle-core-blocks": {"requires": ["base", "node", "io", "dom", "dd", "dd-scroll", "moodle-core-dragdrop", "moodle-core-notification"]}, "moodle-core-chooserdialogue": {"requires": ["base", "panel", "moodle-core-notification"]}, "moodle-core-event": {"requires": ["event-custom"]}, "moodle-core-notification": {"requires": ["moodle-core-notification-dialogue", "moodle-core-notification-alert", "moodle-core-notification-confirm", "moodle-core-notification-exception", "moodle-core-notification-ajaxexception"]}, "moodle-core-notification-dialogue": {"requires": ["base", "node", "panel", "escape", "event-key", "dd-plugin", "moodle-core-widget-focusafterclose", "moodle-core-lockscroll"]}, "moodle-core-notification-alert": {"requires": ["moodle-core-notification-dialogue"]}, "moodle-core-notification-confirm": {"requires": ["moodle-core-notification-dialogue"]}, "moodle-core-notification-exception": {"requires": ["moodle-core-notification-dialogue"]}, "moodle-core-notification-ajaxexception": {"requires": ["moodle-core-notification-dialogue"]}, "moodle-core-formchangechecker": {"requires": ["base", "event-focus", "moodle-core-event"]}, "moodle-core-actionmenu": {"requires": ["base", "event", "node-event-simulate"]}, "moodle-core-checknet": {"requires": ["base-base", "moodle-core-notification-alert", "io-base"]}, "moodle-core-languninstallconfirm": {"requires": ["base", "node", "moodle-core-notification-confirm", "moodle-core-notification-alert"]}, "moodle-core-maintenancemodetimer": {"requires": ["base", "node"]}, "moodle-core-dragdrop": {"requires": ["base", "node", "io", "dom", "dd", "event-key", "event-focus", "moodle-core-notification"]}, "moodle-core-handlebars": {"condition": {"trigger": "handlebars", "when": "after"}}, "moodle-core_availability-form": {"requires": ["base", "node", "event", "event-delegate", "panel", "moodle-core-notification-dialogue", "json"]}, "moodle-backup-confirmcancel": {"requires": ["node", "node-event-simulate", "moodle-core-notification-confirm"]}, "moodle-backup-backupselectall": {"requires": ["node", "event", "node-event-simulate", "anim"]}, "moodle-course-categoryexpander": {"requires": ["node", "event-key"]}, "moodle-course-modchooser": {"requires": ["moodle-core-chooserdialogue", "moodle-course-coursebase"]}, "moodle-course-management": {"requires": ["base", "node", "io-base", "moodle-core-notification-exception", "json-parse", "dd-constrain", "dd-proxy", "dd-drop", "dd-delegate", "node-event-delegate"]}, "moodle-course-formatchooser": {"requires": ["base", "node", "node-event-simulate"]}, "moodle-course-util": {"requires": ["node"], "use": ["moodle-course-util-base"], "submodules": {"moodle-course-util-base": {}, "moodle-course-util-section": {"requires": ["node", "moodle-course-util-base"]}, "moodle-course-util-cm": {"requires": ["node", "moodle-course-util-base"]}}}, "moodle-course-dragdrop": {"requires": ["base", "node", "io", "dom", "dd", "dd-scroll", "moodle-core-dragdrop", "moodle-core-notification", "moodle-course-coursebase", "moodle-course-util"]}, "moodle-form-shortforms": {"requires": ["node", "base", "selector-css3", "moodle-core-event"]}, "moodle-form-passwordunmask": {"requires": []}, "moodle-form-dateselector": {"requires": ["base", "node", "overlay", "calendar"]}, "moodle-question-searchform": {"requires": ["base", "node"]}, "moodle-question-chooser": {"requires": ["moodle-core-chooserdialogue"]}, "moodle-question-preview": {"requires": ["base", "dom", "event-delegate", "event-key", "core_question_engine"]}, "moodle-availability_completion-form": {"requires": ["base", "node", "event", "moodle-core_availability-form"]}, "moodle-availability_date-form": {"requires": ["base", "node", "event", "io", "moodle-core_availability-form"]}, "moodle-availability_grade-form": {"requires": ["base", "node", "event", "moodle-core_availability-form"]}, "moodle-availability_group-form": {"requires": ["base", "node", "event", "moodle-core_availability-form"]}, "moodle-availability_grouping-form": {"requires": ["base", "node", "event", "moodle-core_availability-form"]}, "moodle-availability_profile-form": {"requires": ["base", "node", "event", "moodle-core_availability-form"]}, "moodle-mod_assign-history": {"requires": ["node", "transition"]}, "moodle-mod_bigbluebuttonbn-recordings": {"requires": ["base", "node", "datasource-get", "datasource-jsonschema", "datasource-polling", "moodle-core-notification"]}, "moodle-mod_bigbluebuttonbn-imports": {"requires": ["base", "node"]}, "moodle-mod_bigbluebuttonbn-rooms": {"requires": ["base", "node", "datasource-get", "datasource-jsonschema", "datasource-polling", "moodle-core-notification"]}, "moodle-mod_bigbluebuttonbn-broker": {"requires": ["base", "node", "datasource-get", "datasource-jsonschema", "datasource-polling", "moodle-core-notification"]}, "moodle-mod_bigbluebuttonbn-modform": {"requires": ["base", "node"]}, "moodle-mod_forum-subscriptiontoggle": {"requires": ["base-base", "io-base"]}, "moodle-mod_quiz-questionchooser": {"requires": ["moodle-core-chooserdialogue", "moodle-mod_quiz-util", "querystring-parse"]}, "moodle-mod_quiz-autosave": {"requires": ["base", "node", "event", "event-valuechange", "node-event-delegate", "io-form"]}, "moodle-mod_quiz-quizbase": {"requires": ["base", "node"]}, "moodle-mod_quiz-toolboxes": {"requires": ["base", "node", "event", "event-key", "io", "moodle-mod_quiz-quizbase", "moodle-mod_quiz-util-slot", "moodle-core-notification-ajaxexception"]}, "moodle-mod_quiz-modform": {"requires": ["base", "node", "event"]}, "moodle-mod_quiz-util": {"requires": ["node", "moodle-core-actionmenu"], "use": ["moodle-mod_quiz-util-base"], "submodules": {"moodle-mod_quiz-util-base": {}, "moodle-mod_quiz-util-slot": {"requires": ["node", "moodle-mod_quiz-util-base"]}, "moodle-mod_quiz-util-page": {"requires": ["node", "moodle-mod_quiz-util-base"]}}}, "moodle-mod_quiz-dragdrop": {"requires": ["base", "node", "io", "dom", "dd", "dd-scroll", "moodle-core-dragdrop", "moodle-core-notification", "moodle-mod_quiz-quizbase", "moodle-mod_quiz-util-base", "moodle-mod_quiz-util-page", "moodle-mod_quiz-util-slot", "moodle-course-util"]}, "moodle-message_airnotifier-toolboxes": {"requires": ["base", "node", "io"]}, "moodle-filter_glossary-autolinker": {"requires": ["base", "node", "io-base", "json-parse", "event-delegate", "overlay", "moodle-core-event", "moodle-core-notification-alert", "moodle-core-notification-exception", "moodle-core-notification-ajaxexception"]}, "moodle-filter_mathjaxloader-loader": {"requires": ["moodle-core-event"]}, "moodle-editor_atto-rangy": {"requires": []}, "moodle-editor_atto-editor": {"requires": ["node", "transition", "io", "overlay", "escape", "event", "event-simulate", "event-custom", "node-event-html5", "node-event-simulate", "yui-throttle", "moodle-core-notification-dialogue", "moodle-core-notification-confirm", "moodle-editor_atto-rangy", "handlebars", "timers", "querystring-stringify"]}, "moodle-editor_atto-plugin": {"requires": ["node", "base", "escape", "event", "event-outside", "handlebars", "event-custom", "timers", "moodle-editor_atto-menu"]}, "moodle-editor_atto-menu": {"requires": ["moodle-core-notification-dialogue", "node", "event", "event-custom"]}, "moodle-report_eventlist-eventfilter": {"requires": ["base", "event", "node", "node-event-delegate", "datatable", "autocomplete", "autocomplete-filters"]}, "moodle-report_loglive-fetchlogs": {"requires": ["base", "event", "node", "io", "node-event-delegate"]}, "moodle-gradereport_grader-gradereporttable": {"requires": ["base", "node", "event", "handlebars", "overlay", "event-hover"]}, "moodle-gradereport_history-userselector": {"requires": ["escape", "event-delegate", "event-key", "handlebars", "io-base", "json-parse", "moodle-core-notification-dialogue"]}, "moodle-tool_capability-search": {"requires": ["base", "node"]}, "moodle-tool_lp-dragdrop-reorder": {"requires": ["moodle-core-dragdrop"]}, "moodle-tool_monitor-dropdown": {"requires": ["base", "event", "node"]}, "moodle-assignfeedback_editpdf-editor": {"requires": ["base", "event", "node", "io", "graphics", "json", "event-move", "event-resize", "transition", "querystring-stringify-simple", "moodle-core-notification-dialog", "moodle-core-notification-alert", "moodle-core-notification-warning", "moodle-core-notification-exception", "moodle-core-notification-ajaxexception"]}, "moodle-atto_accessibilitychecker-button": {"requires": ["color-base", "moodle-editor_atto-plugin"]}, "moodle-atto_accessibilityhelper-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_align-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_bold-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_charmap-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_clear-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_collapse-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_emoticon-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_equation-button": {"requires": ["moodle-editor_atto-plugin", "moodle-core-event", "io", "event-valuechange", "tabview", "array-extras"]}, "moodle-atto_html-beautify": {}, "moodle-atto_html-codemirror": {"requires": ["moodle-atto_html-codemirror-skin"]}, "moodle-atto_html-button": {"requires": ["promise", "moodle-editor_atto-plugin", "moodle-atto_html-beautify", "moodle-atto_html-codemirror", "event-valuechange"]}, "moodle-atto_image-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_indent-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_italic-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_link-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_managefiles-usedfiles": {"requires": ["node", "escape"]}, "moodle-atto_managefiles-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_media-button": {"requires": ["moodle-editor_atto-plugin", "moodle-form-shortforms"]}, "moodle-atto_noautolink-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_orderedlist-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_recordrtc-recording": {"requires": ["moodle-atto_recordrtc-button"]}, "moodle-atto_recordrtc-button": {"requires": ["moodle-editor_atto-plugin", "moodle-atto_recordrtc-recording"]}, "moodle-atto_rtl-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_strike-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_subscript-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_superscript-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_table-button": {"requires": ["moodle-editor_atto-plugin", "moodle-editor_atto-menu", "event", "event-valuechange"]}, "moodle-atto_title-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_underline-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_undo-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_unorderedlist-button": {"requires": ["moodle-editor_atto-plugin"]}}}, "gallery": {"name": "gallery", "base": "https:\/\/cacbank.moodlecloud.com\/lib\/yuilib\/gallery\/", "combine": true, "comboBase": "https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?", "ext": false, "root": "gallery\/1569995285\/", "patterns": {"gallery-": {"group": "gallery"}}}}, "modules": {"core_filepicker": {"name": "core_filepicker", "fullpath": "https:\/\/cacbank.moodlecloud.com\/lib\/javascript.php\/1569995285\/repository\/filepicker.js", "requires": ["base", "node", "node-event-simulate", "json", "async-queue", "io-base", "io-upload-iframe", "io-form", "yui2-treeview", "panel", "cookie", "datatable", "datatable-sort", "resize-plugin", "dd-plugin", "escape", "moodle-core_filepicker", "moodle-core-notification-dialogue"]}, "core_comment": {"name": "core_comment", "fullpath": "https:\/\/cacbank.moodlecloud.com\/lib\/javascript.php\/1569995285\/comment\/comment.js", "requires": ["base", "io-base", "node", "json", "yui2-animation", "overlay", "escape"]}, "mathjax": {"name": "mathjax", "fullpath": "https:\/\/cdnjs.cloudflare.com\/ajax\/libs\/mathjax\/2.7.2\/MathJax.js?delayStartupUntil=configured"}}};
            M.yui.loader = {modules: {}};

            //]]>
        </script>

        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="core/first" src="https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/core/first.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="core/str" src="https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/core/str.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="jquery" src="https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/jquery/jquery-3.2.1.min.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="core/config" src="https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/core/config.js"></script><script type="text/x-mathjax-config">
            MathJax.Hub.Config({
            config: ["Accessible.js", "Safe.js"],
            errorSettings: { message: ["!"] },
            skipStartupTypeset: true,
            messageStyle: "none"
            });
        </script></head>
    <body id="page-site-index" class="format-site course path-site chrome dir-ltr lang-en yui-skin-sam yui3-skin-sam cacbank-moodlecloud-com pagelayout-frontpage course-1 context-2 drawer-open-left jsenabled">

        <div id="page-wrapper">

            <div>
                <a class="sr-only sr-only-focusable" href="#maincontent">Skip to main content</a>
            </div><script type="text/javascript" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.js"></script><script type="text/javascript" src="https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/javascript-static.js"></script>
            <script type="text/javascript">
            //<![CDATA[
            document.body.className += ' jsenabled';
            //]]>
            </script>



            <nav class="fixed-top navbar navbar-light bg-white navbar-expand moodle-has-zindex" aria-label="Site navigation">

                <div data-region="drawer-toggle" class="d-inline-block mr-3">
                    <button aria-expanded="true" aria-controls="nav-drawer" type="button" class="btn nav-link float-sm-left mr-1 btn-light bg-gray" data-action="toggle-drawer" data-side="left" data-preference="drawer-open-nav"><i class="fas fa-bars"></i><span class="sr-only">Side panel</span></button>
                </div>

                <a href="homeLec_1.jsp" class="navbar-brand 
                   d-none d-md-block
                   ">
                    <span class="site-name d-none d-md-inline">Question and Test Bank</span>
                </a>
                <ul class="navbar-nav d-none d-md-flex">
                    <!-- custom_menu -->
                    <li class="dropdown nav-item">
                        <a class="dropdown-toggle nav-link" id="drop-down-5dc0249d7928b5dc0249d773cf4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" aria-controls="drop-down-menu-5dc0249d7928b5dc0249d773cf4">
                            English ‎(en)‎
                        </a>
                    </li>

                </ul>
                <ul class="nav navbar-nav ml-auto">
                    <div class="d-none d-lg-block">

                    </div>
                    <!-- navbar_plugin_output -->
                    <li class="nav-item">
                        <div class="float-right popover-region collapsed">
                            <a id="message-drawer-toggle-5dc0249d7e1065dc0249d773cf5" class="nav-link d-inline-block popover-region-toggle position-relative" href="#" role="button">
                                <i class="fas fa-user"></i>
                                <div class="count-container hidden" data-region="count-container" aria-label="There are 0 unread conversations">0</div>
                            </a>
                        </div><div class="popover-region collapsed popover-region-notifications" id="nav-notification-popover-container" data-userid="2" data-region="popover-region">

                            <div id="popover-region-container-5dc0249d80d125dc0249d773cf6" class="popover-region-container" data-region="popover-region-container" aria-expanded="false" aria-hidden="true" aria-label="Notification window" role="region">
                                <div class="popover-region-header-container">
                                    <h3 class="popover-region-header-text" data-region="popover-region-header-text">Notifications</h3>
                                    <div class="popover-region-header-actions" data-region="popover-region-header-actions">        <a class="mark-all-read-button" href="#" title="Mark all as read" data-action="mark-all-read" role="button">
                                            <span class="normal-icon"><i class="icon fa fa-check fa-fw " title="Mark all as read" aria-label="Mark all as read"></i></span>
                                            <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </a>
                                        <a href="#" title="Notification preferences">
                                            <i class="fas fa-cog"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="popover-region-content-container" data-region="popover-region-content-container">
                                    <div class="popover-region-content" data-region="popover-region-content">
                                        <div class="all-notifications" data-region="all-notifications" role="log" aria-busy="false" aria-atomic="false" aria-relevant="additions"></div>
                                        <div class="empty-message" tabindex="0" data-region="empty-message">You have no notifications</div>

                                    </div>
                                    <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                </div>
                                <a class="see-all-link" href="#">
                                    <div class="popover-region-footer-container">
                                        <div class="popover-region-seeall-text">See all</div>
                                    </div>
                                </a>
                            </div>
                        </div><div class="popover-region collapsed popover-region-quotas boost" id="nav-moodlecloud-quota-popover-container" data-userid="" data-region="popover-region">

                            <div id="popover-region-container-5dc0249d826255dc0249d773cf7" class="popover-region-container" data-region="popover-region-container" aria-expanded="false" aria-hidden="true" aria-label="Notification window" role="region">
                                <div class="popover-region-header-container">
                                    <h3 class="popover-region-header-text" data-region="popover-region-header-text">Quotas</h3>
                                    <div class="popover-region-header-actions" data-region="popover-region-header-actions"></div>
                                </div>
                                <div class="popover-region-content-container" data-region="popover-region-content-container">
                                    <div class="popover-region-content" data-region="popover-region-content">




                                    </div>
                                    <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                </div>
                                <a class="see-all-link" href="#">
                                    <div class="popover-region-footer-container">
                                        <div class="popover-region-seeall-text">See all</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <!-- user_menu -->
                    <li class="nav-item d-flex align-items-center">
                    </li>
                    <li class="nav-item d-flex align-items-center">
                        <div class="usermenu"><div class="action-menu moodle-actionmenu nowrap-items d-inline" id="action-menu-1" data-enhance="moodle-core-actionmenu">

                                <div class="menubar d-flex " id="action-menu-1-menubar" role="menubar">
                                    <div class="action-menu-trigger">
                                        <div class="dropdown">
                                            <a href="#" tabindex="0" class=" dropdown-toggle icon-no-margin" id="dropdown-1" aria-label="User menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" aria-controls="action-menu-1-menu">

                                                <span class="userbutton"><span class="usertext mr-1">${USER.name}</span><span class="avatars"><span class="avatar current"></span></span></span>

                                                <b class="caret"></b>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right menu  align-tr-br" id="action-menu-1-menu" data-rel="menu-content" aria-labelledby="action-menu-toggle-1" role="menu" data-align="tr-br">

                                                <div class="dropdown-divider" role="presentation"><span class="filler">&nbsp;</span></div>
                                                <a href="profile.jsp" class="dropdown-item menu-action" role="menuitem" data-title="profile,moodle" aria-labelledby="actionmenuaction-2">

                                                    <span class="menu-action-text" id="actionmenuaction-2">
                                                        Profile
                                                    </span>
                                                </a>
                                                <a href="#" class="dropdown-item menu-action" role="menuitem" data-title="messages,message" aria-labelledby="actionmenuaction-4">

                                                    <span class="menu-action-text" id="actionmenuaction-4">
                                                        Messages
                                                    </span>
                                                </a>
                                                <a href="#" class="dropdown-item menu-action" role="menuitem" data-title="preferences,moodle" aria-labelledby="actionmenuaction-5">

                                                    <span class="menu-action-text" id="actionmenuaction-5">
                                                        Preferences
                                                    </span>
                                                </a>
                                                <a href="logout" class="dropdown-item menu-action" role="menuitem" data-title="logout,moodle" aria-labelledby="actionmenuaction-6">

                                                    <span class="menu-action-text" id="actionmenuaction-6">
                                                        Log out
                                                    </span>
                                                </a>
                                                <div class="dropdown-divider" role="presentation"><span class="filler">&nbsp;</span></div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div></div>
                    </li>
                </ul>
                <!-- search_box -->
            </nav>
            <div id="page" class="container-fluid">
                <header id="page-header" class="row">
                    <div class="col-12 pt-3 pb-3" id="yui_3_17_2_1_1572971976817_783">
                        <div class="card " id="yui_3_17_2_1_1572971976817_782">
                            <div class="card-body " id="yui_3_17_2_1_1572971976817_781">
                                <div class="d-flex" id="yui_3_17_2_1_1572971976817_784">
                                    <div class="mr-auto">
                                        <div class="page-context-header"><div class="page-header-headings"><h1>${course.cName}</h1></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <div id="page-content" class="row pb-3">
                    <div id="region-main-box" class="col-12">
                        <section id="region-main" aria-label="Content">


                            <span class="notifications" id="user-notifications"></span>
                            <div role="main"><span id="maincontent"></span><h2>${compe.comName}</h2><div class="box py-3 quizinfo">
                                </div><h3>All the participants of ${compe.comName}</h3><table class="generaltable quizattemptsummary">
                                    <thead>
                                        <tr>
                                            <th class="header c0"  style="text-align:left;" scope="col">Student Code</th>
                                            <th class="header c1"   style="text-align:left;" scope="col">Student Name</th>
                                            <th class="header c2" style="text-align:left;" scope="col">State</th>    
                                            <th class="header c3" style="text-align:left;" scope="col">Grade/10.00</th>
                                            <th class="header c4 lastcol" style="text-align:center;" scope="col">Review</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${listComLec}" var="c">
                                            <tr class="">
                                                <td class="header c0" style="text-align:left;">${c.stuCode}</td>
                                                <c:forEach items="${listComStu}" var="s">
                                                    <c:if test="${c.stuCode==s.code}">
                                                        <td class="header c1"style="text-align:left;">${s.name}</td>
                                                    </c:if>
                                                </c:forEach>

                                                <td class="header c2" style="text-align:left;">Finished<span class="statedetails">Submitted ${c.dateEndFormat}</span></td>

                                                <td class="header c3" style="text-align:left;">${c.grade}</td>
                                                <td  class="header c4 lastcol" style="text-align:center;"><a href="reviewTestStu?stuCode=${c.stuCode}">Review</a></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <div id="feedback" class="box py-3 generalbox"><h3>Highest grade: ${highestgrade} / 10.00.</h3></div></div>



                        </section>
                    </div>
                </div>
            </div>
            <div id="nav-drawer" data-region="drawer" class="d-print-none moodle-has-zindex " aria-hidden="false" tabindex="-1">
                <nav class="list-group" aria-label="PRJ321">
                    <a class="list-group-item list-group-item-action active" href="viewcourseLec.jsp" data-key="coursehome" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="60" data-nodetype="0" data-collapse="0" data-forceopen="1" data-isactive="1" data-hidden="0" data-preceedwithhr="0">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body font-weight-bold">${course.subCode}</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="viewstudent" data-key="participants" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="90" data-nodetype="1" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="7">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body ">Participants</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="#" data-key="badgesview" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="70" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="7">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body ">Badges</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="#" data-key="competencies" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="70" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="7">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body ">Competencies</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="#" data-key="grades" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="70" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="7">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body ">Grades</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="#" data-key="28" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="30" data-nodetype="1" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="7">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body ">General</span>
                            </div>
                        </div>
                    </a>
                    <c:forEach items="${listchap}" var="c">
                        <a class="list-group-item list-group-item-action " href="#" data-key="29" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="30" data-nodetype="1" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="7">
                            <div class="ml-0">
                                <div class="media">
                                    <span class="media-body ">${c.chapName}</span>
                                </div>
                            </div>
                        </a>
                    </c:forEach>
                </nav>
            </div>
            <footer id="page-footer" class="py-3 bg-dark text-light">
                <div class="container">
                    <div id="course-footer"></div>
                    <div class="logininfo">You are logged in as <a href="profile.jsp" title="View profile">${USER.name}</a> (<a href="logout">Log out</a>)</div>
                    <div class="tool_usertours-resettourcontainer"></div>
                    <div class="sitelink"><a title="Moodle" href="http://moodle.org/"><img src="https://cacbank.moodlecloud.com/theme/image.php/boost/core/1569995285/moodlelogo_grayhat" alt="Moodle logo"></a></div>
                    <nav class="nav navbar-nav d-md-none" aria-label="Custom menu">

                    </nav>
                    <div class="tool_dataprivacy"><a href="#">Data retention summary</a></div><a href="#">Get the mobile app</a><div class="policiesfooter"><a href="#">Policies</a></div>
                    <script type="text/javascript">
                        //<![CDATA[
                        var require = {
                            baseUrl: 'https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/',
                            // We only support AMD modules with an explicit define() statement.
                            enforceDefine: true,
                            skipDataMain: true,
                            waitSeconds: 0,
                            paths: {
                                jquery: 'https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/jquery/jquery-3.2.1.min',
                                jqueryui: 'https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/jquery/ui-1.12.1/jquery-ui.min',
                                jqueryprivate: 'https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/requirejs/jquery-private'
                            },
                            // Custom jquery config map.
                            map: {
                                // '*' means all modules will get 'jqueryprivate'
                                // for their 'jquery' dependency.
                                '*': {jquery: 'jqueryprivate'},
                                // Stub module for 'process'. This is a workaround for a bug in MathJax (see MDL-60458).
                                '*': {process: 'core/first'},
                                // 'jquery-private' wants the real jQuery module
                                // though. If this line was not here, there would
                                // be an unresolvable cyclic dependency.
                                jqueryprivate: {jquery: 'jquery'}
                            }
                        };

                        //]]>
                    </script>
                    <script type="text/javascript" src="https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/requirejs/require.min.js"></script>
                    <script type="text/javascript">
                        //<![CDATA[
                        require(['core/first'], function () {
                            ;
                            require(["media_videojs/loader"], function (loader) {
                                loader.setUp(function (videojs) {
                                    videojs.options.flash.swf = "https://cacbank.moodlecloud.com/media/player/videojs/videojs/video-js.swf";
                                    videojs.addLanguage("en", {
                                        "Audio Player": "Audio Player",
                                        "Video Player": "Video Player",
                                        "Play": "Play",
                                        "Pause": "Pause",
                                        "Replay": "Replay",
                                        "Current Time": "Current Time",
                                        "Duration Time": "Duration Time",
                                        "Remaining Time": "Remaining Time",
                                        "Stream Type": "Stream Type",
                                        "LIVE": "LIVE",
                                        "Loaded": "Loaded",
                                        "Progress": "Progress",
                                        "Progress Bar": "Progress Bar",
                                        "progress bar timing: currentTime={1} duration={2}": "{1} of {2}",
                                        "Fullscreen": "Fullscreen",
                                        "Non-Fullscreen": "Non-Fullscreen",
                                        "Mute": "Mute",
                                        "Unmute": "Unmute",
                                        "Playback Rate": "Playback Rate",
                                        "Subtitles": "Subtitles",
                                        "subtitles off": "subtitles off",
                                        "Captions": "Captions",
                                        "captions off": "captions off",
                                        "Chapters": "Chapters",
                                        "Descriptions": "Descriptions",
                                        "descriptions off": "descriptions off",
                                        "Audio Track": "Audio Track",
                                        "Volume Level": "Volume Level",
                                        "You aborted the media playback": "You aborted the media playback",
                                        "A network error caused the media download to fail part-way.": "A network error caused the media download to fail part-way.",
                                        "The media could not be loaded, either because the server or network failed or because the format is not supported.": "The media could not be loaded, either because the server or network failed or because the format is not supported.",
                                        "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.",
                                        "No compatible source was found for this media.": "No compatible source was found for this media.",
                                        "The media is encrypted and we do not have the keys to decrypt it.": "The media is encrypted and we do not have the keys to decrypt it.",
                                        "Play Video": "Play Video",
                                        "Close": "Close",
                                        "Close Modal Dialog": "Close Modal Dialog",
                                        "Modal Window": "Modal Window",
                                        "This is a modal window": "This is a modal window",
                                        "This modal can be closed by pressing the Escape key or activating the close button.": "This modal can be closed by pressing the Escape key or activating the close button.",
                                        ", opens captions settings dialog": ", opens captions settings dialog",
                                        ", opens subtitles settings dialog": ", opens subtitles settings dialog",
                                        ", opens descriptions settings dialog": ", opens descriptions settings dialog",
                                        ", selected": ", selected",
                                        "captions settings": "captions settings",
                                        "subtitles settings": "subititles settings",
                                        "descriptions settings": "descriptions settings",
                                        "Text": "Text",
                                        "White": "White",
                                        "Black": "Black",
                                        "Red": "Red",
                                        "Green": "Green",
                                        "Blue": "Blue",
                                        "Yellow": "Yellow",
                                        "Magenta": "Magenta",
                                        "Cyan": "Cyan",
                                        "Background": "Background",
                                        "Window": "Window",
                                        "Transparent": "Transparent",
                                        "Semi-Transparent": "Semi-Transparent",
                                        "Opaque": "Opaque",
                                        "Font Size": "Font Size",
                                        "Text Edge Style": "Text Edge Style",
                                        "None": "None",
                                        "Raised": "Raised",
                                        "Depressed": "Depressed",
                                        "Uniform": "Uniform",
                                        "Dropshadow": "Dropshadow",
                                        "Font Family": "Font Family",
                                        "Proportional Sans-Serif": "Proportional Sans-Serif",
                                        "Monospace Sans-Serif": "Monospace Sans-Serif",
                                        "Proportional Serif": "Proportional Serif",
                                        "Monospace Serif": "Monospace Serif",
                                        "Casual": "Casual",
                                        "Script": "Script",
                                        "Small Caps": "Small Caps",
                                        "Reset": "Reset",
                                        "restore all settings to the default values": "restore all settings to the default values",
                                        "Done": "Done",
                                        "Caption Settings Dialog": "Caption Settings Dialog",
                                        "Beginning of dialog window. Escape will cancel and close the window.": "Beginning of dialog window. Escape will cancel and close the window.",
                                        "End of dialog window.": "End of dialog window."
                                    });

                                });
                            });
                            ;

                            require(['jquery'], function ($) {
                                $('#single_select5dc0249d773cf3').change(function () {
                                    var ignore = $(this).find(':selected').attr('data-ignore');
                                    if (typeof ignore === typeof undefined) {
                                        $('#single_select_f5dc0249d773cf2').submit();
                                    }
                                });
                            });
                            ;

                            require(
                                    [
                                        'jquery',
                                        'core_message/message_popover'
                                    ],
                                    function (
                                            $,
                                            Popover
                                            ) {
                                        var toggle = $('#message-drawer-toggle-5dc0249d7e1065dc0249d773cf5');
                                        Popover.init(toggle);
                                    });
                            ;

                            require(['jquery', 'message_popup/notification_popover_controller'], function ($, controller) {
                                var container = $('#nav-notification-popover-container');
                                var controller = new controller(container);
                                controller.registerEventListeners();
                                controller.registerListNavigationEventListeners();
                            });
                            ;

                            require(['jquery', 'local_moodlecloud/quota_popover_controller'], function ($, controller) {
                                var controller = new controller($('#nav-moodlecloud-quota-popover-container'));
                            });
                            ;

                            require(['jquery', 'core_message/message_drawer'], function ($, MessageDrawer) {
                                var root = $('#message-drawer-5dc0249d8b6205dc0249d773cf23');
                                MessageDrawer.init(root, '5dc0249d8b6205dc0249d773cf23', false);
                            });
                            ;

                            require(['jquery'], function ($) {
                                $('#single_select5dc0249d773cf25').change(function () {
                                    var ignore = $(this).find(':selected').attr('data-ignore');
                                    if (typeof ignore === typeof undefined) {
                                        $('#single_select_f5dc0249d773cf24').submit();
                                    }
                                });
                            });
                            ;

                            require(['theme_boost/loader']);
                            require(['theme_boost/drawer'], function (mod) {
                                mod.init();
                            });
                            require(['jquery'], function ($) {
                                $('#portal-link-container a').click(function (e) {
                                    if ("ga" in window) {
                                        // we have multiple trackers, one for region and one for global, so we need to iterate over each
                                        // tracker and send the event data to each
                                        trackers = ga.getAll();
                                        $.each(trackers, function (i, tracker) {
                                            if (tracker) {
                                                tracker.send('event', 'SSO Tab', 'Click', '', {
                                                    transport: 'beacon'
                                                });
                                            }
                                        });
                                    }
                                });
                            });
                            ;
                            require(["core/log"], function (amd) {
                                amd.setConfig({"level": "warn"});
                            });
                            ;
                            require(["core/page_global"], function (amd) {
                                amd.init();
                            });
                        });
                        //]]>
                    </script>
                </div>  
            </footer>
        </div><div id="yui3-css-stamp" style="position: absolute !important; visibility: hidden !important" class=""></div>


    </body></html>