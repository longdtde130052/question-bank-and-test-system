<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<aside id="sidebarA">
    <nav>
        <ul>
            <li><a href="<c:url value = "product.jsp"/>">
                    Home</a></li>
            <li><a href="<c:url value = "viewOrder.jsp"/>">
                    View Shop Cart</a></li>
            <li><a href="<c:url value = "history.jsp"/>">
                    View Order History</a></li>
        </ul>
    </nav>
</aside>