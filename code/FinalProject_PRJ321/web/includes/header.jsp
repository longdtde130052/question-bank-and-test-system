<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html dir="ltr" lang="en" xml:lang="en" class="yui3-js-enabled"><head>
        <title>My new Moodle site</title>
        <link rel="shortcut icon" href="https://cacbank.moodlecloud.com/theme/image.php/boost/theme/1569995285/favicon">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="keywords" content="moodle, My new Moodle site">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cacbank.moodlecloud.com/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.css"><script async="" src="https://www.google-analytics.com/analytics.js"></script><script charset="utf-8" id="yui_3_17_2_1_1572873373103_8" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?m/1569995285/core/event/event-min.js&amp;m/1569995285/filter_mathjaxloader/loader/loader-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1572873373103_19" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?3.17.2/event-mousewheel/event-mousewheel-min.js&amp;3.17.2/event-resize/event-resize-min.js&amp;3.17.2/event-hover/event-hover-min.js&amp;3.17.2/event-touch/event-touch-min.js&amp;3.17.2/event-move/event-move-min.js&amp;3.17.2/event-flick/event-flick-min.js&amp;3.17.2/event-valuechange/event-valuechange-min.js&amp;3.17.2/event-tap/event-tap-min.js" async=""></script><script id="firstthemesheet" type="text/css">/** Required in order to fix style inclusion problems in IE with YUI **/</script><link rel="stylesheet" type="text/css" href="https://cacbank.moodlecloud.com/theme/styles.php/boost/1569995285_1569995291/all">
        <script type="text/javascript">
            
            //<![CDATA[
            var M = {};
            M.yui = {};
            M.pageloadstarttime = new Date();
            M.cfg = {"wwwroot": "https:\/\/cacbank.moodlecloud.com", "sesskey": "fHCfTyJ6FE", "themerev": "1569995285", "slasharguments": 1, "theme": "boost", "iconsystemmodule": "core\/icon_system_fontawesome", "jsrev": "1569995285", "admin": "admin", "svgicons": true, "usertimezone": "Asia\/Ho_Chi_Minh", "contextid": 2};
            var yui1ConfigFn = function (me) {
                if (/-skin|reset|fonts|grids|base/.test(me.name)) {
                    me.type = 'css';
                    me.path = me.path.replace(/\.js/, '.css');
                    me.path = me.path.replace(/\/yui2-skin/, '/assets/skins/sam/yui2-skin')
                }
            };
            var yui2ConfigFn = function (me) {
                var parts = me.name.replace(/^moodle-/, '').split('-'), component = parts.shift(), module = parts[0], min = '-min';
                if (/-(skin|core)$/.test(me.name)) {
                    parts.pop();
                    me.type = 'css';
                    min = ''
                }
                if (module) {
                    var filename = parts.join('-');
                    me.path = component + '/' + module + '/' + filename + min + '.' + me.type
                } else {
                    me.path = component + '/' + component + '.' + me.type
                }
            };
            YUI_config = {"debug": false, "base": "https:\/\/cacbank.moodlecloud.com\/lib\/yuilib\/3.17.2\/", "comboBase": "https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?", "combine": true, "filter": null, "insertBefore": "firstthemesheet", "groups": {"yui2": {"base": "https:\/\/cacbank.moodlecloud.com\/lib\/yuilib\/2in3\/2.9.0\/build\/", "comboBase": "https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?", "combine": true, "ext": false, "root": "2in3\/2.9.0\/build\/", "patterns": {"yui2-": {"group": "yui2", "configFn": yui1ConfigFn}}}, "moodle": {"name": "moodle", "base": "https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?m\/1569995285\/", "combine": true, "comboBase": "https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?", "ext": false, "root": "m\/1569995285\/", "patterns": {"moodle-": {"group": "moodle", "configFn": yui2ConfigFn}}, "filter": null, "modules": {"moodle-core-popuphelp": {"requires": ["moodle-core-tooltip"]}, "moodle-core-lockscroll": {"requires": ["plugin", "base-build"]}, "moodle-core-tooltip": {"requires": ["base", "node", "io-base", "moodle-core-notification-dialogue", "json-parse", "widget-position", "widget-position-align", "event-outside", "cache-base"]}, "moodle-core-blocks": {"requires": ["base", "node", "io", "dom", "dd", "dd-scroll", "moodle-core-dragdrop", "moodle-core-notification"]}, "moodle-core-chooserdialogue": {"requires": ["base", "panel", "moodle-core-notification"]}, "moodle-core-event": {"requires": ["event-custom"]}, "moodle-core-notification": {"requires": ["moodle-core-notification-dialogue", "moodle-core-notification-alert", "moodle-core-notification-confirm", "moodle-core-notification-exception", "moodle-core-notification-ajaxexception"]}, "moodle-core-notification-dialogue": {"requires": ["base", "node", "panel", "escape", "event-key", "dd-plugin", "moodle-core-widget-focusafterclose", "moodle-core-lockscroll"]}, "moodle-core-notification-alert": {"requires": ["moodle-core-notification-dialogue"]}, "moodle-core-notification-confirm": {"requires": ["moodle-core-notification-dialogue"]}, "moodle-core-notification-exception": {"requires": ["moodle-core-notification-dialogue"]}, "moodle-core-notification-ajaxexception": {"requires": ["moodle-core-notification-dialogue"]}, "moodle-core-formchangechecker": {"requires": ["base", "event-focus", "moodle-core-event"]}, "moodle-core-actionmenu": {"requires": ["base", "event", "node-event-simulate"]}, "moodle-core-checknet": {"requires": ["base-base", "moodle-core-notification-alert", "io-base"]}, "moodle-core-languninstallconfirm": {"requires": ["base", "node", "moodle-core-notification-confirm", "moodle-core-notification-alert"]}, "moodle-core-maintenancemodetimer": {"requires": ["base", "node"]}, "moodle-core-dragdrop": {"requires": ["base", "node", "io", "dom", "dd", "event-key", "event-focus", "moodle-core-notification"]}, "moodle-core-handlebars": {"condition": {"trigger": "handlebars", "when": "after"}}, "moodle-core_availability-form": {"requires": ["base", "node", "event", "event-delegate", "panel", "moodle-core-notification-dialogue", "json"]}, "moodle-backup-confirmcancel": {"requires": ["node", "node-event-simulate", "moodle-core-notification-confirm"]}, "moodle-backup-backupselectall": {"requires": ["node", "event", "node-event-simulate", "anim"]}, "moodle-course-categoryexpander": {"requires": ["node", "event-key"]}, "moodle-course-modchooser": {"requires": ["moodle-core-chooserdialogue", "moodle-course-coursebase"]}, "moodle-course-management": {"requires": ["base", "node", "io-base", "moodle-core-notification-exception", "json-parse", "dd-constrain", "dd-proxy", "dd-drop", "dd-delegate", "node-event-delegate"]}, "moodle-course-formatchooser": {"requires": ["base", "node", "node-event-simulate"]}, "moodle-course-util": {"requires": ["node"], "use": ["moodle-course-util-base"], "submodules": {"moodle-course-util-base": {}, "moodle-course-util-section": {"requires": ["node", "moodle-course-util-base"]}, "moodle-course-util-cm": {"requires": ["node", "moodle-course-util-base"]}}}, "moodle-course-dragdrop": {"requires": ["base", "node", "io", "dom", "dd", "dd-scroll", "moodle-core-dragdrop", "moodle-core-notification", "moodle-course-coursebase", "moodle-course-util"]}, "moodle-form-shortforms": {"requires": ["node", "base", "selector-css3", "moodle-core-event"]}, "moodle-form-passwordunmask": {"requires": []}, "moodle-form-dateselector": {"requires": ["base", "node", "overlay", "calendar"]}, "moodle-question-searchform": {"requires": ["base", "node"]}, "moodle-question-chooser": {"requires": ["moodle-core-chooserdialogue"]}, "moodle-question-preview": {"requires": ["base", "dom", "event-delegate", "event-key", "core_question_engine"]}, "moodle-availability_completion-form": {"requires": ["base", "node", "event", "moodle-core_availability-form"]}, "moodle-availability_date-form": {"requires": ["base", "node", "event", "io", "moodle-core_availability-form"]}, "moodle-availability_grade-form": {"requires": ["base", "node", "event", "moodle-core_availability-form"]}, "moodle-availability_group-form": {"requires": ["base", "node", "event", "moodle-core_availability-form"]}, "moodle-availability_grouping-form": {"requires": ["base", "node", "event", "moodle-core_availability-form"]}, "moodle-availability_profile-form": {"requires": ["base", "node", "event", "moodle-core_availability-form"]}, "moodle-mod_assign-history": {"requires": ["node", "transition"]}, "moodle-mod_bigbluebuttonbn-recordings": {"requires": ["base", "node", "datasource-get", "datasource-jsonschema", "datasource-polling", "moodle-core-notification"]}, "moodle-mod_bigbluebuttonbn-imports": {"requires": ["base", "node"]}, "moodle-mod_bigbluebuttonbn-rooms": {"requires": ["base", "node", "datasource-get", "datasource-jsonschema", "datasource-polling", "moodle-core-notification"]}, "moodle-mod_bigbluebuttonbn-broker": {"requires": ["base", "node", "datasource-get", "datasource-jsonschema", "datasource-polling", "moodle-core-notification"]}, "moodle-mod_bigbluebuttonbn-modform": {"requires": ["base", "node"]}, "moodle-mod_forum-subscriptiontoggle": {"requires": ["base-base", "io-base"]}, "moodle-mod_quiz-questionchooser": {"requires": ["moodle-core-chooserdialogue", "moodle-mod_quiz-util", "querystring-parse"]}, "moodle-mod_quiz-autosave": {"requires": ["base", "node", "event", "event-valuechange", "node-event-delegate", "io-form"]}, "moodle-mod_quiz-quizbase": {"requires": ["base", "node"]}, "moodle-mod_quiz-toolboxes": {"requires": ["base", "node", "event", "event-key", "io", "moodle-mod_quiz-quizbase", "moodle-mod_quiz-util-slot", "moodle-core-notification-ajaxexception"]}, "moodle-mod_quiz-modform": {"requires": ["base", "node", "event"]}, "moodle-mod_quiz-util": {"requires": ["node", "moodle-core-actionmenu"], "use": ["moodle-mod_quiz-util-base"], "submodules": {"moodle-mod_quiz-util-base": {}, "moodle-mod_quiz-util-slot": {"requires": ["node", "moodle-mod_quiz-util-base"]}, "moodle-mod_quiz-util-page": {"requires": ["node", "moodle-mod_quiz-util-base"]}}}, "moodle-mod_quiz-dragdrop": {"requires": ["base", "node", "io", "dom", "dd", "dd-scroll", "moodle-core-dragdrop", "moodle-core-notification", "moodle-mod_quiz-quizbase", "moodle-mod_quiz-util-base", "moodle-mod_quiz-util-page", "moodle-mod_quiz-util-slot", "moodle-course-util"]}, "moodle-message_airnotifier-toolboxes": {"requires": ["base", "node", "io"]}, "moodle-filter_glossary-autolinker": {"requires": ["base", "node", "io-base", "json-parse", "event-delegate", "overlay", "moodle-core-event", "moodle-core-notification-alert", "moodle-core-notification-exception", "moodle-core-notification-ajaxexception"]}, "moodle-filter_mathjaxloader-loader": {"requires": ["moodle-core-event"]}, "moodle-editor_atto-rangy": {"requires": []}, "moodle-editor_atto-editor": {"requires": ["node", "transition", "io", "overlay", "escape", "event", "event-simulate", "event-custom", "node-event-html5", "node-event-simulate", "yui-throttle", "moodle-core-notification-dialogue", "moodle-core-notification-confirm", "moodle-editor_atto-rangy", "handlebars", "timers", "querystring-stringify"]}, "moodle-editor_atto-plugin": {"requires": ["node", "base", "escape", "event", "event-outside", "handlebars", "event-custom", "timers", "moodle-editor_atto-menu"]}, "moodle-editor_atto-menu": {"requires": ["moodle-core-notification-dialogue", "node", "event", "event-custom"]}, "moodle-report_eventlist-eventfilter": {"requires": ["base", "event", "node", "node-event-delegate", "datatable", "autocomplete", "autocomplete-filters"]}, "moodle-report_loglive-fetchlogs": {"requires": ["base", "event", "node", "io", "node-event-delegate"]}, "moodle-gradereport_grader-gradereporttable": {"requires": ["base", "node", "event", "handlebars", "overlay", "event-hover"]}, "moodle-gradereport_history-userselector": {"requires": ["escape", "event-delegate", "event-key", "handlebars", "io-base", "json-parse", "moodle-core-notification-dialogue"]}, "moodle-tool_capability-search": {"requires": ["base", "node"]}, "moodle-tool_lp-dragdrop-reorder": {"requires": ["moodle-core-dragdrop"]}, "moodle-tool_monitor-dropdown": {"requires": ["base", "event", "node"]}, "moodle-assignfeedback_editpdf-editor": {"requires": ["base", "event", "node", "io", "graphics", "json", "event-move", "event-resize", "transition", "querystring-stringify-simple", "moodle-core-notification-dialog", "moodle-core-notification-alert", "moodle-core-notification-warning", "moodle-core-notification-exception", "moodle-core-notification-ajaxexception"]}, "moodle-atto_accessibilitychecker-button": {"requires": ["color-base", "moodle-editor_atto-plugin"]}, "moodle-atto_accessibilityhelper-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_align-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_bold-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_charmap-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_clear-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_collapse-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_emoticon-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_equation-button": {"requires": ["moodle-editor_atto-plugin", "moodle-core-event", "io", "event-valuechange", "tabview", "array-extras"]}, "moodle-atto_html-beautify": {}, "moodle-atto_html-codemirror": {"requires": ["moodle-atto_html-codemirror-skin"]}, "moodle-atto_html-button": {"requires": ["promise", "moodle-editor_atto-plugin", "moodle-atto_html-beautify", "moodle-atto_html-codemirror", "event-valuechange"]}, "moodle-atto_image-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_indent-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_italic-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_link-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_managefiles-usedfiles": {"requires": ["node", "escape"]}, "moodle-atto_managefiles-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_media-button": {"requires": ["moodle-editor_atto-plugin", "moodle-form-shortforms"]}, "moodle-atto_noautolink-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_orderedlist-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_recordrtc-recording": {"requires": ["moodle-atto_recordrtc-button"]}, "moodle-atto_recordrtc-button": {"requires": ["moodle-editor_atto-plugin", "moodle-atto_recordrtc-recording"]}, "moodle-atto_rtl-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_strike-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_subscript-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_superscript-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_table-button": {"requires": ["moodle-editor_atto-plugin", "moodle-editor_atto-menu", "event", "event-valuechange"]}, "moodle-atto_title-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_underline-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_undo-button": {"requires": ["moodle-editor_atto-plugin"]}, "moodle-atto_unorderedlist-button": {"requires": ["moodle-editor_atto-plugin"]}}}, "gallery": {"name": "gallery", "base": "https:\/\/cacbank.moodlecloud.com\/lib\/yuilib\/gallery\/", "combine": true, "comboBase": "https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?", "ext": false, "root": "gallery\/1569995285\/", "patterns": {"gallery-": {"group": "gallery"}}}}, "modules": {"core_filepicker": {"name": "core_filepicker", "fullpath": "https:\/\/cacbank.moodlecloud.com\/lib\/javascript.php\/1569995285\/repository\/filepicker.js", "requires": ["base", "node", "node-event-simulate", "json", "async-queue", "io-base", "io-upload-iframe", "io-form", "yui2-treeview", "panel", "cookie", "datatable", "datatable-sort", "resize-plugin", "dd-plugin", "escape", "moodle-core_filepicker", "moodle-core-notification-dialogue"]}, "core_comment": {"name": "core_comment", "fullpath": "https:\/\/cacbank.moodlecloud.com\/lib\/javascript.php\/1569995285\/comment\/comment.js", "requires": ["base", "io-base", "node", "json", "yui2-animation", "overlay", "escape"]}, "mathjax": {"name": "mathjax", "fullpath": "https:\/\/cdnjs.cloudflare.com\/ajax\/libs\/mathjax\/2.7.2\/MathJax.js?delayStartupUntil=configured"}}};
            M.yui.loader = {modules: {}};

            //]]>
        </script>

        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="core/first" src="https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/core/first.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="core/str" src="https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/core/str.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="jquery" src="https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/jquery/jquery-3.2.1.min.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="core/config" src="https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/core/config.js"></script><script type="text/x-mathjax-config">
            MathJax.Hub.Config({
            config: ["Accessible.js", "Safe.js"],
            errorSettings: { message: ["!"] },
            skipStartupTypeset: true,
            messageStyle: "none"
            });
        </script></head>
    <body id="page-site-index" class="format-site course path-site chrome dir-ltr lang-en yui-skin-sam yui3-skin-sam cacbank-moodlecloud-com pagelayout-frontpage course-1 context-2 drawer-open-left jsenabled">

        <div id="page-wrapper">

            <div>
                <a class="sr-only sr-only-focusable" href="#maincontent">Skip to main content</a>
            </div><script type="text/javascript" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.js"></script><script type="text/javascript" src="https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/javascript-static.js"></script>
            <script type="text/javascript">
            //<![CDATA[
            document.body.className += ' jsenabled';
            //]]>
            </script>



            <nav class="fixed-top navbar navbar-light bg-white navbar-expand moodle-has-zindex" aria-label="Site navigation">

                <div data-region="drawer-toggle" class="d-inline-block mr-3">
                    <button aria-expanded="true" aria-controls="nav-drawer" type="button" class="btn nav-link float-sm-left mr-1 btn-light bg-gray" data-action="toggle-drawer" data-side="left" data-preference="drawer-open-nav"><i class="icon fa fa-bars fa-fw " aria-hidden="true"></i><span class="sr-only">Side panel</span></button>
                </div>

                <a href="https://cacbank.moodlecloud.com" class="navbar-brand 
                   d-none d-md-block
                   ">
                    <span class="site-name d-none d-md-inline">My new Moodle site</span>
                </a>

                <ul class="navbar-nav d-none d-md-flex">
                    <!-- custom_menu -->
                    <li class="dropdown nav-item">
                        <a class="dropdown-toggle nav-link" id="drop-down-5dc0249d7928b5dc0249d773cf4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" aria-controls="drop-down-menu-5dc0249d7928b5dc0249d773cf4">
                            English ‎(en)‎
                        </a>
                    </li>

                </ul>
                <ul class="nav navbar-nav ml-auto">
                    <div class="d-none d-lg-block">

                    </div>
                    <!-- navbar_plugin_output -->
                    <li class="nav-item">
                        <div class="float-right popover-region collapsed">
                            <a id="message-drawer-toggle-5dc0249d7e1065dc0249d773cf5" class="nav-link d-inline-block popover-region-toggle position-relative" href="#" role="button">
                                <i class="icon fa fa-comment fa-fw " title="Toggle messaging drawer" aria-label="Toggle messaging drawer"></i>
                                <div class="count-container hidden" data-region="count-container" aria-label="There are 0 unread conversations">0</div>
                            </a>
                        </div><div class="popover-region collapsed popover-region-notifications" id="nav-notification-popover-container" data-userid="2" data-region="popover-region">
                            <div class="popover-region-toggle nav-link" data-region="popover-region-toggle" role="button" aria-controls="popover-region-container-5dc0249d80d125dc0249d773cf6" aria-haspopup="true" aria-label="Show notification window with no new notifications" tabindex="0">
                                <i class="icon fa fa-bell fa-fw " title="Toggle notifications menu" aria-label="Toggle notifications menu"></i>
                                <div class="count-container hidden" data-region="count-container" aria-label="There are 0 unread notifications">0</div>

                            </div>
                            <div id="popover-region-container-5dc0249d80d125dc0249d773cf6" class="popover-region-container" data-region="popover-region-container" aria-expanded="false" aria-hidden="true" aria-label="Notification window" role="region">
                                <div class="popover-region-header-container">
                                    <h3 class="popover-region-header-text" data-region="popover-region-header-text">Notifications</h3>
                                    <div class="popover-region-header-actions" data-region="popover-region-header-actions">        <a class="mark-all-read-button" href="#" title="Mark all as read" data-action="mark-all-read" role="button">
                                            <span class="normal-icon"><i class="icon fa fa-check fa-fw " title="Mark all as read" aria-label="Mark all as read"></i></span>
                                            <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </a>
                                        <a href="https://cacbank.moodlecloud.com/message/notificationpreferences.php?userid=2" title="Notification preferences">
                                            <i class="icon fa fa-cog fa-fw " title="Notification preferences" aria-label="Notification preferences"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="popover-region-content-container" data-region="popover-region-content-container">
                                    <div class="popover-region-content" data-region="popover-region-content">
                                        <div class="all-notifications" data-region="all-notifications" role="log" aria-busy="false" aria-atomic="false" aria-relevant="additions"></div>
                                        <div class="empty-message" tabindex="0" data-region="empty-message">You have no notifications</div>

                                    </div>
                                    <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                </div>
                                <a class="see-all-link" href="https://cacbank.moodlecloud.com/message/output/popup/notifications.php">
                                    <div class="popover-region-footer-container">
                                        <div class="popover-region-seeall-text">See all</div>
                                    </div>
                                </a>
                            </div>
                        </div><div class="popover-region collapsed popover-region-quotas boost" id="nav-moodlecloud-quota-popover-container" data-userid="" data-region="popover-region">
                            <div class="popover-region-toggle nav-link" data-region="popover-region-toggle" role="button" aria-controls="popover-region-container-5dc0249d826255dc0249d773cf7" aria-haspopup="true" aria-label="Show notification window with no new notifications" tabindex="0">
                                <div>
                                    <span class="progresslabel">Users</span>
                                    <progress id="progressbar" max="1" value="0.04" class="ok"></progress>
                                </div>

                                <div>
                                    <span class="progresslabel">Storage</span>
                                    <progress id="progressbar" max="1" value="0.00045931816101074" class="ok"></progress>
                                </div>

                            </div>
                            <div id="popover-region-container-5dc0249d826255dc0249d773cf7" class="popover-region-container" data-region="popover-region-container" aria-expanded="false" aria-hidden="true" aria-label="Notification window" role="region">
                                <div class="popover-region-header-container">
                                    <h3 class="popover-region-header-text" data-region="popover-region-header-text">Quotas</h3>
                                    <div class="popover-region-header-actions" data-region="popover-region-header-actions"></div>
                                </div>
                                <div class="popover-region-content-container" data-region="popover-region-content-container">
                                    <div class="popover-region-content" data-region="popover-region-content">
                                        <div class="items">
                                            <div class="item users">
                                                <h2>2/50</h2>
                                                <h3>Users</h3>
                                                <svg width="160" height="160" xmlns="http://www.w3.org/2000/svg">
                                                <g>
                                                <circle id="circle" style="stroke-dashoffset: 0;" class="circle_animation" r="69.85699" cy="81" cx="81" stroke-width="20" stroke="#dddddd" fill="none"></circle>
                                                <circle id="circle" style="stroke-dashoffset: 422.4;" class="circle_animation" r="69.85699" cy="81" cx="81" stroke-width="19" stroke="#ff7518" fill="none"></circle>
                                                </g>
                                                </svg>
                                            </div>

                                            <div class="item storage">
                                                <h2>0/200</h2>
                                                <h3>MB used</h3>
                                                <svg width="160" height="160" xmlns="http://www.w3.org/2000/svg">
                                                <g>
                                                <circle id="circle" style="stroke-dashoffset: 0;" class="circle_animation" r="69.85699" cy="81" cx="81" stroke-width="20" stroke="#dddddd" fill="none"></circle>
                                                <circle id="circle" style="stroke-dashoffset: 439.79790000916;" class="circle_animation" r="69.85699" cy="81" cx="81" stroke-width="19" stroke="#ff7518" fill="none"></circle>
                                                </g>
                                                </svg>
                                            </div>
                                        </div>

                                    </div>
                                    <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                </div>
                                <a class="see-all-link" href="https://cacbank.moodlecloud.com/admin/tool/fileslist">
                                    <div class="popover-region-footer-container">
                                        <div class="popover-region-seeall-text">See all</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <!-- user_menu -->
                    <li class="nav-item d-flex align-items-center">
                        <div class="usermenu"><div class="action-menu moodle-actionmenu nowrap-items d-inline" id="action-menu-1" data-enhance="moodle-core-actionmenu">

                                <div class="menubar d-flex " id="action-menu-1-menubar" role="menubar">




                                    <div class="action-menu-trigger">
                                        <div class="dropdown">
                                            <a href="#" tabindex="0" class=" dropdown-toggle icon-no-margin" id="dropdown-1" aria-label="User menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" aria-controls="action-menu-1-menu">

                                                <span class="userbutton"><span class="usertext mr-1">Duck Tran</span><span class="avatars"><span class="avatar current"><img src="https://secure.gravatar.com/avatar/d61954afddb9930cd1480352bba3ab7e?s=35&amp;d=mm" class="userpicture defaultuserpic" width="35" height="35" role="presentation" aria-hidden="true"></span></span></span>

                                                <b class="caret"></b>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right menu  align-tr-br" id="action-menu-1-menu" data-rel="menu-content" aria-labelledby="action-menu-toggle-1" role="menu" data-align="tr-br">
                                                <a href="https://cacbank.moodlecloud.com/my/" class="dropdown-item menu-action" role="menuitem" data-title="mymoodle,admin" aria-labelledby="actionmenuaction-1">
                                                    <i class="icon fa fa-tachometer fa-fw " aria-hidden="true"></i>
                                                    <span class="menu-action-text" id="actionmenuaction-1">
                                                        Dashboard
                                                    </span>
                                                </a>
                                                <div class="dropdown-divider" role="presentation"><span class="filler">&nbsp;</span></div>
                                                <a href="https://cacbank.moodlecloud.com/user/profile.php?id=2" class="dropdown-item menu-action" role="menuitem" data-title="profile,moodle" aria-labelledby="actionmenuaction-2">
                                                    <i class="icon fa fa-user fa-fw " aria-hidden="true"></i>
                                                    <span class="menu-action-text" id="actionmenuaction-2">
                                                        Profile
                                                    </span>
                                                </a>
                                                <a href="https://cacbank.moodlecloud.com/grade/report/overview/index.php" class="dropdown-item menu-action" role="menuitem" data-title="grades,grades" aria-labelledby="actionmenuaction-3">
                                                    <i class="icon fa fa-table fa-fw " aria-hidden="true"></i>
                                                    <span class="menu-action-text" id="actionmenuaction-3">
                                                        Grades
                                                    </span>
                                                </a>
                                                <a href="https://cacbank.moodlecloud.com/message/index.php" class="dropdown-item menu-action" role="menuitem" data-title="messages,message" aria-labelledby="actionmenuaction-4">
                                                    <i class="icon fa fa-comment fa-fw " aria-hidden="true"></i>
                                                    <span class="menu-action-text" id="actionmenuaction-4">
                                                        Messages
                                                    </span>
                                                </a>
                                                <a href="https://cacbank.moodlecloud.com/user/preferences.php" class="dropdown-item menu-action" role="menuitem" data-title="preferences,moodle" aria-labelledby="actionmenuaction-5">
                                                    <i class="icon fa fa-wrench fa-fw " aria-hidden="true"></i>
                                                    <span class="menu-action-text" id="actionmenuaction-5">
                                                        Preferences
                                                    </span>
                                                </a>
                                                <a href="https://cacbank.moodlecloud.com/login/logout.php?sesskey=fHCfTyJ6FE" class="dropdown-item menu-action" role="menuitem" data-title="logout,moodle" aria-labelledby="actionmenuaction-6">
                                                    <i class="icon fa fa-sign-out fa-fw " aria-hidden="true"></i>
                                                    <span class="menu-action-text" id="actionmenuaction-6">
                                                        Log out
                                                    </span>
                                                </a>
                                                <div class="dropdown-divider" role="presentation"><span class="filler">&nbsp;</span></div>
                                                <a href="https://cacbank.moodlecloud.com/course/switchrole.php?id=1&amp;switchrole=-1&amp;returnurl=%2F" class="dropdown-item menu-action" role="menuitem" data-title="switchroleto,moodle" aria-labelledby="actionmenuaction-7">
                                                    <i class="icon fa fa-user-secret fa-fw " aria-hidden="true"></i>
                                                    <span class="menu-action-text" id="actionmenuaction-7">
                                                        Switch role to...
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div></div>
                    </li>
                </ul>
                <!-- search_box -->
            </nav>


            <div id="page" class="container-fluid">
                <header id="page-header" class="row">
                    <div class="col-12 pt-3 pb-3">
                        <div class="card ">
                            <div class="card-body ">
                                <div class="d-flex">
                                    <div class="mr-auto">
                                        <div class="page-context-header"><div class="page-header-headings"><h1>My new Moodle site</h1></div></div>
                                    </div>

                                    <div class="context-header-settings-menu">
                                        <div class="action-menu moodle-actionmenu d-inline" id="action-menu-2" data-enhance="moodle-core-actionmenu">

                                            <div class="menubar d-flex " id="action-menu-2-menubar" role="menubar">




                                                <div class="action-menu-trigger">
                                                    <div class="dropdown">
                                                        <a href="#" tabindex="0" class=" dropdown-toggle icon-no-margin" id="dropdown-2" aria-label="Actions menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" aria-controls="action-menu-2-menu">


                                                            <i class="icon fa fa-cog fa-fw " title="Actions menu" aria-label="Actions menu"></i>                                
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right menu align-tr-br" id="action-menu-2-menu" data-rel="menu-content" aria-labelledby="action-menu-toggle-2" role="menu" data-align="tr-br">
                                                            <div class="dropdown-item">

                                                                <a href="https://cacbank.moodlecloud.com/course/view.php?id=1&amp;sesskey=fHCfTyJ6FE&amp;edit=on" id="action_link5dc0249d773cf17" class="" role="menuitem"><i class="icon fa fa-pencil fa-fw " aria-hidden="true"></i>Turn editing on</a>



                                                            </div>
                                                            <div class="dropdown-item">

                                                                <a href="https://cacbank.moodlecloud.com/admin/settings.php?section=frontpagesettings" id="action_link5dc0249d773cf18" class="" role="menuitem"><i class="icon fa fa-cog fa-fw " aria-hidden="true"></i>Edit settings</a>



                                                            </div>
                                                            <div class="dropdown-item">

                                                                <a href="https://cacbank.moodlecloud.com/filter/manage.php?contextid=2" id="action_link5dc0249d773cf19" class="" role="menuitem"><i class="icon fa fa-filter fa-fw " aria-hidden="true"></i>Filters</a>



                                                            </div>
                                                            <div class="dropdown-item">

                                                                <a href="https://cacbank.moodlecloud.com/backup/backup.php?id=1" id="action_link5dc0249d773cf20" class="" role="menuitem"><i class="icon fa fa-file-zip-o fa-fw " aria-hidden="true"></i>Backup</a>



                                                            </div>
                                                            <div class="dropdown-item">

                                                                <a href="https://cacbank.moodlecloud.com/backup/restorefile.php?contextid=2" id="action_link5dc0249d773cf21" class="" role="menuitem"><i class="icon fa fa-level-up fa-fw " aria-hidden="true"></i>Restore</a>



                                                            </div>
                                                            <div class="dropdown-item">

                                                                <a href="https://cacbank.moodlecloud.com/course/admin.php?courseid=1" id="action_link5dc0249d773cf22" class="" role="menuitem"><i class="icon fa fa-cog fa-fw " title="More..." aria-label="More..."></i>More...</a>



                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex flex-wrap">
                                    <div class="ml-auto d-flex">

                                    </div>
                                    <div id="course-header">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <div id="page-content" class="row pb-3">
                    <div id="region-main-box" class="col-12">
                        <section id="region-main" aria-label="Content">
                        </section>
                    </div>
                </div>
            </div>
            <div id="message-drawer-5dc0249d8b6205dc0249d773cf23" class="message-app drawer bg-light hidden" aria-expanded="false" aria-hidden="true" data-region="message-drawer" role="region">
                <div class="header-container position-relative" data-region="header-container">
                    <div class="hidden border-bottom px-2 py-3" aria-hidden="true" data-region="view-contacts">
                        <div class="d-flex align-items-center">
                            <div class="align-self-stretch">
                                <a class="h-100 d-flex align-items-center mr-2" href="#" data-route-back="" role="button">
                                    <div class="icon-back-in-drawer">
                                        <span class="dir-rtl-hide"><i class="icon fa fa-chevron-left fa-fw " aria-hidden="true"></i></span>
                                        <span class="dir-ltr-hide"><i class="icon fa fa-chevron-right fa-fw " aria-hidden="true"></i></span>
                                    </div>
                                    <div class="icon-back-in-app">
                                        <span class="dir-rtl-hide"><i class="icon fa fa-times fa-fw " aria-hidden="true"></i></span>
                                    </div>                    </a>
                            </div>
                            <div>
                                Contacts
                            </div>
                            <div class="ml-auto">
                                <a href="#" data-route="view-search" role="button" aria-label="Search">
                                    <i class="icon fa fa-search fa-fw " aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>        
                    <div class="hidden bg-white position-relative border-bottom px-2 py-2" aria-hidden="true" data-region="view-conversation">
                        <div class="hidden" data-region="header-content"></div>
                        <div class="hidden" data-region="header-edit-mode">

                            <div class="d-flex p-2 align-items-center">
                                Messages selected:
                                <span class="ml-1" data-region="message-selected-court">1</span>
                                <button type="button" class="ml-auto close" aria-label="Cancel message selection" data-action="cancel-edit-mode">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        </div>
                        <div data-region="header-placeholder">
                            <div class="d-flex">
                                <div class="ml-2 rounded-circle bg-pulse-grey align-self-center" style="height: 38px; width: 38px">
                                </div>
                                <div class="ml-2 " style="flex: 1">
                                    <div class="mt-1 bg-pulse-grey w-75" style="height: 16px;">
                                    </div>
                                </div>
                                <div class="ml-2 bg-pulse-grey align-self-center" style="height: 16px; width: 20px">
                                </div>
                            </div>
                        </div>
                        <div class="hidden position-absolute" data-region="confirm-dialogue-container" style="top: 0; bottom: -1px; right: 0; left: 0; background: rgba(0,0,0,0.3);"></div>
                    </div>        <div class="border-bottom px-2 py-3" aria-hidden="false" data-region="view-overview">
                        <div class="d-flex align-items-center">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text pr-0 bg-white">
                                        <i class="icon fa fa-search fa-fw " aria-hidden="true"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control border-left-0" placeholder="Search" aria-label="Search" data-region="view-overview-search-input">
                            </div>
                            <div class="ml-2">
                                <a href="#" data-route="view-settings" data-route-param="2" aria-label="Settings" role="button">
                                    <i class="icon fa fa-cog fa-fw " aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="text-right mt-3">
                            <a href="#" data-route="view-contacts" role="button">
                                <i class="icon fa fa-user fa-fw " aria-hidden="true"></i>
                                Contacts
                                <span class="badge bg-primary ml-2 hidden" data-region="contact-request-count" aria-label="There are 0 pending contact requests">
                                    0
                                </span>
                            </a>
                        </div>
                    </div>

                    <div class="hidden border-bottom px-2 py-3 view-search" aria-hidden="true" data-region="view-search">
                        <div class="d-flex align-items-center">
                            <a class="mr-2 align-self-stretch d-flex align-items-center" href="#" data-route-back="" data-action="cancel-search" role="button">
                                <div class="icon-back-in-drawer">
                                    <span class="dir-rtl-hide"><i class="icon fa fa-chevron-left fa-fw " aria-hidden="true"></i></span>
                                    <span class="dir-ltr-hide"><i class="icon fa fa-chevron-right fa-fw " aria-hidden="true"></i></span>
                                </div>
                                <div class="icon-back-in-app">
                                    <span class="dir-rtl-hide"><i class="icon fa fa-times fa-fw " aria-hidden="true"></i></span>
                                </div>                </a>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search" aria-label="Search" data-region="search-input">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" data-action="search" aria-label="Search">
                                        <span data-region="search-icon-container">
                                            <i class="icon fa fa-search fa-fw " aria-hidden="true"></i>
                                        </span>
                                        <span class="hidden" data-region="loading-icon-container">
                                            <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>        
                    <div class="hidden border-bottom px-2 py-3" aria-hidden="true" data-region="view-settings">
                        <div class="d-flex align-items-center">
                            <div class="align-self-stretch">
                                <a class="h-100 d-flex mr-2 align-items-center" href="#" data-route-back="" role="button">
                                    <div class="icon-back-in-drawer">
                                        <span class="dir-rtl-hide"><i class="icon fa fa-chevron-left fa-fw " aria-hidden="true"></i></span>
                                        <span class="dir-ltr-hide"><i class="icon fa fa-chevron-right fa-fw " aria-hidden="true"></i></span>
                                    </div>
                                    <div class="icon-back-in-app">
                                        <span class="dir-rtl-hide"><i class="icon fa fa-times fa-fw " aria-hidden="true"></i></span>
                                    </div>                    </a>
                            </div>
                            <div>
                                Settings
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body-container position-relative" data-region="body-container">

                    <div class="hidden" data-region="view-contact" aria-hidden="true">
                        <div class="p-2 pt-3" data-region="content-container"></div>
                    </div>        <div class="hidden h-100" data-region="view-contacts" aria-hidden="true" data-user-id="2">
                        <div class="d-flex flex-column h-100">
                            <div class="p-3 border-bottom">
                                <ul class="nav nav-pills nav-fill" role="tablist">
                                    <li class="nav-item">
                                        <a id="contacts-tab-5dc0249d8b6205dc0249d773cf23" class="nav-link active" href="#contacts-tab-panel-5dc0249d8b6205dc0249d773cf23" data-toggle="tab" data-action="show-contacts-section" role="tab" aria-controls="contacts-tab-panel-5dc0249d8b6205dc0249d773cf23" aria-selected="true">
                                            Contacts
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="requests-tab-5dc0249d8b6205dc0249d773cf23" class="nav-link" href="#requests-tab-panel-5dc0249d8b6205dc0249d773cf23" data-toggle="tab" data-action="show-requests-section" role="tab" aria-controls="requests-tab-panel-5dc0249d8b6205dc0249d773cf23" aria-selected="false">
                                            Requests
                                            <span class="badge bg-primary ml-2 hidden" data-region="contact-request-count" aria-label="There are 0 pending contact requests">
                                                0
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content d-flex flex-column h-100">
                                <div class="tab-pane fade show active h-100 lazy-load-list" aria-live="polite" data-region="lazy-load-list" data-user-id="2" id="contacts-tab-panel-5dc0249d8b6205dc0249d773cf23" data-section="contacts" role="tabpanel" aria-labelledby="contacts-tab-5dc0249d8b6205dc0249d773cf23">

                                    <div class="hidden text-center p-2" data-region="empty-message-container">
                                        No contacts
                                    </div>
                                    <div class="hidden list-group" data-region="content-container">

                                    </div>
                                    <div class="list-group" data-region="placeholder-container">

                                    </div>
                                    <div class="w-100 text-center p-3 hidden" data-region="loading-icon-container">
                                        <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                    </div>
                                </div>

                                <div class="tab-pane fade h-100 lazy-load-list" aria-live="polite" data-region="lazy-load-list" data-user-id="2" id="requests-tab-panel-5dc0249d8b6205dc0249d773cf23" data-section="requests" role="tabpanel" aria-labelledby="requests-tab-5dc0249d8b6205dc0249d773cf23">

                                    <div class="hidden text-center p-2" data-region="empty-message-container">
                                        No contact requests
                                    </div>
                                    <div class="hidden list-group" data-region="content-container">

                                    </div>
                                    <div class="list-group" data-region="placeholder-container">

                                    </div>
                                    <div class="w-100 text-center p-3 hidden" data-region="loading-icon-container">
                                        <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>        
                    <div class="view-conversation hidden h-100" aria-hidden="true" data-region="view-conversation" data-user-id="2" data-midnight="1572800400" data-message-poll-min="10" data-message-poll-max="120" data-message-poll-after-max="300" style="overflow-y: auto; overflow-x: hidden">
                        <div class="position-relative h-100" data-region="content-container" style="overflow-y: auto; overflow-x: hidden">
                            <div class="content-message-container hidden h-100 px-2 pt-0" data-region="content-message-container" role="log" style="overflow-y: auto; overflow-x: hidden">
                                <div class="py-3 bg-light sticky-top z-index-1 border-bottom text-center hidden" data-region="contact-request-sent-message-container">
                                    <p class="m-0">Contact request sent</p>
                                    <p class="font-italic font-weight-light" data-region="text"></p>
                                </div>
                                <div class="p-3 bg-light text-center hidden" data-region="self-conversation-message-container">
                                    <p class="m-0">Personal space</p>
                                    <p class="font-italic font-weight-light" data-region="text">Save draft messages, links, notes etc. to access later.</p>
                                </div>
                                <div class="hidden text-center p-3" data-region="more-messages-loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                </div>
                            </div>
                            <div class="p-4 w-100 h-100 hidden position-absolute" data-region="confirm-dialogue-container" style="top: 0; background: rgba(0,0,0,0.3);">

                                <div class="p-3 bg-white" data-region="confirm-dialogue" role="alert">
                                    <h3 class="h6 hidden" data-region="dialogue-header"></h3>
                                    <p class="text-muted" data-region="dialogue-text"></p>
                                    <div class="mb-2 hidden" data-region="delete-messages-for-all-users-toggle-container">
                                        <label class="custom-control-label ml-2 text-muted">
                                            <input type="checkbox" data-region="delete-messages-for-all-users-toggle">
                                            Delete for me and for everyone else
                                        </label>
                                    </div>
                                    <button type="button" class="btn btn-primary btn-block hidden" data-action="confirm-block">
                                        <span data-region="dialogue-button-text">Block</span>
                                        <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-block hidden" data-action="confirm-unblock">
                                        <span data-region="dialogue-button-text">Unblock</span>
                                        <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-block hidden" data-action="confirm-remove-contact">
                                        <span data-region="dialogue-button-text">Remove</span>
                                        <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-block hidden" data-action="confirm-add-contact">
                                        <span data-region="dialogue-button-text">Add</span>
                                        <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-block hidden" data-action="confirm-delete-selected-messages">
                                        <span data-region="dialogue-button-text">Delete</span>
                                        <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-block hidden" data-action="confirm-delete-conversation">
                                        <span data-region="dialogue-button-text">Delete</span>
                                        <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-block hidden" data-action="request-add-contact">
                                        <span data-region="dialogue-button-text">Send contact request</span>
                                        <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-block hidden" data-action="accept-contact-request">
                                        <span data-region="dialogue-button-text">Accept and add to contacts</span>
                                        <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                    </button>
                                    <button type="button" class="btn btn-secondary btn-block hidden" data-action="decline-contact-request">
                                        <span data-region="dialogue-button-text">Decline</span>
                                        <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                    </button>
                                    <button type="button" class="btn btn-secondary btn-block" data-action="cancel-confirm">Cancel</button>
                                </div>
                            </div>
                            <div class="px-2 pb-2 pt-0" data-region="content-placeholder">
                                <div class="h-100 d-flex flex-column">
                                    <div class="px-2 pb-2 pt-0 bg-light h-100" style="overflow-y: auto">
                                        <div class="mt-4">
                                            <div class="mb-4">
                                                <div class="mx-auto bg-white" style="height: 25px; width: 100px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                        </div>                            <div class="mt-4">
                                            <div class="mb-4">
                                                <div class="mx-auto bg-white" style="height: 25px; width: 100px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                        </div>                            <div class="mt-4">
                                            <div class="mb-4">
                                                <div class="mx-auto bg-white" style="height: 25px; width: 100px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                        </div>                            <div class="mt-4">
                                            <div class="mb-4">
                                                <div class="mx-auto bg-white" style="height: 25px; width: 100px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                        </div>                            <div class="mt-4">
                                            <div class="mb-4">
                                                <div class="mx-auto bg-white" style="height: 25px; width: 100px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                            <div class="d-flex flex-column p-2 bg-white rounded mb-2">
                                                <div class="d-flex align-items-center mb-2">
                                                    <div class="mr-2">
                                                        <div class="rounded-circle bg-pulse-grey" style="height: 35px; width: 35px"></div>
                                                    </div>
                                                    <div class="mr-4 w-75 bg-pulse-grey" style="height: 16px"></div>
                                                    <div class="ml-auto bg-pulse-grey" style="width: 35px; height: 16px"></div>
                                                </div>
                                                <div class="bg-pulse-grey w-100" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-100 mt-2" style="height: 16px"></div>
                                                <div class="bg-pulse-grey w-75 mt-2" style="height: 16px"></div>
                                            </div>
                                        </div>                        </div>
                                </div>                </div>
                        </div>
                    </div>

                    <div class="hidden" aria-hidden="true" data-region="view-group-info">
                        <div class="pt-3 h-100 d-flex flex-column" data-region="group-info-content-container" style="overflow-y: auto"></div>
                    </div>        <div class="h-100 view-overview-body" aria-hidden="false" data-region="view-overview" data-user-id="2">
                        <div id="message-drawer-view-overview-container-5dc0249d8b6205dc0249d773cf23" class="d-flex flex-column h-100" style="overflow-y: auto">


                            <div class="section border-0 card" data-region="view-overview-favourites">
                                <div id="view-overview-favourites-toggle" class="card-header p-0" data-region="toggle">
                                    <button class="btn btn-link w-100 text-left p-2 d-flex align-items-center overview-section-toggle collapsed" data-toggle="collapse" data-target="#view-overview-favourites-target-5dc0249d8b6205dc0249d773cf23" aria-expanded="false" aria-controls="view-overview-favourites-target-5dc0249d8b6205dc0249d773cf23">
                                        <span class="collapsed-icon-container">
                                            <i class="icon fa fa-caret-right fa-fw " aria-hidden="true"></i>
                                        </span>
                                        <span class="expanded-icon-container">
                                            <i class="icon fa fa-caret-down fa-fw " aria-hidden="true"></i>
                                        </span>
                                        <span class="font-weight-bold">Starred</span>
                                        <small class="hidden ml-1" data-region="section-total-count-container" aria-label=" total conversations">
                                            (<span data-region="section-total-count"></span>)
                                        </small>
                                        <span class="hidden ml-2" data-region="loading-icon-container">
                                            <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                        <span class="hidden badge badge-pill badge-primary ml-auto bg-primary" data-region="section-unread-count">

                                        </span>
                                    </button>
                                </div>
                                <div class="collapse border-bottom  lazy-load-list" aria-live="polite" data-region="lazy-load-list" data-user-id="2" id="view-overview-favourites-target-5dc0249d8b6205dc0249d773cf23" aria-labelledby="view-overview-favourites-toggle" data-parent="#message-drawer-view-overview-container-5dc0249d8b6205dc0249d773cf23">

                                    <div class="hidden text-center p-2" data-region="empty-message-container">
                                        <p class="text-muted mt-2">No starred conversations</p>

                                    </div>
                                    <div class="hidden list-group" data-region="content-container">

                                    </div>
                                    <div class="list-group" data-region="placeholder-container">
                                        <div class="text-center py-2"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </div>

                                    </div>
                                    <div class="w-100 text-center p-3 hidden" data-region="loading-icon-container">
                                        <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                    </div>
                                </div>
                            </div>


                            <div class="section border-0 card" data-region="view-overview-group-messages">
                                <div id="view-overview-group-messages-toggle" class="card-header p-0" data-region="toggle">
                                    <button class="btn btn-link w-100 text-left p-2 d-flex align-items-center overview-section-toggle collapsed" data-toggle="collapse" data-target="#view-overview-group-messages-target-5dc0249d8b6205dc0249d773cf23" aria-expanded="false" aria-controls="view-overview-group-messages-target-5dc0249d8b6205dc0249d773cf23">
                                        <span class="collapsed-icon-container">
                                            <i class="icon fa fa-caret-right fa-fw " aria-hidden="true"></i>
                                        </span>
                                        <span class="expanded-icon-container">
                                            <i class="icon fa fa-caret-down fa-fw " aria-hidden="true"></i>
                                        </span>
                                        <span class="font-weight-bold">Group</span>
                                        <small class="hidden ml-1" data-region="section-total-count-container" aria-label=" total conversations">
                                            (<span data-region="section-total-count"></span>)
                                        </small>
                                        <span class="hidden ml-2" data-region="loading-icon-container">
                                            <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                        <span class="hidden badge badge-pill badge-primary ml-auto bg-primary" data-region="section-unread-count">

                                        </span>
                                    </button>
                                </div>
                                <div class="collapse border-bottom  lazy-load-list" aria-live="polite" data-region="lazy-load-list" data-user-id="2" id="view-overview-group-messages-target-5dc0249d8b6205dc0249d773cf23" aria-labelledby="view-overview-group-messages-toggle" data-parent="#message-drawer-view-overview-container-5dc0249d8b6205dc0249d773cf23">

                                    <div class="hidden text-center p-2" data-region="empty-message-container">
                                        <p class="text-muted mt-2">No group conversations</p>

                                    </div>
                                    <div class="hidden list-group" data-region="content-container">

                                    </div>
                                    <div class="list-group" data-region="placeholder-container">
                                        <div class="text-center py-2"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </div>

                                    </div>
                                    <div class="w-100 text-center p-3 hidden" data-region="loading-icon-container">
                                        <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                    </div>
                                </div>
                            </div>


                            <div class="section border-0 card" data-region="view-overview-messages">
                                <div id="view-overview-messages-toggle" class="card-header p-0" data-region="toggle">
                                    <button class="btn btn-link w-100 text-left p-2 d-flex align-items-center overview-section-toggle collapsed" data-toggle="collapse" data-target="#view-overview-messages-target-5dc0249d8b6205dc0249d773cf23" aria-expanded="false" aria-controls="view-overview-messages-target-5dc0249d8b6205dc0249d773cf23">
                                        <span class="collapsed-icon-container">
                                            <i class="icon fa fa-caret-right fa-fw " aria-hidden="true"></i>
                                        </span>
                                        <span class="expanded-icon-container">
                                            <i class="icon fa fa-caret-down fa-fw " aria-hidden="true"></i>
                                        </span>
                                        <span class="font-weight-bold">Private</span>
                                        <small class="hidden ml-1" data-region="section-total-count-container" aria-label=" total conversations">
                                            (<span data-region="section-total-count"></span>)
                                        </small>
                                        <span class="hidden ml-2" data-region="loading-icon-container">
                                            <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </span>
                                        <span class="hidden badge badge-pill badge-primary ml-auto bg-primary" data-region="section-unread-count">

                                        </span>
                                    </button>
                                </div>
                                <div class="collapse border-bottom  lazy-load-list" aria-live="polite" data-region="lazy-load-list" data-user-id="2" id="view-overview-messages-target-5dc0249d8b6205dc0249d773cf23" aria-labelledby="view-overview-messages-toggle" data-parent="#message-drawer-view-overview-container-5dc0249d8b6205dc0249d773cf23">

                                    <div class="hidden text-center p-2" data-region="empty-message-container">
                                        <p class="text-muted mt-2">No private conversations</p>

                                    </div>
                                    <div class="hidden list-group" data-region="content-container">

                                    </div>
                                    <div class="list-group" data-region="placeholder-container">
                                        <div class="text-center py-2"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </div>

                                    </div>
                                    <div class="w-100 text-center p-3 hidden" data-region="loading-icon-container">
                                        <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div data-region="view-search" aria-hidden="true" class="h-100 hidden" data-user-id="2" data-users-offset="0" data-messages-offset="0" style="overflow-y: auto">
                        <div class="hidden" data-region="search-results-container" style="overflow-y: auto">

                            <div class="d-flex flex-column">
                                <div class="mb-3 bg-white" data-region="all-contacts-container">
                                    <div data-region="contacts-container" class="pt-2">
                                        <h4 class="h6 px-2">Contacts</h4>
                                        <div class="list-group" data-region="list"></div>
                                    </div>
                                    <div data-region="non-contacts-container" class="pt-2 border-top">
                                        <h4 class="h6 px-2">Non-contacts</h4>
                                        <div class="list-group" data-region="list"></div>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn btn-link text-primary" data-action="load-more-users">
                                            <span data-region="button-text">Load more</span>
                                            <span data-region="loading-icon-container" class="hidden"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                                <div class="bg-white" data-region="messages-container">
                                    <h4 class="h6 px-2 pt-2">Messages</h4>
                                    <div class="list-group" data-region="list"></div>
                                    <div class="text-right">
                                        <button class="btn btn-link text-primary" data-action="load-more-messages">
                                            <span data-region="button-text">Load more</span>
                                            <span data-region="loading-icon-container" class="hidden"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                                <p class="hidden p-3 text-center" data-region="no-results-container">No results</p>
                            </div>            </div>
                        <div class="hidden" data-region="loading-placeholder">
                            <div class="text-center pt-3 icon-size-4"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                            </div>
                        </div>
                        <div class="p-3 text-center" data-region="empty-message-container">
                            <p>Search people and messages</p>
                        </div>
                    </div>        
                    <div class="h-100 hidden bg-white" aria-hidden="true" data-region="view-settings">
                        <div class="hidden" data-region="content-container">

                            <div data-region="settings" class="p-3">
                                <h3 class="h6 font-weight-bold">Privacy</h3>
                                <p>You can restrict who can message you</p>
                                <div data-preference="blocknoncontacts" class="mb-3">
                                    <div class="custom-control custom-radio mb-2">
                                        <input type="radio" name="message_blocknoncontacts" class="custom-control-input" id="block-noncontacts-5dc0249d8b6205dc0249d773cf23-1" value="1">
                                        <label class="custom-control-label ml-2" for="block-noncontacts-5dc0249d8b6205dc0249d773cf23-1">
                                            My contacts only
                                        </label>
                                    </div>
                                    <div class="custom-control custom-radio mb-2">
                                        <input type="radio" name="message_blocknoncontacts" class="custom-control-input" id="block-noncontacts-5dc0249d8b6205dc0249d773cf23-0" value="0">
                                        <label class="custom-control-label ml-2" for="block-noncontacts-5dc0249d8b6205dc0249d773cf23-0">
                                            My contacts and anyone in my courses
                                        </label>
                                    </div>
                                </div>

                                <div class="hidden" data-region="notification-preference-container">
                                    <h3 class="mb-2 mt-4 h6 font-weight-bold">Notification preferences</h3>
                                </div>

                                <h3 class="mb-2 mt-4 h6 font-weight-bold">General</h3>
                                <div data-preference="entertosend">
                                    <span class="switch">
                                        <input type="checkbox" id="enter-to-send-5dc0249d8b6205dc0249d773cf23">
                                        <label for="enter-to-send-5dc0249d8b6205dc0249d773cf23">
                                            Use enter to send
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div data-region="placeholder-container">

                            <div class="d-flex flex-column p-3">
                                <div class="w-25 bg-pulse-grey h6" style="height: 18px"></div>
                                <div class="w-75 bg-pulse-grey mb-4" style="height: 18px"></div>
                                <div class="mb-3">
                                    <div class="w-100 d-flex mb-3">
                                        <div class="bg-pulse-grey rounded-circle" style="width: 18px; height: 18px"></div>
                                        <div class="bg-pulse-grey w-50 ml-2" style="height: 18px"></div>
                                    </div>
                                    <div class="w-100 d-flex mb-3">
                                        <div class="bg-pulse-grey rounded-circle" style="width: 18px; height: 18px"></div>
                                        <div class="bg-pulse-grey w-50 ml-2" style="height: 18px"></div>
                                    </div>
                                    <div class="w-100 d-flex mb-3">
                                        <div class="bg-pulse-grey rounded-circle" style="width: 18px; height: 18px"></div>
                                        <div class="bg-pulse-grey w-50 ml-2" style="height: 18px"></div>
                                    </div>
                                </div>
                                <div class="w-50 bg-pulse-grey h6 mb-3 mt-2" style="height: 18px"></div>
                                <div class="mb-4">
                                    <div class="w-100 d-flex mb-2 align-items-center">
                                        <div class="bg-pulse-grey w-25" style="width: 18px; height: 27px"></div>
                                        <div class="bg-pulse-grey w-25 ml-2" style="height: 18px"></div>
                                    </div>
                                    <div class="w-100 d-flex mb-2 align-items-center">
                                        <div class="bg-pulse-grey w-25" style="width: 18px; height: 27px"></div>
                                        <div class="bg-pulse-grey w-25 ml-2" style="height: 18px"></div>
                                    </div>
                                </div>
                                <div class="w-25 bg-pulse-grey h6 mb-3 mt-2" style="height: 18px"></div>
                                <div class="mb-3">
                                    <div class="w-100 d-flex mb-2 align-items-center">
                                        <div class="bg-pulse-grey w-25" style="width: 18px; height: 27px"></div>
                                        <div class="bg-pulse-grey w-50 ml-2" style="height: 18px"></div>
                                    </div>
                                </div>
                            </div>            </div>
                    </div>    </div>
                <div class="footer-container position-relative" data-region="footer-container">

                    <div class="hidden border-top bg-white position-relative" aria-hidden="true" data-region="view-conversation" data-enter-to-send="0">
                        <div class="hidden p-2" data-region="content-messages-footer-container">

                            <div class="d-flex mt-1">
                                <textarea dir="auto" data-region="send-message-txt" class="form-control bg-light" rows="3" data-auto-rows="" data-min-rows="3" data-max-rows="5" role="textbox" aria-label="Write a message..." placeholder="Write a message..." style="resize: none"></textarea>
                                <button class="btn btn-link btn-icon icon-size-3 ml-1 mt-auto" aria-label="Send message" data-action="send-message">
                                    <span data-region="send-icon-container"><i class="icon fa fa-paper-plane fa-fw " aria-hidden="true"></i></span>
                                    <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <div class="hidden p-2" data-region="content-messages-footer-edit-mode-container">

                            <div class="d-flex p-3 justify-content-end">
                                <button class="btn btn-link btn-icon my-1 icon-size-4" data-action="delete-selected-messages" data-toggle="tooltip" data-placement="top" title="Delete selected messages">
                                    <span data-region="icon-container"><i class="icon fa fa-trash fa-fw " aria-hidden="true"></i></span>
                                    <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                    </span>
                                    <span class="sr-only">Delete selected messages</span>
                                </button>
                            </div>            </div>
                        <div class="hidden bg-secondary p-3" data-region="content-messages-footer-require-contact-container">

                            <div class="p-3 bg-white">
                                <p data-region="title"></p>
                                <p class="text-muted" data-region="text"></p>
                                <button type="button" class="btn btn-primary btn-block" data-action="request-add-contact">
                                    <span data-region="dialogue-button-text">Send contact request</span>
                                    <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <div class="hidden bg-secondary p-3" data-region="content-messages-footer-require-unblock-container">

                            <div class="p-3 bg-white">
                                <p class="text-muted" data-region="text">You have blocked this user.</p>
                                <button type="button" class="btn btn-primary btn-block" data-action="request-unblock">
                                    <span data-region="dialogue-button-text">Unblock user</span>
                                    <span class="hidden" data-region="loading-icon-container"><span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <div class="hidden bg-secondary p-3" data-region="content-messages-footer-unable-to-message">

                            <div class="p-3 bg-white">
                                <p class="text-muted" data-region="text">You are unable to message this user</p>
                            </div>
                        </div>
                        <div class="p-2" data-region="placeholder-container">
                            <div class="d-flex">
                                <div class="bg-pulse-grey w-100" style="height: 80px"></div>
                                <div class="mx-2 mb-2 align-self-end bg-pulse-grey" style="height: 20px; width: 20px"></div>
                            </div>            </div>
                        <div class="hidden position-absolute" data-region="confirm-dialogue-container" style="top: -1px; bottom: 0; right: 0; left: 0; background: rgba(0,0,0,0.3);"></div>
                    </div>            <div data-region="view-overview" class="text-center">
                        <a href="https://cacbank.moodlecloud.com/message/index.php">
                            See all
                        </a>
                    </div>
                </div>
            </div>
            <div id="nav-drawer" data-region="drawer" class="d-print-none moodle-has-zindex " aria-hidden="false" tabindex="-1">
                <nav class="list-group" aria-label="Site">
                    <a class="list-group-item list-group-item-action active" href="https://cacbank.moodlecloud.com/" data-key="home" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="1" data-nodetype="1" data-collapse="0" data-forceopen="1" data-isactive="1" data-hidden="0" data-preceedwithhr="0">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-left">
                                    <i class="icon fa fa-home fa-fw " aria-hidden="true"></i>
                                </span>
                                <span class="media-body font-weight-bold">Home</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="https://cacbank.moodlecloud.com/my/" data-key="myhome" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="70" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="home">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-left">
                                    <i class="icon fa fa-tachometer fa-fw " aria-hidden="true"></i>
                                </span>
                                <span class="media-body ">Dashboard</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="https://cacbank.moodlecloud.com/calendar/view.php?view=month" data-key="calendar" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="60" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="1">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-left">
                                    <i class="icon fa fa-calendar fa-fw " aria-hidden="true"></i>
                                </span>
                                <span class="media-body ">Calendar</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="https://cacbank.moodlecloud.com/user/files.php" data-key="privatefiles" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="70" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="1">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-left">
                                    <i class="icon fa fa-file-o fa-fw " aria-hidden="true"></i>
                                </span>
                                <span class="media-body ">Private files</span>
                            </div>
                        </div>
                    </a>
                    <div class="list-group-item" data-key="mycourses" data-isexpandable="1" data-indent="0" data-showdivider="0" data-type="0" data-nodetype="1" data-collapse="0" data-forceopen="1" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="home">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-left">
                                    <i class="icon fa fa-graduation-cap fa-fw " aria-hidden="true"></i>
                                </span>
                                <span class="media-body">My courses</span>
                            </div>
                        </div>
                    </div>
                    <a class="list-group-item list-group-item-action " href="https://cacbank.moodlecloud.com/course/view.php?id=2" data-key="2" data-isexpandable="1" data-indent="1" data-showdivider="0" data-type="20" data-nodetype="1" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="1" data-preceedwithhr="0" data-parent-key="mycourses">
                        <div class="ml-1">
                            <div class="media">
                                <span class="media-left">
                                    <i class="icon fa fa-graduation-cap fa-fw " aria-hidden="true"></i>
                                </span>
                                <span class="media-body ">Introduction to Moodle</span>
                            </div>
                        </div>
                    </a>
                </nav>
                <nav class="list-group mt-1" aria-label="Site settings">
                    <a class="list-group-item list-group-item-action " href="https://cacbank.moodlecloud.com/admin/search.php" data-key="sitesettings" data-isexpandable="0" data-indent="0" data-showdivider="1" data-type="71" data-nodetype="1" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-left">
                                    <i class="icon fa fa-wrench fa-fw " aria-hidden="true"></i>
                                </span>
                                <span class="media-body ">Site administration</span>
                            </div>
                        </div>
                    </a>
                </nav>
            </div>
            <footer id="page-footer" class="py-3 bg-dark text-light">
                <div class="container">
                    <div id="course-footer"></div>

                    <p class="helplink"><a target="_blank" href="https://moodle.org/community">Support Forums</a> | <a target="_blank" href="https://support.moodlecloud.com">MoodleCloud FAQ</a></p>

                    <div class="logininfo">You are logged in as <a href="https://cacbank.moodlecloud.com/user/profile.php?id=2" title="View profile">Duck Tran</a> (<a href="https://cacbank.moodlecloud.com/login/logout.php?sesskey=fHCfTyJ6FE">Log out</a>)</div>
                    <div class="tool_usertours-resettourcontainer"></div>
                    <div class="sitelink"><a title="Moodle" href="http://moodle.org/"><img src="https://cacbank.moodlecloud.com/theme/image.php/boost/core/1569995285/moodlelogo_grayhat" alt="Moodle logo"></a></div>
                    <nav class="nav navbar-nav d-md-none" aria-label="Custom menu">
                        <ul class="list-unstyled pt-3">
                            <li><a href="#" title="Language">English ‎(en)‎</a></li>
                            <li>
                                <ul class="list-unstyled ml-3">
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=am" title=" አማርኛ ‎(am)‎"> አማርኛ ‎(am)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ba" title="Башҡорт теле ‎(ba)‎">Башҡорт теле ‎(ba)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=be" title="Беларуская ‎(be)‎">Беларуская ‎(be)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=bg" title="Български ‎(bg)‎">Български ‎(bg)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ky" title="Кыргызча ‎(ky)‎">Кыргызча ‎(ky)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=kk" title="Қазақша ‎(kk)‎">Қазақша ‎(kk)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=mk" title="Македонски ‎(mk)‎">Македонски ‎(mk)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=mn" title="Монгол ‎(mn)‎">Монгол ‎(mn)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ru" title="Русский ‎(ru)‎">Русский ‎(ru)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=sr_cr" title="Српски ‎(sr_cr)‎">Српски ‎(sr_cr)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=tt" title="татар теле ‎(tt)‎">татар теле ‎(tt)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=tg" title="Тоҷикӣ ‎(tg)‎">Тоҷикӣ ‎(tg)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=uk" title="Українська ‎(uk)‎">Українська ‎(uk)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=af" title="Afrikaans ‎(af)‎">Afrikaans ‎(af)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=an" title="Aragonés ‎(an)‎">Aragonés ‎(an)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=oc_es" title="Aranés ‎(oc_es)‎">Aranés ‎(oc_es)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ast" title="Asturianu ‎(ast)‎">Asturianu ‎(ast)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=az" title="Azərbaycanca ‎(az)‎">Azərbaycanca ‎(az)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ms" title="Bahasa Melayu ‎(ms)‎">Bahasa Melayu ‎(ms)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=bm" title="Bamanankan ‎(bm)‎">Bamanankan ‎(bm)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=bi" title="Bislama ‎(bi)‎">Bislama ‎(bi)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=bs" title="Bosanski ‎(bs)‎">Bosanski ‎(bs)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=br" title="Breizh ‎(br)‎">Breizh ‎(br)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ca" title="Català ‎(ca)‎">Català ‎(ca)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ca_valencia" title="Català (Valencià) ‎(ca_valencia)‎">Català (Valencià) ‎(ca_valencia)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ca_wp" title="Català per a Workplace ‎(ca_wp)‎">Català per a Workplace ‎(ca_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=cs" title="Čeština ‎(cs)‎">Čeština ‎(cs)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=mis" title="Crnogorski ‎(mis)‎">Crnogorski ‎(mis)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=cy" title="Cymraeg ‎(cy)‎">Cymraeg ‎(cy)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=da" title="Dansk ‎(da)‎">Dansk ‎(da)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=da_kursus" title="Dansk (kursus) ‎(da_kursus)‎">Dansk (kursus) ‎(da_kursus)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=da_rum" title="Dansk Rum ‎(da_rum)‎">Dansk Rum ‎(da_rum)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=se" title="Davvisámegiella ‎(se)‎">Davvisámegiella ‎(se)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=de_du" title="Deutsch - Du ‎(de_du)‎">Deutsch - Du ‎(de_du)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=de_kids" title="Deutsch - Kids ‎(de_kids)‎">Deutsch - Kids ‎(de_kids)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=de_ch" title="Deutsch - Schweiz ‎(de_ch)‎">Deutsch - Schweiz ‎(de_ch)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=de" title="Deutsch ‎(de)‎">Deutsch ‎(de)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=de_comm" title="Deutsch community ‎(de_comm)‎">Deutsch community ‎(de_comm)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=de_wp" title="Deutsch für Arbeitsplatz ‎(de_wp)‎">Deutsch für Arbeitsplatz ‎(de_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=dsb" title="Dolnoserbski ‎(dsb)‎">Dolnoserbski ‎(dsb)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=mh" title="Ebon ‎(mh)‎">Ebon ‎(mh)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=et" title="eesti ‎(et)‎">eesti ‎(et)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=en_ar" title="English - Pirate ‎(en_ar)‎">English - Pirate ‎(en_ar)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=en_us" title="English - United States ‎(en_us)‎">English - United States ‎(en_us)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=en" title="English ‎(en)‎">English ‎(en)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=en_kids" title="English for kids ‎(en_kids)‎">English for kids ‎(en_kids)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=en_wp" title="English for Workplace ‎(en_wp)‎">English for Workplace ‎(en_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=en_us_k12" title="English US - K12 ‎(en_us_k12)‎">English US - K12 ‎(en_us_k12)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=es_co" title="Español - Colombia ‎(es_co)‎">Español - Colombia ‎(es_co)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=es" title="Español - Internacional ‎(es)‎">Español - Internacional ‎(es)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=es_mx" title="Español - México ‎(es_mx)‎">Español - México ‎(es_mx)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=es_mx_kids" title="Español - México para niños ‎(es_mx_kids)‎">Español - México para niños ‎(es_mx_kids)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=es_ve" title="Español - Venezuela ‎(es_ve)‎">Español - Venezuela ‎(es_ve)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=es_wp" title="Español para la Empresa ‎(es_wp)‎">Español para la Empresa ‎(es_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=eo" title="Esperanto ‎(eo)‎">Esperanto ‎(eo)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=eu" title="Euskara ‎(eu)‎">Euskara ‎(eu)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ee" title="Èʋegbe ‎(ee)‎">Èʋegbe ‎(ee)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=fil" title="Filipino ‎(fil)‎">Filipino ‎(fil)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=sv_fi" title="Finlandssvenska ‎(sv_fi)‎">Finlandssvenska ‎(sv_fi)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=fo" title="Føroyskt ‎(fo)‎">Føroyskt ‎(fo)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=fr_ca" title="Français - Canada ‎(fr_ca)‎">Français - Canada ‎(fr_ca)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=fr" title="Français ‎(fr)‎">Français ‎(fr)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=fr_wp" title="Français pour Workplace ‎(fr_wp)‎">Français pour Workplace ‎(fr_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ga" title="Gaeilge ‎(ga)‎">Gaeilge ‎(ga)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=gd" title="Gàidhlig ‎(gd)‎">Gàidhlig ‎(gd)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=gl" title="Galego ‎(gl)‎">Galego ‎(gl)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=oc_gsc" title="Gascon ‎(oc_gsc)‎">Gascon ‎(oc_gsc)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ha" title="Hausa ‎(ha)‎">Hausa ‎(ha)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=hr" title="Hrvatski ‎(hr)‎">Hrvatski ‎(hr)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=haw" title="ʻŌlelo Hawaiʻi ‎(haw)‎">ʻŌlelo Hawaiʻi ‎(haw)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ig" title="Igbo ‎(ig)‎">Igbo ‎(ig)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=id" title="Indonesian ‎(id)‎">Indonesian ‎(id)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=zu" title="isiZulu ‎(zu)‎">isiZulu ‎(zu)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=is" title="Íslenska ‎(is)‎">Íslenska ‎(is)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=it" title="Italiano ‎(it)‎">Italiano ‎(it)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=it_wp" title="Italiano per Workplace ‎(it_wp)‎">Italiano per Workplace ‎(it_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ja_kids" title="Japanese - kids ‎(ja_kids)‎">Japanese - kids ‎(ja_kids)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=kl" title="Kalaallisut ‎(kl)‎">Kalaallisut ‎(kl)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=rw" title="Kinyarwanda ‎(rw)‎">Kinyarwanda ‎(rw)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=sw" title="Kiswahili ‎(sw)‎">Kiswahili ‎(sw)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=hat" title="Kreyòl Ayisyen ‎(hat)‎">Kreyòl Ayisyen ‎(hat)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=kmr" title="Kurmanji ‎(kmr)‎">Kurmanji ‎(kmr)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=lo" title="Laotian ‎(lo)‎">Laotian ‎(lo)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=la" title="Latin ‎(la)‎">Latin ‎(la)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=lv" title="Latviešu ‎(lv)‎">Latviešu ‎(lv)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=oc_lnc" title="Lengadocian ‎(oc_lnc)‎">Lengadocian ‎(oc_lnc)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=lb" title="Lëtzebuergesch ‎(lb)‎">Lëtzebuergesch ‎(lb)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=lt" title="Lietuvių ‎(lt)‎">Lietuvių ‎(lt)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=lt_uni" title="Lithuanian (university) ‎(lt_uni)‎">Lithuanian (university) ‎(lt_uni)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=smj" title="Lulesamisk ‎(smj)‎">Lulesamisk ‎(smj)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=hu" title="magyar ‎(hu)‎">magyar ‎(hu)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=mg" title="Malagasy ‎(mg)‎">Malagasy ‎(mg)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=mi_tn" title="Māori - Tainui ‎(mi_tn)‎">Māori - Tainui ‎(mi_tn)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=mi_wwow" title="Māori - Waikato ‎(mi_wwow)‎">Māori - Waikato ‎(mi_wwow)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=mi" title="Māori (Te Reo) ‎(mi)‎">Māori (Te Reo) ‎(mi)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=mn_mong" title="Mongolian ‎(mn_mong)‎">Mongolian ‎(mn_mong)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=my" title="myanma bhasa ‎(my)‎">myanma bhasa ‎(my)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=nl" title="Nederlands ‎(nl)‎">Nederlands ‎(nl)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=no" title="Norsk - bokmål ‎(no)‎">Norsk - bokmål ‎(no)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=nn" title="Norsk - nynorsk ‎(nn)‎">Norsk - nynorsk ‎(nn)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=no_gr" title="Norsk ‎(no_gr)‎">Norsk ‎(no_gr)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=no_wp" title="Norsk Workplace ‎(no_wp)‎">Norsk Workplace ‎(no_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=uz" title="O'zbekcha ‎(uz)‎">O'zbekcha ‎(uz)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=pcm" title="Pidgin ‎(pcm)‎">Pidgin ‎(pcm)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=pl" title="Polski ‎(pl)‎">Polski ‎(pl)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=pt_br" title="Português - Brasil ‎(pt_br)‎">Português - Brasil ‎(pt_br)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=pt" title="Português - Portugal ‎(pt)‎">Português - Portugal ‎(pt)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ro" title="Română ‎(ro)‎">Română ‎(ro)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=rm_surs" title="Romansh Sursilvan ‎(rm_surs)‎">Romansh Sursilvan ‎(rm_surs)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=sm" title="Samoan ‎(sm)‎">Samoan ‎(sm)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=tn" title="Setswana ‎(tn)‎">Setswana ‎(tn)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=sq" title="Shqip ‎(sq)‎">Shqip ‎(sq)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=sk" title="Slovenčina ‎(sk)‎">Slovenčina ‎(sk)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=sl" title="Slovenščina ‎(sl)‎">Slovenščina ‎(sl)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=so" title="Soomaali ‎(so)‎">Soomaali ‎(so)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=sma" title="Sørsamisk ‎(sma)‎">Sørsamisk ‎(sma)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=sr_lt" title="Srpski ‎(sr_lt)‎">Srpski ‎(sr_lt)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=fi" title="Suomi ‎(fi)‎">Suomi ‎(fi)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=fi_co" title="Suomi+ ‎(fi_co)‎">Suomi+ ‎(fi_co)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=sv" title="Svenska ‎(sv)‎">Svenska ‎(sv)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=tl" title="Tagalog ‎(tl)‎">Tagalog ‎(tl)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ta" title="Tamil ‎(ta)‎">Tamil ‎(ta)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=kab" title="Taqbaylit ‎(kab)‎">Taqbaylit ‎(kab)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=th" title="Thai ‎(th)‎">Thai ‎(th)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=to" title="Tongan ‎(to)‎">Tongan ‎(to)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=tr" title="Türkçe ‎(tr)‎">Türkçe ‎(tr)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=tk" title="Turkmen ‎(tk)‎">Turkmen ‎(tk)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ug_lt" title="Uyghur - latin ‎(ug_lt)‎">Uyghur - latin ‎(ug_lt)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=fj" title="VakaViti ‎(fj)‎">VakaViti ‎(fj)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=vi" title="Vietnamese ‎(vi)‎">Vietnamese ‎(vi)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=wo" title="Wolof ‎(wo)‎">Wolof ‎(wo)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ro_wp" title="Workplace în limba română ‎(ro_wp)‎">Workplace în limba română ‎(ro_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=zh_cn_wp" title="Workplace 简体中文版 ‎(zh_cn_wp)‎">Workplace 简体中文版 ‎(zh_cn_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=zh_tw_wp" title="Workplace 繁體中文版 ‎(zh_tw_wp)‎">Workplace 繁體中文版 ‎(zh_tw_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=el" title="Ελληνικά ‎(el)‎">Ελληνικά ‎(el)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=el_kids" title="Ελληνικά για παιδιά ‎(el_kids)‎">Ελληνικά για παιδιά ‎(el_kids)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=el_uni" title="Ελληνικά για σχολές ‎(el_uni)‎">Ελληνικά για σχολές ‎(el_uni)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=el_wp" title="Ελληνικά για χώρους εργασίας ‎(el_wp)‎">Ελληνικά για χώρους εργασίας ‎(el_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ka" title="ქართული ‎(ka)‎">ქართული ‎(ka)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=hy" title="Հայերեն ‎(hy)‎">Հայերեն ‎(hy)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=he" title="עברית ‎(he)‎">עברית ‎(he)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=he_kids" title="עברית בתי־ספר ‎(he_kids)‎">עברית בתי־ספר ‎(he_kids)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=he_wp" title="עברית עבור Workplace ‎(he_wp)‎">עברית עבור Workplace ‎(he_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ug_ug" title="ئۇيغۇرچە ‎(ug_ug)‎">ئۇيغۇرچە ‎(ug_ug)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ur" title="اردو ‎(ur)‎">اردو ‎(ur)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ps" title="پښتو ‎(ps)‎">پښتو ‎(ps)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ckb" title="سۆرانی ‎(ckb)‎">سۆرانی ‎(ckb)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ar" title="عربي ‎(ar)‎">عربي ‎(ar)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=fa" title="فارسی ‎(fa)‎">فارسی ‎(fa)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=dv" title="ދިވެހި ‎(dv)‎">ދިވެހި ‎(dv)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=zgh" title="ⵜⴰⵎⴰⵣⵉⵖⵜ ‎(zgh)‎">ⵜⴰⵎⴰⵣⵉⵖⵜ ‎(zgh)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ti" title="ትግርኛ ‎(ti)‎">ትግርኛ ‎(ti)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ne" title="नेपाली ‎(ne)‎">नेपाली ‎(ne)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=mr" title="मराठी ‎(mr)‎">मराठी ‎(mr)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=hi_wp" title="वर्कप्लेस  के लिए  हिंदी ‎(hi_wp)‎">वर्कप्लेस  के लिए  हिंदी ‎(hi_wp)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=hi" title="हिंदी ‎(hi)‎">हिंदी ‎(hi)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=bn" title="বাংলা ‎(bn)‎">বাংলা ‎(bn)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=pan" title="ਪੰਜਾਬੀ ‎(pan)‎">ਪੰਜਾਬੀ ‎(pan)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=gu" title="ગુજરાતી ‎(gu)‎">ગુજરાતી ‎(gu)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=or" title="ଓଡ଼ିଆ ‎(or)‎">ଓଡ଼ିଆ ‎(or)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ta_lk" title="தமிழ் ‎(ta_lk)‎">தமிழ் ‎(ta_lk)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=te" title="తెలుగు  ‎(te)‎">తెలుగు  ‎(te)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=kn" title="ಕನ್ನಡ ‎(kn)‎">ಕನ್ನಡ ‎(kn)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ml" title="മലയാളം ‎(ml)‎">മലയാളം ‎(ml)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=si" title="සිංහල ‎(si)‎">සිංහල ‎(si)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=xct" title="བོད་ཡིག ‎(xct)‎">བོད་ཡིག ‎(xct)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=dz" title="རྫོང་ཁ ‎(dz)‎">རྫོང་ཁ ‎(dz)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=km" title="ខ្មែរ ‎(km)‎">ខ្មែរ ‎(km)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ko" title="한국어 ‎(ko)‎">한국어 ‎(ko)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=ja" title="日本語 ‎(ja)‎">日本語 ‎(ja)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=zh_tw" title="正體中文 ‎(zh_tw)‎">正體中文 ‎(zh_tw)‎</a></li>
                                    <li><a href="https://cacbank.moodlecloud.com/?lang=zh_cn" title="简体中文 ‎(zh_cn)‎">简体中文 ‎(zh_cn)‎</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <div class="tool_dataprivacy"><a href="https://cacbank.moodlecloud.com/admin/tool/dataprivacy/summary.php">Data retention summary</a></div><a href="https://download.moodle.org/mobile?version=2019052002&amp;lang=en&amp;iosappid=633359593&amp;androidappid=com.moodle.moodlemobile">Get the mobile app</a><div class="policiesfooter"><a href="https://cacbank.moodlecloud.com/admin/tool/policy/viewall.php?returnurl=https%3A%2F%2Fcacbank.moodlecloud.com%2F">Policies</a></div>
                    <script type="text/javascript">
                        //<![CDATA[
                        var require = {
                            baseUrl: 'https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/',
                            // We only support AMD modules with an explicit define() statement.
                            enforceDefine: true,
                            skipDataMain: true,
                            waitSeconds: 0,
                            paths: {
                                jquery: 'https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/jquery/jquery-3.2.1.min',
                                jqueryui: 'https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/jquery/ui-1.12.1/jquery-ui.min',
                                jqueryprivate: 'https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/requirejs/jquery-private'
                            },
                            // Custom jquery config map.
                            map: {
                                // '*' means all modules will get 'jqueryprivate'
                                // for their 'jquery' dependency.
                                '*': {jquery: 'jqueryprivate'},
                                // Stub module for 'process'. This is a workaround for a bug in MathJax (see MDL-60458).
                                '*': {process: 'core/first'},
                                // 'jquery-private' wants the real jQuery module
                                // though. If this line was not here, there would
                                // be an unresolvable cyclic dependency.
                                jqueryprivate: {jquery: 'jquery'}
                            }
                        };

                        //]]>
                    </script>
                    <script type="text/javascript" src="https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/requirejs/require.min.js"></script>
                    <script type="text/javascript">
                        //<![CDATA[
                        require(['core/first'], function () {
                            ;
                            require(["media_videojs/loader"], function (loader) {
                                loader.setUp(function (videojs) {
                                    videojs.options.flash.swf = "https://cacbank.moodlecloud.com/media/player/videojs/videojs/video-js.swf";
                                    videojs.addLanguage("en", {
                                        "Audio Player": "Audio Player",
                                        "Video Player": "Video Player",
                                        "Play": "Play",
                                        "Pause": "Pause",
                                        "Replay": "Replay",
                                        "Current Time": "Current Time",
                                        "Duration Time": "Duration Time",
                                        "Remaining Time": "Remaining Time",
                                        "Stream Type": "Stream Type",
                                        "LIVE": "LIVE",
                                        "Loaded": "Loaded",
                                        "Progress": "Progress",
                                        "Progress Bar": "Progress Bar",
                                        "progress bar timing: currentTime={1} duration={2}": "{1} of {2}",
                                        "Fullscreen": "Fullscreen",
                                        "Non-Fullscreen": "Non-Fullscreen",
                                        "Mute": "Mute",
                                        "Unmute": "Unmute",
                                        "Playback Rate": "Playback Rate",
                                        "Subtitles": "Subtitles",
                                        "subtitles off": "subtitles off",
                                        "Captions": "Captions",
                                        "captions off": "captions off",
                                        "Chapters": "Chapters",
                                        "Descriptions": "Descriptions",
                                        "descriptions off": "descriptions off",
                                        "Audio Track": "Audio Track",
                                        "Volume Level": "Volume Level",
                                        "You aborted the media playback": "You aborted the media playback",
                                        "A network error caused the media download to fail part-way.": "A network error caused the media download to fail part-way.",
                                        "The media could not be loaded, either because the server or network failed or because the format is not supported.": "The media could not be loaded, either because the server or network failed or because the format is not supported.",
                                        "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.",
                                        "No compatible source was found for this media.": "No compatible source was found for this media.",
                                        "The media is encrypted and we do not have the keys to decrypt it.": "The media is encrypted and we do not have the keys to decrypt it.",
                                        "Play Video": "Play Video",
                                        "Close": "Close",
                                        "Close Modal Dialog": "Close Modal Dialog",
                                        "Modal Window": "Modal Window",
                                        "This is a modal window": "This is a modal window",
                                        "This modal can be closed by pressing the Escape key or activating the close button.": "This modal can be closed by pressing the Escape key or activating the close button.",
                                        ", opens captions settings dialog": ", opens captions settings dialog",
                                        ", opens subtitles settings dialog": ", opens subtitles settings dialog",
                                        ", opens descriptions settings dialog": ", opens descriptions settings dialog",
                                        ", selected": ", selected",
                                        "captions settings": "captions settings",
                                        "subtitles settings": "subititles settings",
                                        "descriptions settings": "descriptions settings",
                                        "Text": "Text",
                                        "White": "White",
                                        "Black": "Black",
                                        "Red": "Red",
                                        "Green": "Green",
                                        "Blue": "Blue",
                                        "Yellow": "Yellow",
                                        "Magenta": "Magenta",
                                        "Cyan": "Cyan",
                                        "Background": "Background",
                                        "Window": "Window",
                                        "Transparent": "Transparent",
                                        "Semi-Transparent": "Semi-Transparent",
                                        "Opaque": "Opaque",
                                        "Font Size": "Font Size",
                                        "Text Edge Style": "Text Edge Style",
                                        "None": "None",
                                        "Raised": "Raised",
                                        "Depressed": "Depressed",
                                        "Uniform": "Uniform",
                                        "Dropshadow": "Dropshadow",
                                        "Font Family": "Font Family",
                                        "Proportional Sans-Serif": "Proportional Sans-Serif",
                                        "Monospace Sans-Serif": "Monospace Sans-Serif",
                                        "Proportional Serif": "Proportional Serif",
                                        "Monospace Serif": "Monospace Serif",
                                        "Casual": "Casual",
                                        "Script": "Script",
                                        "Small Caps": "Small Caps",
                                        "Reset": "Reset",
                                        "restore all settings to the default values": "restore all settings to the default values",
                                        "Done": "Done",
                                        "Caption Settings Dialog": "Caption Settings Dialog",
                                        "Beginning of dialog window. Escape will cancel and close the window.": "Beginning of dialog window. Escape will cancel and close the window.",
                                        "End of dialog window.": "End of dialog window."
                                    });

                                });
                            });
                            ;

                            require(['jquery'], function ($) {
                                $('#single_select5dc0249d773cf3').change(function () {
                                    var ignore = $(this).find(':selected').attr('data-ignore');
                                    if (typeof ignore === typeof undefined) {
                                        $('#single_select_f5dc0249d773cf2').submit();
                                    }
                                });
                            });
                            ;

                            require(
                                    [
                                        'jquery',
                                        'core_message/message_popover'
                                    ],
                                    function (
                                            $,
                                            Popover
                                            ) {
                                        var toggle = $('#message-drawer-toggle-5dc0249d7e1065dc0249d773cf5');
                                        Popover.init(toggle);
                                    });
                            ;

                            require(['jquery', 'message_popup/notification_popover_controller'], function ($, controller) {
                                var container = $('#nav-notification-popover-container');
                                var controller = new controller(container);
                                controller.registerEventListeners();
                                controller.registerListNavigationEventListeners();
                            });
                            ;

                            require(['jquery', 'local_moodlecloud/quota_popover_controller'], function ($, controller) {
                                var controller = new controller($('#nav-moodlecloud-quota-popover-container'));
                            });
                            ;

                            require(['jquery', 'core_message/message_drawer'], function ($, MessageDrawer) {
                                var root = $('#message-drawer-5dc0249d8b6205dc0249d773cf23');
                                MessageDrawer.init(root, '5dc0249d8b6205dc0249d773cf23', false);
                            });
                            ;

                            require(['jquery'], function ($) {
                                $('#single_select5dc0249d773cf25').change(function () {
                                    var ignore = $(this).find(':selected').attr('data-ignore');
                                    if (typeof ignore === typeof undefined) {
                                        $('#single_select_f5dc0249d773cf24').submit();
                                    }
                                });
                            });
                            ;

                            require(['theme_boost/loader']);
                            require(['theme_boost/drawer'], function (mod) {
                                mod.init();
                            });
                            require(['jquery'], function ($) {
                                $('#portal-link-container a').click(function (e) {
                                    if ("ga" in window) {
                                        // we have multiple trackers, one for region and one for global, so we need to iterate over each
                                        // tracker and send the event data to each
                                        trackers = ga.getAll();
                                        $.each(trackers, function (i, tracker) {
                                            if (tracker) {
                                                tracker.send('event', 'SSO Tab', 'Click', '', {
                                                    transport: 'beacon'
                                                });
                                            }
                                        });
                                    }
                                });
                            });
                            ;
                            require(["core/log"], function (amd) {
                                amd.setConfig({"level": "warn"});
                            });
                            ;
                            require(["core/page_global"], function (amd) {
                                amd.init();
                            });
                        });
                        //]]>
                    </script>
                    <script type="text/javascript" src="https://cacbank.moodlecloud.com/theme/javascript.php/boost/1569995285/footer"></script>
                    <script type="text/javascript">
                        //<![CDATA[
                        M.str = {"moodle": {"lastmodified": "Last modified", "name": "Name", "error": "Error", "info": "Information", "yes": "Yes", "no": "No", "cancel": "Cancel", "confirm": "Confirm", "areyousure": "Are you sure?", "closebuttontitle": "Close", "unknownerror": "Unknown error"}, "repository": {"type": "Type", "size": "Size", "invalidjson": "Invalid JSON string", "nofilesattached": "No files attached", "filepicker": "File picker", "logout": "Logout", "nofilesavailable": "No files available", "norepositoriesavailable": "Sorry, none of your current repositories can return files in the required format.", "fileexistsdialogheader": "File exists", "fileexistsdialog_editor": "A file with that name has already been attached to the text you are editing.", "fileexistsdialog_filemanager": "A file with that name has already been attached", "renameto": "Rename to \"{$a}\"", "referencesexist": "There are {$a} alias\/shortcut files that use this file as their source", "select": "Select"}, "admin": {"confirmdeletecomments": "You are about to delete comments, are you sure?", "confirmation": "Confirmation"}};
                        //]]>
                    </script>
                    <script type="text/javascript">
                        //<![CDATA[
                        (function () {
                            Y.use("moodle-filter_mathjaxloader-loader", function () {
                                M.filter_mathjaxloader.configure({"mathjaxconfig": "\nMathJax.Hub.Config({\n    config: [\"Accessible.js\", \"Safe.js\"],\n    errorSettings: { message: [\"!\"] },\n    skipStartupTypeset: true,\n    messageStyle: \"none\"\n});\n", "lang": "en"});
                            });
                            M.util.help_popups.setup(Y);
                            M.util.js_pending('random5dc0249d773cf26');
                            Y.on('domready', function () {
                                M.util.js_complete("init");
                                M.util.js_complete('random5dc0249d773cf26');
                            });
                        })();
                        //]]>
                    </script>

                </div>
                <div id="portal-link-container">
                    <a id="portal-link" target="_blank" href="https://cacbank.moodlecloud.com/auth/moodlecloud/portal.php" title="MoodleCloud portal link">
                        <img src="https://cacbank.moodlecloud.com/theme/image.php/boost/theme/1569995285/cloud-logo-inverted">
                        Portal
                    </a>
                </div>
                <script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

                    ga('create', 'UA-65124233-5', 'auto', 'mcglobal');
                    ga('create', 'UA-65124233-2', 'auto', 'mcregion');

                    ga('mcglobal.set', 'dimension1', 'free.2');
                    ga('mcregion.set', 'dimension1', 'free.2');

                    ga('mcglobal.send', 'pageview');
                    ga('mcregion.send', 'pageview');
                </script>
            </footer>
        </div><div id="yui3-css-stamp" style="position: absolute !important; visibility: hidden !important" class=""></div>


    </body></html>