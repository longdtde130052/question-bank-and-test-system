
<%@page import="model.Test1"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Course"%>
<%@page import="java.util.Map"%>
<%@page import="com.opensymphony.xwork2.ActionContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html dir="ltr" lang="en" xml:lang="en" class="yui3-js-enabled">
    <head>

    <title>Creating Quiz</title>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
     <link rel="shortcut icon" href="https://cacbank.moodlecloud.com/theme/image.php/boost/theme/1569995285/favicon">
    
<meta name="keywords" content="moodle, Editing Quiz">
<link rel="stylesheet" type="text/css" href="https://cacbank.moodlecloud.com/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.css"><script async="" src="https://www.google-analytics.com/analytics.js"></script><script charset="utf-8" id="yui_3_17_2_1_1573384683206_8" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?m/1569995285/core/event/event-min.js&amp;m/1569995285/filter_mathjaxloader/loader/loader-min.js" async=""></script><link charset="utf-8" rel="stylesheet" id="yui_3_17_2_1_1573384683206_23" href="https://cacbank.moodlecloud.com/theme/yui_combo.php?3.17.2/cssgrids/cssgrids-min.css&amp;3.17.2/calendar-base/assets/skins/sam/calendar-base.css&amp;3.17.2/calendarnavigator/assets/skins/sam/calendarnavigator.css&amp;3.17.2/calendar/assets/skins/sam/calendar.css"><script charset="utf-8" id="yui_3_17_2_1_1573384683206_24" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?3.17.2/intl/intl-min.js&amp;3.17.2/calendar-base/lang/calendar-base_en.js&amp;3.17.2/datatype-date-parse/datatype-date-parse-min.js&amp;3.17.2/datatype-date-format/lang/datatype-date-format_en-US.js&amp;3.17.2/datatype-date-format/datatype-date-format-min.js&amp;3.17.2/datatype-date-math/datatype-date-math-min.js&amp;3.17.2/calendar-base/calendar-base-min.js&amp;3.17.2/plugin/plugin-min.js&amp;3.17.2/calendarnavigator/calendarnavigator-min.js&amp;3.17.2/calendar/calendar-min.js&amp;m/1569995285/form/dateselector/dateselector-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1573384683206_195" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?3.17.2/event-mousewheel/event-mousewheel-min.js&amp;3.17.2/event-resize/event-resize-min.js&amp;3.17.2/event-hover/event-hover-min.js&amp;3.17.2/event-touch/event-touch-min.js&amp;3.17.2/event-move/event-move-min.js&amp;3.17.2/event-flick/event-flick-min.js&amp;3.17.2/event-valuechange/event-valuechange-min.js&amp;3.17.2/event-tap/event-tap-min.js&amp;m/1569995285/mod_quiz/modform/modform-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1573384683206_202" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?m/1569995285/core/formchangechecker/formchangechecker-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1573384683206_224" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?m/1569995285/form/shortforms/shortforms-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1573384683206_301" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?delayStartupUntil=configured" async=""></script><link charset="utf-8" rel="stylesheet" id="yui_3_17_2_1_1573384683206_302" href="https://cacbank.moodlecloud.com/theme/yui_combo.php?3.17.2/cssbutton/cssbutton-min.css&amp;3.17.2/tabview/assets/skins/sam/tabview.css&amp;m/1569995285/atto_html/codemirror/codemirror.css"><script charset="utf-8" id="yui_3_17_2_1_1573384683206_303" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?3.17.2/datatype-xml-parse/datatype-xml-parse-min.js&amp;3.17.2/io-xdr/io-xdr-min.js&amp;3.17.2/io-form/io-form-min.js&amp;3.17.2/io-upload-iframe/io-upload-iframe-min.js&amp;3.17.2/queue-promote/queue-promote-min.js&amp;3.17.2/io-queue/io-queue-min.js&amp;3.17.2/event-simulate/event-simulate-min.js&amp;3.17.2/node-event-html5/node-event-html5-min.js&amp;3.17.2/async-queue/async-queue-min.js&amp;3.17.2/gesture-simulate/gesture-simulate-min.js&amp;3.17.2/node-event-simulate/node-event-simulate-min.js&amp;m/1569995285/core/widget/widget-focusafterclose-min.js&amp;m/1569995285/core/lockscroll/lockscroll-min.js&amp;m/1569995285/core/notification/notification-dialogue-min.js&amp;m/1569995285/core/notification/notification-confirm-min.js&amp;m/1569995285/editor_atto/rangy/rangy-min.js&amp;3.17.2/handlebars-base/handlebars-base-min.js&amp;3.17.2/handlebars-compiler/handlebars-compiler-min.js&amp;m/1569995285/core/handlebars/handlebars-min.js&amp;3.17.2/timers/timers-min.js&amp;3.17.2/querystring-stringify/querystring-stringify-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1573384683206_304" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?m/1569995285/editor_atto/editor/editor-min.js&amp;m/1569995285/editor_atto/menu/menu-min.js&amp;m/1569995285/editor_atto/plugin/plugin-min.js&amp;m/1569995285/atto_collapse/button/button-min.js&amp;m/1569995285/atto_title/button/button-min.js&amp;m/1569995285/atto_bold/button/button-min.js&amp;m/1569995285/atto_italic/button/button-min.js&amp;m/1569995285/atto_unorderedlist/button/button-min.js&amp;m/1569995285/atto_orderedlist/button/button-min.js&amp;m/1569995285/atto_link/button/button-min.js&amp;m/1569995285/atto_image/button/button-min.js&amp;m/1569995285/atto_media/button/button-min.js&amp;m/1569995285/atto_recordrtc/button/button-min.js&amp;m/1569995285/atto_recordrtc/recording/recording-min.js&amp;m/1569995285/atto_managefiles/button/button-min.js&amp;m/1569995285/atto_underline/button/button-min.js&amp;m/1569995285/atto_strike/button/button-min.js&amp;m/1569995285/atto_subscript/button/button-min.js&amp;m/1569995285/atto_superscript/button/button-min.js&amp;m/1569995285/atto_align/button/button-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1573384683206_305" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?m/1569995285/atto_indent/button/button-min.js&amp;3.17.2/arraylist/arraylist-min.js&amp;3.17.2/widget-parent/widget-parent-min.js&amp;3.17.2/widget-child/widget-child-min.js&amp;3.17.2/tabview-base/tabview-base-min.js&amp;3.17.2/node-focusmanager/node-focusmanager-min.js&amp;3.17.2/tabview/tabview-min.js&amp;3.17.2/array-extras/array-extras-min.js&amp;m/1569995285/atto_equation/button/button-min.js&amp;m/1569995285/atto_charmap/button/button-min.js&amp;m/1569995285/atto_table/button/button-min.js&amp;m/1569995285/atto_clear/button/button-min.js&amp;m/1569995285/atto_undo/button/button-min.js&amp;m/1569995285/atto_accessibilitychecker/button/button-min.js&amp;m/1569995285/atto_accessibilityhelper/button/button-min.js&amp;3.17.2/promise/promise-min.js&amp;m/1569995285/atto_html/beautify/beautify-min.js&amp;m/1569995285/atto_html/codemirror/codemirror-min.js&amp;m/1569995285/atto_html/button/button-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1573384683206_1456" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?3.17.2/json-stringify/json-stringify-min.js&amp;m/1569995285/core_availability/form/form-min.js&amp;m/1569995285/availability_completion/form/form-min.js&amp;m/1569995285/availability_date/form/form-min.js&amp;m/1569995285/availability_grade/form/form-min.js&amp;m/1569995285/availability_group/form/form-min.js&amp;m/1569995285/availability_grouping/form/form-min.js&amp;m/1569995285/availability_profile/form/form-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1573384683206_1503" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?3.17.2/dd-ddm/dd-ddm-min.js&amp;3.17.2/dd-ddm-drop/dd-ddm-drop-min.js&amp;3.17.2/dd-proxy/dd-proxy-min.js&amp;3.17.2/dd-constrain/dd-constrain-min.js&amp;3.17.2/dd-drop/dd-drop-min.js&amp;3.17.2/dd-scroll/dd-scroll-min.js&amp;3.17.2/dd-drop-plugin/dd-drop-plugin-min.js&amp;3.17.2/dd-delegate/dd-delegate-min.js&amp;m/1569995285/core/notification/notification-alert-min.js&amp;m/1569995285/core/notification/notification-exception-min.js&amp;m/1569995285/core/notification/notification-ajaxexception-min.js&amp;m/1569995285/core/notification/notification-min.js&amp;m/1569995285/core/dragdrop/dragdrop-min.js&amp;m/1569995285/core/blocks/blocks-min.js" async=""></script><link charset="utf-8" rel="stylesheet" id="yui_3_17_2_1_1573384683206_1615" href="https://cacbank.moodlecloud.com/theme/yui_combo.php?3.17.2/datatable-base/assets/skins/sam/datatable-base.css&amp;3.17.2/datatable-message/assets/skins/sam/datatable-message.css&amp;3.17.2/datatable-sort/assets/skins/sam/datatable-sort.css&amp;3.17.2/resize-base/assets/skins/sam/resize-base.css"><script charset="utf-8" id="yui_3_17_2_1_1573384683206_1616" src="https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/repository/filepicker.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1573384683206_1617" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?2in3/2.9.0/build/yui2-treeview/yui2-treeview-min.js&amp;3.17.2/cookie/cookie-min.js&amp;3.17.2/array-invoke/array-invoke-min.js&amp;3.17.2/model/model-min.js&amp;3.17.2/model-list/model-list-min.js&amp;3.17.2/datatable-core/datatable-core-min.js&amp;3.17.2/view/view-min.js&amp;3.17.2/datatable-head/datatable-head-min.js&amp;3.17.2/datatable-body/datatable-body-min.js&amp;3.17.2/datatable-table/datatable-table-min.js&amp;3.17.2/datatable-base/datatable-base-min.js&amp;3.17.2/datatable-column-widths/datatable-column-widths-min.js&amp;3.17.2/datatable-message/lang/datatable-message_en.js&amp;3.17.2/datatable-message/datatable-message-min.js&amp;3.17.2/datatable-mutable/datatable-mutable-min.js&amp;3.17.2/datatable-sort/lang/datatable-sort_en.js&amp;3.17.2/datatable-sort/datatable-sort-min.js&amp;3.17.2/datasource-local/datasource-local-min.js&amp;3.17.2/datatable-datasource/datatable-datasource-min.js&amp;3.17.2/resize-base/resize-base-min.js&amp;3.17.2/resize-plugin/resize-plugin-min.js&amp;m/1569995285/core_filepicker/core_filepicker.js" async=""></script><link charset="utf-8" rel="stylesheet" id="yui_3_17_2_1_1573384683206_1635" href="https://cacbank.moodlecloud.com/theme/yui_combo.php?2in3/2.9.0/build/yui2-skin-sam-treeview/assets/skins/sam/yui2-skin-sam-treeview-min.css"><script charset="utf-8" id="yui_3_17_2_1_1573384683206_1636" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?2in3/2.9.0/build/yui2-yahoo/yui2-yahoo-min.js&amp;2in3/2.9.0/build/yui2-dom/yui2-dom-min.js&amp;2in3/2.9.0/build/yui2-event/yui2-event-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1573384683206_1637" src="https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/form/form.js" async=""></script><script id="firstthemesheet" type="text/css">/** Required in order to fix style inclusion problems in IE with YUI **/</script><link rel="stylesheet" type="text/css" href="https://cacbank.moodlecloud.com/theme/styles.php/boost/1569995285_1569995291/all">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        
<script type="text/javascript">
//<![CDATA[
var M = {}; M.yui = {};
M.pageloadstarttime = new Date();
M.cfg = {"wwwroot":"https:\/\/cacbank.moodlecloud.com","sesskey":"UkQaQYey1V","themerev":"1569995285","slasharguments":1,"theme":"boost","iconsystemmodule":"core\/icon_system_fontawesome","jsrev":"1569995285","admin":"admin","svgicons":true,"usertimezone":"Asia\/Ho_Chi_Minh","contextid":52};var yui1ConfigFn = function(me) {if(/-skin|reset|fonts|grids|base/.test(me.name)){me.type='css';me.path=me.path.replace(/\.js/,'.css');me.path=me.path.replace(/\/yui2-skin/,'/assets/skins/sam/yui2-skin')}};
var yui2ConfigFn = function(me) {var parts=me.name.replace(/^moodle-/,'').split('-'),component=parts.shift(),module=parts[0],min='-min';if(/-(skin|core)$/.test(me.name)){parts.pop();me.type='css';min=''}
if(module){var filename=parts.join('-');me.path=component+'/'+module+'/'+filename+min+'.'+me.type}else{me.path=component+'/'+component+'.'+me.type}};
YUI_config = {"debug":false,"base":"https:\/\/cacbank.moodlecloud.com\/lib\/yuilib\/3.17.2\/","comboBase":"https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?","combine":true,"filter":null,"insertBefore":"firstthemesheet","groups":{"yui2":{"base":"https:\/\/cacbank.moodlecloud.com\/lib\/yuilib\/2in3\/2.9.0\/build\/","comboBase":"https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?","combine":true,"ext":false,"root":"2in3\/2.9.0\/build\/","patterns":{"yui2-":{"group":"yui2","configFn":yui1ConfigFn}}},"moodle":{"name":"moodle","base":"https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?m\/1569995285\/","combine":true,"comboBase":"https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?","ext":false,"root":"m\/1569995285\/","patterns":{"moodle-":{"group":"moodle","configFn":yui2ConfigFn}},"filter":null,"modules":{"moodle-core-popuphelp":{"requires":["moodle-core-tooltip"]},"moodle-core-lockscroll":{"requires":["plugin","base-build"]},"moodle-core-tooltip":{"requires":["base","node","io-base","moodle-core-notification-dialogue","json-parse","widget-position","widget-position-align","event-outside","cache-base"]},"moodle-core-blocks":{"requires":["base","node","io","dom","dd","dd-scroll","moodle-core-dragdrop","moodle-core-notification"]},"moodle-core-chooserdialogue":{"requires":["base","panel","moodle-core-notification"]},"moodle-core-event":{"requires":["event-custom"]},"moodle-core-notification":{"requires":["moodle-core-notification-dialogue","moodle-core-notification-alert","moodle-core-notification-confirm","moodle-core-notification-exception","moodle-core-notification-ajaxexception"]},"moodle-core-notification-dialogue":{"requires":["base","node","panel","escape","event-key","dd-plugin","moodle-core-widget-focusafterclose","moodle-core-lockscroll"]},"moodle-core-notification-alert":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-notification-confirm":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-notification-exception":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-notification-ajaxexception":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-formchangechecker":{"requires":["base","event-focus","moodle-core-event"]},"moodle-core-actionmenu":{"requires":["base","event","node-event-simulate"]},"moodle-core-checknet":{"requires":["base-base","moodle-core-notification-alert","io-base"]},"moodle-core-languninstallconfirm":{"requires":["base","node","moodle-core-notification-confirm","moodle-core-notification-alert"]},"moodle-core-maintenancemodetimer":{"requires":["base","node"]},"moodle-core-dragdrop":{"requires":["base","node","io","dom","dd","event-key","event-focus","moodle-core-notification"]},"moodle-core-handlebars":{"condition":{"trigger":"handlebars","when":"after"}},"moodle-core_availability-form":{"requires":["base","node","event","event-delegate","panel","moodle-core-notification-dialogue","json"]},"moodle-backup-confirmcancel":{"requires":["node","node-event-simulate","moodle-core-notification-confirm"]},"moodle-backup-backupselectall":{"requires":["node","event","node-event-simulate","anim"]},"moodle-course-categoryexpander":{"requires":["node","event-key"]},"moodle-course-modchooser":{"requires":["moodle-core-chooserdialogue","moodle-course-coursebase"]},"moodle-course-management":{"requires":["base","node","io-base","moodle-core-notification-exception","json-parse","dd-constrain","dd-proxy","dd-drop","dd-delegate","node-event-delegate"]},"moodle-course-formatchooser":{"requires":["base","node","node-event-simulate"]},"moodle-course-util":{"requires":["node"],"use":["moodle-course-util-base"],"submodules":{"moodle-course-util-base":{},"moodle-course-util-section":{"requires":["node","moodle-course-util-base"]},"moodle-course-util-cm":{"requires":["node","moodle-course-util-base"]}}},"moodle-course-dragdrop":{"requires":["base","node","io","dom","dd","dd-scroll","moodle-core-dragdrop","moodle-core-notification","moodle-course-coursebase","moodle-course-util"]},"moodle-form-shortforms":{"requires":["node","base","selector-css3","moodle-core-event"]},"moodle-form-passwordunmask":{"requires":[]},"moodle-form-dateselector":{"requires":["base","node","overlay","calendar"]},"moodle-question-searchform":{"requires":["base","node"]},"moodle-question-chooser":{"requires":["moodle-core-chooserdialogue"]},"moodle-question-preview":{"requires":["base","dom","event-delegate","event-key","core_question_engine"]},"moodle-availability_completion-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_date-form":{"requires":["base","node","event","io","moodle-core_availability-form"]},"moodle-availability_grade-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_group-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_grouping-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_profile-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-mod_assign-history":{"requires":["node","transition"]},"moodle-mod_bigbluebuttonbn-recordings":{"requires":["base","node","datasource-get","datasource-jsonschema","datasource-polling","moodle-core-notification"]},"moodle-mod_bigbluebuttonbn-imports":{"requires":["base","node"]},"moodle-mod_bigbluebuttonbn-rooms":{"requires":["base","node","datasource-get","datasource-jsonschema","datasource-polling","moodle-core-notification"]},"moodle-mod_bigbluebuttonbn-broker":{"requires":["base","node","datasource-get","datasource-jsonschema","datasource-polling","moodle-core-notification"]},"moodle-mod_bigbluebuttonbn-modform":{"requires":["base","node"]},"moodle-mod_forum-subscriptiontoggle":{"requires":["base-base","io-base"]},"moodle-mod_quiz-questionchooser":{"requires":["moodle-core-chooserdialogue","moodle-mod_quiz-util","querystring-parse"]},"moodle-mod_quiz-autosave":{"requires":["base","node","event","event-valuechange","node-event-delegate","io-form"]},"moodle-mod_quiz-quizbase":{"requires":["base","node"]},"moodle-mod_quiz-toolboxes":{"requires":["base","node","event","event-key","io","moodle-mod_quiz-quizbase","moodle-mod_quiz-util-slot","moodle-core-notification-ajaxexception"]},"moodle-mod_quiz-modform":{"requires":["base","node","event"]},"moodle-mod_quiz-util":{"requires":["node","moodle-core-actionmenu"],"use":["moodle-mod_quiz-util-base"],"submodules":{"moodle-mod_quiz-util-base":{},"moodle-mod_quiz-util-slot":{"requires":["node","moodle-mod_quiz-util-base"]},"moodle-mod_quiz-util-page":{"requires":["node","moodle-mod_quiz-util-base"]}}},"moodle-mod_quiz-dragdrop":{"requires":["base","node","io","dom","dd","dd-scroll","moodle-core-dragdrop","moodle-core-notification","moodle-mod_quiz-quizbase","moodle-mod_quiz-util-base","moodle-mod_quiz-util-page","moodle-mod_quiz-util-slot","moodle-course-util"]},"moodle-message_airnotifier-toolboxes":{"requires":["base","node","io"]},"moodle-filter_glossary-autolinker":{"requires":["base","node","io-base","json-parse","event-delegate","overlay","moodle-core-event","moodle-core-notification-alert","moodle-core-notification-exception","moodle-core-notification-ajaxexception"]},"moodle-filter_mathjaxloader-loader":{"requires":["moodle-core-event"]},"moodle-editor_atto-rangy":{"requires":[]},"moodle-editor_atto-editor":{"requires":["node","transition","io","overlay","escape","event","event-simulate","event-custom","node-event-html5","node-event-simulate","yui-throttle","moodle-core-notification-dialogue","moodle-core-notification-confirm","moodle-editor_atto-rangy","handlebars","timers","querystring-stringify"]},"moodle-editor_atto-plugin":{"requires":["node","base","escape","event","event-outside","handlebars","event-custom","timers","moodle-editor_atto-menu"]},"moodle-editor_atto-menu":{"requires":["moodle-core-notification-dialogue","node","event","event-custom"]},"moodle-report_eventlist-eventfilter":{"requires":["base","event","node","node-event-delegate","datatable","autocomplete","autocomplete-filters"]},"moodle-report_loglive-fetchlogs":{"requires":["base","event","node","io","node-event-delegate"]},"moodle-gradereport_grader-gradereporttable":{"requires":["base","node","event","handlebars","overlay","event-hover"]},"moodle-gradereport_history-userselector":{"requires":["escape","event-delegate","event-key","handlebars","io-base","json-parse","moodle-core-notification-dialogue"]},"moodle-tool_capability-search":{"requires":["base","node"]},"moodle-tool_lp-dragdrop-reorder":{"requires":["moodle-core-dragdrop"]},"moodle-tool_monitor-dropdown":{"requires":["base","event","node"]},"moodle-assignfeedback_editpdf-editor":{"requires":["base","event","node","io","graphics","json","event-move","event-resize","transition","querystring-stringify-simple","moodle-core-notification-dialog","moodle-core-notification-alert","moodle-core-notification-warning","moodle-core-notification-exception","moodle-core-notification-ajaxexception"]},"moodle-atto_accessibilitychecker-button":{"requires":["color-base","moodle-editor_atto-plugin"]},"moodle-atto_accessibilityhelper-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_align-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_bold-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_charmap-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_clear-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_collapse-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_emoticon-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_equation-button":{"requires":["moodle-editor_atto-plugin","moodle-core-event","io","event-valuechange","tabview","array-extras"]},"moodle-atto_html-beautify":{},"moodle-atto_html-codemirror":{"requires":["moodle-atto_html-codemirror-skin"]},"moodle-atto_html-button":{"requires":["promise","moodle-editor_atto-plugin","moodle-atto_html-beautify","moodle-atto_html-codemirror","event-valuechange"]},"moodle-atto_image-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_indent-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_italic-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_link-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_managefiles-usedfiles":{"requires":["node","escape"]},"moodle-atto_managefiles-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_media-button":{"requires":["moodle-editor_atto-plugin","moodle-form-shortforms"]},"moodle-atto_noautolink-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_orderedlist-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_recordrtc-recording":{"requires":["moodle-atto_recordrtc-button"]},"moodle-atto_recordrtc-button":{"requires":["moodle-editor_atto-plugin","moodle-atto_recordrtc-recording"]},"moodle-atto_rtl-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_strike-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_subscript-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_superscript-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_table-button":{"requires":["moodle-editor_atto-plugin","moodle-editor_atto-menu","event","event-valuechange"]},"moodle-atto_title-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_underline-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_undo-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_unorderedlist-button":{"requires":["moodle-editor_atto-plugin"]}}},"gallery":{"name":"gallery","base":"https:\/\/cacbank.moodlecloud.com\/lib\/yuilib\/gallery\/","combine":true,"comboBase":"https:\/\/cacbank.moodlecloud.com\/theme\/yui_combo.php?","ext":false,"root":"gallery\/1569995285\/","patterns":{"gallery-":{"group":"gallery"}}}},"modules":{"core_filepicker":{"name":"core_filepicker","fullpath":"https:\/\/cacbank.moodlecloud.com\/lib\/javascript.php\/1569995285\/repository\/filepicker.js","requires":["base","node","node-event-simulate","json","async-queue","io-base","io-upload-iframe","io-form","yui2-treeview","panel","cookie","datatable","datatable-sort","resize-plugin","dd-plugin","escape","moodle-core_filepicker","moodle-core-notification-dialogue"]},"core_comment":{"name":"core_comment","fullpath":"https:\/\/cacbank.moodlecloud.com\/lib\/javascript.php\/1569995285\/comment\/comment.js","requires":["base","io-base","node","json","yui2-animation","overlay","escape"]},"mathjax":{"name":"mathjax","fullpath":"https:\/\/cdnjs.cloudflare.com\/ajax\/libs\/mathjax\/2.7.2\/MathJax.js?delayStartupUntil=configured"}}};
M.yui.loader = {modules: {}};

//]]>
</script>

<meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="core/first" src="https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/core/first.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="core/str" src="https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/core/str.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="jquery" src="https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/jquery/jquery-3.2.1.min.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="core/config" src="https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/core/config.js"></script><script type="text/x-mathjax-config;executed=true">
MathJax.Hub.Config({
    config: ["Accessible.js", "Safe.js"],
    errorSettings: { message: ["!"] },
    skipStartupTypeset: true,
    messageStyle: "none"
});
</script><style type="text/css">.MathJax_Hover_Frame {border-radius: .25em; -webkit-border-radius: .25em; -moz-border-radius: .25em; -khtml-border-radius: .25em; box-shadow: 0px 0px 15px #83A; -webkit-box-shadow: 0px 0px 15px #83A; -moz-box-shadow: 0px 0px 15px #83A; -khtml-box-shadow: 0px 0px 15px #83A; border: 1px solid #A6D ! important; display: inline-block; position: absolute}
.MathJax_Menu_Button .MathJax_Hover_Arrow {position: absolute; cursor: pointer; display: inline-block; border: 2px solid #AAA; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px; font-family: 'Courier New',Courier; font-size: 9px; color: #F0F0F0}
.MathJax_Menu_Button .MathJax_Hover_Arrow span {display: block; background-color: #AAA; border: 1px solid; border-radius: 3px; line-height: 0; padding: 4px}
.MathJax_Hover_Arrow:hover {color: white!important; border: 2px solid #CCC!important}
.MathJax_Hover_Arrow:hover span {background-color: #CCC!important}
</style><style type="text/css">#MathJax_About {position: fixed; left: 50%; width: auto; text-align: center; border: 3px outset; padding: 1em 2em; background-color: #DDDDDD; color: black; cursor: default; font-family: message-box; font-size: 120%; font-style: normal; text-indent: 0; text-transform: none; line-height: normal; letter-spacing: normal; word-spacing: normal; word-wrap: normal; white-space: nowrap; float: none; z-index: 201; border-radius: 15px; -webkit-border-radius: 15px; -moz-border-radius: 15px; -khtml-border-radius: 15px; box-shadow: 0px 10px 20px #808080; -webkit-box-shadow: 0px 10px 20px #808080; -moz-box-shadow: 0px 10px 20px #808080; -khtml-box-shadow: 0px 10px 20px #808080; filter: progid:DXImageTransform.Microsoft.dropshadow(OffX=2, OffY=2, Color='gray', Positive='true')}
#MathJax_About.MathJax_MousePost {outline: none}
.MathJax_Menu {position: absolute; background-color: white; color: black; width: auto; padding: 2px; border: 1px solid #CCCCCC; margin: 0; cursor: default; font: menu; text-align: left; text-indent: 0; text-transform: none; line-height: normal; letter-spacing: normal; word-spacing: normal; word-wrap: normal; white-space: nowrap; float: none; z-index: 201; box-shadow: 0px 10px 20px #808080; -webkit-box-shadow: 0px 10px 20px #808080; -moz-box-shadow: 0px 10px 20px #808080; -khtml-box-shadow: 0px 10px 20px #808080; filter: progid:DXImageTransform.Microsoft.dropshadow(OffX=2, OffY=2, Color='gray', Positive='true')}
.MathJax_MenuItem {padding: 2px 2em; background: transparent}
.MathJax_MenuArrow {position: absolute; right: .5em; padding-top: .25em; color: #666666; font-size: .75em}
.MathJax_MenuActive .MathJax_MenuArrow {color: white}
.MathJax_MenuArrow.RTL {left: .5em; right: auto}
.MathJax_MenuCheck {position: absolute; left: .7em}
.MathJax_MenuCheck.RTL {right: .7em; left: auto}
.MathJax_MenuRadioCheck {position: absolute; left: 1em}
.MathJax_MenuRadioCheck.RTL {right: 1em; left: auto}
.MathJax_MenuLabel {padding: 2px 2em 4px 1.33em; font-style: italic}
.MathJax_MenuRule {border-top: 1px solid #CCCCCC; margin: 4px 1px 0px}
.MathJax_MenuDisabled {color: GrayText}
.MathJax_MenuActive {background-color: Highlight; color: HighlightText}
.MathJax_MenuDisabled:focus, .MathJax_MenuLabel:focus {background-color: #E8E8E8}
.MathJax_ContextMenu:focus {outline: none}
.MathJax_ContextMenu .MathJax_MenuItem:focus {outline: none}
#MathJax_AboutClose {top: .2em; right: .2em}
.MathJax_Menu .MathJax_MenuClose {top: -10px; left: -10px}
.MathJax_MenuClose {position: absolute; cursor: pointer; display: inline-block; border: 2px solid #AAA; border-radius: 18px; -webkit-border-radius: 18px; -moz-border-radius: 18px; -khtml-border-radius: 18px; font-family: 'Courier New',Courier; font-size: 24px; color: #F0F0F0}
.MathJax_MenuClose span {display: block; background-color: #AAA; border: 1.5px solid; border-radius: 18px; -webkit-border-radius: 18px; -moz-border-radius: 18px; -khtml-border-radius: 18px; line-height: 0; padding: 8px 0 6px}
.MathJax_MenuClose:hover {color: white!important; border: 2px solid #CCC!important}
.MathJax_MenuClose:hover span {background-color: #CCC!important}
.MathJax_MenuClose:hover:focus {outline: none}
</style><style type="text/css">.MathJax_Preview .MJXf-math {color: inherit!important}
</style><style type="text/css">.MJX_Assistive_MathML {position: absolute!important; top: 0; left: 0; clip: rect(1px, 1px, 1px, 1px); padding: 1px 0 0 0!important; border: 0!important; height: 1px!important; width: 1px!important; overflow: hidden!important; display: block!important; -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none}
.MJX_Assistive_MathML.MJX_Assistive_MathML_Block {width: 100%!important}
</style><style type="text/css">#MathJax_Zoom {position: absolute; background-color: #F0F0F0; overflow: auto; display: block; z-index: 301; padding: .5em; border: 1px solid black; margin: 0; font-weight: normal; font-style: normal; text-align: left; text-indent: 0; text-transform: none; line-height: normal; letter-spacing: normal; word-spacing: normal; word-wrap: normal; white-space: nowrap; float: none; -webkit-box-sizing: content-box; -moz-box-sizing: content-box; box-sizing: content-box; box-shadow: 5px 5px 15px #AAAAAA; -webkit-box-shadow: 5px 5px 15px #AAAAAA; -moz-box-shadow: 5px 5px 15px #AAAAAA; -khtml-box-shadow: 5px 5px 15px #AAAAAA; filter: progid:DXImageTransform.Microsoft.dropshadow(OffX=2, OffY=2, Color='gray', Positive='true')}
#MathJax_ZoomOverlay {position: absolute; left: 0; top: 0; z-index: 300; display: inline-block; width: 100%; height: 100%; border: 0; padding: 0; margin: 0; background-color: white; opacity: 0; filter: alpha(opacity=0)}
#MathJax_ZoomFrame {position: relative; display: inline-block; height: 0; width: 0}
#MathJax_ZoomEventTrap {position: absolute; left: 0; top: 0; z-index: 302; display: inline-block; border: 0; padding: 0; margin: 0; background-color: white; opacity: 0; filter: alpha(opacity=0)}
</style><style type="text/css">.MathJax_Preview {color: #888}
#MathJax_Message {position: fixed; left: 1em; bottom: 1.5em; background-color: #E6E6E6; border: 1px solid #959595; margin: 0px; padding: 2px 8px; z-index: 102; color: black; font-size: 80%; width: auto; white-space: nowrap}
#MathJax_MSIE_Frame {position: absolute; top: 0; left: 0; width: 0px; z-index: 101; border: 0px; margin: 0px; padding: 0px}
.MathJax_Error {color: #CC0000; font-style: italic}
</style><style type="text/css">.MJXp-script {font-size: .8em}
.MJXp-right {-webkit-transform-origin: right; -moz-transform-origin: right; -ms-transform-origin: right; -o-transform-origin: right; transform-origin: right}
.MJXp-bold {font-weight: bold}
.MJXp-italic {font-style: italic}
.MJXp-scr {font-family: MathJax_Script,'Times New Roman',Times,STIXGeneral,serif}
.MJXp-frak {font-family: MathJax_Fraktur,'Times New Roman',Times,STIXGeneral,serif}
.MJXp-sf {font-family: MathJax_SansSerif,'Times New Roman',Times,STIXGeneral,serif}
.MJXp-cal {font-family: MathJax_Caligraphic,'Times New Roman',Times,STIXGeneral,serif}
.MJXp-mono {font-family: MathJax_Typewriter,'Times New Roman',Times,STIXGeneral,serif}
.MJXp-largeop {font-size: 150%}
.MJXp-largeop.MJXp-int {vertical-align: -.2em}
.MJXp-math {display: inline-block; line-height: 1.2; text-indent: 0; font-family: 'Times New Roman',Times,STIXGeneral,serif; white-space: nowrap; border-collapse: collapse}
.MJXp-display {display: block; text-align: center; margin: 1em 0}
.MJXp-math span {display: inline-block}
.MJXp-box {display: block!important; text-align: center}
.MJXp-box:after {content: " "}
.MJXp-rule {display: block!important; margin-top: .1em}
.MJXp-char {display: block!important}
.MJXp-mo {margin: 0 .15em}
.MJXp-mfrac {margin: 0 .125em; vertical-align: .25em}
.MJXp-denom {display: inline-table!important; width: 100%}
.MJXp-denom > * {display: table-row!important}
.MJXp-surd {vertical-align: top}
.MJXp-surd > * {display: block!important}
.MJXp-script-box > *  {display: table!important; height: 50%}
.MJXp-script-box > * > * {display: table-cell!important; vertical-align: top}
.MJXp-script-box > *:last-child > * {vertical-align: bottom}
.MJXp-script-box > * > * > * {display: block!important}
.MJXp-mphantom {visibility: hidden}
.MJXp-munderover {display: inline-table!important}
.MJXp-over {display: inline-block!important; text-align: center}
.MJXp-over > * {display: block!important}
.MJXp-munderover > * {display: table-row!important}
.MJXp-mtable {vertical-align: .25em; margin: 0 .125em}
.MJXp-mtable > * {display: inline-table!important; vertical-align: middle}
.MJXp-mtr {display: table-row!important}
.MJXp-mtd {display: table-cell!important; text-align: center; padding: .5em 0 0 .5em}
.MJXp-mtr > .MJXp-mtd:first-child {padding-left: 0}
.MJXp-mtr:first-child > .MJXp-mtd {padding-top: 0}
.MJXp-mlabeledtr {display: table-row!important}
.MJXp-mlabeledtr > .MJXp-mtd:first-child {padding-left: 0}
.MJXp-mlabeledtr:first-child > .MJXp-mtd {padding-top: 0}
.MJXp-merror {background-color: #FFFF88; color: #CC0000; border: 1px solid #CC0000; padding: 1px 3px; font-style: normal; font-size: 90%}
.MJXp-scale0 {-webkit-transform: scaleX(.0); -moz-transform: scaleX(.0); -ms-transform: scaleX(.0); -o-transform: scaleX(.0); transform: scaleX(.0)}
.MJXp-scale1 {-webkit-transform: scaleX(.1); -moz-transform: scaleX(.1); -ms-transform: scaleX(.1); -o-transform: scaleX(.1); transform: scaleX(.1)}
.MJXp-scale2 {-webkit-transform: scaleX(.2); -moz-transform: scaleX(.2); -ms-transform: scaleX(.2); -o-transform: scaleX(.2); transform: scaleX(.2)}
.MJXp-scale3 {-webkit-transform: scaleX(.3); -moz-transform: scaleX(.3); -ms-transform: scaleX(.3); -o-transform: scaleX(.3); transform: scaleX(.3)}
.MJXp-scale4 {-webkit-transform: scaleX(.4); -moz-transform: scaleX(.4); -ms-transform: scaleX(.4); -o-transform: scaleX(.4); transform: scaleX(.4)}
.MJXp-scale5 {-webkit-transform: scaleX(.5); -moz-transform: scaleX(.5); -ms-transform: scaleX(.5); -o-transform: scaleX(.5); transform: scaleX(.5)}
.MJXp-scale6 {-webkit-transform: scaleX(.6); -moz-transform: scaleX(.6); -ms-transform: scaleX(.6); -o-transform: scaleX(.6); transform: scaleX(.6)}
.MJXp-scale7 {-webkit-transform: scaleX(.7); -moz-transform: scaleX(.7); -ms-transform: scaleX(.7); -o-transform: scaleX(.7); transform: scaleX(.7)}
.MJXp-scale8 {-webkit-transform: scaleX(.8); -moz-transform: scaleX(.8); -ms-transform: scaleX(.8); -o-transform: scaleX(.8); transform: scaleX(.8)}
.MJXp-scale9 {-webkit-transform: scaleX(.9); -moz-transform: scaleX(.9); -ms-transform: scaleX(.9); -o-transform: scaleX(.9); transform: scaleX(.9)}
.MathJax_PHTML .noError { font-size: 90%; text-align: left; color: black; padding: 1px 3px; border: 1px solid}
</style><script type="text/javascript">
require(['jquery'], function($) {
    // Set the minimum width of the input so that the placeholder is whole displayed.
    var inputElement = $(document.getElementById('form_autocomplete_input-1573384684030'));
    if (inputElement.length) {
        inputElement.css('min-width', inputElement.attr('placeholder').length + 'ch');
    }
});
</script><script type="text/javascript">
require(['jquery'], function($) {
    // Set the minimum width of the input so that the placeholder is whole displayed.
    var inputElement = $(document.getElementById('form_autocomplete_input-1573384684031'));
    if (inputElement.length) {
        inputElement.css('min-width', inputElement.attr('placeholder').length + 'ch');
    }
});
</script>
    

    
    </head>
    <body id="page-site-index" class="format-site course path-site chrome dir-ltr lang-en yui-skin-sam yui3-skin-sam cacbank-moodlecloud-com pagelayout-frontpage course-1 context-2 drawer-open-left jsenabled">

        <div id="page-wrapper">

            <div>
                <a class="sr-only sr-only-focusable" href="#maincontent">Skip to main content</a>
            </div><script type="text/javascript" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.js"></script><script type="text/javascript" src="https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/javascript-static.js"></script>
            <script type="text/javascript">
            //<![CDATA[
            document.body.className += ' jsenabled';
            //]]>
            </script>



            <nav class="fixed-top navbar navbar-light bg-white navbar-expand moodle-has-zindex" aria-label="Site navigation">

                <div data-region="drawer-toggle" class="d-inline-block mr-3">
                    <button aria-expanded="true" aria-controls="nav-drawer" type="button" class="btn nav-link float-sm-left mr-1 btn-light bg-gray" data-action="toggle-drawer" data-side="left" data-preference="drawer-open-nav"><i class="fas fa-bars"></i><span class="sr-only">Side panel</span></button>
                </div>

                <a href="homeLec_1.jsp" class="navbar-brand 
                   d-none d-md-block
                   ">
                    <span class="site-name d-none d-md-inline">Question and Test Bank</span>
                </a>
                <ul class="navbar-nav d-none d-md-flex">
                    <!-- custom_menu -->
                    <li class="dropdown nav-item">
                        <a class="dropdown-toggle nav-link" id="drop-down-5dc0249d7928b5dc0249d773cf4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" aria-controls="drop-down-menu-5dc0249d7928b5dc0249d773cf4">
                            English ‎(en)‎
                        </a>
                    </li>

                </ul>
                <ul class="nav navbar-nav ml-auto">
                    <div class="d-none d-lg-block">

                    </div>
                    <!-- navbar_plugin_output -->
                    <li class="nav-item">
                        <div class="float-right popover-region collapsed">
                            <a id="message-drawer-toggle-5dc0249d7e1065dc0249d773cf5" class="nav-link d-inline-block popover-region-toggle position-relative" href="#" role="button">
                                <i class="fas fa-user"></i>
                                <div class="count-container hidden" data-region="count-container" aria-label="There are 0 unread conversations">0</div>
                            </a>
                        </div><div class="popover-region collapsed popover-region-notifications" id="nav-notification-popover-container" data-userid="2" data-region="popover-region">

                            <div id="popover-region-container-5dc0249d80d125dc0249d773cf6" class="popover-region-container" data-region="popover-region-container" aria-expanded="false" aria-hidden="true" aria-label="Notification window" role="region">
                                <div class="popover-region-header-container">
                                    <h3 class="popover-region-header-text" data-region="popover-region-header-text">Notifications</h3>
                                    <div class="popover-region-header-actions" data-region="popover-region-header-actions">        <a class="mark-all-read-button" href="#" title="Mark all as read" data-action="mark-all-read" role="button">
                                            <span class="normal-icon"><i class="icon fa fa-check fa-fw " title="Mark all as read" aria-label="Mark all as read"></i></span>
                                            <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                        </a>
                                        <a href="#" title="Notification preferences">
                                            <i class="fas fa-cog"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="popover-region-content-container" data-region="popover-region-content-container">
                                    <div class="popover-region-content" data-region="popover-region-content">
                                        <div class="all-notifications" data-region="all-notifications" role="log" aria-busy="false" aria-atomic="false" aria-relevant="additions"></div>
                                        <div class="empty-message" tabindex="0" data-region="empty-message">You have no notifications</div>

                                    </div>
                                    <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                </div>
                                <a class="see-all-link" href="#">
                                    <div class="popover-region-footer-container">
                                        <div class="popover-region-seeall-text">See all</div>
                                    </div>
                                </a>
                            </div>
                        </div><div class="popover-region collapsed popover-region-quotas boost" id="nav-moodlecloud-quota-popover-container" data-userid="" data-region="popover-region">

                            <div id="popover-region-container-5dc0249d826255dc0249d773cf7" class="popover-region-container" data-region="popover-region-container" aria-expanded="false" aria-hidden="true" aria-label="Notification window" role="region">
                                <div class="popover-region-header-container">
                                    <h3 class="popover-region-header-text" data-region="popover-region-header-text">Quotas</h3>
                                    <div class="popover-region-header-actions" data-region="popover-region-header-actions"></div>
                                </div>
                                <div class="popover-region-content-container" data-region="popover-region-content-container">
                                    <div class="popover-region-content" data-region="popover-region-content">
                                        

                                    </div>
                                    <span class="loading-icon icon-no-margin"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>
                                </div>
                                <a class="see-all-link" href="#">
                                    <div class="popover-region-footer-container">
                                        <div class="popover-region-seeall-text">See all</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <!-- user_menu -->
                    <li class="nav-item d-flex align-items-center">
                    </li>
                    <li class="nav-item d-flex align-items-center">
                        <div class="usermenu"><div class="action-menu moodle-actionmenu nowrap-items d-inline" id="action-menu-1" data-enhance="moodle-core-actionmenu">

                                <div class="menubar d-flex " id="action-menu-1-menubar" role="menubar">
                                    <div class="action-menu-trigger">
                                        <div class="dropdown">
                                            <a href="#" tabindex="0" class=" dropdown-toggle icon-no-margin" id="dropdown-1" aria-label="User menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" aria-controls="action-menu-1-menu">

                                                <span class="userbutton"><span class="usertext mr-1">${USER.name}</span><span class="avatars"><span class="avatar current"></span></span></span>

                                                <b class="caret"></b>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right menu  align-tr-br" id="action-menu-1-menu" data-rel="menu-content" aria-labelledby="action-menu-toggle-1" role="menu" data-align="tr-br">
                                                
                                                <div class="dropdown-divider" role="presentation"><span class="filler">&nbsp;</span></div>
                                                <a href="profile.jsp" class="dropdown-item menu-action" role="menuitem" data-title="profile,moodle" aria-labelledby="actionmenuaction-2">
                                                    
                                                    <span class="menu-action-text" id="actionmenuaction-2">
                                                        Profile
                                                    </span>
                                                </a>
                                                <a href="#" class="dropdown-item menu-action" role="menuitem" data-title="messages,message" aria-labelledby="actionmenuaction-4">
    
                                                    <span class="menu-action-text" id="actionmenuaction-4">
                                                        Messages
                                                    </span>
                                                </a>
                                                <a href="logout" class="dropdown-item menu-action" role="menuitem" data-title="logout,moodle" aria-labelledby="actionmenuaction-6">
                                                    
                                                    <span class="menu-action-text" id="actionmenuaction-6">
                                                        Log out
                                                    </span>
                                                </a>
                                                <div class="dropdown-divider" role="presentation"><span class="filler">&nbsp;</span></div>
                                                
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div></div>
                    </li>
                </ul>
                <!-- search_box -->
            </nav>
            <div id="page" class="container-fluid">
                <header id="page-header" class="row">
                    <div class="col-12 pt-3 pb-3" id="yui_3_17_2_1_1572971976817_783">
                        <div class="card " id="yui_3_17_2_1_1572971976817_782">
                            <div class="card-body " id="yui_3_17_2_1_1572971976817_781">
                                <div class="d-flex" id="yui_3_17_2_1_1572971976817_784">
                                    <div class="mr-auto">
                                        <div class="page-context-header"><div class="page-header-headings"><h1>${course.cName}</h1></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <div id="page-content" class="row pb-3">
                    <div id="region-main-box" class="col-12">
                        <section id="region-main" aria-label="Content">
                            <section id="region-main" aria-label="Content">

                    <span class="notifications" id="user-notifications"></span>
                    <div role="main" id="yui_3_17_2_1_1573384683206_231"><span id="maincontent"></span><h2><img class="icon icon iconlarge" alt="" aria-hidden="true" src="https://cacbank.moodlecloud.com/theme/image.php/boost/quiz/1569995285/icon">Adding a new Quiz<a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right" data-content="<div class=&quot;no-overflow&quot;><p>The quiz activity enables a teacher to create quizzes comprising questions of various types, including multiple choice, matching, short-answer and numerical.</p>

<p>The teacher can allow the quiz to be attempted multiple times, with the questions shuffled or randomly selected from the question bank. A time limit may be set.</p>

<p>Each attempt is marked automatically, with the exception of essay questions, and the grade is recorded in the gradebook.</p>

<p>The teacher can choose when and if hints, feedback and correct answers are shown to students.</p>

<p>Quizzes may be used</p>

<ul><li>As course exams</li>
<li>As mini tests for reading assignments or at the end of a topic</li>
<li>As exam practice using questions from past exams</li>
<li>To deliver immediate feedback about performance</li>
<li>For self-assessment</li>
</ul></div> <div class=&quot;helpdoclink&quot;><a href=&quot;http://docs.moodle.org/37/en/mod/quiz/view&quot; class=&quot;helplinkpopup&quot;><i class=&quot;icon fa fa-info-circle fa-fw iconhelp icon-pre&quot; aria-hidden=&quot;true&quot;  ></i>More help</a></div>" data-html="true" tabindex="0" data-trigger="focus">
  <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Quiz" aria-label="Help with Quiz"></i>
</a></h2>

	<div style="display: none;"><input name="grade" type="hidden" value="10">
<input name="boundary_repeats" type="hidden" value="1" id="yui_3_17_2_1_1573384683206_1967">
<input name="completionunlocked" type="hidden" value="1" id="yui_3_17_2_1_1573384683206_1968">
<input name="course" type="hidden" value="7" id="yui_3_17_2_1_1573384683206_1969">
<input name="coursemodule" type="hidden" value="" id="yui_3_17_2_1_1573384683206_1970">
<input name="section" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1971">
<input name="module" type="hidden" value="16" id="yui_3_17_2_1_1573384683206_1972">
<input name="modulename" type="hidden" value="quiz" id="yui_3_17_2_1_1573384683206_1973">
<input name="instance" type="hidden" value="" id="yui_3_17_2_1_1573384683206_1974">
<input name="add" type="hidden" value="quiz" id="yui_3_17_2_1_1573384683206_1975">
<input name="update" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1976">
<input name="return" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1977">
<input name="sr" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1978">
<input name="sesskey" type="hidden" value="UkQaQYey1V" id="yui_3_17_2_1_1573384683206_1979">
<input name="_qf__mod_quiz_mod_form" type="hidden" value="1" id="yui_3_17_2_1_1573384683206_1980">
<input name="mform_showmore_id_layouthdr" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1981">
<input name="mform_showmore_id_interactionhdr" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1982">
<input name="mform_showmore_id_display" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1983">
<input name="mform_showmore_id_security" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1984">
<input name="mform_isexpanded_id_general" type="hidden" value="1" id="yui_3_17_2_1_1573384683206_1985">
<input name="mform_isexpanded_id_timing" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1986">
<input name="mform_isexpanded_id_modstandardgrade" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1987">
<input name="mform_isexpanded_id_layouthdr" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1988">
<input name="mform_isexpanded_id_interactionhdr" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1989">
<input name="mform_isexpanded_id_reviewoptionshdr" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1990">
<input name="mform_isexpanded_id_display" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1991">
<input name="mform_isexpanded_id_security" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1992">
<input name="mform_isexpanded_id_overallfeedbackhdr" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1993">
<input name="mform_isexpanded_id_modstandardelshdr" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1994">
<input name="mform_isexpanded_id_availabilityconditionsheader" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1995">
<input name="mform_isexpanded_id_activitycompletionheader" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1996">
<input name="mform_isexpanded_id_tagshdr" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1997">
<input name="mform_isexpanded_id_competenciessection" type="hidden" value="0" id="yui_3_17_2_1_1573384683206_1998">
</div>

	
	<fieldset class="clearfix collapsible" id="id_general">
             <form action="createtest" method="POST"> 
		<legend class="ftoggler"><a href="#" class="fheader" role="button" aria-controls="id_general" aria-expanded="true" id="yui_3_17_2_1_1573384683206_229">General</a></legend>
		<div class="fcontainer clearfix" id="yui_3_17_2_1_1573384683206_708">
		<div id="fitem_id_name" class="form-group row  fitem   ">
    <div class="col-md-3">
        <span class="float-sm-right text-nowrap">
            
        </span>
        <label class="col-form-label d-inline " for="id_name">
            Name
        </label>
    </div>
    <div class="col-md-9 form-inline felement" data-fieldtype="text">
        <input type="text" class="form-control " name="name" id="id_name" value="" size="64">
        <div class="form-control-feedback invalid-feedback" id="id_error_name">
            
        </div>
    </div>
</div>
                    <div class="form-group row  fitem  ">
    <div class="col-md-3">
    </div>
   
</div>
		</div></fieldset>
<fieldset class="clearfix collapsible" id="id_timing">
   
		<legend class="ftoggler" id="yui_3_17_2_1_1573388941196_2105"><a href="#" class="fheader" role="button" aria-controls="id_timing" aria-expanded="true" id="yui_3_17_2_1_1573388941196_235">Timing</a></legend>
		<div class="fcontainer clearfix" id="yui_3_17_2_1_1573388941196_2116">
		<div id="fitem_id_timeopen" class="form-group row  fitem   " data-groupname="timeopen">
    <div class="col-md-3">
        <span class="float-sm-right text-nowrap">
            
            
            <a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right" data-content="<div class=&quot;no-overflow&quot;><p>Students can only start their attempt(s) after the open time and they must complete their attempts before the close time.</p>
</div> <div class=&quot;helpdoclink&quot;><a href=&quot;http://docs.moodle.org/37/en/mod/quiz/timing&quot; class=&quot;helplinkpopup&quot;><i class=&quot;icon fa fa-info-circle fa-fw iconhelp icon-pre&quot; aria-hidden=&quot;true&quot;  ></i>More help</a></div>" data-html="true" tabindex="0" data-trigger="focus">
  </a>
        </span>
        <label class="col-form-label d-inline " for="id_timeopen">
            Open the quiz
        </label>
    </div>
    <div class="col-md-9 form-inline felement" data-fieldtype="date_time_selector" id="yui_3_17_2_1_1573388941196_2115">
        <div class="fdate_time_selector d-flex flex-wrap align-items-center" id="yui_3_17_2_1_1573388941196_119">
            
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2003">
    <label class="col-form-label sr-only" for="id_timeopen_day">
        Day 
        
        
    </label>
    <span data-fieldtype="select" id="yui_3_17_2_1_1573388941196_2002">
    <select class="custom-select
                   
                   " name="timeopenday" id="id_timeopen_day">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10" selected="">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option>
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
    </select>
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_timeopen_day">
        
    </div>
</div>
            &nbsp;
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2006">
    <label class="col-form-label sr-only" for="id_timeopen_month">
        Month 
        
        
    </label>
    <span data-fieldtype="select" id="yui_3_17_2_1_1573388941196_2005">
    <select class="custom-select
                   
                   " name="timeopenmonth" id="id_timeopen_month">
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11" selected="">November</option>
        <option value="12">December</option>
    </select>
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_timeopen_month">
        
    </div>
</div>
            &nbsp;
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2008">
    <label class="col-form-label sr-only" for="id_timeopen_year">
        Year 
        
        
    </label>
    <span data-fieldtype="select" id="yui_3_17_2_1_1573388941196_2007">
    <select class="custom-select
                   
                   " name="timeopenyear" id="id_timeopen_year">
        <option value="1900">1900</option>
        <option value="1901">1901</option>
        <option value="1902">1902</option>
        <option value="1903">1903</option>
        <option value="1904">1904</option>
        <option value="1905">1905</option>
        <option value="1906">1906</option>
        <option value="1907">1907</option>
        <option value="1908">1908</option>
        <option value="1909">1909</option>
        <option value="1910">1910</option>
        <option value="1911">1911</option>
        <option value="1912">1912</option>
        <option value="1913">1913</option>
        <option value="1914">1914</option>
        <option value="1915">1915</option>
        <option value="1916">1916</option>
        <option value="1917">1917</option>
        <option value="1918">1918</option>
        <option value="1919">1919</option>
        <option value="1920">1920</option>
        <option value="1921">1921</option>
        <option value="1922">1922</option>
        <option value="1923">1923</option>
        <option value="1924">1924</option>
        <option value="1925">1925</option>
        <option value="1926">1926</option>
        <option value="1927">1927</option>
        <option value="1928">1928</option>
        <option value="1929">1929</option>
        <option value="1930">1930</option>
        <option value="1931">1931</option>
        <option value="1932">1932</option>
        <option value="1933">1933</option>
        <option value="1934">1934</option>
        <option value="1935">1935</option>
        <option value="1936">1936</option>
        <option value="1937">1937</option>
        <option value="1938">1938</option>
        <option value="1939">1939</option>
        <option value="1940">1940</option>
        <option value="1941">1941</option>
        <option value="1942">1942</option>
        <option value="1943">1943</option>
        <option value="1944">1944</option>
        <option value="1945">1945</option>
        <option value="1946">1946</option>
        <option value="1947">1947</option>
        <option value="1948">1948</option>
        <option value="1949">1949</option>
        <option value="1950">1950</option>
        <option value="1951">1951</option>
        <option value="1952">1952</option>
        <option value="1953">1953</option>
        <option value="1954">1954</option>
        <option value="1955">1955</option>
        <option value="1956">1956</option>
        <option value="1957">1957</option>
        <option value="1958">1958</option>
        <option value="1959">1959</option>
        <option value="1960">1960</option>
        <option value="1961">1961</option>
        <option value="1962">1962</option>
        <option value="1963">1963</option>
        <option value="1964">1964</option>
        <option value="1965">1965</option>
        <option value="1966">1966</option>
        <option value="1967">1967</option>
        <option value="1968">1968</option>
        <option value="1969">1969</option>
        <option value="1970">1970</option>
        <option value="1971">1971</option>
        <option value="1972">1972</option>
        <option value="1973">1973</option>
        <option value="1974">1974</option>
        <option value="1975">1975</option>
        <option value="1976">1976</option>
        <option value="1977">1977</option>
        <option value="1978">1978</option>
        <option value="1979">1979</option>
        <option value="1980">1980</option>
        <option value="1981">1981</option>
        <option value="1982">1982</option>
        <option value="1983">1983</option>
        <option value="1984">1984</option>
        <option value="1985">1985</option>
        <option value="1986">1986</option>
        <option value="1987">1987</option>
        <option value="1988">1988</option>
        <option value="1989">1989</option>
        <option value="1990">1990</option>
        <option value="1991">1991</option>
        <option value="1992">1992</option>
        <option value="1993">1993</option>
        <option value="1994">1994</option>
        <option value="1995">1995</option>
        <option value="1996">1996</option>
        <option value="1997">1997</option>
        <option value="1998">1998</option>
        <option value="1999">1999</option>
        <option value="2000">2000</option>
        <option value="2001">2001</option>
        <option value="2002">2002</option>
        <option value="2003">2003</option>
        <option value="2004">2004</option>
        <option value="2005">2005</option>
        <option value="2006">2006</option>
        <option value="2007">2007</option>
        <option value="2008">2008</option>
        <option value="2009">2009</option>
        <option value="2010">2010</option>
        <option value="2011">2011</option>
        <option value="2012">2012</option>
        <option value="2013">2013</option>
        <option value="2014">2014</option>
        <option value="2015">2015</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
        <option value="2019" selected="">2019</option>
        <option value="2020">2020</option>
        <option value="2021">2021</option>
        <option value="2022">2022</option>
        <option value="2023">2023</option>
        <option value="2024">2024</option>
        <option value="2025">2025</option>
        <option value="2026">2026</option>
        <option value="2027">2027</option>
        <option value="2028">2028</option>
        <option value="2029">2029</option>
        <option value="2030">2030</option>
        <option value="2031">2031</option>
        <option value="2032">2032</option>
        <option value="2033">2033</option>
        <option value="2034">2034</option>
        <option value="2035">2035</option>
        <option value="2036">2036</option>
        <option value="2037">2037</option>
        <option value="2038">2038</option>
        <option value="2039">2039</option>
        <option value="2040">2040</option>
        <option value="2041">2041</option>
        <option value="2042">2042</option>
        <option value="2043">2043</option>
        <option value="2044">2044</option>
        <option value="2045">2045</option>
        <option value="2046">2046</option>
        <option value="2047">2047</option>
        <option value="2048">2048</option>
        <option value="2049">2049</option>
        <option value="2050">2050</option>
    </select>
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_timeopen_year">
        
    </div>
</div>
            &nbsp;
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2010">
    <label class="col-form-label sr-only" for="id_timeopen_hour">
        Hour 
        
        
    </label>
    <span data-fieldtype="select" id="yui_3_17_2_1_1573388941196_2009">
    <select class="custom-select
                   
                   " name="timeopenhour" id="id_timeopen_hour">
        <option value="0">00</option>
        <option value="1">01</option>
        <option value="2">02</option>
        <option value="3">03</option>
        <option value="4">04</option>
        <option value="5">05</option>
        <option value="6">06</option>
        <option value="7">07</option>
        <option value="8">08</option>
        <option value="9">09</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19" selected="">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
    </select>
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_timeopen_hour">
        
    </div>
</div>
            &nbsp;
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2012">
    <label class="col-form-label sr-only" for="id_timeopen_minute">
        Minute 
        
        
    </label>
    <span data-fieldtype="select" id="yui_3_17_2_1_1573388941196_2011">
    <select class="custom-select
                   
                   " name="timeopenminute" id="id_timeopen_minute">
        <option value="0">00</option>
        <option value="1">01</option>
        <option value="2">02</option>
        <option value="3">03</option>
        <option value="4">04</option>
        <option value="5">05</option>
        <option value="6">06</option>
        <option value="7">07</option>
        <option value="8">08</option>
        <option value="9">09</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28" selected="">28</option>
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
        <option value="32">32</option>
        <option value="33">33</option>
        <option value="34">34</option>
        <option value="35">35</option>
        <option value="36">36</option>
        <option value="37">37</option>
        <option value="38">38</option>
        <option value="39">39</option>
        <option value="40">40</option>
        <option value="41">41</option>
        <option value="42">42</option>
        <option value="43">43</option>
        <option value="44">44</option>
        <option value="45">45</option>
        <option value="46">46</option>
        <option value="47">47</option>
        <option value="48">48</option>
        <option value="49">49</option>
        <option value="50">50</option>
        <option value="51">51</option>
        <option value="52">52</option>
        <option value="53">53</option>
        <option value="54">54</option>
        <option value="55">55</option>
        <option value="56">56</option>
        <option value="57">57</option>
        <option value="58">58</option>
        <option value="59">59</option>
    </select>
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_timeopen_minute">
        
    </div>
</div>
            &nbsp;
           
            &nbsp;
            
            

<span class="form-control-feedback invalid-feedback" id="id_error_timeopen_enabled">
    
</span>
        </div>
        <div class="form-control-feedback invalid-feedback" id="id_error_timeopen">
            
        </div>
    </div>
</div><div id="fitem_id_timeclose" class="form-group row  fitem   " data-groupname="timeclose">
    <div class="col-md-3">
        <span class="float-sm-right text-nowrap">
            
            
            
        </span>
        <label class="col-form-label d-inline " for="id_timeclose">
            Close the quiz
        </label>
    </div>
    <div class="col-md-9 form-inline felement" data-fieldtype="date_time_selector" id="yui_3_17_2_1_1573388941196_2121">
        <div class="fdate_time_selector d-flex flex-wrap align-items-center" id="yui_3_17_2_1_1573388941196_146">
            
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2014">
    <label class="col-form-label sr-only" for="id_timeclose_day">
        Day 
        
        
    </label>
    <span data-fieldtype="select" id="yui_3_17_2_1_1573388941196_2013">
    <select class="custom-select
                   
                   " name="timecloseday" id="id_timeclose_day">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10" selected="">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option>
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
    </select>
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_timeclose_day">
        
    </div>
</div>
            &nbsp;
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2016">
    <label class="col-form-label sr-only" for="id_timeclose_month">
        Month 
        
        
    </label>
    <span data-fieldtype="select" id="yui_3_17_2_1_1573388941196_2015">
    <select class="custom-select
                   
                   " name="timeclosemonth" id="id_timeclose_month">
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11" selected="">November</option>
        <option value="12">December</option>
    </select>
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_timeclose_month">
        
    </div>
</div>
            &nbsp;
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2018">
    <label class="col-form-label sr-only" for="id_timeclose_year">
        Year 
        
        
    </label>
    <span data-fieldtype="select" id="yui_3_17_2_1_1573388941196_2017">
    <select class="custom-select
                   
                   " name="timecloseyear" id="id_timeclose_year">
        <option value="1900">1900</option>
        <option value="1901">1901</option>
        <option value="1902">1902</option>
        <option value="1903">1903</option>
        <option value="1904">1904</option>
        <option value="1905">1905</option>
        <option value="1906">1906</option>
        <option value="1907">1907</option>
        <option value="1908">1908</option>
        <option value="1909">1909</option>
        <option value="1910">1910</option>
        <option value="1911">1911</option>
        <option value="1912">1912</option>
        <option value="1913">1913</option>
        <option value="1914">1914</option>
        <option value="1915">1915</option>
        <option value="1916">1916</option>
        <option value="1917">1917</option>
        <option value="1918">1918</option>
        <option value="1919">1919</option>
        <option value="1920">1920</option>
        <option value="1921">1921</option>
        <option value="1922">1922</option>
        <option value="1923">1923</option>
        <option value="1924">1924</option>
        <option value="1925">1925</option>
        <option value="1926">1926</option>
        <option value="1927">1927</option>
        <option value="1928">1928</option>
        <option value="1929">1929</option>
        <option value="1930">1930</option>
        <option value="1931">1931</option>
        <option value="1932">1932</option>
        <option value="1933">1933</option>
        <option value="1934">1934</option>
        <option value="1935">1935</option>
        <option value="1936">1936</option>
        <option value="1937">1937</option>
        <option value="1938">1938</option>
        <option value="1939">1939</option>
        <option value="1940">1940</option>
        <option value="1941">1941</option>
        <option value="1942">1942</option>
        <option value="1943">1943</option>
        <option value="1944">1944</option>
        <option value="1945">1945</option>
        <option value="1946">1946</option>
        <option value="1947">1947</option>
        <option value="1948">1948</option>
        <option value="1949">1949</option>
        <option value="1950">1950</option>
        <option value="1951">1951</option>
        <option value="1952">1952</option>
        <option value="1953">1953</option>
        <option value="1954">1954</option>
        <option value="1955">1955</option>
        <option value="1956">1956</option>
        <option value="1957">1957</option>
        <option value="1958">1958</option>
        <option value="1959">1959</option>
        <option value="1960">1960</option>
        <option value="1961">1961</option>
        <option value="1962">1962</option>
        <option value="1963">1963</option>
        <option value="1964">1964</option>
        <option value="1965">1965</option>
        <option value="1966">1966</option>
        <option value="1967">1967</option>
        <option value="1968">1968</option>
        <option value="1969">1969</option>
        <option value="1970">1970</option>
        <option value="1971">1971</option>
        <option value="1972">1972</option>
        <option value="1973">1973</option>
        <option value="1974">1974</option>
        <option value="1975">1975</option>
        <option value="1976">1976</option>
        <option value="1977">1977</option>
        <option value="1978">1978</option>
        <option value="1979">1979</option>
        <option value="1980">1980</option>
        <option value="1981">1981</option>
        <option value="1982">1982</option>
        <option value="1983">1983</option>
        <option value="1984">1984</option>
        <option value="1985">1985</option>
        <option value="1986">1986</option>
        <option value="1987">1987</option>
        <option value="1988">1988</option>
        <option value="1989">1989</option>
        <option value="1990">1990</option>
        <option value="1991">1991</option>
        <option value="1992">1992</option>
        <option value="1993">1993</option>
        <option value="1994">1994</option>
        <option value="1995">1995</option>
        <option value="1996">1996</option>
        <option value="1997">1997</option>
        <option value="1998">1998</option>
        <option value="1999">1999</option>
        <option value="2000">2000</option>
        <option value="2001">2001</option>
        <option value="2002">2002</option>
        <option value="2003">2003</option>
        <option value="2004">2004</option>
        <option value="2005">2005</option>
        <option value="2006">2006</option>
        <option value="2007">2007</option>
        <option value="2008">2008</option>
        <option value="2009">2009</option>
        <option value="2010">2010</option>
        <option value="2011">2011</option>
        <option value="2012">2012</option>
        <option value="2013">2013</option>
        <option value="2014">2014</option>
        <option value="2015">2015</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
        <option value="2019" selected="">2019</option>
        <option value="2020">2020</option>
        <option value="2021">2021</option>
        <option value="2022">2022</option>
        <option value="2023">2023</option>
        <option value="2024">2024</option>
        <option value="2025">2025</option>
        <option value="2026">2026</option>
        <option value="2027">2027</option>
        <option value="2028">2028</option>
        <option value="2029">2029</option>
        <option value="2030">2030</option>
        <option value="2031">2031</option>
        <option value="2032">2032</option>
        <option value="2033">2033</option>
        <option value="2034">2034</option>
        <option value="2035">2035</option>
        <option value="2036">2036</option>
        <option value="2037">2037</option>
        <option value="2038">2038</option>
        <option value="2039">2039</option>
        <option value="2040">2040</option>
        <option value="2041">2041</option>
        <option value="2042">2042</option>
        <option value="2043">2043</option>
        <option value="2044">2044</option>
        <option value="2045">2045</option>
        <option value="2046">2046</option>
        <option value="2047">2047</option>
        <option value="2048">2048</option>
        <option value="2049">2049</option>
        <option value="2050">2050</option>
    </select>
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_timeclose_year">
        
    </div>
</div>
            &nbsp;
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2020">
    <label class="col-form-label sr-only" for="id_timeclose_hour">
        Hour 
        
        
    </label>
    <span data-fieldtype="select" id="yui_3_17_2_1_1573388941196_2019">
    <select class="custom-select
                   
                   " name="timeclosehour" id="id_timeclose_hour">
        <option value="0">00</option>
        <option value="1">01</option>
        <option value="2">02</option>
        <option value="3">03</option>
        <option value="4">04</option>
        <option value="5">05</option>
        <option value="6">06</option>
        <option value="7">07</option>
        <option value="8">08</option>
        <option value="9">09</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19" selected="">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
    </select>
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_timeclose_hour">
        
    </div>
</div>
            &nbsp;
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2022">
    <label class="col-form-label sr-only" for="id_timeclose_minute">
        Minute 
        
        
    </label>
    <span data-fieldtype="select" id="yui_3_17_2_1_1573388941196_2021">
    <select class="custom-select
                   
                   " name="timecloseminute" id="id_timeclose_minute">
        <option value="0">00</option>
        <option value="1">01</option>
        <option value="2">02</option>
        <option value="3">03</option>
        <option value="4">04</option>
        <option value="5">05</option>
        <option value="6">06</option>
        <option value="7">07</option>
        <option value="8">08</option>
        <option value="9">09</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28" selected="">28</option>
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
        <option value="32">32</option>
        <option value="33">33</option>
        <option value="34">34</option>
        <option value="35">35</option>
        <option value="36">36</option>
        <option value="37">37</option>
        <option value="38">38</option>
        <option value="39">39</option>
        <option value="40">40</option>
        <option value="41">41</option>
        <option value="42">42</option>
        <option value="43">43</option>
        <option value="44">44</option>
        <option value="45">45</option>
        <option value="46">46</option>
        <option value="47">47</option>
        <option value="48">48</option>
        <option value="49">49</option>
        <option value="50">50</option>
        <option value="51">51</option>
        <option value="52">52</option>
        <option value="53">53</option>
        <option value="54">54</option>
        <option value="55">55</option>
        <option value="56">56</option>
        <option value="57">57</option>
        <option value="58">58</option>
        <option value="59">59</option>
    </select>
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_timeclose_minute">
        
    </div>
</div>
            &nbsp;
            
            &nbsp;
            

<span class="form-control-feedback invalid-feedback" id="id_error_timeclose_enabled">
    
</span>
        </div>
        <div class="form-control-feedback invalid-feedback" id="id_error_timeclose">
            
        </div>
    </div>
</div><div id="fitem_id_timelimit" class="form-group row  fitem   " data-groupname="timelimit">
    <div class="col-md-3">
        <span class="float-sm-right text-nowrap">
            
            
            <a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right" data-content="<div class=&quot;no-overflow&quot;><p>If enabled, the time limit is stated on the initial quiz page and a countdown timer is displayed in the quiz navigation block.</p>
</div> <div class=&quot;helpdoclink&quot;><a href=&quot;http://docs.moodle.org/37/en/mod/quiz/timing&quot; class=&quot;helplinkpopup&quot;><i class=&quot;icon fa fa-info-circle fa-fw iconhelp icon-pre&quot; aria-hidden=&quot;true&quot;  ></i>More help</a></div>" data-html="true" tabindex="0" data-trigger="focus">
 </a>
        </span>
        <label class="col-form-label d-inline " for="id_timelimit">
            Time limit
        </label>
    </div>
    <div class="col-md-9 form-inline felement" data-fieldtype="duration" id="yui_3_17_2_1_1573388941196_2372">
            
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2031">
    <label class="col-form-label sr-only" for="id_timelimit_number">
        Time 
        
        
    </label>
    <span data-fieldtype="text" id="yui_3_17_2_1_1573388941196_2030">
    <input type="text" class="form-control " name="timelimitnumber" id="id_timelimit_number" value="0" size="3">
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_timelimit_number">
        
    </div>
</div>
            &nbsp;
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2033">
    <label class="col-form-label sr-only" for="id_timelimit_timeunit">
        Time unit 
        
        
    </label>
    <span data-fieldtype="select" id="yui_3_17_2_1_1573388941196_2032">
    <select class="custom-select" name="timelimittimeunit" id="id_timelimit_timeunit">
        <option value="60" selected="">minutes</option>
    </select>
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_timelimit_timeunit">
        
    </div>
</div>
            &nbsp;
          

<span class="form-control-feedback invalid-feedback" id="id_error_timelimit_enabled">
    
</span>
        <div class="form-control-feedback invalid-feedback" id="id_error_timelimit">
            
        </div>
    </div>
</div><div id="fitem_id_graceperiod" class="form-group row  fitem   " data-groupname="graceperiod" hidden="hidden" style="display: none;">
    <div class="col-md-3">
        <span class="float-sm-right text-nowrap">
            
            
            <a class="btn btn-link p-0" role="button" data-container="body" data-toggle="popover" data-placement="right" data-content="<div class=&quot;no-overflow&quot;><p>If what to do when time expires is set to 'Allow a grace period to submit, but not change any responses', this is the amount of extra time that is allowed.</p>
</div> " data-html="true" tabindex="0" data-trigger="focus">
  <i class="icon fa fa-question-circle text-info fa-fw " title="Help with Submission grace period" aria-label="Help with Submission grace period"></i>
</a>
        </span>
        <label class="col-form-label d-inline " for="id_graceperiod">
            Submission grace period
        </label>
    </div>
    <div class="col-md-9 form-inline felement" data-fieldtype="duration">
            
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2065" hidden="hidden" style="display: none;">
    <label class="col-form-label sr-only" for="id_graceperiod_number" hidden="hidden" style="display: none;">
        Time 
        
        
    </label>
    <span data-fieldtype="text" id="yui_3_17_2_1_1573388941196_2064">
    <input type="text" class="form-control " name="graceperiod[number]" id="id_graceperiod_number" value="1" size="3" disabled="disabled">
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_graceperiod_number">
        
    </div>
</div>
            &nbsp;
            <div class="form-group  fitem  " id="yui_3_17_2_1_1573388941196_2067" hidden="hidden" style="display: none;">
    <label class="col-form-label sr-only" for="id_graceperiod_timeunit" hidden="hidden" style="display: none;">
        Time unit 
        
        
    </label>
    <span data-fieldtype="select" id="yui_3_17_2_1_1573388941196_2066">
    <select class="custom-select
                   
                   " name="graceperiod[timeunit]" id="id_graceperiod_timeunit" disabled="disabled">
        <option value="604800">weeks</option>
        <option value="86400" selected="">days</option>
        <option value="3600">hours</option>
        <option value="60">minutes</option>
        <option value="1">seconds</option>
    </select>
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_graceperiod_timeunit">
        
        
        
    </div>
    </div>
            
</div>
            &nbsp;
           
<span class="form-control-feedback invalid-feedback" id="id_error_graceperiod_enabled">
  
</span>
        <div class="form-control-feedback invalid-feedback" id="id_error_graceperiod">
            
        </div>
    </div>
</div>
		</div>
                    <legend class="ftoggler" id="yui_3_17_2_1_1573388941196_2105"><a href="#" class="fheader" role="button" aria-controls="id_timing" aria-expanded="true" id="yui_3_17_2_1_1573388941196_235">Test</a></legend>
		<div class="fcontainer clearfix" id="yui_3_17_2_1_1573384683206_708">
		<div id="fitem_id_name" class="form-group row  fitem   ">
    <div class="col-md-3">
        <span class="float-sm-right text-nowrap">
            
        </span>
        <label class="col-form-label d-inline " for="id_name">
         Choose a Test
        </label>
    </div>
    <div class="col-md-9 form-inline felement" data-fieldtype="text">
       <select class="custom-select
                   
                   " name="code" id="id_timeopen_day">
           <%
               Course c = (Course) session.getAttribute("course");
               System.out.println(c);
               ArrayList<Test1> lt = Test1.getTestBySub(c.getSubCode());
               session.setAttribute("listTest", lt);
               
           %>
           <c:forEach items="${listTest}" var="p">
               <option value ="${p.tCode}">${p.tName}</option>
           </c:forEach>
               
           
    </select>
        <div class="form-control-feedback invalid-feedback" id="id_error_name">
            
        </div>
    </div>
</div>
                    <div class="form-group row  fitem  ">
    <div class="col-md-3">
    </div>
   
</div>
		</div>
            
                     <legend class="ftoggler" id="yui_3_17_2_1_1573388941196_2105"><a href="#" class="fheader" role="button" aria-controls="id_timing" aria-expanded="true" id="yui_3_17_2_1_1573388941196_235">Enrollment</a></legend>
		<div class="fcontainer clearfix" id="yui_3_17_2_1_1573384683206_708">
		<div id="fitem_id_name" class="form-group row  fitem   ">
    <div class="col-md-3">
        <span class="float-sm-right text-nowrap">
            
        </span>
        <label class="col-form-label d-inline " for="id_name">
          Enroll
        </label>
    </div>
    <div class="col-md-9 form-inline felement" data-fieldtype="text">
        <input type="text" class="form-control " name="password" id="id_name" value="" size="64">
        <div class="form-control-feedback invalid-feedback" id="id_error_name">
            
        </div>
    </div>
</div>
                    <div class="form-group row  fitem  ">
    <div class="col-md-3">
    </div>
   
</div>
		</div>
            
      
        
      
             
   
<div id="fgroup_id_buttonar" class="form-group row  fitem femptylabel  " data-groupname="buttonar">
 
    <div class="col-md-9 form-inline felement" data-fieldtype="group">
            
            
             
            <div class="form-group  fitem  ">
    <label class="col-form-label " for="id_submitbutton">
         
        
        
    </label>
    <span data-fieldtype="submit">
        <input type="submit" class="btn
                        btn-primary
                        
                    
                    " name="submitbutton" id="id_submitbutton" value="Save and display">
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_submitbutton">
        
    </div>
</div>
             
            <div class="form-group  fitem   btn-cancel">
    <label class="col-form-label " for="id_cancel">
         
        
        
    </label>
    <span data-fieldtype="submit">
        <input type="submit" class="btn
                        
                        btn-secondary
                    
                    " name="cancel" id="id_cancel" value="Cancel" data-skip-validation="1" data-cancel="1" onclick="skipClientValidation = true; return true;">
    </span>
    <div class="form-control-feedback invalid-feedback" id="id_error_cancel">
        
    </div>
</div>
        <div class="form-control-feedback invalid-feedback" id="fgroup_id_buttonar">
            
        </div>
    </div>
</div>
                 
               
          
    </form>	
</fieldset>
<script>var skipClientValidation = false;</script>
                        </section>
                    </div>
                </div>
            </div>

            <div id="nav-drawer" data-region="drawer" class="d-print-none moodle-has-zindex " aria-hidden="false" tabindex="-1">
                <nav class="list-group" aria-label="PRJ321">
                    <a class="list-group-item list-group-item-action active" href="viewcourseLec.jsp" data-key="coursehome" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="60" data-nodetype="0" data-collapse="0" data-forceopen="1" data-isactive="1" data-hidden="0" data-preceedwithhr="0">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body font-weight-bold">${course.subCode}</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="#" data-key="participants" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="90" data-nodetype="1" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="7">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body ">Participants</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="#" data-key="badgesview" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="70" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="7">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body ">Badges</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="#" data-key="competencies" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="70" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="7">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body ">Competencies</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="#" data-key="grades" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="70" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="7">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body ">Grades</span>
                            </div>
                        </div>
                    </a>
                    <a class="list-group-item list-group-item-action " href="#" data-key="28" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="30" data-nodetype="1" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="7">
                        <div class="ml-0">
                            <div class="media">
                                <span class="media-body ">General</span>
                            </div>
                        </div>
                    </a>
                    <c:forEach items="${listchap}" var="c">
                        <a class="list-group-item list-group-item-action " href="#" data-key="29" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="30" data-nodetype="1" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="7">
                            <div class="ml-0">
                                <div class="media">
                                    <span class="media-body ">${c.chapName}</span>
                                </div>
                            </div>
                        </a>
                    </c:forEach>
                </nav>
            </div>
            <footer id="page-footer" class="py-3 bg-dark text-light">
                <div class="container">
                    <div id="course-footer"></div>
                    <div class="logininfo">You are logged in as <a href="profile.jsp" title="View profile">${USER.name}</a> (<a href="logout">Log out</a>)</div>
                    <div class="tool_usertours-resettourcontainer"></div>
                    <div class="sitelink"><a title="Moodle" href="http://moodle.org/"><img src="https://cacbank.moodlecloud.com/theme/image.php/boost/core/1569995285/moodlelogo_grayhat" alt="Moodle logo"></a></div>
                    <nav class="nav navbar-nav d-md-none" aria-label="Custom menu">

                    </nav>
                    <div class="tool_dataprivacy"><a href="#">Data retention summary</a></div><a href="#">Get the mobile app</a><div class="policiesfooter">
                        <a href="#">Policies</a></div>
                    
  
            <script type="text/javascript">
//<![CDATA[
var require = {
    baseUrl : 'https://cacbank.moodlecloud.com/lib/requirejs.php/1569995285/',
    // We only support AMD modules with an explicit define() statement.
    enforceDefine: true,
    skipDataMain: true,
    waitSeconds : 0,

    paths: {
        jquery: 'https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/jquery/jquery-3.2.1.min',
        jqueryui: 'https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/jquery/ui-1.12.1/jquery-ui.min',
        jqueryprivate: 'https://cacbank.moodlecloud.com/lib/javascript.php/1569995285/lib/requirejs/jquery-private'
    },

    // Custom jquery config map.
    map: {
      // '*' means all modules will get 'jqueryprivate'
      // for their 'jquery' dependency.
      '*': { jquery: 'jqueryprivate' },
      // Stub module for 'process'. This is a workaround for a bug in MathJax (see MDL-60458).
      '*': { process: 'core/first' },

      // 'jquery-private' wants the real jQuery module
      // though. If this line was not here, there would
      // be an unresolvable cyclic dependency.
      jqueryprivate: { jquery: 'jquery' }
    }
};

//]]>
</script>

<script type="text/javascript" src="https://cacbank.moodlecloud.com/theme/javascript.php/boost/1569995285/footer"></script>
<script type="text/javascript">
//<![CDATA[
M.str = {"moodle":{"lastmodified":"Last modified","name":"Name","error":"Error","info":"Information","yes":"Yes","no":"No","none":"None","cancel":"Cancel","delete":"Delete","choosedots":"Choose...","movecontent":"Move {$a}","tocontent":"To item \"{$a}\"","emptydragdropregion":"empty region","changesmadereallygoaway":"You have made changes. Are you sure you want to navigate away and lose your changes?","collapseall":"Collapse all","expandall":"Expand all","warning":"Warning","confirm":"Confirm","areyousure":"Are you sure?","closebuttontitle":"Close","unknownerror":"Unknown error"},"repository":{"type":"Type","size":"Size","invalidjson":"Invalid JSON string","nofilesattached":"No files attached","filepicker":"File picker","logout":"Logout","nofilesavailable":"No files available","norepositoriesavailable":"Sorry, none of your current repositories can return files in the required format.","fileexistsdialogheader":"File exists","fileexistsdialog_editor":"A file with that name has already been attached to the text you are editing.","fileexistsdialog_filemanager":"A file with that name has already been attached","renameto":"Rename to \"{$a}\"","referencesexist":"There are {$a} alias\/shortcut files that use this file as their source","select":"Select"},"admin":{"confirmdeletecomments":"You are about to delete comments, are you sure?","confirmation":"Confirmation"},"availability_completion":{"option_complete":"must be marked complete","option_fail":"must be complete with fail grade","option_incomplete":"must not be marked complete","option_pass":"must be complete with pass grade","label_cm":"Activity or resource","label_completion":"Required completion status","title":"Activity completion","description":"Require students to complete (or not complete) another activity."},"availability_date":{"ajaxerror":"Error contacting server to convert times","direction_before":"Date","direction_from":"from","direction_until":"until","direction_label":"Direction","title":"Date","description":"Prevent access until (or from) a specified date and time."},"availability_grade":{"option_min":"must be &#x2265;","option_max":"must be <","label_min":"Minimum grade percentage (inclusive)","label_max":"Maximum grade percentage (exclusive)","title":"Grade","description":"Require students to achieve a specified grade."},"availability_group":{"anygroup":"(Any group)","title":"Group","description":"Allow only students who belong to a specified group, or all groups."},"availability_grouping":{"title":"Grouping","description":"Allow only students who belong to a group within a specified grouping."},"availability_profile":{"op_contains":"contains","op_doesnotcontain":"doesn't contain","op_endswith":"ends with","op_isempty":"is empty","op_isequalto":"is equal to","op_isnotempty":"is not empty","op_startswith":"starts with","conditiontitle":"User profile field","label_operator":"Method of comparison","label_value":"Value to compare against","title":"User profile","description":"Control access based on fields within the student's profile."},"availability":{"addrestriction":"Add restriction...","invalid":"Please set","listheader_sign_before":"Student","listheader_sign_pos":"must","listheader_sign_neg":"must not","listheader_single":"match the following","listheader_multi_after":"of the following","listheader_multi_before":"match","listheader_multi_or":"any","listheader_multi_and":"all","unknowncondition":"Unknown condition (deleted condition plugin)","hide_verb":"Click to hide","hidden_individual":"Hidden entirely if user does not meet this condition","show_verb":"Click to display greyed-out","shown_individual":"Displayed greyed-out if user does not meet this condition","hidden_all":"Hidden entirely if user does not meet conditions","shown_all":"Displayed greyed-out if user does not meet conditions","condition_group":"Restriction set","condition_group_info":"Add a set of nested restrictions to apply complex logic.","and":"and","or":"or","label_multi":"Required restrictions","label_sign":"Restriction type","setheading":"{$a->number} Set of {$a->count} restriction(s)","itemheading":"{$a->number} {$a->type} restriction","missingplugin":"Missing restriction plugin"},"atto_collapse":{"showmore":"Show more buttons","showfewer":"Show fewer buttons","pluginname":"Show\/hide advanced buttons"},"atto_title":{"h3":"Heading (large)","h4":"Heading (medium)","h5":"Heading (small)","pre":"Pre-formatted","p":"Paragraph","pluginname":"Paragraph styles"},"atto_bold":{"pluginname":"Bold"},"atto_italic":{"pluginname":"Italic"},"atto_unorderedlist":{"pluginname":"Unordered list"},"atto_orderedlist":{"pluginname":"Ordered list"},"atto_link":{"createlink":"Create link","unlink":"Unlink","enterurl":"Enter a URL","browserepositories":"Browse repositories...","openinnewwindow":"Open in new window","pluginname":"Link"},"atto_image":{"alignment":"Alignment","alignment_bottom":"Bottom","alignment_left":"Left","alignment_middle":"Middle","alignment_right":"Right","alignment_top":"Top","browserepositories":"Browse repositories...","constrain":"Auto size","saveimage":"Save image","imageproperties":"Image properties","customstyle":"Custom style","enterurl":"Enter URL","enteralt":"Describe this image for someone who cannot see it","height":"Height","presentation":"Description not necessary","presentationoraltrequired":"Images must have a description, except if the description is marked as not necessary.","size":"Size","width":"Width","uploading":"Uploading, please wait...","pluginname":"Insert or edit image"},"atto_media":{"add":"Add","addcaptionstrack":"Add caption track","addchapterstrack":"Add chapter track","adddescriptionstrack":"Add description track","addmetadatatrack":"Add metadata track","addsource":"Add alternative source","addsubtitlestrack":"Add subtitle track","addtrack":"Add track","advancedsettings":"Advanced settings","audio":"Audio","audiosourcelabel":"Audio source URL","autoplay":"Play automatically","browserepositories":"Browse repositories...","captions":"Captions","captionssourcelabel":"Caption track URL","chapters":"Chapters","chapterssourcelabel":"Chapter track URL","controls":"Show controls","createmedia":"Insert media","default":"Default","descriptions":"Descriptions","descriptionssourcelabel":"Description track URL","displayoptions":"Display options","entername":"Enter name","entertitle":"Enter title","entersource":"Source URL","enterurl":"Enter URL","height":"Height","kind":"Type","label":"Label","languagesavailable":"Languages available","languagesinstalled":"Languages installed","link":"Link","loop":"Loop","metadata":"Metadata","metadatasourcelabel":"Metadata track URL","mute":"Muted","poster":"Thumbnail URL","remove":"Remove","size":"Size","srclang":"Language","subtitles":"Subtitles","subtitlessourcelabel":"Subtitle track URL","track":"Track URL","tracks":"Subtitles and captions","video":"Video","videoheight":"Video height","videosourcelabel":"Video source URL","videowidth":"Video width","width":"Width","pluginname":"Insert or edit an audio\/video file"},"atto_recordrtc":{"audiortc":"Record audio","videortc":"Record video","nowebrtc_title":"WebRTC not supported","nowebrtc":"Your browser offers limited or no support for WebRTC technologies yet, and cannot be used with this plugin. Please switch or upgrade your browser","gumabort_title":"Something happened","gumabort":"Something strange happened which prevented the webcam\/microphone from being used","gumnotallowed_title":"Wrong permissions","gumnotallowed":"The user must allow the browser access to the webcam\/microphone","gumnotfound_title":"Device missing","gumnotfound":"There is no input device connected or enabled","gumnotreadable_title":"Hardware error","gumnotreadable":"Something is preventing the browser from accessing the webcam\/microphone","gumnotsupported":"Your browser does not support recording over an insecure connection and must close the plugin","gumnotsupported_title":"No support for insecure connection","gumoverconstrained_title":"Problem with constraints","gumoverconstrained":"The current webcam\/microphone can not produce a stream with the required constraints","gumsecurity_title":"No support for insecure connection","gumsecurity":"Your browser does not support recording over an insecure connection and must close the plugin","gumtype_title":"No constraints specified","gumtype":"Tried to get stream from the webcam\/microphone, but no constraints were specified","insecurealert_title":"Insecure connection!","insecurealert":"Your browser might not allow this plugin to work unless it is used either over HTTPS or from localhost","startrecording":"Start recording","recordagain":"Record again","stoprecording":"Stop recording","recordingfailed":"Recording failed, try again","attachrecording":"Attach recording","norecordingfound_title":"No recording found","norecordingfound":"Something appears to have gone wrong, it seems nothing has been recorded","nearingmaxsize_title":"Recording stopped","nearingmaxsize":"You have attained the maximum size limit for file uploads","uploadprogress":"completed","uploadfailed":"Upload failed:","uploadfailed404":"Upload failed: file too large","uploadaborted":"Upload aborted:","pluginname":"RecordRTC"},"atto_managefiles":{"managefiles":"Manage files","pluginname":"Manage files"},"atto_underline":{"pluginname":"Underline"},"atto_strike":{"pluginname":"Strike through"},"atto_subscript":{"pluginname":"Subscript"},"atto_superscript":{"pluginname":"Superscript"},"atto_align":{"center":"Center","leftalign":"Left align","rightalign":"Right align","pluginname":"Text align"},"atto_indent":{"indent":"Indent","outdent":"Outdent","pluginname":"Indent"},"atto_equation":{"saveequation":"Save equation","editequation":"Edit equation using <a href=\"{$a}\" target=\"_blank\">TeX<\/a>","preview":"Equation preview","cursorinfo":"An arrow indicates the position that new elements from the element library will be inserted.","update":"Update","librarygroup1":"Operators","librarygroup2":"Arrows","librarygroup3":"Greek symbols","librarygroup4":"Advanced","pluginname":"Equation editor"},"atto_charmap":{"amacron":"a - macron","emacron":"e - macron","imacron":"i - macron","omacron":"o - macron","umacron":"u - macron","amacron_caps":"A - macron","emacron_caps":"E - macron","imacron_caps":"I - macron","omacron_caps":"O - macron","umacron_caps":"U - macron","interrobang":"interrobang","insertcharacter":"Insert character","nobreakspace":"no-break space","ampersand":"ampersand","quotationmark":"quotation mark","centsign":"cent sign","eurosign":"euro sign","poundsign":"pound sign","yensign":"yen sign","copyrightsign":"copyright sign","registeredsign":"registered sign","trademarksign":"trade mark sign","permillesign":"per mille sign","microsign":"micro sign","middledot":"middle dot","bullet":"bullet","threedotleader":"three dot leader","minutesfeet":"minutes \/ feet","secondsinches":"seconds \/ inches","sectionsign":"section sign","paragraphsign":"paragraph sign","sharpsesszed":"sharp s \/ ess-zed","singleleftpointinganglequotationmark":"single left-pointing angle quotation mark","singlerightpointinganglequotationmark":"single right-pointing angle quotation mark","leftpointingguillemet":"left pointing guillemet","rightpointingguillemet":"right pointing guillemet","leftsinglequotationmark":"left single quotation mark","rightsinglequotationmark":"right single quotation mark","leftdoublequotationmark":"left double quotation mark","rightdoublequotationmark":"right double quotation mark","singlelow9quotationmark":"single low-9 quotation mark","doublelow9quotationmark":"double low-9 quotation mark","lessthansign":"less-than sign","greaterthansign":"greater-than sign","lessthanorequalto":"less-than or equal to","greaterthanorequalto":"greater-than or equal to","endash":"en dash","emdash":"em dash","macron":"macron","overline":"overline","currencysign":"currency sign","brokenbar":"broken bar","diaeresis":"diaeresis","invertedexclamationmark":"inverted exclamation mark","turnedquestionmark":"turned question mark","circumflexaccent":"circumflex accent","smalltilde":"small tilde","degreesign":"degree sign","minussign":"minus sign","plusminussign":"plus-minus sign","divisionsign":"division sign","fractionslash":"fraction slash","multiplicationsign":"multiplication sign","superscriptone":"superscript one","superscripttwo":"superscript two","superscriptthree":"superscript three","fractiononequarter":"fraction one quarter","fractiononehalf":"fraction one half","fractionthreequarters":"fraction three quarters","functionflorin":"function \/ florin","integral":"integral","narysumation":"n-ary sumation","infinity":"infinity","squareroot":"square root","almostequalto":"almost equal to","notequalto":"not equal to","identicalto":"identical to","naryproduct":"n-ary product","notsign":"not sign","intersection":"intersection","partialdifferential":"partial differential","acuteaccent":"acute accent","cedilla":"cedilla","feminineordinalindicator":"feminine ordinal indicator","masculineordinalindicator":"masculine ordinal indicator","dagger":"dagger","doubledagger":"double dagger","agrave_caps":"A - grave","aacute_caps":"A - acute","acircumflex_caps":"A - circumflex","atilde_caps":"A - tilde","adiaeresis_caps":"A - diaeresis","aringabove_caps":"A - ring above","ligatureae_caps":"ligature AE","ccedilla_caps":"C - cedilla","egrave_caps":"E - grave","eacute_caps":"E - acute","ecircumflex_caps":"E - circumflex","ediaeresis_caps":"E - diaeresis","igrave_caps":"I - grave","iacute_caps":"I - acute","icircumflex_caps":"I - circumflex","idiaeresis_caps":"I - diaeresis","eth_caps":"ETH","ntilde_caps":"N - tilde","ograve_caps":"O - grave","oacute_caps":"O - acute","ocircumflex_caps":"O - circumflex","otilde_caps":"O - tilde","odiaeresis_caps":"O - diaeresis","oslash_caps":"O - slash","ligatureoe_caps":"ligature OE","scaron_caps":"S - caron","ugrave_caps":"U - grave","uacute_caps":"U - acute","ucircumflex_caps":"U - circumflex","udiaeresis_caps":"U - diaeresis","yacute_caps":"Y - acute","ydiaeresis_caps":"Y - diaeresis","thorn_caps":"THORN","agrave":"a - grave","aacute":"a - acute","acircumflex":"a - circumflex","atilde":"a - tilde","adiaeresis":"a - diaeresis","aringabove":"a - ring above","ligatureae":"ligature ae","ccedilla":"c - cedilla","egrave":"e - grave","eacute":"e - acute","ecircumflex":"e - circumflex","ediaeresis":"e - diaeresis","igrave":"i - grave","iacute":"i - acute","icircumflex":"i - circumflex","idiaeresis":"i - diaeresis","eth":"eth","ntilde":"n - tilde","ograve":"o - grave","oacute":"o - acute","ocircumflex":"o - circumflex","otilde":"o - tilde","odiaeresis":"o - diaeresis","oslash":"o slash","ligatureoe":"ligature oe","scaron":"s - caron","ugrave":"u - grave","uacute":"u - acute","ucircumflex":"u - circumflex","udiaeresis":"u - diaeresis","yacute":"y - acute","thorn":"thorn","ydiaeresis":"y - diaeresis","alpha_caps":"Alpha","beta_caps":"Beta","gamma_caps":"Gamma","delta_caps":"Delta","epsilon_caps":"Epsilon","zeta_caps":"Zeta","eta_caps":"Eta","theta_caps":"Theta","iota_caps":"Iota","kappa_caps":"Kappa","lambda_caps":"Lambda","mu_caps":"Mu","nu_caps":"Nu","xi_caps":"Xi","omicron_caps":"Omicron","pi_caps":"Pi","rho_caps":"Rho","sigma_caps":"Sigma","tau_caps":"Tau","upsilon_caps":"Upsilon","phi_caps":"Phi","chi_caps":"Chi","psi_caps":"Psi","omega_caps":"Omega","alpha":"alpha","beta":"beta","gamma":"gamma","delta":"delta","epsilon":"epsilon","zeta":"zeta","eta":"eta","theta":"theta","iota":"iota","kappa":"kappa","lambda":"lambda","mu":"mu","nu":"nu","xi":"xi","omicron":"omicron","pi":"pi","rho":"rho","finalsigma":"final sigma","sigma":"sigma","tau":"tau","upsilon":"upsilon","phi":"phi","chi":"chi","psi":"psi","omega":"omega","leftwardsarrow":"leftwards arrow","upwardsarrow":"upwards arrow","rightwardsarrow":"rightwards arrow","downwardsarrow":"downwards arrow","leftrightarrow":"left right arrow","lozenge":"lozenge","blackspadesuit":"black spade suit","blackclubsuit":"black club suit","blackheartsuit":"black heart suit","blackdiamondsuit":"black diamond suit","pluginname":"Insert character"},"atto_table":{"createtable":"Create table","updatetable":"Update table","appearance":"Appearance","headers":"Define headers on","caption":"Caption","columns":"Columns","rows":"Rows","numberofcolumns":"Number of columns","numberofrows":"Number of rows","both":"Both","edittable":"Edit table","addcolumnafter":"Insert column after","addrowafter":"Insert row after","movecolumnright":"Move column right","movecolumnleft":"Move column left","moverowdown":"Move row down","moverowup":"Move row up","deleterow":"Delete row","deletecolumn":"Delete column","captionposition":"Caption position","borders":"Borders","bordersize":"Size of borders","bordercolour":"Border colour","borderstyles":"Style of borders","none":"None","all":"Around each cell","backgroundcolour":"Background colour","width":"Table width (in %)","outer":"Around table","noborder":"No border","themedefault":"Theme default","dotted":"Dotted","dashed":"Dashed","solid":"Solid","pluginname":"Table"},"editor":{"top":"Top","bottom":"Bottom"},"atto_clear":{"pluginname":"Clear formatting"},"atto_undo":{"redo":"Redo","undo":"Undo","pluginname":"Undo\/Redo"},"atto_accessibilitychecker":{"nowarnings":"Congratulations, no accessibility problems found!","report":"Accessibility report:","imagesmissingalt":"Images require alternative text. To fix this warning, add an alt attribute to your img tags. An empty alt attribute may be used, but only when the image is purely decorative and carries no information.","needsmorecontrast":"The colours of the foreground and background text do not have enough contrast. To fix this warning, change either foreground or background colour of the text so that it is easier to read.","needsmoreheadings":"There is a lot of text with no headings. Headings will allow screen reader users to navigate through the page easily and will make the page more usable for everyone.","tableswithmergedcells":"Tables should not contain merged cells. Despite being standard markup for tables for many years, some screen readers still do not fully support complex tables. When possible, try to \"flatten\" the table and avoid merged cells.","tablesmissingcaption":"Tables should have captions. While it is not necessary for each table to have a caption, a caption is generally very helpful.","emptytext":"Empty text","entiredocument":"Entire document","tablesmissingheaders":"Tables should use row and\/or column headers.","pluginname":"Accessibility checker"},"atto_accessibilityhelper":{"liststyles":"Styles for current selection:","nostyles":"No styles","listlinks":"Links in text editor:","nolinks":"No links","selectlink":"Select link","listimages":"Images in text editor:","noimages":"No images","selectimage":"Select image","pluginname":"Screenreader helper"},"atto_html":{"pluginname":"HTML"},"editor_atto":{"editor_command_keycode":"Cmd + {$a}","editor_control_keycode":"Ctrl + {$a}","plugin_title_shortcut":"{$a->title} [{$a->shortcut}]","textrecovered":"A draft version of this text was automatically restored.","autosavefailed":"Could not connect to the server. If you submit this page now, your changes may be lost.","autosavesucceeded":"Draft saved.","errortextrecovery":"Unfortunately the draft version could not be restored."}};
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
(function() {Y.use("moodle-filter_mathjaxloader-loader",function() {M.filter_mathjaxloader.configure({"mathjaxconfig":"\nMathJax.Hub.Config({\n    config: [\"Accessible.js\", \"Safe.js\"],\n    errorSettings: { message: [\"!\"] },\n    skipStartupTypeset: true,\n    messageStyle: \"none\"\n});\n","lang":"en"});
});
Y.use("moodle-form-dateselector",function() {M.form.dateselector.init_date_selectors({"firstdayofweek":0,"mon":"Mon","tue":"Tue","wed":"Wed","thu":"Thu","fri":"Fri","sat":"Sat","sun":"Sun","january":"January","february":"February","march":"March","april":"April","may":"May","june":"June","july":"July","august":"August","september":"September","october":"October","november":"November","december":"December"});
});
Y.on('domready', function() { Y.use("moodle-core_availability-form","base","node","panel","moodle-core-notification-dialogue","json","moodle-availability_completion-form","moodle-availability_date-form","moodle-availability_grade-form","moodle-availability_group-form","moodle-availability_grouping-form","moodle-availability_profile-form",function() {M.core_availability.form.init({"completion":["availability_completion",true,[[{"id":17,"name":"Progress Test 1","completiongradeitemnumber":null}]]],"date":["availability_date",true,["<span class=\"availability-group\"><label><span class=\"accesshide\">day <\/span><select name=\"x[day]\" class=\"custom-select\"><option value=\"1\">1<\/option><option value=\"2\">2<\/option><option value=\"3\">3<\/option><option value=\"4\">4<\/option><option value=\"5\">5<\/option><option value=\"6\">6<\/option><option value=\"7\">7<\/option><option value=\"8\">8<\/option><option value=\"9\">9<\/option><option value=\"10\" selected=\"selected\">10<\/option><option value=\"11\">11<\/option><option value=\"12\">12<\/option><option value=\"13\">13<\/option><option value=\"14\">14<\/option><option value=\"15\">15<\/option><option value=\"16\">16<\/option><option value=\"17\">17<\/option><option value=\"18\">18<\/option><option value=\"19\">19<\/option><option value=\"20\">20<\/option><option value=\"21\">21<\/option><option value=\"22\">22<\/option><option value=\"23\">23<\/option><option value=\"24\">24<\/option><option value=\"25\">25<\/option><option value=\"26\">26<\/option><option value=\"27\">27<\/option><option value=\"28\">28<\/option><option value=\"29\">29<\/option><option value=\"30\">30<\/option><option value=\"31\">31<\/option><\/select><\/label> <label><span class=\"accesshide\">Month <\/span><select name=\"x[month]\" class=\"custom-select\"><option value=\"1\">January<\/option><option value=\"2\">February<\/option><option value=\"3\">March<\/option><option value=\"4\">April<\/option><option value=\"5\">May<\/option><option value=\"6\">June<\/option><option value=\"7\">July<\/option><option value=\"8\">August<\/option><option value=\"9\">September<\/option><option value=\"10\">October<\/option><option value=\"11\" selected=\"selected\">November<\/option><option value=\"12\">December<\/option><\/select><\/label> <label><span class=\"accesshide\">year <\/span><select name=\"x[year]\" class=\"custom-select\"><option value=\"1900\">1900<\/option><option value=\"1901\">1901<\/option><option value=\"1902\">1902<\/option><option value=\"1903\">1903<\/option><option value=\"1904\">1904<\/option><option value=\"1905\">1905<\/option><option value=\"1906\">1906<\/option><option value=\"1907\">1907<\/option><option value=\"1908\">1908<\/option><option value=\"1909\">1909<\/option><option value=\"1910\">1910<\/option><option value=\"1911\">1911<\/option><option value=\"1912\">1912<\/option><option value=\"1913\">1913<\/option><option value=\"1914\">1914<\/option><option value=\"1915\">1915<\/option><option value=\"1916\">1916<\/option><option value=\"1917\">1917<\/option><option value=\"1918\">1918<\/option><option value=\"1919\">1919<\/option><option value=\"1920\">1920<\/option><option value=\"1921\">1921<\/option><option value=\"1922\">1922<\/option><option value=\"1923\">1923<\/option><option value=\"1924\">1924<\/option><option value=\"1925\">1925<\/option><option value=\"1926\">1926<\/option><option value=\"1927\">1927<\/option><option value=\"1928\">1928<\/option><option value=\"1929\">1929<\/option><option value=\"1930\">1930<\/option><option value=\"1931\">1931<\/option><option value=\"1932\">1932<\/option><option value=\"1933\">1933<\/option><option value=\"1934\">1934<\/option><option value=\"1935\">1935<\/option><option value=\"1936\">1936<\/option><option value=\"1937\">1937<\/option><option value=\"1938\">1938<\/option><option value=\"1939\">1939<\/option><option value=\"1940\">1940<\/option><option value=\"1941\">1941<\/option><option value=\"1942\">1942<\/option><option value=\"1943\">1943<\/option><option value=\"1944\">1944<\/option><option value=\"1945\">1945<\/option><option value=\"1946\">1946<\/option><option value=\"1947\">1947<\/option><option value=\"1948\">1948<\/option><option value=\"1949\">1949<\/option><option value=\"1950\">1950<\/option><option value=\"1951\">1951<\/option><option value=\"1952\">1952<\/option><option value=\"1953\">1953<\/option><option value=\"1954\">1954<\/option><option value=\"1955\">1955<\/option><option value=\"1956\">1956<\/option><option value=\"1957\">1957<\/option><option value=\"1958\">1958<\/option><option value=\"1959\">1959<\/option><option value=\"1960\">1960<\/option><option value=\"1961\">1961<\/option><option value=\"1962\">1962<\/option><option value=\"1963\">1963<\/option><option value=\"1964\">1964<\/option><option value=\"1965\">1965<\/option><option value=\"1966\">1966<\/option><option value=\"1967\">1967<\/option><option value=\"1968\">1968<\/option><option value=\"1969\">1969<\/option><option value=\"1970\">1970<\/option><option value=\"1971\">1971<\/option><option value=\"1972\">1972<\/option><option value=\"1973\">1973<\/option><option value=\"1974\">1974<\/option><option value=\"1975\">1975<\/option><option value=\"1976\">1976<\/option><option value=\"1977\">1977<\/option><option value=\"1978\">1978<\/option><option value=\"1979\">1979<\/option><option value=\"1980\">1980<\/option><option value=\"1981\">1981<\/option><option value=\"1982\">1982<\/option><option value=\"1983\">1983<\/option><option value=\"1984\">1984<\/option><option value=\"1985\">1985<\/option><option value=\"1986\">1986<\/option><option value=\"1987\">1987<\/option><option value=\"1988\">1988<\/option><option value=\"1989\">1989<\/option><option value=\"1990\">1990<\/option><option value=\"1991\">1991<\/option><option value=\"1992\">1992<\/option><option value=\"1993\">1993<\/option><option value=\"1994\">1994<\/option><option value=\"1995\">1995<\/option><option value=\"1996\">1996<\/option><option value=\"1997\">1997<\/option><option value=\"1998\">1998<\/option><option value=\"1999\">1999<\/option><option value=\"2000\">2000<\/option><option value=\"2001\">2001<\/option><option value=\"2002\">2002<\/option><option value=\"2003\">2003<\/option><option value=\"2004\">2004<\/option><option value=\"2005\">2005<\/option><option value=\"2006\">2006<\/option><option value=\"2007\">2007<\/option><option value=\"2008\">2008<\/option><option value=\"2009\">2009<\/option><option value=\"2010\">2010<\/option><option value=\"2011\">2011<\/option><option value=\"2012\">2012<\/option><option value=\"2013\">2013<\/option><option value=\"2014\">2014<\/option><option value=\"2015\">2015<\/option><option value=\"2016\">2016<\/option><option value=\"2017\">2017<\/option><option value=\"2018\">2018<\/option><option value=\"2019\" selected=\"selected\">2019<\/option><option value=\"2020\">2020<\/option><option value=\"2021\">2021<\/option><option value=\"2022\">2022<\/option><option value=\"2023\">2023<\/option><option value=\"2024\">2024<\/option><option value=\"2025\">2025<\/option><option value=\"2026\">2026<\/option><option value=\"2027\">2027<\/option><option value=\"2028\">2028<\/option><option value=\"2029\">2029<\/option><option value=\"2030\">2030<\/option><option value=\"2031\">2031<\/option><option value=\"2032\">2032<\/option><option value=\"2033\">2033<\/option><option value=\"2034\">2034<\/option><option value=\"2035\">2035<\/option><option value=\"2036\">2036<\/option><option value=\"2037\">2037<\/option><option value=\"2038\">2038<\/option><option value=\"2039\">2039<\/option><option value=\"2040\">2040<\/option><option value=\"2041\">2041<\/option><option value=\"2042\">2042<\/option><option value=\"2043\">2043<\/option><option value=\"2044\">2044<\/option><option value=\"2045\">2045<\/option><option value=\"2046\">2046<\/option><option value=\"2047\">2047<\/option><option value=\"2048\">2048<\/option><option value=\"2049\">2049<\/option><option value=\"2050\">2050<\/option><\/select><\/label><\/span> <span class=\"availability-group\"><label><span class=\"accesshide\">hour <\/span><select name=\"x[hour]\" class=\"custom-select\"><option value=\"0\" selected=\"selected\">00<\/option><option value=\"1\">01<\/option><option value=\"2\">02<\/option><option value=\"3\">03<\/option><option value=\"4\">04<\/option><option value=\"5\">05<\/option><option value=\"6\">06<\/option><option value=\"7\">07<\/option><option value=\"8\">08<\/option><option value=\"9\">09<\/option><option value=\"10\">10<\/option><option value=\"11\">11<\/option><option value=\"12\">12<\/option><option value=\"13\">13<\/option><option value=\"14\">14<\/option><option value=\"15\">15<\/option><option value=\"16\">16<\/option><option value=\"17\">17<\/option><option value=\"18\">18<\/option><option value=\"19\">19<\/option><option value=\"20\">20<\/option><option value=\"21\">21<\/option><option value=\"22\">22<\/option><option value=\"23\">23<\/option><\/select><\/label> : <label><span class=\"accesshide\">minute <\/span><select name=\"x[minute]\" class=\"custom-select\"><option value=\"0\" selected=\"selected\">00<\/option><option value=\"5\">05<\/option><option value=\"10\">10<\/option><option value=\"15\">15<\/option><option value=\"20\">20<\/option><option value=\"25\">25<\/option><option value=\"30\">30<\/option><option value=\"35\">35<\/option><option value=\"40\">40<\/option><option value=\"45\">45<\/option><option value=\"50\">50<\/option><option value=\"55\">55<\/option><\/select><\/label><\/span>",1573318800]],"grade":["availability_grade",true,[[{"id":6,"name":"Course total"},{"id":10,"name":"Progress Test 1"}]]],"group":["availability_group",false,[[]]],"grouping":["availability_grouping",false,[[]]],"profile":["availability_profile",true,[[{"field":"address","display":"Address"},{"field":"aim","display":"AIM ID"},{"field":"city","display":"City\/town"},{"field":"country","display":"Country"},{"field":"department","display":"Department"},{"field":"email","display":"Email address"},{"field":"firstname","display":"First name"},{"field":"icq","display":"ICQ number"},{"field":"idnumber","display":"ID number"},{"field":"institution","display":"Institution"},{"field":"phone2","display":"Mobile phone"},{"field":"msn","display":"MSN ID"},{"field":"phone1","display":"Phone"},{"field":"skype","display":"Skype ID"},{"field":"lastname","display":"Surname"},{"field":"url","display":"Web page"},{"field":"yahoo","display":"Yahoo ID"}],[]]]});
}); });
Y.use("moodle-mod_quiz-modform",function() {M.mod_quiz.modform.init();
});
M.util.help_popups.setup(Y);
Y.on('domready', function() { Y.use("moodle-core-blocks",function() {M.core_blocks.init_dragdrop({"courseid":"7","pagetype":"mod-quiz-mod","pagelayout":"admin","subpage":"","regions":["side-pre"],"contextid":52});
}); });
Y.use("moodle-core-formchangechecker",function() {M.core_formchangechecker.init({"formid":"mform1_HhDcviUoF7VpRrw","initialdirtystate":false});
});
Y.use("moodle-form-shortforms",function() {M.form.shortforms({"formid":"mform1_HhDcviUoF7VpRrw"});
});
 M.util.js_pending('random5dc8028a67caa20'); Y.on('domready', function() {  M.util.js_pending('random5dc8028a67caa20'); Y.use('core_filepicker', function(Y) { M.core_filepicker.set_templates(Y, {"generallayout":"<div class=\"container\">\n    <div tabindex=\"0\" class=\"file-picker fp-generallayout row\" role=\"dialog\" aria-live=\"assertive\">\n        <div class=\"fp-repo-area col-md-3 pr-2 nav nav-pills flex-column\" role=\"tablist\">\n                <div class=\"fp-repo nav-item\" role=\"tab\" aria-selected=\"false\" tabindex=\"-1\">\n                    <a href=\"#\" class=\"nav-link\" tabindex=\"-1\"><img class=\"fp-repo-icon\" alt=\" \" width=\"16\" height=\"16\" \/>&nbsp;<span class=\"fp-repo-name\"><\/span><\/a>\n                <\/div>\n\n        <\/div>\n        <div class=\"col-md-9 p-0\">\n            <div class=\"fp-repo-items\" tabindex=\"0\">\n                <div class=\"fp-navbar bg-faded card mb-0 clearfix icon-no-spacing\">\n                    <div>\n                        <div class=\"fp-toolbar\">\n                            <div class=\"fp-tb-back\">\n                                <a href=\"#\" class=\"btn btn-secondary btn-sm\">Back<\/a>\n                            <\/div>\n                            <div class=\"fp-tb-search\">\n                                <form><\/form>\n                            <\/div>\n                            <div class=\"fp-tb-refresh\">\n                                <a title=\"Refresh\" class=\"btn btn-secondary btn-sm\" href=\"#\">\n                                    <i class=\"icon fa fa-refresh fa-fw \" aria-hidden=\"true\"  ><\/i>\n                                <\/a>\n                            <\/div>\n                            <div class=\"fp-tb-logout\">\n                                <a title=\"Logout\" class=\"btn btn-secondary btn-sm\" href=\"#\">\n                                    <i class=\"icon fa fa-sign-out fa-fw \" aria-hidden=\"true\"  ><\/i>\n                                <\/a>\n                            <\/div>\n                            <div class=\"fp-tb-manage\">\n                                <a title=\"Manage\" class=\"btn btn-secondary btn-sm\" href=\"#\">\n                                    <i class=\"icon fa fa-cog fa-fw \" aria-hidden=\"true\"  ><\/i>\n                                <\/a>\n                            <\/div>\n                            <div class=\"fp-tb-help\">\n                                <a title=\"Help\" class=\"btn btn-secondary btn-sm\" href=\"#\">\n                                    <i class=\"icon fa fa-question-circle text-info fa-fw \" aria-hidden=\"true\"  ><\/i>\n                                <\/a>\n                            <\/div>\n                            <div class=\"fp-tb-message\"><\/div>\n                        <\/div>\n                        <div class=\"fp-viewbar btn-group float-sm-right\">\n                            <a role=\"button\" title=\"Display folder with file icons\" class=\"fp-vb-icons btn btn-secondary btn-sm\" href=\"#\">\n                                <i class=\"icon fa fa-th fa-fw \" aria-hidden=\"true\"  ><\/i>\n                            <\/a>\n                            <a role=\"button\" title=\"Display folder with file details\" class=\"fp-vb-details btn btn-secondary btn-sm\" href=\"#\">\n                                <i class=\"icon fa fa-list fa-fw \" aria-hidden=\"true\"  ><\/i>\n                            <\/a>\n                            <a role=\"button\" title=\"Display folder as file tree\" class=\"fp-vb-tree btn btn-secondary btn-sm\" href=\"#\">\n                                <i class=\"icon fa fa-folder fa-fw \" aria-hidden=\"true\"  ><\/i>\n                            <\/a>\n                        <\/div>\n                        <div class=\"fp-clear-left\"><\/div>\n                    <\/div>\n                    <div class=\"fp-pathbar\">\n                        <span class=\"fp-path-folder\"><a class=\"fp-path-folder-name\" href=\"#\"><\/a><\/span>\n                    <\/div>\n                <\/div>\n                <div class=\"fp-content card\"><\/div>\n            <\/div>\n        <\/div>\n    <\/div>\n<\/div>","iconfilename":"\n<a class=\"fp-file\" href=\"#\" >\n    <div style=\"position:relative;\">\n        <div class=\"fp-thumbnail\"><\/div>\n        <div class=\"fp-reficons1\"><\/div>\n        <div class=\"fp-reficons2\"><\/div>\n    <\/div>\n    <div class=\"fp-filename-field\">\n        <p class=\"fp-filename text-truncate\"><\/p>\n    <\/div>\n<\/a>","listfilename":"\n<span class=\"fp-filename-icon\">\n    <a href=\"#\">\n        <span class=\"fp-icon\"><\/span>\n        <span class=\"fp-filename\"><\/span>\n    <\/a>\n<\/span>","nextpage":"\n<div class=\"fp-nextpage\">\n    <div class=\"fp-nextpage-link\"><a href=\"#\">more<\/a><\/div>\n    <div class=\"fp-nextpage-loading\">\n        <i class=\"icon fa fa-circle-o-notch fa-spin fa-fw \" aria-hidden=\"true\"  ><\/i>\n    <\/div>\n<\/div>","selectlayout":"<div class=\"file-picker fp-select\">\n    <div class=\"fp-select-loading\">\n        <span class=\"sr-only\">Loading...<\/span>\n        <i class=\"icon fa fa-circle-o-notch fa-spin fa-fw \" aria-hidden=\"true\"  ><\/i>\n    <\/div>\n    <div class=\"container\">\n        <form>\n            <fieldset class=\"form-group row\">\n                <div class=\"form-check fp-linktype-2\">\n                    <label class=\"form-check-label\">\n                        <input class=\"form-check-input\" type=\"radio\">\n                        Make a copy of the file\n                    <\/label>\n                <\/div>\n                <div class=\"form-check fp-linktype-1\">\n                    <label class=\"form-check-label\">\n                        <input class=\"form-check-input\" type=\"radio\">\n                        Link to the file directly\n                    <\/label>\n                <\/div>\n                <div class=\"form-check fp-linktype-4\">\n                    <label class=\"form-check-label\">\n                        <input class=\"form-check-input\" type=\"radio\">\n                        Create an alias\/shortcut to the file\n                    <\/label>\n                <\/div>\n                <div class=\"form-check fp-linktype-8\">\n                    <label class=\"form-check-label\">\n                        <input class=\"form-check-input\" type=\"radio\">\n                        Create an access controlled link to the file\n                    <\/label>\n                <\/div>\n            <\/fieldset>\n            <div class=\"fp-saveas form-group row\">\n                <label class=\"col-form-label\">Save as<\/label>\n                <input class=\"form-control\" type=\"text\">\n            <\/div>\n            <div class=\"fp-setauthor form-group row\">\n                <label class=\"col-form-label\">Author<\/label>\n                <input class=\"form-control\" type=\"text\">\n            <\/div>\n            <div class=\"fp-setlicense form-group row\">\n                <label class=\"col-form-label\">Choose license<\/label>\n                <select class=\"custom-select\"><\/select>\n            <\/div>\n            <div class=\"form-group row\">\n                <div class=\"fp-select-buttons\">\n                    <button class=\"fp-select-confirm btn-primary btn\">Select this file<\/button>\n                    <button class=\"fp-select-cancel btn-secondary btn\">Cancel<\/button>\n                <\/div>\n            <\/div>\n        <\/form>\n    <\/div>\n    <div class=\"fp-info clearfix\">\n        <hr>\n        <p class=\"fp-thumbnail\"><\/p>\n        <div class=\"fp-fileinfo\">\n            <div class=\"fp-datemodified\">Last modified<span class=\"fp-value\"><\/span><\/div>\n            <div class=\"fp-datecreated\">Created<span class=\"fp-value\"><\/span><\/div>\n            <div class=\"fp-size\">Size<span class=\"fp-value\"><\/span><\/div>\n            <div class=\"fp-license\">License<span class=\"fp-value\"><\/span><\/div>\n            <div class=\"fp-author\">Author<span class=\"fp-value\"><\/span><\/div>\n            <div class=\"fp-dimensions\">Dimensions<span class=\"fp-value\" dir=\"ltr\"><\/span><\/div>\n        <\/div>\n    <\/div>\n<\/div>","uploadform":"<div class=\"fp-upload-form\">\n    <div class=\"fp-content-center\">\n        <form enctype=\"multipart\/form-data\" method=\"POST\" class=\"form\">\n            <div class=\"fp-formset\">\n                <div class=\"fp-file form-group\">\n                    <label>Attachment<\/label>\n                    <div class=\"p-x-1\">\n                        <input  type=\"file\"\/>\n                    <\/div>\n                <\/div>\n                <div class=\"fp-saveas form-group\">\n                    <label>Save as<\/label>\n                    <input type=\"text\" class=\"form-control\"\/>\n                <\/div>\n                <div class=\"fp-setauthor form-group\">\n                    <label>Author<\/label>\n                    <input type=\"text\" class=\"form-control\"\/>\n                <\/div>\n                <div class=\"fp-setlicense control-group\">\n                    <label>Choose license<\/label>\n                    <select class=\"custom-select\"><\/select>\n                <\/div>\n            <\/div>\n        <\/form>\n        <div class=\"mdl-align\">\n            <button class=\"fp-upload-btn btn-primary btn\">Upload this file<\/button>\n        <\/div>\n    <\/div>\n<\/div>","loading":"\n<div class=\"fp-content-loading\">\n    <div class=\"fp-content-center\">\n        <i class=\"icon fa fa-circle-o-notch fa-spin fa-fw \" aria-hidden=\"true\"  ><\/i>\n    <\/div>\n<\/div>","error":"\n<div class=\"fp-content-error\" ><div class=\"fp-error\"><\/div><\/div>","message":"\n<div class=\"file-picker fp-msg\" role=\"alertdialog\" aria-live=\"assertive\" aria-labelledby=\"fp-msg-labelledby\">\n    <p class=\"fp-msg-text\" id=\"fp-msg-labelledby\"><\/p>\n    <button class=\"fp-msg-butok btn-primary btn\">OK<\/button>\n<\/div>","processexistingfile":"<div class=\"file-picker fp-dlg\">\n    <p class=\"fp-dlg-text\"><\/p>\n    <div class=\"fp-dlg-buttons\">\n        <button class=\"fp-dlg-butoverwrite btn btn-primary\">Overwrite<\/button>\n        <button class=\"fp-dlg-butrename btn btn-primary\"><\/button>\n        <button class=\"fp-dlg-butcancel btn btn-secondary\">Cancel<\/button>\n    <\/div>\n<\/div>","processexistingfilemultiple":"<div class=\"file-picker fp-dlg\">\n    <p class=\"fp-dlg-text\"><\/p>\n    <a class=\"fp-dlg-butoverwrite btn btn-primary\" href=\"#\">Overwrite<\/a>\n    <a class=\"fp-dlg-butrename btn btn-primary\" href=\"#\"><\/a>\n    <a class=\"fp-dlg-butcancel btn btn-secondary\" href=\"#\">Cancel<\/a>\n    <br\/>\n    <a class=\"fp-dlg-butoverwriteall btn btn-primary\" href=\"#\">Overwrite all<\/a>\n    <a class=\"fp-dlg-butrenameall btn btn-primary\" href=\"#\">Rename all<\/a>\n<\/div>","loginform":"<div class=\"fp-login-form\">\n    <div class=\"fp-content-center\">\n        <form class=\"form\">\n            <div class=\"fp-formset\">\n                <div class=\"fp-login-popup form-group\">\n                    <div class=\"fp-popup\">\n                        <p class=\"mdl-align\">\n                            <button class=\"fp-login-popup-but btn-primary btn\">Log in to your account<\/button>\n                        <\/p>\n                    <\/div>\n                <\/div>\n                <div class=\"fp-login-textarea form-group\">\n                    <textarea class=\"form-control\"><\/textarea>\n                <\/div>\n                <div class=\"fp-login-select form-group\">\n                    <label class=\"form-control-label\"><\/label>\n                    <select class=\"custom-select\"><\/select>\n                <\/div>\n                <div class=\"fp-login-input form-group\">\n                    <label class=\"form-control-label\"><\/label>\n                    <input class=\"form-control\"\/>\n                <\/div>\n                <div class=\"fp-login-radiogroup form-group\">\n                    <label class=\"form-control-label\"><\/label>\n                    <div class=\"fp-login-radio\"><input class=\"form-control\" \/> <label><\/label><\/div>\n                <\/div>\n                <div class=\"fp-login-checkbox form-group form-inline\">\n                    <label class=\"form-control-label\"><\/label>\n                    <input class=\"form-control\"\/>\n                <\/div>\n            <\/div>\n            <p class=\"mdl-align\"><button class=\"fp-login-submit btn-primary btn\">Submit<\/button><\/p>\n        <\/form>\n    <\/div>\n<\/div>"});  M.util.js_complete('random5dc8028a67caa20'); });  M.util.js_complete('random5dc8028a67caa20'); });
Y.use("moodle-filter_mathjaxloader-loader",function() {M.filter_mathjaxloader.typeset();
});
Y.use("moodle-editor_atto-editor","moodle-atto_collapse-button","moodle-atto_title-button","moodle-atto_bold-button","moodle-atto_italic-button","moodle-atto_unorderedlist-button","moodle-atto_orderedlist-button","moodle-atto_link-button","moodle-atto_image-button","moodle-atto_media-button","moodle-atto_recordrtc-button","moodle-atto_managefiles-button","moodle-atto_underline-button","moodle-atto_strike-button","moodle-atto_subscript-button","moodle-atto_superscript-button","moodle-atto_align-button","moodle-atto_indent-button","moodle-atto_equation-button","moodle-atto_charmap-button","moodle-atto_table-button","moodle-atto_clear-button","moodle-atto_undo-button","moodle-atto_accessibilitychecker-button","moodle-atto_accessibilityhelper-button","moodle-atto_html-button",function() {Y.M.editor_atto.Editor.init({"elementid":"id_introeditor","content_css":"https:\/\/cacbank.moodlecloud.com\/theme\/styles.php\/boost\/1569995285_1569995291\/editor","contextid":52,"autosaveEnabled":true,"autosaveFrequency":"60","language":"en","directionality":"ltr","filepickeroptions":{"image":{"defaultlicense":"allrightsreserved","licenses":[{"shortname":"unknown","fullname":"Other"},{"shortname":"allrightsreserved","fullname":"All rights reserved"},{"shortname":"public","fullname":"Public domain"},{"shortname":"cc","fullname":"Creative Commons"},{"shortname":"cc-nd","fullname":"Creative Commons - NoDerivs"},{"shortname":"cc-nc-nd","fullname":"Creative Commons - No Commercial NoDerivs"},{"shortname":"cc-nc","fullname":"Creative Commons - No Commercial"},{"shortname":"cc-nc-sa","fullname":"Creative Commons - No Commercial ShareAlike"},{"shortname":"cc-sa","fullname":"Creative Commons - ShareAlike"}],"author":"Duck Tran","repositories":{"1":{"id":"1","name":"Embedded files","type":"areafiles","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_areafiles\/1569995285\/icon","supported_types":[],"return_types":1,"defaultreturntype":2,"sortorder":1},"2":{"id":"2","name":"Server files","type":"local","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_local\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":2},"3":{"id":"3","name":"Recent files","type":"recent","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_recent\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":3},"4":{"id":"4","name":"Upload a file","type":"upload","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_upload\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":4},"5":{"id":"5","name":"URL downloader","type":"url","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_url\/1569995285\/icon","supported_types":[".gif",".jpe",".jpeg",".jpg",".png",".svg",".svgz"],"return_types":3,"defaultreturntype":2,"sortorder":5},"6":{"id":"6","name":"Private files","type":"user","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_user\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":6},"7":{"id":"7","name":"Wikimedia","type":"wikimedia","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_wikimedia\/1569995285\/icon","supported_types":[],"return_types":3,"defaultreturntype":2,"sortorder":7}},"externallink":true,"userprefs":{"recentrepository":"2","recentlicense":"allrightsreserved","recentviewmode":"3"},"accepted_types":[".gif",".jpe",".jpeg",".jpg",".png",".svg",".svgz"],"return_types":15,"context":{"id":52,"contextlevel":50,"instanceid":"7","path":"\/1\/3\/52","depth":"3","locked":false},"client_id":"5dc8028a8393b","maxbytes":0,"areamaxbytes":-1,"env":"editor","itemid":667718437},"media":{"defaultlicense":"allrightsreserved","licenses":[{"shortname":"unknown","fullname":"Other"},{"shortname":"allrightsreserved","fullname":"All rights reserved"},{"shortname":"public","fullname":"Public domain"},{"shortname":"cc","fullname":"Creative Commons"},{"shortname":"cc-nd","fullname":"Creative Commons - NoDerivs"},{"shortname":"cc-nc-nd","fullname":"Creative Commons - No Commercial NoDerivs"},{"shortname":"cc-nc","fullname":"Creative Commons - No Commercial"},{"shortname":"cc-nc-sa","fullname":"Creative Commons - No Commercial ShareAlike"},{"shortname":"cc-sa","fullname":"Creative Commons - ShareAlike"}],"author":"Duck Tran","repositories":{"1":{"id":"1","name":"Embedded files","type":"areafiles","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_areafiles\/1569995285\/icon","supported_types":[],"return_types":1,"defaultreturntype":2,"sortorder":1},"2":{"id":"2","name":"Server files","type":"local","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_local\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":2},"3":{"id":"3","name":"Recent files","type":"recent","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_recent\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":3},"4":{"id":"4","name":"Upload a file","type":"upload","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_upload\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":4},"6":{"id":"6","name":"Private files","type":"user","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_user\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":6},"7":{"id":"7","name":"Wikimedia","type":"wikimedia","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_wikimedia\/1569995285\/icon","supported_types":[],"return_types":3,"defaultreturntype":2,"sortorder":7}},"externallink":true,"userprefs":{"recentrepository":"2","recentlicense":"allrightsreserved","recentviewmode":"3"},"accepted_types":[".3gp",".avi",".dv",".dif",".flv",".f4v",".mov",".movie",".mp4",".m4v",".mpeg",".mpe",".mpg",".ogv",".qt",".rmvb",".rv",".swf",".swfl",".webm",".wmv",".asf",".aac",".aif",".aiff",".aifc",".au",".flac",".m3u",".mp3",".m4a",".oga",".ogg",".ra",".ram",".rm",".wav",".wma"],"return_types":15,"context":{"id":52,"contextlevel":50,"instanceid":"7","path":"\/1\/3\/52","depth":"3","locked":false},"client_id":"5dc8028a841d8","maxbytes":0,"areamaxbytes":-1,"env":"editor","itemid":667718437},"link":{"defaultlicense":"allrightsreserved","licenses":[{"shortname":"unknown","fullname":"Other"},{"shortname":"allrightsreserved","fullname":"All rights reserved"},{"shortname":"public","fullname":"Public domain"},{"shortname":"cc","fullname":"Creative Commons"},{"shortname":"cc-nd","fullname":"Creative Commons - NoDerivs"},{"shortname":"cc-nc-nd","fullname":"Creative Commons - No Commercial NoDerivs"},{"shortname":"cc-nc","fullname":"Creative Commons - No Commercial"},{"shortname":"cc-nc-sa","fullname":"Creative Commons - No Commercial ShareAlike"},{"shortname":"cc-sa","fullname":"Creative Commons - ShareAlike"}],"author":"Duck Tran","repositories":{"1":{"id":"1","name":"Embedded files","type":"areafiles","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_areafiles\/1569995285\/icon","supported_types":[],"return_types":1,"defaultreturntype":2,"sortorder":1},"2":{"id":"2","name":"Server files","type":"local","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_local\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":2},"3":{"id":"3","name":"Recent files","type":"recent","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_recent\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":3},"4":{"id":"4","name":"Upload a file","type":"upload","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_upload\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":4},"5":{"id":"5","name":"URL downloader","type":"url","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_url\/1569995285\/icon","supported_types":[".gif",".jpe",".jpeg",".jpg",".png",".svg",".svgz"],"return_types":3,"defaultreturntype":2,"sortorder":5},"6":{"id":"6","name":"Private files","type":"user","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_user\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":6},"7":{"id":"7","name":"Wikimedia","type":"wikimedia","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_wikimedia\/1569995285\/icon","supported_types":[],"return_types":3,"defaultreturntype":2,"sortorder":7}},"externallink":true,"userprefs":{"recentrepository":"2","recentlicense":"allrightsreserved","recentviewmode":"3"},"accepted_types":[],"return_types":15,"context":{"id":52,"contextlevel":50,"instanceid":"7","path":"\/1\/3\/52","depth":"3","locked":false},"client_id":"5dc8028a84a0a","maxbytes":0,"areamaxbytes":-1,"env":"editor","itemid":667718437},"subtitle":{"defaultlicense":"allrightsreserved","licenses":[{"shortname":"unknown","fullname":"Other"},{"shortname":"allrightsreserved","fullname":"All rights reserved"},{"shortname":"public","fullname":"Public domain"},{"shortname":"cc","fullname":"Creative Commons"},{"shortname":"cc-nd","fullname":"Creative Commons - NoDerivs"},{"shortname":"cc-nc-nd","fullname":"Creative Commons - No Commercial NoDerivs"},{"shortname":"cc-nc","fullname":"Creative Commons - No Commercial"},{"shortname":"cc-nc-sa","fullname":"Creative Commons - No Commercial ShareAlike"},{"shortname":"cc-sa","fullname":"Creative Commons - ShareAlike"}],"author":"Duck Tran","repositories":{"1":{"id":"1","name":"Embedded files","type":"areafiles","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_areafiles\/1569995285\/icon","supported_types":[],"return_types":1,"defaultreturntype":2,"sortorder":1},"2":{"id":"2","name":"Server files","type":"local","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_local\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":2},"3":{"id":"3","name":"Recent files","type":"recent","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_recent\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":3},"4":{"id":"4","name":"Upload a file","type":"upload","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_upload\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":4},"6":{"id":"6","name":"Private files","type":"user","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_user\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":6},"7":{"id":"7","name":"Wikimedia","type":"wikimedia","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_wikimedia\/1569995285\/icon","supported_types":[],"return_types":3,"defaultreturntype":2,"sortorder":7}},"externallink":true,"userprefs":{"recentrepository":"2","recentlicense":"allrightsreserved","recentviewmode":"3"},"accepted_types":[".vtt"],"return_types":15,"context":{"id":52,"contextlevel":50,"instanceid":"7","path":"\/1\/3\/52","depth":"3","locked":false},"client_id":"5dc8028a85264","maxbytes":0,"areamaxbytes":-1,"env":"editor","itemid":667718437}},"plugins":[{"group":"collapse","plugins":[{"name":"collapse","params":[],"showgroups":"5"}]},{"group":"style1","plugins":[{"name":"title","params":[]},{"name":"bold","params":[]},{"name":"italic","params":[]}]},{"group":"list","plugins":[{"name":"unorderedlist","params":[]},{"name":"orderedlist","params":[]}]},{"group":"links","plugins":[{"name":"link","params":[]}]},{"group":"files","plugins":[{"name":"image","params":[]},{"name":"media","params":[],"langs":{"installed":[{"lang":" \u12a0\u121b\u122d\u129b \u200e(am)\u200e","code":"am","default":false},{"lang":"Afrikaans \u200e(af)\u200e","code":"af","default":false},{"lang":"Aragon\u00e9s \u200e(an)\u200e","code":"an","default":false},{"lang":"Aran\u00e9s \u200e(oc_es)\u200e","code":"oc_es","default":false},{"lang":"Asturianu \u200e(ast)\u200e","code":"ast","default":false},{"lang":"Az\u0259rbaycanca \u200e(az)\u200e","code":"az","default":false},{"lang":"Bahasa Melayu \u200e(ms)\u200e","code":"ms","default":false},{"lang":"Bamanankan \u200e(bm)\u200e","code":"bm","default":false},{"lang":"Bislama \u200e(bi)\u200e","code":"bi","default":false},{"lang":"Bosanski \u200e(bs)\u200e","code":"bs","default":false},{"lang":"Breizh \u200e(br)\u200e","code":"br","default":false},{"lang":"Catal\u00e0 \u200e(ca)\u200e","code":"ca","default":false},{"lang":"Catal\u00e0 (Valenci\u00e0) \u200e(ca_valencia)\u200e","code":"ca_valencia","default":false},{"lang":"Catal\u00e0 per a Workplace \u200e(ca_wp)\u200e","code":"ca_wp","default":false},{"lang":"\u010ce\u0161tina \u200e(cs)\u200e","code":"cs","default":false},{"lang":"Crnogorski \u200e(mis)\u200e","code":"mis","default":false},{"lang":"Croatian schools \u200e(hr_schools)\u200e","code":"hr_schools","default":false},{"lang":"Cymraeg \u200e(cy)\u200e","code":"cy","default":false},{"lang":"Dansk \u200e(da)\u200e","code":"da","default":false},{"lang":"Dansk (kursus) \u200e(da_kursus)\u200e","code":"da_kursus","default":false},{"lang":"Dansk Rum \u200e(da_rum)\u200e","code":"da_rum","default":false},{"lang":"Davvis\u00e1megiella \u200e(se)\u200e","code":"se","default":false},{"lang":"Deutsch - Du \u200e(de_du)\u200e","code":"de_du","default":false},{"lang":"Deutsch - Kids \u200e(de_kids)\u200e","code":"de_kids","default":false},{"lang":"Deutsch - Schweiz \u200e(de_ch)\u200e","code":"de_ch","default":false},{"lang":"Deutsch \u200e(de)\u200e","code":"de","default":false},{"lang":"Deutsch community \u200e(de_comm)\u200e","code":"de_comm","default":false},{"lang":"Deutsch f\u00fcr Arbeitsplatz \u200e(de_wp)\u200e","code":"de_wp","default":false},{"lang":"Dolnoserbski \u200e(dsb)\u200e","code":"dsb","default":false},{"lang":"Ebon \u200e(mh)\u200e","code":"mh","default":false},{"lang":"eesti \u200e(et)\u200e","code":"et","default":false},{"lang":"English - Pirate \u200e(en_ar)\u200e","code":"en_ar","default":false},{"lang":"English - United States \u200e(en_us)\u200e","code":"en_us","default":false},{"lang":"English \u200e(en)\u200e","code":"en","default":true},{"lang":"English for kids \u200e(en_kids)\u200e","code":"en_kids","default":false},{"lang":"English for Workplace \u200e(en_wp)\u200e","code":"en_wp","default":false},{"lang":"English US - K12 \u200e(en_us_k12)\u200e","code":"en_us_k12","default":false},{"lang":"Espa\u00f1ol - Colombia \u200e(es_co)\u200e","code":"es_co","default":false},{"lang":"Espa\u00f1ol - Internacional \u200e(es)\u200e","code":"es","default":false},{"lang":"Espa\u00f1ol - M\u00e9xico \u200e(es_mx)\u200e","code":"es_mx","default":false},{"lang":"Espa\u00f1ol - M\u00e9xico para ni\u00f1os \u200e(es_mx_kids)\u200e","code":"es_mx_kids","default":false},{"lang":"Espa\u00f1ol - Venezuela \u200e(es_ve)\u200e","code":"es_ve","default":false},{"lang":"Espa\u00f1ol para la Empresa \u200e(es_wp)\u200e","code":"es_wp","default":false},{"lang":"Esperanto \u200e(eo)\u200e","code":"eo","default":false},{"lang":"Euskara \u200e(eu)\u200e","code":"eu","default":false},{"lang":"\u00c8\u028begbe \u200e(ee)\u200e","code":"ee","default":false},{"lang":"Filipino \u200e(fil)\u200e","code":"fil","default":false},{"lang":"Finlandssvenska \u200e(sv_fi)\u200e","code":"sv_fi","default":false},{"lang":"F\u00f8royskt \u200e(fo)\u200e","code":"fo","default":false},{"lang":"Fran\u00e7ais - Canada \u200e(fr_ca)\u200e","code":"fr_ca","default":false},{"lang":"Fran\u00e7ais \u200e(fr)\u200e","code":"fr","default":false},{"lang":"Fran\u00e7ais pour Workplace \u200e(fr_wp)\u200e","code":"fr_wp","default":false},{"lang":"Gaeilge \u200e(ga)\u200e","code":"ga","default":false},{"lang":"G\u00e0idhlig \u200e(gd)\u200e","code":"gd","default":false},{"lang":"Galego \u200e(gl)\u200e","code":"gl","default":false},{"lang":"Gascon \u200e(oc_gsc)\u200e","code":"oc_gsc","default":false},{"lang":"Hausa \u200e(ha)\u200e","code":"ha","default":false},{"lang":"Hrvatski \u200e(hr)\u200e","code":"hr","default":false},{"lang":"\u02bb\u014clelo Hawai\u02bbi \u200e(haw)\u200e","code":"haw","default":false},{"lang":"Igbo \u200e(ig)\u200e","code":"ig","default":false},{"lang":"Indonesian \u200e(id)\u200e","code":"id","default":false},{"lang":"isiZulu \u200e(zu)\u200e","code":"zu","default":false},{"lang":"\u00cdslenska \u200e(is)\u200e","code":"is","default":false},{"lang":"Italiano \u200e(it)\u200e","code":"it","default":false},{"lang":"Italiano per Workplace \u200e(it_wp)\u200e","code":"it_wp","default":false},{"lang":"Japanese - kids \u200e(ja_kids)\u200e","code":"ja_kids","default":false},{"lang":"Kalaallisut \u200e(kl)\u200e","code":"kl","default":false},{"lang":"Kinyarwanda \u200e(rw)\u200e","code":"rw","default":false},{"lang":"Kiswahili \u200e(sw)\u200e","code":"sw","default":false},{"lang":"Krey\u00f2l Ayisyen \u200e(hat)\u200e","code":"hat","default":false},{"lang":"Kurmanji \u200e(kmr)\u200e","code":"kmr","default":false},{"lang":"Laotian \u200e(lo)\u200e","code":"lo","default":false},{"lang":"Latin \u200e(la)\u200e","code":"la","default":false},{"lang":"Latvie\u0161u \u200e(lv)\u200e","code":"lv","default":false},{"lang":"Lengadocian \u200e(oc_lnc)\u200e","code":"oc_lnc","default":false},{"lang":"L\u00ebtzebuergesch \u200e(lb)\u200e","code":"lb","default":false},{"lang":"Lietuvi\u0173 \u200e(lt)\u200e","code":"lt","default":false},{"lang":"Lithuanian (university) \u200e(lt_uni)\u200e","code":"lt_uni","default":false},{"lang":"Lulesamisk \u200e(smj)\u200e","code":"smj","default":false},{"lang":"magyar \u200e(hu)\u200e","code":"hu","default":false},{"lang":"Malagasy \u200e(mg)\u200e","code":"mg","default":false},{"lang":"M\u0101ori - Tainui \u200e(mi_tn)\u200e","code":"mi_tn","default":false},{"lang":"M\u0101ori - Waikato \u200e(mi_wwow)\u200e","code":"mi_wwow","default":false},{"lang":"M\u0101ori (Te Reo) \u200e(mi)\u200e","code":"mi","default":false},{"lang":"Mongolian \u200e(mn_mong)\u200e","code":"mn_mong","default":false},{"lang":"myanma bhasa \u200e(my)\u200e","code":"my","default":false},{"lang":"Nederlands \u200e(nl)\u200e","code":"nl","default":false},{"lang":"Norsk - bokm\u00e5l \u200e(no)\u200e","code":"no","default":false},{"lang":"Norsk - nynorsk \u200e(nn)\u200e","code":"nn","default":false},{"lang":"Norsk \u200e(no_gr)\u200e","code":"no_gr","default":false},{"lang":"Norsk Workplace \u200e(no_wp)\u200e","code":"no_wp","default":false},{"lang":"O'zbekcha \u200e(uz)\u200e","code":"uz","default":false},{"lang":"Pidgin \u200e(pcm)\u200e","code":"pcm","default":false},{"lang":"Polski \u200e(pl)\u200e","code":"pl","default":false},{"lang":"Portugu\u00eas - Brasil \u200e(pt_br)\u200e","code":"pt_br","default":false},{"lang":"Portugu\u00eas - Portugal \u200e(pt)\u200e","code":"pt","default":false},{"lang":"Rom\u00e2n\u0103 \u200e(ro)\u200e","code":"ro","default":false},{"lang":"Romansh Sursilvan \u200e(rm_surs)\u200e","code":"rm_surs","default":false},{"lang":"Samoan \u200e(sm)\u200e","code":"sm","default":false},{"lang":"Setswana \u200e(tn)\u200e","code":"tn","default":false},{"lang":"Shqip \u200e(sq)\u200e","code":"sq","default":false},{"lang":"Sloven\u010dina \u200e(sk)\u200e","code":"sk","default":false},{"lang":"Sloven\u0161\u010dina \u200e(sl)\u200e","code":"sl","default":false},{"lang":"Soomaali \u200e(so)\u200e","code":"so","default":false},{"lang":"S\u00f8rsamisk \u200e(sma)\u200e","code":"sma","default":false},{"lang":"Srpski \u200e(sr_lt)\u200e","code":"sr_lt","default":false},{"lang":"Suomi \u200e(fi)\u200e","code":"fi","default":false},{"lang":"Suomi+ \u200e(fi_co)\u200e","code":"fi_co","default":false},{"lang":"Svenska \u200e(sv)\u200e","code":"sv","default":false},{"lang":"Tagalog \u200e(tl)\u200e","code":"tl","default":false},{"lang":"Tamil \u200e(ta)\u200e","code":"ta","default":false},{"lang":"Taqbaylit \u200e(kab)\u200e","code":"kab","default":false},{"lang":"Thai \u200e(th)\u200e","code":"th","default":false},{"lang":"Tongan \u200e(to)\u200e","code":"to","default":false},{"lang":"T\u00fcrk\u00e7e \u200e(tr)\u200e","code":"tr","default":false},{"lang":"Turkmen \u200e(tk)\u200e","code":"tk","default":false},{"lang":"Uyghur - latin \u200e(ug_lt)\u200e","code":"ug_lt","default":false},{"lang":"VakaViti \u200e(fj)\u200e","code":"fj","default":false},{"lang":"Valenci\u00e0_RACV \u200e(ca_valencia_racv)\u200e","code":"ca_valencia_racv","default":false},{"lang":"Vietnamese \u200e(vi)\u200e","code":"vi","default":false},{"lang":"Wolof \u200e(wo)\u200e","code":"wo","default":false},{"lang":"Workplace \u00een limba rom\u00e2n\u0103 \u200e(ro_wp)\u200e","code":"ro_wp","default":false},{"lang":"Workplace \u7b80\u4f53\u4e2d\u6587\u7248 \u200e(zh_cn_wp)\u200e","code":"zh_cn_wp","default":false},{"lang":"Workplace \u7e41\u9ad4\u4e2d\u6587\u7248 \u200e(zh_tw_wp)\u200e","code":"zh_tw_wp","default":false},{"lang":"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac \u200e(el)\u200e","code":"el","default":false},{"lang":"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac \u03b3\u03b9\u03b1 \u03c0\u03b1\u03b9\u03b4\u03b9\u03ac \u200e(el_kids)\u200e","code":"el_kids","default":false},{"lang":"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac \u03b3\u03b9\u03b1 \u03c3\u03c7\u03bf\u03bb\u03ad\u03c2 \u200e(el_uni)\u200e","code":"el_uni","default":false},{"lang":"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac \u03b3\u03b9\u03b1 \u03c7\u03ce\u03c1\u03bf\u03c5\u03c2 \u03b5\u03c1\u03b3\u03b1\u03c3\u03af\u03b1\u03c2 \u200e(el_wp)\u200e","code":"el_wp","default":false},{"lang":"\u0411\u0430\u0448\u04a1\u043e\u0440\u0442 \u0442\u0435\u043b\u0435 \u200e(ba)\u200e","code":"ba","default":false},{"lang":"\u0411\u0435\u043b\u0430\u0440\u0443\u0441\u043a\u0430\u044f \u200e(be)\u200e","code":"be","default":false},{"lang":"\u0411\u044a\u043b\u0433\u0430\u0440\u0441\u043a\u0438 \u200e(bg)\u200e","code":"bg","default":false},{"lang":"\u041a\u044b\u0440\u0433\u044b\u0437\u0447\u0430 \u200e(ky)\u200e","code":"ky","default":false},{"lang":"\u049a\u0430\u0437\u0430\u049b\u0448\u0430 \u200e(kk)\u200e","code":"kk","default":false},{"lang":"\u041c\u0430\u043a\u0435\u0434\u043e\u043d\u0441\u043a\u0438 \u200e(mk)\u200e","code":"mk","default":false},{"lang":"\u041c\u043e\u043d\u0433\u043e\u043b \u200e(mn)\u200e","code":"mn","default":false},{"lang":"\u0420\u0443\u0441\u0441\u043a\u0438\u0439 \u200e(ru)\u200e","code":"ru","default":false},{"lang":"\u0421\u0440\u043f\u0441\u043a\u0438 \u200e(sr_cr)\u200e","code":"sr_cr","default":false},{"lang":"\u0442\u0430\u0442\u0430\u0440 \u0442\u0435\u043b\u0435 \u200e(tt)\u200e","code":"tt","default":false},{"lang":"\u0422\u043e\u04b7\u0438\u043a\u04e3 \u200e(tg)\u200e","code":"tg","default":false},{"lang":"\u0423\u043a\u0440\u0430\u0457\u043d\u0441\u044c\u043a\u0430 \u200e(uk)\u200e","code":"uk","default":false},{"lang":"\u10e5\u10d0\u10e0\u10d7\u10e3\u10da\u10d8 \u200e(ka)\u200e","code":"ka","default":false},{"lang":"\u0540\u0561\u0575\u0565\u0580\u0565\u0576 \u200e(hy)\u200e","code":"hy","default":false},{"lang":"\u05e2\u05d1\u05e8\u05d9\u05ea \u200e(he)\u200e","code":"he","default":false},{"lang":"\u05e2\u05d1\u05e8\u05d9\u05ea \u05d1\u05ea\u05d9\u05be\u05e1\u05e4\u05e8 \u200e(he_kids)\u200e","code":"he_kids","default":false},{"lang":"\u05e2\u05d1\u05e8\u05d9\u05ea \u05e2\u05d1\u05d5\u05e8 Workplace \u200e(he_wp)\u200e","code":"he_wp","default":false},{"lang":"\u0626\u06c7\u064a\u063a\u06c7\u0631\u0686\u06d5 \u200e(ug_ug)\u200e","code":"ug_ug","default":false},{"lang":"\u0627\u0631\u062f\u0648 \u200e(ur)\u200e","code":"ur","default":false},{"lang":"\u067e\u069a\u062a\u0648 \u200e(ps)\u200e","code":"ps","default":false},{"lang":"\u0633\u06c6\u0631\u0627\u0646\u06cc \u200e(ckb)\u200e","code":"ckb","default":false},{"lang":"\u0639\u0631\u0628\u064a \u200e(ar)\u200e","code":"ar","default":false},{"lang":"\u0641\u0627\u0631\u0633\u06cc \u200e(fa)\u200e","code":"fa","default":false},{"lang":"\u078b\u07a8\u0788\u07ac\u0780\u07a8 \u200e(dv)\u200e","code":"dv","default":false},{"lang":"\u2d5c\u2d30\u2d4e\u2d30\u2d63\u2d49\u2d56\u2d5c \u200e(zgh)\u200e","code":"zgh","default":false},{"lang":"\u1275\u130d\u122d\u129b \u200e(ti)\u200e","code":"ti","default":false},{"lang":"\u0928\u0947\u092a\u093e\u0932\u0940 \u200e(ne)\u200e","code":"ne","default":false},{"lang":"\u092e\u0930\u093e\u0920\u0940 \u200e(mr)\u200e","code":"mr","default":false},{"lang":"\u0935\u0930\u094d\u0915\u092a\u094d\u0932\u0947\u0938  \u0915\u0947 \u0932\u093f\u090f  \u0939\u093f\u0902\u0926\u0940 \u200e(hi_wp)\u200e","code":"hi_wp","default":false},{"lang":"\u0939\u093f\u0902\u0926\u0940 \u200e(hi)\u200e","code":"hi","default":false},{"lang":"\u09ac\u09be\u0982\u09b2\u09be \u200e(bn)\u200e","code":"bn","default":false},{"lang":"\u0a2a\u0a70\u0a1c\u0a3e\u0a2c\u0a40 \u200e(pan)\u200e","code":"pan","default":false},{"lang":"\u0a97\u0ac1\u0a9c\u0ab0\u0abe\u0aa4\u0ac0 \u200e(gu)\u200e","code":"gu","default":false},{"lang":"\u0b13\u0b21\u0b3c\u0b3f\u0b06 \u200e(or)\u200e","code":"or","default":false},{"lang":"\u0ba4\u0bae\u0bbf\u0bb4\u0bcd \u200e(ta_lk)\u200e","code":"ta_lk","default":false},{"lang":"\u0c24\u0c46\u0c32\u0c41\u0c17\u0c41  \u200e(te)\u200e","code":"te","default":false},{"lang":"\u0c95\u0ca8\u0ccd\u0ca8\u0ca1 \u200e(kn)\u200e","code":"kn","default":false},{"lang":"\u0d2e\u0d32\u0d2f\u0d3e\u0d33\u0d02 \u200e(ml)\u200e","code":"ml","default":false},{"lang":"\u0dc3\u0dd2\u0d82\u0dc4\u0dbd \u200e(si)\u200e","code":"si","default":false},{"lang":"\u0f56\u0f7c\u0f51\u0f0b\u0f61\u0f72\u0f42 \u200e(xct)\u200e","code":"xct","default":false},{"lang":"\u0f62\u0fab\u0f7c\u0f44\u0f0b\u0f41 \u200e(dz)\u200e","code":"dz","default":false},{"lang":"\u1781\u17d2\u1798\u17c2\u179a \u200e(km)\u200e","code":"km","default":false},{"lang":"\ud55c\uad6d\uc5b4 \u200e(ko)\u200e","code":"ko","default":false},{"lang":"\u65e5\u672c\u8a9e \u200e(ja)\u200e","code":"ja","default":false},{"lang":"\u6b63\u9ad4\u4e2d\u6587 \u200e(zh_tw)\u200e","code":"zh_tw","default":false},{"lang":"\u7b80\u4f53\u4e2d\u6587 \u200e(zh_cn)\u200e","code":"zh_cn","default":false}],"available":[{"lang":"Afar \u200e(aa)\u200e","code":"aa"},{"lang":"Abkhazian \u200e(ab)\u200e","code":"ab"},{"lang":"Avestan \u200e(ae)\u200e","code":"ae"},{"lang":"Afrikaans \u200e(af)\u200e","code":"af"},{"lang":"Akan \u200e(ak)\u200e","code":"ak"},{"lang":"Amharic \u200e(am)\u200e","code":"am"},{"lang":"Aragonese \u200e(an)\u200e","code":"an"},{"lang":"Arabic \u200e(ar)\u200e","code":"ar"},{"lang":"Assamese \u200e(as)\u200e","code":"as"},{"lang":"Avaric \u200e(av)\u200e","code":"av"},{"lang":"Aymara \u200e(ay)\u200e","code":"ay"},{"lang":"Azerbaijani \u200e(az)\u200e","code":"az"},{"lang":"Bashkir \u200e(ba)\u200e","code":"ba"},{"lang":"Belarusian \u200e(be)\u200e","code":"be"},{"lang":"Bulgarian \u200e(bg)\u200e","code":"bg"},{"lang":"Bihari languages \u200e(bh)\u200e","code":"bh"},{"lang":"Bislama \u200e(bi)\u200e","code":"bi"},{"lang":"Bambara \u200e(bm)\u200e","code":"bm"},{"lang":"Bengali \u200e(bn)\u200e","code":"bn"},{"lang":"Tibetan \u200e(bo)\u200e","code":"bo"},{"lang":"Breton \u200e(br)\u200e","code":"br"},{"lang":"Bosnian \u200e(bs)\u200e","code":"bs"},{"lang":"Catalan; Valencian \u200e(ca)\u200e","code":"ca"},{"lang":"Chechen \u200e(ce)\u200e","code":"ce"},{"lang":"Chamorro \u200e(ch)\u200e","code":"ch"},{"lang":"Corsican \u200e(co)\u200e","code":"co"},{"lang":"Cree \u200e(cr)\u200e","code":"cr"},{"lang":"Czech \u200e(cs)\u200e","code":"cs"},{"lang":"Church Slavic; Old Slavonic \u200e(cu)\u200e","code":"cu"},{"lang":"Chuvash \u200e(cv)\u200e","code":"cv"},{"lang":"Welsh \u200e(cy)\u200e","code":"cy"},{"lang":"Danish \u200e(da)\u200e","code":"da"},{"lang":"German \u200e(de)\u200e","code":"de"},{"lang":"Divehi; Dhivehi; Maldivian \u200e(dv)\u200e","code":"dv"},{"lang":"Dzongkha \u200e(dz)\u200e","code":"dz"},{"lang":"Ewe \u200e(ee)\u200e","code":"ee"},{"lang":"Greek, Modern (1453-) \u200e(el)\u200e","code":"el"},{"lang":"English \u200e(en)\u200e","code":"en"},{"lang":"Esperanto \u200e(eo)\u200e","code":"eo"},{"lang":"Spanish; Castilian \u200e(es)\u200e","code":"es"},{"lang":"Estonian \u200e(et)\u200e","code":"et"},{"lang":"Basque \u200e(eu)\u200e","code":"eu"},{"lang":"Persian \u200e(fa)\u200e","code":"fa"},{"lang":"Fulah \u200e(ff)\u200e","code":"ff"},{"lang":"Finnish \u200e(fi)\u200e","code":"fi"},{"lang":"Fijian \u200e(fj)\u200e","code":"fj"},{"lang":"Faroese \u200e(fo)\u200e","code":"fo"},{"lang":"French \u200e(fr)\u200e","code":"fr"},{"lang":"Western Frisian \u200e(fy)\u200e","code":"fy"},{"lang":"Irish \u200e(ga)\u200e","code":"ga"},{"lang":"Gaelic; Scottish Gaelic \u200e(gd)\u200e","code":"gd"},{"lang":"Galician \u200e(gl)\u200e","code":"gl"},{"lang":"Guarani \u200e(gn)\u200e","code":"gn"},{"lang":"Gujarati \u200e(gu)\u200e","code":"gu"},{"lang":"Manx \u200e(gv)\u200e","code":"gv"},{"lang":"Hausa \u200e(ha)\u200e","code":"ha"},{"lang":"Hebrew \u200e(he)\u200e","code":"he"},{"lang":"Hindi \u200e(hi)\u200e","code":"hi"},{"lang":"Hiri Motu \u200e(ho)\u200e","code":"ho"},{"lang":"Croatian \u200e(hr)\u200e","code":"hr"},{"lang":"Haitian; Haitian Creole \u200e(ht)\u200e","code":"ht"},{"lang":"Hungarian \u200e(hu)\u200e","code":"hu"},{"lang":"Armenian \u200e(hy)\u200e","code":"hy"},{"lang":"Herero \u200e(hz)\u200e","code":"hz"},{"lang":"Interlingua (IALA) \u200e(ia)\u200e","code":"ia"},{"lang":"Indonesian \u200e(id)\u200e","code":"id"},{"lang":"Interlingue; Occidental \u200e(ie)\u200e","code":"ie"},{"lang":"Igbo \u200e(ig)\u200e","code":"ig"},{"lang":"Sichuan Yi; Nuosu \u200e(ii)\u200e","code":"ii"},{"lang":"Inupiaq \u200e(ik)\u200e","code":"ik"},{"lang":"Ido \u200e(io)\u200e","code":"io"},{"lang":"Icelandic \u200e(is)\u200e","code":"is"},{"lang":"Italian \u200e(it)\u200e","code":"it"},{"lang":"Inuktitut \u200e(iu)\u200e","code":"iu"},{"lang":"Japanese \u200e(ja)\u200e","code":"ja"},{"lang":"Javanese \u200e(jv)\u200e","code":"jv"},{"lang":"Georgian \u200e(ka)\u200e","code":"ka"},{"lang":"Kongo \u200e(kg)\u200e","code":"kg"},{"lang":"Kikuyu; Gikuyu \u200e(ki)\u200e","code":"ki"},{"lang":"Kuanyama; Kwanyama \u200e(kj)\u200e","code":"kj"},{"lang":"Kazakh \u200e(kk)\u200e","code":"kk"},{"lang":"Kalaallisut; Greenlandic \u200e(kl)\u200e","code":"kl"},{"lang":"Central Khmer \u200e(km)\u200e","code":"km"},{"lang":"Kannada \u200e(kn)\u200e","code":"kn"},{"lang":"Korean \u200e(ko)\u200e","code":"ko"},{"lang":"Kanuri \u200e(kr)\u200e","code":"kr"},{"lang":"Kashmiri \u200e(ks)\u200e","code":"ks"},{"lang":"Kurdish \u200e(ku)\u200e","code":"ku"},{"lang":"Komi \u200e(kv)\u200e","code":"kv"},{"lang":"Cornish \u200e(kw)\u200e","code":"kw"},{"lang":"Kirghiz; Kyrgyz \u200e(ky)\u200e","code":"ky"},{"lang":"Latin \u200e(la)\u200e","code":"la"},{"lang":"Luxembourgish \u200e(lb)\u200e","code":"lb"},{"lang":"Ganda \u200e(lg)\u200e","code":"lg"},{"lang":"Limburgan \u200e(li)\u200e","code":"li"},{"lang":"Lingala \u200e(ln)\u200e","code":"ln"},{"lang":"Lao \u200e(lo)\u200e","code":"lo"},{"lang":"Lithuanian \u200e(lt)\u200e","code":"lt"},{"lang":"Luba-Katanga \u200e(lu)\u200e","code":"lu"},{"lang":"Latvian \u200e(lv)\u200e","code":"lv"},{"lang":"Malagasy \u200e(mg)\u200e","code":"mg"},{"lang":"Marshallese \u200e(mh)\u200e","code":"mh"},{"lang":"Maori \u200e(mi)\u200e","code":"mi"},{"lang":"Macedonian \u200e(mk)\u200e","code":"mk"},{"lang":"Malayalam \u200e(ml)\u200e","code":"ml"},{"lang":"Mongolian \u200e(mn)\u200e","code":"mn"},{"lang":"Marathi \u200e(mr)\u200e","code":"mr"},{"lang":"Malay \u200e(ms)\u200e","code":"ms"},{"lang":"Maltese \u200e(mt)\u200e","code":"mt"},{"lang":"Burmese \u200e(my)\u200e","code":"my"},{"lang":"Nauru \u200e(na)\u200e","code":"na"},{"lang":"Norwegian Bokm\u00e5l \u200e(nb)\u200e","code":"nb"},{"lang":"Ndebele, North \u200e(nd)\u200e","code":"nd"},{"lang":"Nepali \u200e(ne)\u200e","code":"ne"},{"lang":"Ndonga \u200e(ng)\u200e","code":"ng"},{"lang":"Dutch; Flemish \u200e(nl)\u200e","code":"nl"},{"lang":"Norwegian Nynorsk \u200e(nn)\u200e","code":"nn"},{"lang":"Norwegian \u200e(no)\u200e","code":"no"},{"lang":"Ndebele, South \u200e(nr)\u200e","code":"nr"},{"lang":"Navajo; Navaho \u200e(nv)\u200e","code":"nv"},{"lang":"Chichewa \u200e(ny)\u200e","code":"ny"},{"lang":"Occitan (post 1500); Proven\u00e7al \u200e(oc)\u200e","code":"oc"},{"lang":"Ojibwa \u200e(oj)\u200e","code":"oj"},{"lang":"Oromo \u200e(om)\u200e","code":"om"},{"lang":"Oriya \u200e(or)\u200e","code":"or"},{"lang":"Ossetian; Ossetic \u200e(os)\u200e","code":"os"},{"lang":"Panjabi; Punjabi \u200e(pa)\u200e","code":"pa"},{"lang":"Pali \u200e(pi)\u200e","code":"pi"},{"lang":"Polish \u200e(pl)\u200e","code":"pl"},{"lang":"Pushto; Pashto \u200e(ps)\u200e","code":"ps"},{"lang":"Portuguese \u200e(pt)\u200e","code":"pt"},{"lang":"Quechua \u200e(qu)\u200e","code":"qu"},{"lang":"Romansh \u200e(rm)\u200e","code":"rm"},{"lang":"Rundi \u200e(rn)\u200e","code":"rn"},{"lang":"Romanian; Moldavian; Moldovan \u200e(ro)\u200e","code":"ro"},{"lang":"Russian \u200e(ru)\u200e","code":"ru"},{"lang":"Kinyarwanda \u200e(rw)\u200e","code":"rw"},{"lang":"Sanskrit \u200e(sa)\u200e","code":"sa"},{"lang":"Sardinian \u200e(sc)\u200e","code":"sc"},{"lang":"Sindhi \u200e(sd)\u200e","code":"sd"},{"lang":"Northern Sami \u200e(se)\u200e","code":"se"},{"lang":"Sango \u200e(sg)\u200e","code":"sg"},{"lang":"Sinhala; Sinhalese \u200e(si)\u200e","code":"si"},{"lang":"Slovak \u200e(sk)\u200e","code":"sk"},{"lang":"Slovenian \u200e(sl)\u200e","code":"sl"},{"lang":"Samoan \u200e(sm)\u200e","code":"sm"},{"lang":"Shona \u200e(sn)\u200e","code":"sn"},{"lang":"Somali \u200e(so)\u200e","code":"so"},{"lang":"Albanian \u200e(sq)\u200e","code":"sq"},{"lang":"Serbian \u200e(sr)\u200e","code":"sr"},{"lang":"Swati \u200e(ss)\u200e","code":"ss"},{"lang":"Sotho, Southern \u200e(st)\u200e","code":"st"},{"lang":"Sundanese \u200e(su)\u200e","code":"su"},{"lang":"Swedish \u200e(sv)\u200e","code":"sv"},{"lang":"Swahili \u200e(sw)\u200e","code":"sw"},{"lang":"Tamil \u200e(ta)\u200e","code":"ta"},{"lang":"Telugu \u200e(te)\u200e","code":"te"},{"lang":"Tajik \u200e(tg)\u200e","code":"tg"},{"lang":"Thai \u200e(th)\u200e","code":"th"},{"lang":"Tigrinya \u200e(ti)\u200e","code":"ti"},{"lang":"Turkmen \u200e(tk)\u200e","code":"tk"},{"lang":"Tagalog \u200e(tl)\u200e","code":"tl"},{"lang":"Tswana \u200e(tn)\u200e","code":"tn"},{"lang":"Tonga (Tonga Islands) \u200e(to)\u200e","code":"to"},{"lang":"Turkish \u200e(tr)\u200e","code":"tr"},{"lang":"Tsonga \u200e(ts)\u200e","code":"ts"},{"lang":"Tatar \u200e(tt)\u200e","code":"tt"},{"lang":"Twi \u200e(tw)\u200e","code":"tw"},{"lang":"Tahitian \u200e(ty)\u200e","code":"ty"},{"lang":"Uighur; Uyghur \u200e(ug)\u200e","code":"ug"},{"lang":"Ukrainian \u200e(uk)\u200e","code":"uk"},{"lang":"Urdu \u200e(ur)\u200e","code":"ur"},{"lang":"Uzbek \u200e(uz)\u200e","code":"uz"},{"lang":"Venda \u200e(ve)\u200e","code":"ve"},{"lang":"Vietnamese \u200e(vi)\u200e","code":"vi"},{"lang":"Volap\u00fck \u200e(vo)\u200e","code":"vo"},{"lang":"Walloon \u200e(wa)\u200e","code":"wa"},{"lang":"Wolof \u200e(wo)\u200e","code":"wo"},{"lang":"Xhosa \u200e(xh)\u200e","code":"xh"},{"lang":"Yiddish \u200e(yi)\u200e","code":"yi"},{"lang":"Yoruba \u200e(yo)\u200e","code":"yo"},{"lang":"Zhuang; Chuang \u200e(za)\u200e","code":"za"},{"lang":"Chinese \u200e(zh)\u200e","code":"zh"},{"lang":"Zulu \u200e(zu)\u200e","code":"zu"}]},"help":{"addsource":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;It is recommended that an alternative media source is provided, since desktop and mobile browsers vary in which file formats they support.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Add alternative source\" aria-label=\"Help with Add alternative source\"><\/i>\n<\/a>","tracks":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Subtitles, captions, chapters and descriptions can be added via a WebVTT (Web Video Text Tracks) format file. Track labels will be shown in the selection drop-down menu. For each type of track, any track set as default will be pre-selected at the start of the video.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Subtitles and captions\" aria-label=\"Help with Subtitles and captions\"><\/i>\n<\/a>","subtitles":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Subtitles may be used to provide a transcription or translation of the dialogue.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Subtitles\" aria-label=\"Help with Subtitles\"><\/i>\n<\/a>","captions":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Captions may be used to describe everything happening in the track, including non-verbal sounds such as a phone ringing.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Captions\" aria-label=\"Help with Captions\"><\/i>\n<\/a>","descriptions":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Audio descriptions may be used to provide a narration which explains visual details not apparent from the audio alone.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Descriptions\" aria-label=\"Help with Descriptions\"><\/i>\n<\/a>","chapters":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Chapter titles may be provided for use in navigating the media resource.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Chapters\" aria-label=\"Help with Chapters\"><\/i>\n<\/a>","metadata":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Metadata tracks, for use from a script, may be used only if the player supports metadata.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Metadata\" aria-label=\"Help with Metadata\"><\/i>\n<\/a>"}},{"name":"recordrtc","params":[],"contextid":52,"sesskey":"DyXZbRiLhr","allowedtypes":"both","audiobitrate":"128000","videobitrate":"2500000","timelimit":"120","audiortcicon":"i\/audiortc","videortcicon":"i\/videortc","maxrecsize":1073741824},{"name":"managefiles","params":[],"disabled":false,"area":{"context":52,"areamaxbytes":-1,"maxbytes":0,"subdirs":1,"return_types":15,"removeorphaneddrafts":false},"usercontext":5}]},{"group":"style2","plugins":[{"name":"underline","params":[]},{"name":"strike","params":[]},{"name":"subscript","params":[]},{"name":"superscript","params":[]}]},{"group":"align","plugins":[{"name":"align","params":[]}]},{"group":"indent","plugins":[{"name":"indent","params":[]}]},{"group":"insert","plugins":[{"name":"equation","params":[],"texfilteractive":true,"contextid":52,"library":{"group1":{"groupname":"librarygroup1","elements":"\n\\cdot\n\\times\n\\ast\n\\div\n\\diamond\n\\pm\n\\mp\n\\oplus\n\\ominus\n\\otimes\n\\oslash\n\\odot\n\\circ\n\\bullet\n\\asymp\n\\equiv\n\\subseteq\n\\supseteq\n\\leq\n\\geq\n\\preceq\n\\succeq\n\\sim\n\\simeq\n\\approx\n\\subset\n\\supset\n\\ll\n\\gg\n\\prec\n\\succ\n\\infty\n\\in\n\\ni\n\\forall\n\\exists\n\\neq\n"},"group2":{"groupname":"librarygroup2","elements":"\n\\leftarrow\n\\rightarrow\n\\uparrow\n\\downarrow\n\\leftrightarrow\n\\nearrow\n\\searrow\n\\swarrow\n\\nwarrow\n\\Leftarrow\n\\Rightarrow\n\\Uparrow\n\\Downarrow\n\\Leftrightarrow\n"},"group3":{"groupname":"librarygroup3","elements":"\n\\alpha\n\\beta\n\\gamma\n\\delta\n\\epsilon\n\\zeta\n\\eta\n\\theta\n\\iota\n\\kappa\n\\lambda\n\\mu\n\\nu\n\\xi\n\\pi\n\\rho\n\\sigma\n\\tau\n\\upsilon\n\\phi\n\\chi\n\\psi\n\\omega\n\\Gamma\n\\Delta\n\\Theta\n\\Lambda\n\\Xi\n\\Pi\n\\Sigma\n\\Upsilon\n\\Phi\n\\Psi\n\\Omega\n"},"group4":{"groupname":"librarygroup4","elements":"\n\\sum{a,b}\n\\sqrt[a]{b+c}\n\\int_{a}^{b}{c}\n\\iint_{a}^{b}{c}\n\\iiint_{a}^{b}{c}\n\\oint{a}\n(a)\n[a]\n\\lbrace{a}\\rbrace\n\\left| \\begin{matrix} a_1 & a_2 \\ a_3 & a_4 \\end{matrix} \\right|\n\\frac{a}{b+c}\n\\vec{a}\n\\binom {a} {b}\n{a \\brack b}\n{a \\brace b}\n"}},"texdocsurl":"http:\/\/docs.moodle.org\/37\/en\/Using_TeX_Notation"},{"name":"charmap","params":[]},{"name":"table","params":[],"allowBorders":false,"allowWidth":false,"allowBackgroundColour":false},{"name":"clear","params":[]}]},{"group":"undo","plugins":[{"name":"undo","params":[]}]},{"group":"accessibility","plugins":[{"name":"accessibilitychecker","params":[]},{"name":"accessibilityhelper","params":[]}]},{"group":"other","plugins":[{"name":"html","params":[]}]}],"pageHash":"2a434d39b60e9093356609ce9c50e1f359b65473"});
});
Y.use("moodle-filter_mathjaxloader-loader",function() {M.filter_mathjaxloader.typeset();
});
Y.use("moodle-editor_atto-editor","moodle-atto_collapse-button","moodle-atto_title-button","moodle-atto_bold-button","moodle-atto_italic-button","moodle-atto_unorderedlist-button","moodle-atto_orderedlist-button","moodle-atto_link-button","moodle-atto_image-button","moodle-atto_media-button","moodle-atto_recordrtc-button","moodle-atto_managefiles-button","moodle-atto_underline-button","moodle-atto_strike-button","moodle-atto_subscript-button","moodle-atto_superscript-button","moodle-atto_align-button","moodle-atto_indent-button","moodle-atto_equation-button","moodle-atto_charmap-button","moodle-atto_table-button","moodle-atto_clear-button","moodle-atto_undo-button","moodle-atto_accessibilitychecker-button","moodle-atto_accessibilityhelper-button","moodle-atto_html-button",function() {Y.M.editor_atto.Editor.init({"elementid":"id_feedbacktext_0","content_css":"https:\/\/cacbank.moodlecloud.com\/theme\/styles.php\/boost\/1569995285_1569995291\/editor","contextid":52,"autosaveEnabled":true,"autosaveFrequency":"60","language":"en","directionality":"ltr","filepickeroptions":{"image":{"defaultlicense":"allrightsreserved","licenses":[{"shortname":"unknown","fullname":"Other"},{"shortname":"allrightsreserved","fullname":"All rights reserved"},{"shortname":"public","fullname":"Public domain"},{"shortname":"cc","fullname":"Creative Commons"},{"shortname":"cc-nd","fullname":"Creative Commons - NoDerivs"},{"shortname":"cc-nc-nd","fullname":"Creative Commons - No Commercial NoDerivs"},{"shortname":"cc-nc","fullname":"Creative Commons - No Commercial"},{"shortname":"cc-nc-sa","fullname":"Creative Commons - No Commercial ShareAlike"},{"shortname":"cc-sa","fullname":"Creative Commons - ShareAlike"}],"author":"Duck Tran","repositories":{"1":{"id":"1","name":"Embedded files","type":"areafiles","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_areafiles\/1569995285\/icon","supported_types":[],"return_types":1,"defaultreturntype":2,"sortorder":1},"2":{"id":"2","name":"Server files","type":"local","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_local\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":2},"3":{"id":"3","name":"Recent files","type":"recent","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_recent\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":3},"4":{"id":"4","name":"Upload a file","type":"upload","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_upload\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":4},"5":{"id":"5","name":"URL downloader","type":"url","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_url\/1569995285\/icon","supported_types":[".gif",".jpe",".jpeg",".jpg",".png",".svg",".svgz"],"return_types":3,"defaultreturntype":2,"sortorder":5},"6":{"id":"6","name":"Private files","type":"user","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_user\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":6},"7":{"id":"7","name":"Wikimedia","type":"wikimedia","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_wikimedia\/1569995285\/icon","supported_types":[],"return_types":3,"defaultreturntype":2,"sortorder":7}},"externallink":true,"userprefs":{"recentrepository":"2","recentlicense":"allrightsreserved","recentviewmode":"3"},"accepted_types":[".gif",".jpe",".jpeg",".jpg",".png",".svg",".svgz"],"return_types":15,"context":{"id":52,"contextlevel":50,"instanceid":"7","path":"\/1\/3\/52","depth":"3","locked":false},"client_id":"5dc8028a8dd51","maxbytes":0,"areamaxbytes":-1,"env":"editor","itemid":841266077},"media":{"defaultlicense":"allrightsreserved","licenses":[{"shortname":"unknown","fullname":"Other"},{"shortname":"allrightsreserved","fullname":"All rights reserved"},{"shortname":"public","fullname":"Public domain"},{"shortname":"cc","fullname":"Creative Commons"},{"shortname":"cc-nd","fullname":"Creative Commons - NoDerivs"},{"shortname":"cc-nc-nd","fullname":"Creative Commons - No Commercial NoDerivs"},{"shortname":"cc-nc","fullname":"Creative Commons - No Commercial"},{"shortname":"cc-nc-sa","fullname":"Creative Commons - No Commercial ShareAlike"},{"shortname":"cc-sa","fullname":"Creative Commons - ShareAlike"}],"author":"Duck Tran","repositories":{"1":{"id":"1","name":"Embedded files","type":"areafiles","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_areafiles\/1569995285\/icon","supported_types":[],"return_types":1,"defaultreturntype":2,"sortorder":1},"2":{"id":"2","name":"Server files","type":"local","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_local\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":2},"3":{"id":"3","name":"Recent files","type":"recent","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_recent\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":3},"4":{"id":"4","name":"Upload a file","type":"upload","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_upload\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":4},"6":{"id":"6","name":"Private files","type":"user","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_user\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":6},"7":{"id":"7","name":"Wikimedia","type":"wikimedia","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_wikimedia\/1569995285\/icon","supported_types":[],"return_types":3,"defaultreturntype":2,"sortorder":7}},"externallink":true,"userprefs":{"recentrepository":"2","recentlicense":"allrightsreserved","recentviewmode":"3"},"accepted_types":[".3gp",".avi",".dv",".dif",".flv",".f4v",".mov",".movie",".mp4",".m4v",".mpeg",".mpe",".mpg",".ogv",".qt",".rmvb",".rv",".swf",".swfl",".webm",".wmv",".asf",".aac",".aif",".aiff",".aifc",".au",".flac",".m3u",".mp3",".m4a",".oga",".ogg",".ra",".ram",".rm",".wav",".wma"],"return_types":15,"context":{"id":52,"contextlevel":50,"instanceid":"7","path":"\/1\/3\/52","depth":"3","locked":false},"client_id":"5dc8028a8e16b","maxbytes":0,"areamaxbytes":-1,"env":"editor","itemid":841266077},"link":{"defaultlicense":"allrightsreserved","licenses":[{"shortname":"unknown","fullname":"Other"},{"shortname":"allrightsreserved","fullname":"All rights reserved"},{"shortname":"public","fullname":"Public domain"},{"shortname":"cc","fullname":"Creative Commons"},{"shortname":"cc-nd","fullname":"Creative Commons - NoDerivs"},{"shortname":"cc-nc-nd","fullname":"Creative Commons - No Commercial NoDerivs"},{"shortname":"cc-nc","fullname":"Creative Commons - No Commercial"},{"shortname":"cc-nc-sa","fullname":"Creative Commons - No Commercial ShareAlike"},{"shortname":"cc-sa","fullname":"Creative Commons - ShareAlike"}],"author":"Duck Tran","repositories":{"1":{"id":"1","name":"Embedded files","type":"areafiles","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_areafiles\/1569995285\/icon","supported_types":[],"return_types":1,"defaultreturntype":2,"sortorder":1},"2":{"id":"2","name":"Server files","type":"local","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_local\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":2},"3":{"id":"3","name":"Recent files","type":"recent","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_recent\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":3},"4":{"id":"4","name":"Upload a file","type":"upload","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_upload\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":4},"5":{"id":"5","name":"URL downloader","type":"url","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_url\/1569995285\/icon","supported_types":[".gif",".jpe",".jpeg",".jpg",".png",".svg",".svgz"],"return_types":3,"defaultreturntype":2,"sortorder":5},"6":{"id":"6","name":"Private files","type":"user","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_user\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":6},"7":{"id":"7","name":"Wikimedia","type":"wikimedia","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_wikimedia\/1569995285\/icon","supported_types":[],"return_types":3,"defaultreturntype":2,"sortorder":7}},"externallink":true,"userprefs":{"recentrepository":"2","recentlicense":"allrightsreserved","recentviewmode":"3"},"accepted_types":[],"return_types":15,"context":{"id":52,"contextlevel":50,"instanceid":"7","path":"\/1\/3\/52","depth":"3","locked":false},"client_id":"5dc8028a8e584","maxbytes":0,"areamaxbytes":-1,"env":"editor","itemid":841266077},"subtitle":{"defaultlicense":"allrightsreserved","licenses":[{"shortname":"unknown","fullname":"Other"},{"shortname":"allrightsreserved","fullname":"All rights reserved"},{"shortname":"public","fullname":"Public domain"},{"shortname":"cc","fullname":"Creative Commons"},{"shortname":"cc-nd","fullname":"Creative Commons - NoDerivs"},{"shortname":"cc-nc-nd","fullname":"Creative Commons - No Commercial NoDerivs"},{"shortname":"cc-nc","fullname":"Creative Commons - No Commercial"},{"shortname":"cc-nc-sa","fullname":"Creative Commons - No Commercial ShareAlike"},{"shortname":"cc-sa","fullname":"Creative Commons - ShareAlike"}],"author":"Duck Tran","repositories":{"1":{"id":"1","name":"Embedded files","type":"areafiles","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_areafiles\/1569995285\/icon","supported_types":[],"return_types":1,"defaultreturntype":2,"sortorder":1},"2":{"id":"2","name":"Server files","type":"local","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_local\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":2},"3":{"id":"3","name":"Recent files","type":"recent","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_recent\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":3},"4":{"id":"4","name":"Upload a file","type":"upload","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_upload\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":4},"6":{"id":"6","name":"Private files","type":"user","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_user\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":6},"7":{"id":"7","name":"Wikimedia","type":"wikimedia","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_wikimedia\/1569995285\/icon","supported_types":[],"return_types":3,"defaultreturntype":2,"sortorder":7}},"externallink":true,"userprefs":{"recentrepository":"2","recentlicense":"allrightsreserved","recentviewmode":"3"},"accepted_types":[".vtt"],"return_types":15,"context":{"id":52,"contextlevel":50,"instanceid":"7","path":"\/1\/3\/52","depth":"3","locked":false},"client_id":"5dc8028a8e98d","maxbytes":0,"areamaxbytes":-1,"env":"editor","itemid":841266077}},"plugins":[{"group":"collapse","plugins":[{"name":"collapse","params":[],"showgroups":"5"}]},{"group":"style1","plugins":[{"name":"title","params":[]},{"name":"bold","params":[]},{"name":"italic","params":[]}]},{"group":"list","plugins":[{"name":"unorderedlist","params":[]},{"name":"orderedlist","params":[]}]},{"group":"links","plugins":[{"name":"link","params":[]}]},{"group":"files","plugins":[{"name":"image","params":[]},{"name":"media","params":[],"langs":{"installed":[{"lang":" \u12a0\u121b\u122d\u129b \u200e(am)\u200e","code":"am","default":false},{"lang":"Afrikaans \u200e(af)\u200e","code":"af","default":false},{"lang":"Aragon\u00e9s \u200e(an)\u200e","code":"an","default":false},{"lang":"Aran\u00e9s \u200e(oc_es)\u200e","code":"oc_es","default":false},{"lang":"Asturianu \u200e(ast)\u200e","code":"ast","default":false},{"lang":"Az\u0259rbaycanca \u200e(az)\u200e","code":"az","default":false},{"lang":"Bahasa Melayu \u200e(ms)\u200e","code":"ms","default":false},{"lang":"Bamanankan \u200e(bm)\u200e","code":"bm","default":false},{"lang":"Bislama \u200e(bi)\u200e","code":"bi","default":false},{"lang":"Bosanski \u200e(bs)\u200e","code":"bs","default":false},{"lang":"Breizh \u200e(br)\u200e","code":"br","default":false},{"lang":"Catal\u00e0 \u200e(ca)\u200e","code":"ca","default":false},{"lang":"Catal\u00e0 (Valenci\u00e0) \u200e(ca_valencia)\u200e","code":"ca_valencia","default":false},{"lang":"Catal\u00e0 per a Workplace \u200e(ca_wp)\u200e","code":"ca_wp","default":false},{"lang":"\u010ce\u0161tina \u200e(cs)\u200e","code":"cs","default":false},{"lang":"Crnogorski \u200e(mis)\u200e","code":"mis","default":false},{"lang":"Croatian schools \u200e(hr_schools)\u200e","code":"hr_schools","default":false},{"lang":"Cymraeg \u200e(cy)\u200e","code":"cy","default":false},{"lang":"Dansk \u200e(da)\u200e","code":"da","default":false},{"lang":"Dansk (kursus) \u200e(da_kursus)\u200e","code":"da_kursus","default":false},{"lang":"Dansk Rum \u200e(da_rum)\u200e","code":"da_rum","default":false},{"lang":"Davvis\u00e1megiella \u200e(se)\u200e","code":"se","default":false},{"lang":"Deutsch - Du \u200e(de_du)\u200e","code":"de_du","default":false},{"lang":"Deutsch - Kids \u200e(de_kids)\u200e","code":"de_kids","default":false},{"lang":"Deutsch - Schweiz \u200e(de_ch)\u200e","code":"de_ch","default":false},{"lang":"Deutsch \u200e(de)\u200e","code":"de","default":false},{"lang":"Deutsch community \u200e(de_comm)\u200e","code":"de_comm","default":false},{"lang":"Deutsch f\u00fcr Arbeitsplatz \u200e(de_wp)\u200e","code":"de_wp","default":false},{"lang":"Dolnoserbski \u200e(dsb)\u200e","code":"dsb","default":false},{"lang":"Ebon \u200e(mh)\u200e","code":"mh","default":false},{"lang":"eesti \u200e(et)\u200e","code":"et","default":false},{"lang":"English - Pirate \u200e(en_ar)\u200e","code":"en_ar","default":false},{"lang":"English - United States \u200e(en_us)\u200e","code":"en_us","default":false},{"lang":"English \u200e(en)\u200e","code":"en","default":true},{"lang":"English for kids \u200e(en_kids)\u200e","code":"en_kids","default":false},{"lang":"English for Workplace \u200e(en_wp)\u200e","code":"en_wp","default":false},{"lang":"English US - K12 \u200e(en_us_k12)\u200e","code":"en_us_k12","default":false},{"lang":"Espa\u00f1ol - Colombia \u200e(es_co)\u200e","code":"es_co","default":false},{"lang":"Espa\u00f1ol - Internacional \u200e(es)\u200e","code":"es","default":false},{"lang":"Espa\u00f1ol - M\u00e9xico \u200e(es_mx)\u200e","code":"es_mx","default":false},{"lang":"Espa\u00f1ol - M\u00e9xico para ni\u00f1os \u200e(es_mx_kids)\u200e","code":"es_mx_kids","default":false},{"lang":"Espa\u00f1ol - Venezuela \u200e(es_ve)\u200e","code":"es_ve","default":false},{"lang":"Espa\u00f1ol para la Empresa \u200e(es_wp)\u200e","code":"es_wp","default":false},{"lang":"Esperanto \u200e(eo)\u200e","code":"eo","default":false},{"lang":"Euskara \u200e(eu)\u200e","code":"eu","default":false},{"lang":"\u00c8\u028begbe \u200e(ee)\u200e","code":"ee","default":false},{"lang":"Filipino \u200e(fil)\u200e","code":"fil","default":false},{"lang":"Finlandssvenska \u200e(sv_fi)\u200e","code":"sv_fi","default":false},{"lang":"F\u00f8royskt \u200e(fo)\u200e","code":"fo","default":false},{"lang":"Fran\u00e7ais - Canada \u200e(fr_ca)\u200e","code":"fr_ca","default":false},{"lang":"Fran\u00e7ais \u200e(fr)\u200e","code":"fr","default":false},{"lang":"Fran\u00e7ais pour Workplace \u200e(fr_wp)\u200e","code":"fr_wp","default":false},{"lang":"Gaeilge \u200e(ga)\u200e","code":"ga","default":false},{"lang":"G\u00e0idhlig \u200e(gd)\u200e","code":"gd","default":false},{"lang":"Galego \u200e(gl)\u200e","code":"gl","default":false},{"lang":"Gascon \u200e(oc_gsc)\u200e","code":"oc_gsc","default":false},{"lang":"Hausa \u200e(ha)\u200e","code":"ha","default":false},{"lang":"Hrvatski \u200e(hr)\u200e","code":"hr","default":false},{"lang":"\u02bb\u014clelo Hawai\u02bbi \u200e(haw)\u200e","code":"haw","default":false},{"lang":"Igbo \u200e(ig)\u200e","code":"ig","default":false},{"lang":"Indonesian \u200e(id)\u200e","code":"id","default":false},{"lang":"isiZulu \u200e(zu)\u200e","code":"zu","default":false},{"lang":"\u00cdslenska \u200e(is)\u200e","code":"is","default":false},{"lang":"Italiano \u200e(it)\u200e","code":"it","default":false},{"lang":"Italiano per Workplace \u200e(it_wp)\u200e","code":"it_wp","default":false},{"lang":"Japanese - kids \u200e(ja_kids)\u200e","code":"ja_kids","default":false},{"lang":"Kalaallisut \u200e(kl)\u200e","code":"kl","default":false},{"lang":"Kinyarwanda \u200e(rw)\u200e","code":"rw","default":false},{"lang":"Kiswahili \u200e(sw)\u200e","code":"sw","default":false},{"lang":"Krey\u00f2l Ayisyen \u200e(hat)\u200e","code":"hat","default":false},{"lang":"Kurmanji \u200e(kmr)\u200e","code":"kmr","default":false},{"lang":"Laotian \u200e(lo)\u200e","code":"lo","default":false},{"lang":"Latin \u200e(la)\u200e","code":"la","default":false},{"lang":"Latvie\u0161u \u200e(lv)\u200e","code":"lv","default":false},{"lang":"Lengadocian \u200e(oc_lnc)\u200e","code":"oc_lnc","default":false},{"lang":"L\u00ebtzebuergesch \u200e(lb)\u200e","code":"lb","default":false},{"lang":"Lietuvi\u0173 \u200e(lt)\u200e","code":"lt","default":false},{"lang":"Lithuanian (university) \u200e(lt_uni)\u200e","code":"lt_uni","default":false},{"lang":"Lulesamisk \u200e(smj)\u200e","code":"smj","default":false},{"lang":"magyar \u200e(hu)\u200e","code":"hu","default":false},{"lang":"Malagasy \u200e(mg)\u200e","code":"mg","default":false},{"lang":"M\u0101ori - Tainui \u200e(mi_tn)\u200e","code":"mi_tn","default":false},{"lang":"M\u0101ori - Waikato \u200e(mi_wwow)\u200e","code":"mi_wwow","default":false},{"lang":"M\u0101ori (Te Reo) \u200e(mi)\u200e","code":"mi","default":false},{"lang":"Mongolian \u200e(mn_mong)\u200e","code":"mn_mong","default":false},{"lang":"myanma bhasa \u200e(my)\u200e","code":"my","default":false},{"lang":"Nederlands \u200e(nl)\u200e","code":"nl","default":false},{"lang":"Norsk - bokm\u00e5l \u200e(no)\u200e","code":"no","default":false},{"lang":"Norsk - nynorsk \u200e(nn)\u200e","code":"nn","default":false},{"lang":"Norsk \u200e(no_gr)\u200e","code":"no_gr","default":false},{"lang":"Norsk Workplace \u200e(no_wp)\u200e","code":"no_wp","default":false},{"lang":"O'zbekcha \u200e(uz)\u200e","code":"uz","default":false},{"lang":"Pidgin \u200e(pcm)\u200e","code":"pcm","default":false},{"lang":"Polski \u200e(pl)\u200e","code":"pl","default":false},{"lang":"Portugu\u00eas - Brasil \u200e(pt_br)\u200e","code":"pt_br","default":false},{"lang":"Portugu\u00eas - Portugal \u200e(pt)\u200e","code":"pt","default":false},{"lang":"Rom\u00e2n\u0103 \u200e(ro)\u200e","code":"ro","default":false},{"lang":"Romansh Sursilvan \u200e(rm_surs)\u200e","code":"rm_surs","default":false},{"lang":"Samoan \u200e(sm)\u200e","code":"sm","default":false},{"lang":"Setswana \u200e(tn)\u200e","code":"tn","default":false},{"lang":"Shqip \u200e(sq)\u200e","code":"sq","default":false},{"lang":"Sloven\u010dina \u200e(sk)\u200e","code":"sk","default":false},{"lang":"Sloven\u0161\u010dina \u200e(sl)\u200e","code":"sl","default":false},{"lang":"Soomaali \u200e(so)\u200e","code":"so","default":false},{"lang":"S\u00f8rsamisk \u200e(sma)\u200e","code":"sma","default":false},{"lang":"Srpski \u200e(sr_lt)\u200e","code":"sr_lt","default":false},{"lang":"Suomi \u200e(fi)\u200e","code":"fi","default":false},{"lang":"Suomi+ \u200e(fi_co)\u200e","code":"fi_co","default":false},{"lang":"Svenska \u200e(sv)\u200e","code":"sv","default":false},{"lang":"Tagalog \u200e(tl)\u200e","code":"tl","default":false},{"lang":"Tamil \u200e(ta)\u200e","code":"ta","default":false},{"lang":"Taqbaylit \u200e(kab)\u200e","code":"kab","default":false},{"lang":"Thai \u200e(th)\u200e","code":"th","default":false},{"lang":"Tongan \u200e(to)\u200e","code":"to","default":false},{"lang":"T\u00fcrk\u00e7e \u200e(tr)\u200e","code":"tr","default":false},{"lang":"Turkmen \u200e(tk)\u200e","code":"tk","default":false},{"lang":"Uyghur - latin \u200e(ug_lt)\u200e","code":"ug_lt","default":false},{"lang":"VakaViti \u200e(fj)\u200e","code":"fj","default":false},{"lang":"Valenci\u00e0_RACV \u200e(ca_valencia_racv)\u200e","code":"ca_valencia_racv","default":false},{"lang":"Vietnamese \u200e(vi)\u200e","code":"vi","default":false},{"lang":"Wolof \u200e(wo)\u200e","code":"wo","default":false},{"lang":"Workplace \u00een limba rom\u00e2n\u0103 \u200e(ro_wp)\u200e","code":"ro_wp","default":false},{"lang":"Workplace \u7b80\u4f53\u4e2d\u6587\u7248 \u200e(zh_cn_wp)\u200e","code":"zh_cn_wp","default":false},{"lang":"Workplace \u7e41\u9ad4\u4e2d\u6587\u7248 \u200e(zh_tw_wp)\u200e","code":"zh_tw_wp","default":false},{"lang":"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac \u200e(el)\u200e","code":"el","default":false},{"lang":"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac \u03b3\u03b9\u03b1 \u03c0\u03b1\u03b9\u03b4\u03b9\u03ac \u200e(el_kids)\u200e","code":"el_kids","default":false},{"lang":"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac \u03b3\u03b9\u03b1 \u03c3\u03c7\u03bf\u03bb\u03ad\u03c2 \u200e(el_uni)\u200e","code":"el_uni","default":false},{"lang":"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac \u03b3\u03b9\u03b1 \u03c7\u03ce\u03c1\u03bf\u03c5\u03c2 \u03b5\u03c1\u03b3\u03b1\u03c3\u03af\u03b1\u03c2 \u200e(el_wp)\u200e","code":"el_wp","default":false},{"lang":"\u0411\u0430\u0448\u04a1\u043e\u0440\u0442 \u0442\u0435\u043b\u0435 \u200e(ba)\u200e","code":"ba","default":false},{"lang":"\u0411\u0435\u043b\u0430\u0440\u0443\u0441\u043a\u0430\u044f \u200e(be)\u200e","code":"be","default":false},{"lang":"\u0411\u044a\u043b\u0433\u0430\u0440\u0441\u043a\u0438 \u200e(bg)\u200e","code":"bg","default":false},{"lang":"\u041a\u044b\u0440\u0433\u044b\u0437\u0447\u0430 \u200e(ky)\u200e","code":"ky","default":false},{"lang":"\u049a\u0430\u0437\u0430\u049b\u0448\u0430 \u200e(kk)\u200e","code":"kk","default":false},{"lang":"\u041c\u0430\u043a\u0435\u0434\u043e\u043d\u0441\u043a\u0438 \u200e(mk)\u200e","code":"mk","default":false},{"lang":"\u041c\u043e\u043d\u0433\u043e\u043b \u200e(mn)\u200e","code":"mn","default":false},{"lang":"\u0420\u0443\u0441\u0441\u043a\u0438\u0439 \u200e(ru)\u200e","code":"ru","default":false},{"lang":"\u0421\u0440\u043f\u0441\u043a\u0438 \u200e(sr_cr)\u200e","code":"sr_cr","default":false},{"lang":"\u0442\u0430\u0442\u0430\u0440 \u0442\u0435\u043b\u0435 \u200e(tt)\u200e","code":"tt","default":false},{"lang":"\u0422\u043e\u04b7\u0438\u043a\u04e3 \u200e(tg)\u200e","code":"tg","default":false},{"lang":"\u0423\u043a\u0440\u0430\u0457\u043d\u0441\u044c\u043a\u0430 \u200e(uk)\u200e","code":"uk","default":false},{"lang":"\u10e5\u10d0\u10e0\u10d7\u10e3\u10da\u10d8 \u200e(ka)\u200e","code":"ka","default":false},{"lang":"\u0540\u0561\u0575\u0565\u0580\u0565\u0576 \u200e(hy)\u200e","code":"hy","default":false},{"lang":"\u05e2\u05d1\u05e8\u05d9\u05ea \u200e(he)\u200e","code":"he","default":false},{"lang":"\u05e2\u05d1\u05e8\u05d9\u05ea \u05d1\u05ea\u05d9\u05be\u05e1\u05e4\u05e8 \u200e(he_kids)\u200e","code":"he_kids","default":false},{"lang":"\u05e2\u05d1\u05e8\u05d9\u05ea \u05e2\u05d1\u05d5\u05e8 Workplace \u200e(he_wp)\u200e","code":"he_wp","default":false},{"lang":"\u0626\u06c7\u064a\u063a\u06c7\u0631\u0686\u06d5 \u200e(ug_ug)\u200e","code":"ug_ug","default":false},{"lang":"\u0627\u0631\u062f\u0648 \u200e(ur)\u200e","code":"ur","default":false},{"lang":"\u067e\u069a\u062a\u0648 \u200e(ps)\u200e","code":"ps","default":false},{"lang":"\u0633\u06c6\u0631\u0627\u0646\u06cc \u200e(ckb)\u200e","code":"ckb","default":false},{"lang":"\u0639\u0631\u0628\u064a \u200e(ar)\u200e","code":"ar","default":false},{"lang":"\u0641\u0627\u0631\u0633\u06cc \u200e(fa)\u200e","code":"fa","default":false},{"lang":"\u078b\u07a8\u0788\u07ac\u0780\u07a8 \u200e(dv)\u200e","code":"dv","default":false},{"lang":"\u2d5c\u2d30\u2d4e\u2d30\u2d63\u2d49\u2d56\u2d5c \u200e(zgh)\u200e","code":"zgh","default":false},{"lang":"\u1275\u130d\u122d\u129b \u200e(ti)\u200e","code":"ti","default":false},{"lang":"\u0928\u0947\u092a\u093e\u0932\u0940 \u200e(ne)\u200e","code":"ne","default":false},{"lang":"\u092e\u0930\u093e\u0920\u0940 \u200e(mr)\u200e","code":"mr","default":false},{"lang":"\u0935\u0930\u094d\u0915\u092a\u094d\u0932\u0947\u0938  \u0915\u0947 \u0932\u093f\u090f  \u0939\u093f\u0902\u0926\u0940 \u200e(hi_wp)\u200e","code":"hi_wp","default":false},{"lang":"\u0939\u093f\u0902\u0926\u0940 \u200e(hi)\u200e","code":"hi","default":false},{"lang":"\u09ac\u09be\u0982\u09b2\u09be \u200e(bn)\u200e","code":"bn","default":false},{"lang":"\u0a2a\u0a70\u0a1c\u0a3e\u0a2c\u0a40 \u200e(pan)\u200e","code":"pan","default":false},{"lang":"\u0a97\u0ac1\u0a9c\u0ab0\u0abe\u0aa4\u0ac0 \u200e(gu)\u200e","code":"gu","default":false},{"lang":"\u0b13\u0b21\u0b3c\u0b3f\u0b06 \u200e(or)\u200e","code":"or","default":false},{"lang":"\u0ba4\u0bae\u0bbf\u0bb4\u0bcd \u200e(ta_lk)\u200e","code":"ta_lk","default":false},{"lang":"\u0c24\u0c46\u0c32\u0c41\u0c17\u0c41  \u200e(te)\u200e","code":"te","default":false},{"lang":"\u0c95\u0ca8\u0ccd\u0ca8\u0ca1 \u200e(kn)\u200e","code":"kn","default":false},{"lang":"\u0d2e\u0d32\u0d2f\u0d3e\u0d33\u0d02 \u200e(ml)\u200e","code":"ml","default":false},{"lang":"\u0dc3\u0dd2\u0d82\u0dc4\u0dbd \u200e(si)\u200e","code":"si","default":false},{"lang":"\u0f56\u0f7c\u0f51\u0f0b\u0f61\u0f72\u0f42 \u200e(xct)\u200e","code":"xct","default":false},{"lang":"\u0f62\u0fab\u0f7c\u0f44\u0f0b\u0f41 \u200e(dz)\u200e","code":"dz","default":false},{"lang":"\u1781\u17d2\u1798\u17c2\u179a \u200e(km)\u200e","code":"km","default":false},{"lang":"\ud55c\uad6d\uc5b4 \u200e(ko)\u200e","code":"ko","default":false},{"lang":"\u65e5\u672c\u8a9e \u200e(ja)\u200e","code":"ja","default":false},{"lang":"\u6b63\u9ad4\u4e2d\u6587 \u200e(zh_tw)\u200e","code":"zh_tw","default":false},{"lang":"\u7b80\u4f53\u4e2d\u6587 \u200e(zh_cn)\u200e","code":"zh_cn","default":false}],"available":[{"lang":"Afar \u200e(aa)\u200e","code":"aa"},{"lang":"Abkhazian \u200e(ab)\u200e","code":"ab"},{"lang":"Avestan \u200e(ae)\u200e","code":"ae"},{"lang":"Afrikaans \u200e(af)\u200e","code":"af"},{"lang":"Akan \u200e(ak)\u200e","code":"ak"},{"lang":"Amharic \u200e(am)\u200e","code":"am"},{"lang":"Aragonese \u200e(an)\u200e","code":"an"},{"lang":"Arabic \u200e(ar)\u200e","code":"ar"},{"lang":"Assamese \u200e(as)\u200e","code":"as"},{"lang":"Avaric \u200e(av)\u200e","code":"av"},{"lang":"Aymara \u200e(ay)\u200e","code":"ay"},{"lang":"Azerbaijani \u200e(az)\u200e","code":"az"},{"lang":"Bashkir \u200e(ba)\u200e","code":"ba"},{"lang":"Belarusian \u200e(be)\u200e","code":"be"},{"lang":"Bulgarian \u200e(bg)\u200e","code":"bg"},{"lang":"Bihari languages \u200e(bh)\u200e","code":"bh"},{"lang":"Bislama \u200e(bi)\u200e","code":"bi"},{"lang":"Bambara \u200e(bm)\u200e","code":"bm"},{"lang":"Bengali \u200e(bn)\u200e","code":"bn"},{"lang":"Tibetan \u200e(bo)\u200e","code":"bo"},{"lang":"Breton \u200e(br)\u200e","code":"br"},{"lang":"Bosnian \u200e(bs)\u200e","code":"bs"},{"lang":"Catalan; Valencian \u200e(ca)\u200e","code":"ca"},{"lang":"Chechen \u200e(ce)\u200e","code":"ce"},{"lang":"Chamorro \u200e(ch)\u200e","code":"ch"},{"lang":"Corsican \u200e(co)\u200e","code":"co"},{"lang":"Cree \u200e(cr)\u200e","code":"cr"},{"lang":"Czech \u200e(cs)\u200e","code":"cs"},{"lang":"Church Slavic; Old Slavonic \u200e(cu)\u200e","code":"cu"},{"lang":"Chuvash \u200e(cv)\u200e","code":"cv"},{"lang":"Welsh \u200e(cy)\u200e","code":"cy"},{"lang":"Danish \u200e(da)\u200e","code":"da"},{"lang":"German \u200e(de)\u200e","code":"de"},{"lang":"Divehi; Dhivehi; Maldivian \u200e(dv)\u200e","code":"dv"},{"lang":"Dzongkha \u200e(dz)\u200e","code":"dz"},{"lang":"Ewe \u200e(ee)\u200e","code":"ee"},{"lang":"Greek, Modern (1453-) \u200e(el)\u200e","code":"el"},{"lang":"English \u200e(en)\u200e","code":"en"},{"lang":"Esperanto \u200e(eo)\u200e","code":"eo"},{"lang":"Spanish; Castilian \u200e(es)\u200e","code":"es"},{"lang":"Estonian \u200e(et)\u200e","code":"et"},{"lang":"Basque \u200e(eu)\u200e","code":"eu"},{"lang":"Persian \u200e(fa)\u200e","code":"fa"},{"lang":"Fulah \u200e(ff)\u200e","code":"ff"},{"lang":"Finnish \u200e(fi)\u200e","code":"fi"},{"lang":"Fijian \u200e(fj)\u200e","code":"fj"},{"lang":"Faroese \u200e(fo)\u200e","code":"fo"},{"lang":"French \u200e(fr)\u200e","code":"fr"},{"lang":"Western Frisian \u200e(fy)\u200e","code":"fy"},{"lang":"Irish \u200e(ga)\u200e","code":"ga"},{"lang":"Gaelic; Scottish Gaelic \u200e(gd)\u200e","code":"gd"},{"lang":"Galician \u200e(gl)\u200e","code":"gl"},{"lang":"Guarani \u200e(gn)\u200e","code":"gn"},{"lang":"Gujarati \u200e(gu)\u200e","code":"gu"},{"lang":"Manx \u200e(gv)\u200e","code":"gv"},{"lang":"Hausa \u200e(ha)\u200e","code":"ha"},{"lang":"Hebrew \u200e(he)\u200e","code":"he"},{"lang":"Hindi \u200e(hi)\u200e","code":"hi"},{"lang":"Hiri Motu \u200e(ho)\u200e","code":"ho"},{"lang":"Croatian \u200e(hr)\u200e","code":"hr"},{"lang":"Haitian; Haitian Creole \u200e(ht)\u200e","code":"ht"},{"lang":"Hungarian \u200e(hu)\u200e","code":"hu"},{"lang":"Armenian \u200e(hy)\u200e","code":"hy"},{"lang":"Herero \u200e(hz)\u200e","code":"hz"},{"lang":"Interlingua (IALA) \u200e(ia)\u200e","code":"ia"},{"lang":"Indonesian \u200e(id)\u200e","code":"id"},{"lang":"Interlingue; Occidental \u200e(ie)\u200e","code":"ie"},{"lang":"Igbo \u200e(ig)\u200e","code":"ig"},{"lang":"Sichuan Yi; Nuosu \u200e(ii)\u200e","code":"ii"},{"lang":"Inupiaq \u200e(ik)\u200e","code":"ik"},{"lang":"Ido \u200e(io)\u200e","code":"io"},{"lang":"Icelandic \u200e(is)\u200e","code":"is"},{"lang":"Italian \u200e(it)\u200e","code":"it"},{"lang":"Inuktitut \u200e(iu)\u200e","code":"iu"},{"lang":"Japanese \u200e(ja)\u200e","code":"ja"},{"lang":"Javanese \u200e(jv)\u200e","code":"jv"},{"lang":"Georgian \u200e(ka)\u200e","code":"ka"},{"lang":"Kongo \u200e(kg)\u200e","code":"kg"},{"lang":"Kikuyu; Gikuyu \u200e(ki)\u200e","code":"ki"},{"lang":"Kuanyama; Kwanyama \u200e(kj)\u200e","code":"kj"},{"lang":"Kazakh \u200e(kk)\u200e","code":"kk"},{"lang":"Kalaallisut; Greenlandic \u200e(kl)\u200e","code":"kl"},{"lang":"Central Khmer \u200e(km)\u200e","code":"km"},{"lang":"Kannada \u200e(kn)\u200e","code":"kn"},{"lang":"Korean \u200e(ko)\u200e","code":"ko"},{"lang":"Kanuri \u200e(kr)\u200e","code":"kr"},{"lang":"Kashmiri \u200e(ks)\u200e","code":"ks"},{"lang":"Kurdish \u200e(ku)\u200e","code":"ku"},{"lang":"Komi \u200e(kv)\u200e","code":"kv"},{"lang":"Cornish \u200e(kw)\u200e","code":"kw"},{"lang":"Kirghiz; Kyrgyz \u200e(ky)\u200e","code":"ky"},{"lang":"Latin \u200e(la)\u200e","code":"la"},{"lang":"Luxembourgish \u200e(lb)\u200e","code":"lb"},{"lang":"Ganda \u200e(lg)\u200e","code":"lg"},{"lang":"Limburgan \u200e(li)\u200e","code":"li"},{"lang":"Lingala \u200e(ln)\u200e","code":"ln"},{"lang":"Lao \u200e(lo)\u200e","code":"lo"},{"lang":"Lithuanian \u200e(lt)\u200e","code":"lt"},{"lang":"Luba-Katanga \u200e(lu)\u200e","code":"lu"},{"lang":"Latvian \u200e(lv)\u200e","code":"lv"},{"lang":"Malagasy \u200e(mg)\u200e","code":"mg"},{"lang":"Marshallese \u200e(mh)\u200e","code":"mh"},{"lang":"Maori \u200e(mi)\u200e","code":"mi"},{"lang":"Macedonian \u200e(mk)\u200e","code":"mk"},{"lang":"Malayalam \u200e(ml)\u200e","code":"ml"},{"lang":"Mongolian \u200e(mn)\u200e","code":"mn"},{"lang":"Marathi \u200e(mr)\u200e","code":"mr"},{"lang":"Malay \u200e(ms)\u200e","code":"ms"},{"lang":"Maltese \u200e(mt)\u200e","code":"mt"},{"lang":"Burmese \u200e(my)\u200e","code":"my"},{"lang":"Nauru \u200e(na)\u200e","code":"na"},{"lang":"Norwegian Bokm\u00e5l \u200e(nb)\u200e","code":"nb"},{"lang":"Ndebele, North \u200e(nd)\u200e","code":"nd"},{"lang":"Nepali \u200e(ne)\u200e","code":"ne"},{"lang":"Ndonga \u200e(ng)\u200e","code":"ng"},{"lang":"Dutch; Flemish \u200e(nl)\u200e","code":"nl"},{"lang":"Norwegian Nynorsk \u200e(nn)\u200e","code":"nn"},{"lang":"Norwegian \u200e(no)\u200e","code":"no"},{"lang":"Ndebele, South \u200e(nr)\u200e","code":"nr"},{"lang":"Navajo; Navaho \u200e(nv)\u200e","code":"nv"},{"lang":"Chichewa \u200e(ny)\u200e","code":"ny"},{"lang":"Occitan (post 1500); Proven\u00e7al \u200e(oc)\u200e","code":"oc"},{"lang":"Ojibwa \u200e(oj)\u200e","code":"oj"},{"lang":"Oromo \u200e(om)\u200e","code":"om"},{"lang":"Oriya \u200e(or)\u200e","code":"or"},{"lang":"Ossetian; Ossetic \u200e(os)\u200e","code":"os"},{"lang":"Panjabi; Punjabi \u200e(pa)\u200e","code":"pa"},{"lang":"Pali \u200e(pi)\u200e","code":"pi"},{"lang":"Polish \u200e(pl)\u200e","code":"pl"},{"lang":"Pushto; Pashto \u200e(ps)\u200e","code":"ps"},{"lang":"Portuguese \u200e(pt)\u200e","code":"pt"},{"lang":"Quechua \u200e(qu)\u200e","code":"qu"},{"lang":"Romansh \u200e(rm)\u200e","code":"rm"},{"lang":"Rundi \u200e(rn)\u200e","code":"rn"},{"lang":"Romanian; Moldavian; Moldovan \u200e(ro)\u200e","code":"ro"},{"lang":"Russian \u200e(ru)\u200e","code":"ru"},{"lang":"Kinyarwanda \u200e(rw)\u200e","code":"rw"},{"lang":"Sanskrit \u200e(sa)\u200e","code":"sa"},{"lang":"Sardinian \u200e(sc)\u200e","code":"sc"},{"lang":"Sindhi \u200e(sd)\u200e","code":"sd"},{"lang":"Northern Sami \u200e(se)\u200e","code":"se"},{"lang":"Sango \u200e(sg)\u200e","code":"sg"},{"lang":"Sinhala; Sinhalese \u200e(si)\u200e","code":"si"},{"lang":"Slovak \u200e(sk)\u200e","code":"sk"},{"lang":"Slovenian \u200e(sl)\u200e","code":"sl"},{"lang":"Samoan \u200e(sm)\u200e","code":"sm"},{"lang":"Shona \u200e(sn)\u200e","code":"sn"},{"lang":"Somali \u200e(so)\u200e","code":"so"},{"lang":"Albanian \u200e(sq)\u200e","code":"sq"},{"lang":"Serbian \u200e(sr)\u200e","code":"sr"},{"lang":"Swati \u200e(ss)\u200e","code":"ss"},{"lang":"Sotho, Southern \u200e(st)\u200e","code":"st"},{"lang":"Sundanese \u200e(su)\u200e","code":"su"},{"lang":"Swedish \u200e(sv)\u200e","code":"sv"},{"lang":"Swahili \u200e(sw)\u200e","code":"sw"},{"lang":"Tamil \u200e(ta)\u200e","code":"ta"},{"lang":"Telugu \u200e(te)\u200e","code":"te"},{"lang":"Tajik \u200e(tg)\u200e","code":"tg"},{"lang":"Thai \u200e(th)\u200e","code":"th"},{"lang":"Tigrinya \u200e(ti)\u200e","code":"ti"},{"lang":"Turkmen \u200e(tk)\u200e","code":"tk"},{"lang":"Tagalog \u200e(tl)\u200e","code":"tl"},{"lang":"Tswana \u200e(tn)\u200e","code":"tn"},{"lang":"Tonga (Tonga Islands) \u200e(to)\u200e","code":"to"},{"lang":"Turkish \u200e(tr)\u200e","code":"tr"},{"lang":"Tsonga \u200e(ts)\u200e","code":"ts"},{"lang":"Tatar \u200e(tt)\u200e","code":"tt"},{"lang":"Twi \u200e(tw)\u200e","code":"tw"},{"lang":"Tahitian \u200e(ty)\u200e","code":"ty"},{"lang":"Uighur; Uyghur \u200e(ug)\u200e","code":"ug"},{"lang":"Ukrainian \u200e(uk)\u200e","code":"uk"},{"lang":"Urdu \u200e(ur)\u200e","code":"ur"},{"lang":"Uzbek \u200e(uz)\u200e","code":"uz"},{"lang":"Venda \u200e(ve)\u200e","code":"ve"},{"lang":"Vietnamese \u200e(vi)\u200e","code":"vi"},{"lang":"Volap\u00fck \u200e(vo)\u200e","code":"vo"},{"lang":"Walloon \u200e(wa)\u200e","code":"wa"},{"lang":"Wolof \u200e(wo)\u200e","code":"wo"},{"lang":"Xhosa \u200e(xh)\u200e","code":"xh"},{"lang":"Yiddish \u200e(yi)\u200e","code":"yi"},{"lang":"Yoruba \u200e(yo)\u200e","code":"yo"},{"lang":"Zhuang; Chuang \u200e(za)\u200e","code":"za"},{"lang":"Chinese \u200e(zh)\u200e","code":"zh"},{"lang":"Zulu \u200e(zu)\u200e","code":"zu"}]},"help":{"addsource":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;It is recommended that an alternative media source is provided, since desktop and mobile browsers vary in which file formats they support.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Add alternative source\" aria-label=\"Help with Add alternative source\"><\/i>\n<\/a>","tracks":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Subtitles, captions, chapters and descriptions can be added via a WebVTT (Web Video Text Tracks) format file. Track labels will be shown in the selection drop-down menu. For each type of track, any track set as default will be pre-selected at the start of the video.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Subtitles and captions\" aria-label=\"Help with Subtitles and captions\"><\/i>\n<\/a>","subtitles":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Subtitles may be used to provide a transcription or translation of the dialogue.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Subtitles\" aria-label=\"Help with Subtitles\"><\/i>\n<\/a>","captions":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Captions may be used to describe everything happening in the track, including non-verbal sounds such as a phone ringing.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Captions\" aria-label=\"Help with Captions\"><\/i>\n<\/a>","descriptions":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Audio descriptions may be used to provide a narration which explains visual details not apparent from the audio alone.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Descriptions\" aria-label=\"Help with Descriptions\"><\/i>\n<\/a>","chapters":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Chapter titles may be provided for use in navigating the media resource.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Chapters\" aria-label=\"Help with Chapters\"><\/i>\n<\/a>","metadata":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Metadata tracks, for use from a script, may be used only if the player supports metadata.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Metadata\" aria-label=\"Help with Metadata\"><\/i>\n<\/a>"}},{"name":"recordrtc","params":[],"contextid":52,"sesskey":"DyXZbRiLhr","allowedtypes":"both","audiobitrate":"128000","videobitrate":"2500000","timelimit":"120","audiortcicon":"i\/audiortc","videortcicon":"i\/videortc","maxrecsize":1073741824},{"name":"managefiles","params":[],"disabled":false,"area":{"context":52,"areamaxbytes":-1,"maxbytes":0,"subdirs":0,"return_types":15,"removeorphaneddrafts":false},"usercontext":5}]},{"group":"style2","plugins":[{"name":"underline","params":[]},{"name":"strike","params":[]},{"name":"subscript","params":[]},{"name":"superscript","params":[]}]},{"group":"align","plugins":[{"name":"align","params":[]}]},{"group":"indent","plugins":[{"name":"indent","params":[]}]},{"group":"insert","plugins":[{"name":"equation","params":[],"texfilteractive":true,"contextid":52,"library":{"group1":{"groupname":"librarygroup1","elements":"\n\\cdot\n\\times\n\\ast\n\\div\n\\diamond\n\\pm\n\\mp\n\\oplus\n\\ominus\n\\otimes\n\\oslash\n\\odot\n\\circ\n\\bullet\n\\asymp\n\\equiv\n\\subseteq\n\\supseteq\n\\leq\n\\geq\n\\preceq\n\\succeq\n\\sim\n\\simeq\n\\approx\n\\subset\n\\supset\n\\ll\n\\gg\n\\prec\n\\succ\n\\infty\n\\in\n\\ni\n\\forall\n\\exists\n\\neq\n"},"group2":{"groupname":"librarygroup2","elements":"\n\\leftarrow\n\\rightarrow\n\\uparrow\n\\downarrow\n\\leftrightarrow\n\\nearrow\n\\searrow\n\\swarrow\n\\nwarrow\n\\Leftarrow\n\\Rightarrow\n\\Uparrow\n\\Downarrow\n\\Leftrightarrow\n"},"group3":{"groupname":"librarygroup3","elements":"\n\\alpha\n\\beta\n\\gamma\n\\delta\n\\epsilon\n\\zeta\n\\eta\n\\theta\n\\iota\n\\kappa\n\\lambda\n\\mu\n\\nu\n\\xi\n\\pi\n\\rho\n\\sigma\n\\tau\n\\upsilon\n\\phi\n\\chi\n\\psi\n\\omega\n\\Gamma\n\\Delta\n\\Theta\n\\Lambda\n\\Xi\n\\Pi\n\\Sigma\n\\Upsilon\n\\Phi\n\\Psi\n\\Omega\n"},"group4":{"groupname":"librarygroup4","elements":"\n\\sum{a,b}\n\\sqrt[a]{b+c}\n\\int_{a}^{b}{c}\n\\iint_{a}^{b}{c}\n\\iiint_{a}^{b}{c}\n\\oint{a}\n(a)\n[a]\n\\lbrace{a}\\rbrace\n\\left| \\begin{matrix} a_1 & a_2 \\ a_3 & a_4 \\end{matrix} \\right|\n\\frac{a}{b+c}\n\\vec{a}\n\\binom {a} {b}\n{a \\brack b}\n{a \\brace b}\n"}},"texdocsurl":"http:\/\/docs.moodle.org\/37\/en\/Using_TeX_Notation"},{"name":"charmap","params":[]},{"name":"table","params":[],"allowBorders":false,"allowWidth":false,"allowBackgroundColour":false},{"name":"clear","params":[]}]},{"group":"undo","plugins":[{"name":"undo","params":[]}]},{"group":"accessibility","plugins":[{"name":"accessibilitychecker","params":[]},{"name":"accessibilityhelper","params":[]}]},{"group":"other","plugins":[{"name":"html","params":[]}]}],"pageHash":"2a434d39b60e9093356609ce9c50e1f359b65473"});
});
Y.use("moodle-filter_mathjaxloader-loader",function() {M.filter_mathjaxloader.typeset();
});
Y.use("moodle-editor_atto-editor","moodle-atto_collapse-button","moodle-atto_title-button","moodle-atto_bold-button","moodle-atto_italic-button","moodle-atto_unorderedlist-button","moodle-atto_orderedlist-button","moodle-atto_link-button","moodle-atto_image-button","moodle-atto_media-button","moodle-atto_recordrtc-button","moodle-atto_managefiles-button","moodle-atto_underline-button","moodle-atto_strike-button","moodle-atto_subscript-button","moodle-atto_superscript-button","moodle-atto_align-button","moodle-atto_indent-button","moodle-atto_equation-button","moodle-atto_charmap-button","moodle-atto_table-button","moodle-atto_clear-button","moodle-atto_undo-button","moodle-atto_accessibilitychecker-button","moodle-atto_accessibilityhelper-button","moodle-atto_html-button",function() {Y.M.editor_atto.Editor.init({"elementid":"id_feedbacktext_1","content_css":"https:\/\/cacbank.moodlecloud.com\/theme\/styles.php\/boost\/1569995285_1569995291\/editor","contextid":52,"autosaveEnabled":true,"autosaveFrequency":"60","language":"en","directionality":"ltr","filepickeroptions":{"image":{"defaultlicense":"allrightsreserved","licenses":[{"shortname":"unknown","fullname":"Other"},{"shortname":"allrightsreserved","fullname":"All rights reserved"},{"shortname":"public","fullname":"Public domain"},{"shortname":"cc","fullname":"Creative Commons"},{"shortname":"cc-nd","fullname":"Creative Commons - NoDerivs"},{"shortname":"cc-nc-nd","fullname":"Creative Commons - No Commercial NoDerivs"},{"shortname":"cc-nc","fullname":"Creative Commons - No Commercial"},{"shortname":"cc-nc-sa","fullname":"Creative Commons - No Commercial ShareAlike"},{"shortname":"cc-sa","fullname":"Creative Commons - ShareAlike"}],"author":"Duck Tran","repositories":{"1":{"id":"1","name":"Embedded files","type":"areafiles","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_areafiles\/1569995285\/icon","supported_types":[],"return_types":1,"defaultreturntype":2,"sortorder":1},"2":{"id":"2","name":"Server files","type":"local","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_local\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":2},"3":{"id":"3","name":"Recent files","type":"recent","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_recent\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":3},"4":{"id":"4","name":"Upload a file","type":"upload","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_upload\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":4},"5":{"id":"5","name":"URL downloader","type":"url","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_url\/1569995285\/icon","supported_types":[".gif",".jpe",".jpeg",".jpg",".png",".svg",".svgz"],"return_types":3,"defaultreturntype":2,"sortorder":5},"6":{"id":"6","name":"Private files","type":"user","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_user\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":6},"7":{"id":"7","name":"Wikimedia","type":"wikimedia","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_wikimedia\/1569995285\/icon","supported_types":[],"return_types":3,"defaultreturntype":2,"sortorder":7}},"externallink":true,"userprefs":{"recentrepository":"2","recentlicense":"allrightsreserved","recentviewmode":"3"},"accepted_types":[".gif",".jpe",".jpeg",".jpg",".png",".svg",".svgz"],"return_types":15,"context":{"id":52,"contextlevel":50,"instanceid":"7","path":"\/1\/3\/52","depth":"3","locked":false},"client_id":"5dc8028a90036","maxbytes":0,"areamaxbytes":-1,"env":"editor","itemid":320008600},"media":{"defaultlicense":"allrightsreserved","licenses":[{"shortname":"unknown","fullname":"Other"},{"shortname":"allrightsreserved","fullname":"All rights reserved"},{"shortname":"public","fullname":"Public domain"},{"shortname":"cc","fullname":"Creative Commons"},{"shortname":"cc-nd","fullname":"Creative Commons - NoDerivs"},{"shortname":"cc-nc-nd","fullname":"Creative Commons - No Commercial NoDerivs"},{"shortname":"cc-nc","fullname":"Creative Commons - No Commercial"},{"shortname":"cc-nc-sa","fullname":"Creative Commons - No Commercial ShareAlike"},{"shortname":"cc-sa","fullname":"Creative Commons - ShareAlike"}],"author":"Duck Tran","repositories":{"1":{"id":"1","name":"Embedded files","type":"areafiles","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_areafiles\/1569995285\/icon","supported_types":[],"return_types":1,"defaultreturntype":2,"sortorder":1},"2":{"id":"2","name":"Server files","type":"local","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_local\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":2},"3":{"id":"3","name":"Recent files","type":"recent","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_recent\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":3},"4":{"id":"4","name":"Upload a file","type":"upload","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_upload\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":4},"6":{"id":"6","name":"Private files","type":"user","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_user\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":6},"7":{"id":"7","name":"Wikimedia","type":"wikimedia","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_wikimedia\/1569995285\/icon","supported_types":[],"return_types":3,"defaultreturntype":2,"sortorder":7}},"externallink":true,"userprefs":{"recentrepository":"2","recentlicense":"allrightsreserved","recentviewmode":"3"},"accepted_types":[".3gp",".avi",".dv",".dif",".flv",".f4v",".mov",".movie",".mp4",".m4v",".mpeg",".mpe",".mpg",".ogv",".qt",".rmvb",".rv",".swf",".swfl",".webm",".wmv",".asf",".aac",".aif",".aiff",".aifc",".au",".flac",".m3u",".mp3",".m4a",".oga",".ogg",".ra",".ram",".rm",".wav",".wma"],"return_types":15,"context":{"id":52,"contextlevel":50,"instanceid":"7","path":"\/1\/3\/52","depth":"3","locked":false},"client_id":"5dc8028a9043c","maxbytes":0,"areamaxbytes":-1,"env":"editor","itemid":320008600},"link":{"defaultlicense":"allrightsreserved","licenses":[{"shortname":"unknown","fullname":"Other"},{"shortname":"allrightsreserved","fullname":"All rights reserved"},{"shortname":"public","fullname":"Public domain"},{"shortname":"cc","fullname":"Creative Commons"},{"shortname":"cc-nd","fullname":"Creative Commons - NoDerivs"},{"shortname":"cc-nc-nd","fullname":"Creative Commons - No Commercial NoDerivs"},{"shortname":"cc-nc","fullname":"Creative Commons - No Commercial"},{"shortname":"cc-nc-sa","fullname":"Creative Commons - No Commercial ShareAlike"},{"shortname":"cc-sa","fullname":"Creative Commons - ShareAlike"}],"author":"Duck Tran","repositories":{"1":{"id":"1","name":"Embedded files","type":"areafiles","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_areafiles\/1569995285\/icon","supported_types":[],"return_types":1,"defaultreturntype":2,"sortorder":1},"2":{"id":"2","name":"Server files","type":"local","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_local\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":2},"3":{"id":"3","name":"Recent files","type":"recent","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_recent\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":3},"4":{"id":"4","name":"Upload a file","type":"upload","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_upload\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":4},"5":{"id":"5","name":"URL downloader","type":"url","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_url\/1569995285\/icon","supported_types":[".gif",".jpe",".jpeg",".jpg",".png",".svg",".svgz"],"return_types":3,"defaultreturntype":2,"sortorder":5},"6":{"id":"6","name":"Private files","type":"user","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_user\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":6},"7":{"id":"7","name":"Wikimedia","type":"wikimedia","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_wikimedia\/1569995285\/icon","supported_types":[],"return_types":3,"defaultreturntype":2,"sortorder":7}},"externallink":true,"userprefs":{"recentrepository":"2","recentlicense":"allrightsreserved","recentviewmode":"3"},"accepted_types":[],"return_types":15,"context":{"id":52,"contextlevel":50,"instanceid":"7","path":"\/1\/3\/52","depth":"3","locked":false},"client_id":"5dc8028a90848","maxbytes":0,"areamaxbytes":-1,"env":"editor","itemid":320008600},"subtitle":{"defaultlicense":"allrightsreserved","licenses":[{"shortname":"unknown","fullname":"Other"},{"shortname":"allrightsreserved","fullname":"All rights reserved"},{"shortname":"public","fullname":"Public domain"},{"shortname":"cc","fullname":"Creative Commons"},{"shortname":"cc-nd","fullname":"Creative Commons - NoDerivs"},{"shortname":"cc-nc-nd","fullname":"Creative Commons - No Commercial NoDerivs"},{"shortname":"cc-nc","fullname":"Creative Commons - No Commercial"},{"shortname":"cc-nc-sa","fullname":"Creative Commons - No Commercial ShareAlike"},{"shortname":"cc-sa","fullname":"Creative Commons - ShareAlike"}],"author":"Duck Tran","repositories":{"1":{"id":"1","name":"Embedded files","type":"areafiles","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_areafiles\/1569995285\/icon","supported_types":[],"return_types":1,"defaultreturntype":2,"sortorder":1},"2":{"id":"2","name":"Server files","type":"local","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_local\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":2},"3":{"id":"3","name":"Recent files","type":"recent","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_recent\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":3},"4":{"id":"4","name":"Upload a file","type":"upload","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_upload\/1569995285\/icon","supported_types":[],"return_types":2,"defaultreturntype":2,"sortorder":4},"6":{"id":"6","name":"Private files","type":"user","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_user\/1569995285\/icon","supported_types":[],"return_types":6,"defaultreturntype":2,"sortorder":6},"7":{"id":"7","name":"Wikimedia","type":"wikimedia","icon":"https:\/\/cacbank.moodlecloud.com\/theme\/image.php\/boost\/repository_wikimedia\/1569995285\/icon","supported_types":[],"return_types":3,"defaultreturntype":2,"sortorder":7}},"externallink":true,"userprefs":{"recentrepository":"2","recentlicense":"allrightsreserved","recentviewmode":"3"},"accepted_types":[".vtt"],"return_types":15,"context":{"id":52,"contextlevel":50,"instanceid":"7","path":"\/1\/3\/52","depth":"3","locked":false},"client_id":"5dc8028a90c2d","maxbytes":0,"areamaxbytes":-1,"env":"editor","itemid":320008600}},"plugins":[{"group":"collapse","plugins":[{"name":"collapse","params":[],"showgroups":"5"}]},{"group":"style1","plugins":[{"name":"title","params":[]},{"name":"bold","params":[]},{"name":"italic","params":[]}]},{"group":"list","plugins":[{"name":"unorderedlist","params":[]},{"name":"orderedlist","params":[]}]},{"group":"links","plugins":[{"name":"link","params":[]}]},{"group":"files","plugins":[{"name":"image","params":[]},{"name":"media","params":[],"langs":{"installed":[{"lang":" \u12a0\u121b\u122d\u129b \u200e(am)\u200e","code":"am","default":false},{"lang":"Afrikaans \u200e(af)\u200e","code":"af","default":false},{"lang":"Aragon\u00e9s \u200e(an)\u200e","code":"an","default":false},{"lang":"Aran\u00e9s \u200e(oc_es)\u200e","code":"oc_es","default":false},{"lang":"Asturianu \u200e(ast)\u200e","code":"ast","default":false},{"lang":"Az\u0259rbaycanca \u200e(az)\u200e","code":"az","default":false},{"lang":"Bahasa Melayu \u200e(ms)\u200e","code":"ms","default":false},{"lang":"Bamanankan \u200e(bm)\u200e","code":"bm","default":false},{"lang":"Bislama \u200e(bi)\u200e","code":"bi","default":false},{"lang":"Bosanski \u200e(bs)\u200e","code":"bs","default":false},{"lang":"Breizh \u200e(br)\u200e","code":"br","default":false},{"lang":"Catal\u00e0 \u200e(ca)\u200e","code":"ca","default":false},{"lang":"Catal\u00e0 (Valenci\u00e0) \u200e(ca_valencia)\u200e","code":"ca_valencia","default":false},{"lang":"Catal\u00e0 per a Workplace \u200e(ca_wp)\u200e","code":"ca_wp","default":false},{"lang":"\u010ce\u0161tina \u200e(cs)\u200e","code":"cs","default":false},{"lang":"Crnogorski \u200e(mis)\u200e","code":"mis","default":false},{"lang":"Croatian schools \u200e(hr_schools)\u200e","code":"hr_schools","default":false},{"lang":"Cymraeg \u200e(cy)\u200e","code":"cy","default":false},{"lang":"Dansk \u200e(da)\u200e","code":"da","default":false},{"lang":"Dansk (kursus) \u200e(da_kursus)\u200e","code":"da_kursus","default":false},{"lang":"Dansk Rum \u200e(da_rum)\u200e","code":"da_rum","default":false},{"lang":"Davvis\u00e1megiella \u200e(se)\u200e","code":"se","default":false},{"lang":"Deutsch - Du \u200e(de_du)\u200e","code":"de_du","default":false},{"lang":"Deutsch - Kids \u200e(de_kids)\u200e","code":"de_kids","default":false},{"lang":"Deutsch - Schweiz \u200e(de_ch)\u200e","code":"de_ch","default":false},{"lang":"Deutsch \u200e(de)\u200e","code":"de","default":false},{"lang":"Deutsch community \u200e(de_comm)\u200e","code":"de_comm","default":false},{"lang":"Deutsch f\u00fcr Arbeitsplatz \u200e(de_wp)\u200e","code":"de_wp","default":false},{"lang":"Dolnoserbski \u200e(dsb)\u200e","code":"dsb","default":false},{"lang":"Ebon \u200e(mh)\u200e","code":"mh","default":false},{"lang":"eesti \u200e(et)\u200e","code":"et","default":false},{"lang":"English - Pirate \u200e(en_ar)\u200e","code":"en_ar","default":false},{"lang":"English - United States \u200e(en_us)\u200e","code":"en_us","default":false},{"lang":"English \u200e(en)\u200e","code":"en","default":true},{"lang":"English for kids \u200e(en_kids)\u200e","code":"en_kids","default":false},{"lang":"English for Workplace \u200e(en_wp)\u200e","code":"en_wp","default":false},{"lang":"English US - K12 \u200e(en_us_k12)\u200e","code":"en_us_k12","default":false},{"lang":"Espa\u00f1ol - Colombia \u200e(es_co)\u200e","code":"es_co","default":false},{"lang":"Espa\u00f1ol - Internacional \u200e(es)\u200e","code":"es","default":false},{"lang":"Espa\u00f1ol - M\u00e9xico \u200e(es_mx)\u200e","code":"es_mx","default":false},{"lang":"Espa\u00f1ol - M\u00e9xico para ni\u00f1os \u200e(es_mx_kids)\u200e","code":"es_mx_kids","default":false},{"lang":"Espa\u00f1ol - Venezuela \u200e(es_ve)\u200e","code":"es_ve","default":false},{"lang":"Espa\u00f1ol para la Empresa \u200e(es_wp)\u200e","code":"es_wp","default":false},{"lang":"Esperanto \u200e(eo)\u200e","code":"eo","default":false},{"lang":"Euskara \u200e(eu)\u200e","code":"eu","default":false},{"lang":"\u00c8\u028begbe \u200e(ee)\u200e","code":"ee","default":false},{"lang":"Filipino \u200e(fil)\u200e","code":"fil","default":false},{"lang":"Finlandssvenska \u200e(sv_fi)\u200e","code":"sv_fi","default":false},{"lang":"F\u00f8royskt \u200e(fo)\u200e","code":"fo","default":false},{"lang":"Fran\u00e7ais - Canada \u200e(fr_ca)\u200e","code":"fr_ca","default":false},{"lang":"Fran\u00e7ais \u200e(fr)\u200e","code":"fr","default":false},{"lang":"Fran\u00e7ais pour Workplace \u200e(fr_wp)\u200e","code":"fr_wp","default":false},{"lang":"Gaeilge \u200e(ga)\u200e","code":"ga","default":false},{"lang":"G\u00e0idhlig \u200e(gd)\u200e","code":"gd","default":false},{"lang":"Galego \u200e(gl)\u200e","code":"gl","default":false},{"lang":"Gascon \u200e(oc_gsc)\u200e","code":"oc_gsc","default":false},{"lang":"Hausa \u200e(ha)\u200e","code":"ha","default":false},{"lang":"Hrvatski \u200e(hr)\u200e","code":"hr","default":false},{"lang":"\u02bb\u014clelo Hawai\u02bbi \u200e(haw)\u200e","code":"haw","default":false},{"lang":"Igbo \u200e(ig)\u200e","code":"ig","default":false},{"lang":"Indonesian \u200e(id)\u200e","code":"id","default":false},{"lang":"isiZulu \u200e(zu)\u200e","code":"zu","default":false},{"lang":"\u00cdslenska \u200e(is)\u200e","code":"is","default":false},{"lang":"Italiano \u200e(it)\u200e","code":"it","default":false},{"lang":"Italiano per Workplace \u200e(it_wp)\u200e","code":"it_wp","default":false},{"lang":"Japanese - kids \u200e(ja_kids)\u200e","code":"ja_kids","default":false},{"lang":"Kalaallisut \u200e(kl)\u200e","code":"kl","default":false},{"lang":"Kinyarwanda \u200e(rw)\u200e","code":"rw","default":false},{"lang":"Kiswahili \u200e(sw)\u200e","code":"sw","default":false},{"lang":"Krey\u00f2l Ayisyen \u200e(hat)\u200e","code":"hat","default":false},{"lang":"Kurmanji \u200e(kmr)\u200e","code":"kmr","default":false},{"lang":"Laotian \u200e(lo)\u200e","code":"lo","default":false},{"lang":"Latin \u200e(la)\u200e","code":"la","default":false},{"lang":"Latvie\u0161u \u200e(lv)\u200e","code":"lv","default":false},{"lang":"Lengadocian \u200e(oc_lnc)\u200e","code":"oc_lnc","default":false},{"lang":"L\u00ebtzebuergesch \u200e(lb)\u200e","code":"lb","default":false},{"lang":"Lietuvi\u0173 \u200e(lt)\u200e","code":"lt","default":false},{"lang":"Lithuanian (university) \u200e(lt_uni)\u200e","code":"lt_uni","default":false},{"lang":"Lulesamisk \u200e(smj)\u200e","code":"smj","default":false},{"lang":"magyar \u200e(hu)\u200e","code":"hu","default":false},{"lang":"Malagasy \u200e(mg)\u200e","code":"mg","default":false},{"lang":"M\u0101ori - Tainui \u200e(mi_tn)\u200e","code":"mi_tn","default":false},{"lang":"M\u0101ori - Waikato \u200e(mi_wwow)\u200e","code":"mi_wwow","default":false},{"lang":"M\u0101ori (Te Reo) \u200e(mi)\u200e","code":"mi","default":false},{"lang":"Mongolian \u200e(mn_mong)\u200e","code":"mn_mong","default":false},{"lang":"myanma bhasa \u200e(my)\u200e","code":"my","default":false},{"lang":"Nederlands \u200e(nl)\u200e","code":"nl","default":false},{"lang":"Norsk - bokm\u00e5l \u200e(no)\u200e","code":"no","default":false},{"lang":"Norsk - nynorsk \u200e(nn)\u200e","code":"nn","default":false},{"lang":"Norsk \u200e(no_gr)\u200e","code":"no_gr","default":false},{"lang":"Norsk Workplace \u200e(no_wp)\u200e","code":"no_wp","default":false},{"lang":"O'zbekcha \u200e(uz)\u200e","code":"uz","default":false},{"lang":"Pidgin \u200e(pcm)\u200e","code":"pcm","default":false},{"lang":"Polski \u200e(pl)\u200e","code":"pl","default":false},{"lang":"Portugu\u00eas - Brasil \u200e(pt_br)\u200e","code":"pt_br","default":false},{"lang":"Portugu\u00eas - Portugal \u200e(pt)\u200e","code":"pt","default":false},{"lang":"Rom\u00e2n\u0103 \u200e(ro)\u200e","code":"ro","default":false},{"lang":"Romansh Sursilvan \u200e(rm_surs)\u200e","code":"rm_surs","default":false},{"lang":"Samoan \u200e(sm)\u200e","code":"sm","default":false},{"lang":"Setswana \u200e(tn)\u200e","code":"tn","default":false},{"lang":"Shqip \u200e(sq)\u200e","code":"sq","default":false},{"lang":"Sloven\u010dina \u200e(sk)\u200e","code":"sk","default":false},{"lang":"Sloven\u0161\u010dina \u200e(sl)\u200e","code":"sl","default":false},{"lang":"Soomaali \u200e(so)\u200e","code":"so","default":false},{"lang":"S\u00f8rsamisk \u200e(sma)\u200e","code":"sma","default":false},{"lang":"Srpski \u200e(sr_lt)\u200e","code":"sr_lt","default":false},{"lang":"Suomi \u200e(fi)\u200e","code":"fi","default":false},{"lang":"Suomi+ \u200e(fi_co)\u200e","code":"fi_co","default":false},{"lang":"Svenska \u200e(sv)\u200e","code":"sv","default":false},{"lang":"Tagalog \u200e(tl)\u200e","code":"tl","default":false},{"lang":"Tamil \u200e(ta)\u200e","code":"ta","default":false},{"lang":"Taqbaylit \u200e(kab)\u200e","code":"kab","default":false},{"lang":"Thai \u200e(th)\u200e","code":"th","default":false},{"lang":"Tongan \u200e(to)\u200e","code":"to","default":false},{"lang":"T\u00fcrk\u00e7e \u200e(tr)\u200e","code":"tr","default":false},{"lang":"Turkmen \u200e(tk)\u200e","code":"tk","default":false},{"lang":"Uyghur - latin \u200e(ug_lt)\u200e","code":"ug_lt","default":false},{"lang":"VakaViti \u200e(fj)\u200e","code":"fj","default":false},{"lang":"Valenci\u00e0_RACV \u200e(ca_valencia_racv)\u200e","code":"ca_valencia_racv","default":false},{"lang":"Vietnamese \u200e(vi)\u200e","code":"vi","default":false},{"lang":"Wolof \u200e(wo)\u200e","code":"wo","default":false},{"lang":"Workplace \u00een limba rom\u00e2n\u0103 \u200e(ro_wp)\u200e","code":"ro_wp","default":false},{"lang":"Workplace \u7b80\u4f53\u4e2d\u6587\u7248 \u200e(zh_cn_wp)\u200e","code":"zh_cn_wp","default":false},{"lang":"Workplace \u7e41\u9ad4\u4e2d\u6587\u7248 \u200e(zh_tw_wp)\u200e","code":"zh_tw_wp","default":false},{"lang":"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac \u200e(el)\u200e","code":"el","default":false},{"lang":"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac \u03b3\u03b9\u03b1 \u03c0\u03b1\u03b9\u03b4\u03b9\u03ac \u200e(el_kids)\u200e","code":"el_kids","default":false},{"lang":"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac \u03b3\u03b9\u03b1 \u03c3\u03c7\u03bf\u03bb\u03ad\u03c2 \u200e(el_uni)\u200e","code":"el_uni","default":false},{"lang":"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac \u03b3\u03b9\u03b1 \u03c7\u03ce\u03c1\u03bf\u03c5\u03c2 \u03b5\u03c1\u03b3\u03b1\u03c3\u03af\u03b1\u03c2 \u200e(el_wp)\u200e","code":"el_wp","default":false},{"lang":"\u0411\u0430\u0448\u04a1\u043e\u0440\u0442 \u0442\u0435\u043b\u0435 \u200e(ba)\u200e","code":"ba","default":false},{"lang":"\u0411\u0435\u043b\u0430\u0440\u0443\u0441\u043a\u0430\u044f \u200e(be)\u200e","code":"be","default":false},{"lang":"\u0411\u044a\u043b\u0433\u0430\u0440\u0441\u043a\u0438 \u200e(bg)\u200e","code":"bg","default":false},{"lang":"\u041a\u044b\u0440\u0433\u044b\u0437\u0447\u0430 \u200e(ky)\u200e","code":"ky","default":false},{"lang":"\u049a\u0430\u0437\u0430\u049b\u0448\u0430 \u200e(kk)\u200e","code":"kk","default":false},{"lang":"\u041c\u0430\u043a\u0435\u0434\u043e\u043d\u0441\u043a\u0438 \u200e(mk)\u200e","code":"mk","default":false},{"lang":"\u041c\u043e\u043d\u0433\u043e\u043b \u200e(mn)\u200e","code":"mn","default":false},{"lang":"\u0420\u0443\u0441\u0441\u043a\u0438\u0439 \u200e(ru)\u200e","code":"ru","default":false},{"lang":"\u0421\u0440\u043f\u0441\u043a\u0438 \u200e(sr_cr)\u200e","code":"sr_cr","default":false},{"lang":"\u0442\u0430\u0442\u0430\u0440 \u0442\u0435\u043b\u0435 \u200e(tt)\u200e","code":"tt","default":false},{"lang":"\u0422\u043e\u04b7\u0438\u043a\u04e3 \u200e(tg)\u200e","code":"tg","default":false},{"lang":"\u0423\u043a\u0440\u0430\u0457\u043d\u0441\u044c\u043a\u0430 \u200e(uk)\u200e","code":"uk","default":false},{"lang":"\u10e5\u10d0\u10e0\u10d7\u10e3\u10da\u10d8 \u200e(ka)\u200e","code":"ka","default":false},{"lang":"\u0540\u0561\u0575\u0565\u0580\u0565\u0576 \u200e(hy)\u200e","code":"hy","default":false},{"lang":"\u05e2\u05d1\u05e8\u05d9\u05ea \u200e(he)\u200e","code":"he","default":false},{"lang":"\u05e2\u05d1\u05e8\u05d9\u05ea \u05d1\u05ea\u05d9\u05be\u05e1\u05e4\u05e8 \u200e(he_kids)\u200e","code":"he_kids","default":false},{"lang":"\u05e2\u05d1\u05e8\u05d9\u05ea \u05e2\u05d1\u05d5\u05e8 Workplace \u200e(he_wp)\u200e","code":"he_wp","default":false},{"lang":"\u0626\u06c7\u064a\u063a\u06c7\u0631\u0686\u06d5 \u200e(ug_ug)\u200e","code":"ug_ug","default":false},{"lang":"\u0627\u0631\u062f\u0648 \u200e(ur)\u200e","code":"ur","default":false},{"lang":"\u067e\u069a\u062a\u0648 \u200e(ps)\u200e","code":"ps","default":false},{"lang":"\u0633\u06c6\u0631\u0627\u0646\u06cc \u200e(ckb)\u200e","code":"ckb","default":false},{"lang":"\u0639\u0631\u0628\u064a \u200e(ar)\u200e","code":"ar","default":false},{"lang":"\u0641\u0627\u0631\u0633\u06cc \u200e(fa)\u200e","code":"fa","default":false},{"lang":"\u078b\u07a8\u0788\u07ac\u0780\u07a8 \u200e(dv)\u200e","code":"dv","default":false},{"lang":"\u2d5c\u2d30\u2d4e\u2d30\u2d63\u2d49\u2d56\u2d5c \u200e(zgh)\u200e","code":"zgh","default":false},{"lang":"\u1275\u130d\u122d\u129b \u200e(ti)\u200e","code":"ti","default":false},{"lang":"\u0928\u0947\u092a\u093e\u0932\u0940 \u200e(ne)\u200e","code":"ne","default":false},{"lang":"\u092e\u0930\u093e\u0920\u0940 \u200e(mr)\u200e","code":"mr","default":false},{"lang":"\u0935\u0930\u094d\u0915\u092a\u094d\u0932\u0947\u0938  \u0915\u0947 \u0932\u093f\u090f  \u0939\u093f\u0902\u0926\u0940 \u200e(hi_wp)\u200e","code":"hi_wp","default":false},{"lang":"\u0939\u093f\u0902\u0926\u0940 \u200e(hi)\u200e","code":"hi","default":false},{"lang":"\u09ac\u09be\u0982\u09b2\u09be \u200e(bn)\u200e","code":"bn","default":false},{"lang":"\u0a2a\u0a70\u0a1c\u0a3e\u0a2c\u0a40 \u200e(pan)\u200e","code":"pan","default":false},{"lang":"\u0a97\u0ac1\u0a9c\u0ab0\u0abe\u0aa4\u0ac0 \u200e(gu)\u200e","code":"gu","default":false},{"lang":"\u0b13\u0b21\u0b3c\u0b3f\u0b06 \u200e(or)\u200e","code":"or","default":false},{"lang":"\u0ba4\u0bae\u0bbf\u0bb4\u0bcd \u200e(ta_lk)\u200e","code":"ta_lk","default":false},{"lang":"\u0c24\u0c46\u0c32\u0c41\u0c17\u0c41  \u200e(te)\u200e","code":"te","default":false},{"lang":"\u0c95\u0ca8\u0ccd\u0ca8\u0ca1 \u200e(kn)\u200e","code":"kn","default":false},{"lang":"\u0d2e\u0d32\u0d2f\u0d3e\u0d33\u0d02 \u200e(ml)\u200e","code":"ml","default":false},{"lang":"\u0dc3\u0dd2\u0d82\u0dc4\u0dbd \u200e(si)\u200e","code":"si","default":false},{"lang":"\u0f56\u0f7c\u0f51\u0f0b\u0f61\u0f72\u0f42 \u200e(xct)\u200e","code":"xct","default":false},{"lang":"\u0f62\u0fab\u0f7c\u0f44\u0f0b\u0f41 \u200e(dz)\u200e","code":"dz","default":false},{"lang":"\u1781\u17d2\u1798\u17c2\u179a \u200e(km)\u200e","code":"km","default":false},{"lang":"\ud55c\uad6d\uc5b4 \u200e(ko)\u200e","code":"ko","default":false},{"lang":"\u65e5\u672c\u8a9e \u200e(ja)\u200e","code":"ja","default":false},{"lang":"\u6b63\u9ad4\u4e2d\u6587 \u200e(zh_tw)\u200e","code":"zh_tw","default":false},{"lang":"\u7b80\u4f53\u4e2d\u6587 \u200e(zh_cn)\u200e","code":"zh_cn","default":false}],"available":[{"lang":"Afar \u200e(aa)\u200e","code":"aa"},{"lang":"Abkhazian \u200e(ab)\u200e","code":"ab"},{"lang":"Avestan \u200e(ae)\u200e","code":"ae"},{"lang":"Afrikaans \u200e(af)\u200e","code":"af"},{"lang":"Akan \u200e(ak)\u200e","code":"ak"},{"lang":"Amharic \u200e(am)\u200e","code":"am"},{"lang":"Aragonese \u200e(an)\u200e","code":"an"},{"lang":"Arabic \u200e(ar)\u200e","code":"ar"},{"lang":"Assamese \u200e(as)\u200e","code":"as"},{"lang":"Avaric \u200e(av)\u200e","code":"av"},{"lang":"Aymara \u200e(ay)\u200e","code":"ay"},{"lang":"Azerbaijani \u200e(az)\u200e","code":"az"},{"lang":"Bashkir \u200e(ba)\u200e","code":"ba"},{"lang":"Belarusian \u200e(be)\u200e","code":"be"},{"lang":"Bulgarian \u200e(bg)\u200e","code":"bg"},{"lang":"Bihari languages \u200e(bh)\u200e","code":"bh"},{"lang":"Bislama \u200e(bi)\u200e","code":"bi"},{"lang":"Bambara \u200e(bm)\u200e","code":"bm"},{"lang":"Bengali \u200e(bn)\u200e","code":"bn"},{"lang":"Tibetan \u200e(bo)\u200e","code":"bo"},{"lang":"Breton \u200e(br)\u200e","code":"br"},{"lang":"Bosnian \u200e(bs)\u200e","code":"bs"},{"lang":"Catalan; Valencian \u200e(ca)\u200e","code":"ca"},{"lang":"Chechen \u200e(ce)\u200e","code":"ce"},{"lang":"Chamorro \u200e(ch)\u200e","code":"ch"},{"lang":"Corsican \u200e(co)\u200e","code":"co"},{"lang":"Cree \u200e(cr)\u200e","code":"cr"},{"lang":"Czech \u200e(cs)\u200e","code":"cs"},{"lang":"Church Slavic; Old Slavonic \u200e(cu)\u200e","code":"cu"},{"lang":"Chuvash \u200e(cv)\u200e","code":"cv"},{"lang":"Welsh \u200e(cy)\u200e","code":"cy"},{"lang":"Danish \u200e(da)\u200e","code":"da"},{"lang":"German \u200e(de)\u200e","code":"de"},{"lang":"Divehi; Dhivehi; Maldivian \u200e(dv)\u200e","code":"dv"},{"lang":"Dzongkha \u200e(dz)\u200e","code":"dz"},{"lang":"Ewe \u200e(ee)\u200e","code":"ee"},{"lang":"Greek, Modern (1453-) \u200e(el)\u200e","code":"el"},{"lang":"English \u200e(en)\u200e","code":"en"},{"lang":"Esperanto \u200e(eo)\u200e","code":"eo"},{"lang":"Spanish; Castilian \u200e(es)\u200e","code":"es"},{"lang":"Estonian \u200e(et)\u200e","code":"et"},{"lang":"Basque \u200e(eu)\u200e","code":"eu"},{"lang":"Persian \u200e(fa)\u200e","code":"fa"},{"lang":"Fulah \u200e(ff)\u200e","code":"ff"},{"lang":"Finnish \u200e(fi)\u200e","code":"fi"},{"lang":"Fijian \u200e(fj)\u200e","code":"fj"},{"lang":"Faroese \u200e(fo)\u200e","code":"fo"},{"lang":"French \u200e(fr)\u200e","code":"fr"},{"lang":"Western Frisian \u200e(fy)\u200e","code":"fy"},{"lang":"Irish \u200e(ga)\u200e","code":"ga"},{"lang":"Gaelic; Scottish Gaelic \u200e(gd)\u200e","code":"gd"},{"lang":"Galician \u200e(gl)\u200e","code":"gl"},{"lang":"Guarani \u200e(gn)\u200e","code":"gn"},{"lang":"Gujarati \u200e(gu)\u200e","code":"gu"},{"lang":"Manx \u200e(gv)\u200e","code":"gv"},{"lang":"Hausa \u200e(ha)\u200e","code":"ha"},{"lang":"Hebrew \u200e(he)\u200e","code":"he"},{"lang":"Hindi \u200e(hi)\u200e","code":"hi"},{"lang":"Hiri Motu \u200e(ho)\u200e","code":"ho"},{"lang":"Croatian \u200e(hr)\u200e","code":"hr"},{"lang":"Haitian; Haitian Creole \u200e(ht)\u200e","code":"ht"},{"lang":"Hungarian \u200e(hu)\u200e","code":"hu"},{"lang":"Armenian \u200e(hy)\u200e","code":"hy"},{"lang":"Herero \u200e(hz)\u200e","code":"hz"},{"lang":"Interlingua (IALA) \u200e(ia)\u200e","code":"ia"},{"lang":"Indonesian \u200e(id)\u200e","code":"id"},{"lang":"Interlingue; Occidental \u200e(ie)\u200e","code":"ie"},{"lang":"Igbo \u200e(ig)\u200e","code":"ig"},{"lang":"Sichuan Yi; Nuosu \u200e(ii)\u200e","code":"ii"},{"lang":"Inupiaq \u200e(ik)\u200e","code":"ik"},{"lang":"Ido \u200e(io)\u200e","code":"io"},{"lang":"Icelandic \u200e(is)\u200e","code":"is"},{"lang":"Italian \u200e(it)\u200e","code":"it"},{"lang":"Inuktitut \u200e(iu)\u200e","code":"iu"},{"lang":"Japanese \u200e(ja)\u200e","code":"ja"},{"lang":"Javanese \u200e(jv)\u200e","code":"jv"},{"lang":"Georgian \u200e(ka)\u200e","code":"ka"},{"lang":"Kongo \u200e(kg)\u200e","code":"kg"},{"lang":"Kikuyu; Gikuyu \u200e(ki)\u200e","code":"ki"},{"lang":"Kuanyama; Kwanyama \u200e(kj)\u200e","code":"kj"},{"lang":"Kazakh \u200e(kk)\u200e","code":"kk"},{"lang":"Kalaallisut; Greenlandic \u200e(kl)\u200e","code":"kl"},{"lang":"Central Khmer \u200e(km)\u200e","code":"km"},{"lang":"Kannada \u200e(kn)\u200e","code":"kn"},{"lang":"Korean \u200e(ko)\u200e","code":"ko"},{"lang":"Kanuri \u200e(kr)\u200e","code":"kr"},{"lang":"Kashmiri \u200e(ks)\u200e","code":"ks"},{"lang":"Kurdish \u200e(ku)\u200e","code":"ku"},{"lang":"Komi \u200e(kv)\u200e","code":"kv"},{"lang":"Cornish \u200e(kw)\u200e","code":"kw"},{"lang":"Kirghiz; Kyrgyz \u200e(ky)\u200e","code":"ky"},{"lang":"Latin \u200e(la)\u200e","code":"la"},{"lang":"Luxembourgish \u200e(lb)\u200e","code":"lb"},{"lang":"Ganda \u200e(lg)\u200e","code":"lg"},{"lang":"Limburgan \u200e(li)\u200e","code":"li"},{"lang":"Lingala \u200e(ln)\u200e","code":"ln"},{"lang":"Lao \u200e(lo)\u200e","code":"lo"},{"lang":"Lithuanian \u200e(lt)\u200e","code":"lt"},{"lang":"Luba-Katanga \u200e(lu)\u200e","code":"lu"},{"lang":"Latvian \u200e(lv)\u200e","code":"lv"},{"lang":"Malagasy \u200e(mg)\u200e","code":"mg"},{"lang":"Marshallese \u200e(mh)\u200e","code":"mh"},{"lang":"Maori \u200e(mi)\u200e","code":"mi"},{"lang":"Macedonian \u200e(mk)\u200e","code":"mk"},{"lang":"Malayalam \u200e(ml)\u200e","code":"ml"},{"lang":"Mongolian \u200e(mn)\u200e","code":"mn"},{"lang":"Marathi \u200e(mr)\u200e","code":"mr"},{"lang":"Malay \u200e(ms)\u200e","code":"ms"},{"lang":"Maltese \u200e(mt)\u200e","code":"mt"},{"lang":"Burmese \u200e(my)\u200e","code":"my"},{"lang":"Nauru \u200e(na)\u200e","code":"na"},{"lang":"Norwegian Bokm\u00e5l \u200e(nb)\u200e","code":"nb"},{"lang":"Ndebele, North \u200e(nd)\u200e","code":"nd"},{"lang":"Nepali \u200e(ne)\u200e","code":"ne"},{"lang":"Ndonga \u200e(ng)\u200e","code":"ng"},{"lang":"Dutch; Flemish \u200e(nl)\u200e","code":"nl"},{"lang":"Norwegian Nynorsk \u200e(nn)\u200e","code":"nn"},{"lang":"Norwegian \u200e(no)\u200e","code":"no"},{"lang":"Ndebele, South \u200e(nr)\u200e","code":"nr"},{"lang":"Navajo; Navaho \u200e(nv)\u200e","code":"nv"},{"lang":"Chichewa \u200e(ny)\u200e","code":"ny"},{"lang":"Occitan (post 1500); Proven\u00e7al \u200e(oc)\u200e","code":"oc"},{"lang":"Ojibwa \u200e(oj)\u200e","code":"oj"},{"lang":"Oromo \u200e(om)\u200e","code":"om"},{"lang":"Oriya \u200e(or)\u200e","code":"or"},{"lang":"Ossetian; Ossetic \u200e(os)\u200e","code":"os"},{"lang":"Panjabi; Punjabi \u200e(pa)\u200e","code":"pa"},{"lang":"Pali \u200e(pi)\u200e","code":"pi"},{"lang":"Polish \u200e(pl)\u200e","code":"pl"},{"lang":"Pushto; Pashto \u200e(ps)\u200e","code":"ps"},{"lang":"Portuguese \u200e(pt)\u200e","code":"pt"},{"lang":"Quechua \u200e(qu)\u200e","code":"qu"},{"lang":"Romansh \u200e(rm)\u200e","code":"rm"},{"lang":"Rundi \u200e(rn)\u200e","code":"rn"},{"lang":"Romanian; Moldavian; Moldovan \u200e(ro)\u200e","code":"ro"},{"lang":"Russian \u200e(ru)\u200e","code":"ru"},{"lang":"Kinyarwanda \u200e(rw)\u200e","code":"rw"},{"lang":"Sanskrit \u200e(sa)\u200e","code":"sa"},{"lang":"Sardinian \u200e(sc)\u200e","code":"sc"},{"lang":"Sindhi \u200e(sd)\u200e","code":"sd"},{"lang":"Northern Sami \u200e(se)\u200e","code":"se"},{"lang":"Sango \u200e(sg)\u200e","code":"sg"},{"lang":"Sinhala; Sinhalese \u200e(si)\u200e","code":"si"},{"lang":"Slovak \u200e(sk)\u200e","code":"sk"},{"lang":"Slovenian \u200e(sl)\u200e","code":"sl"},{"lang":"Samoan \u200e(sm)\u200e","code":"sm"},{"lang":"Shona \u200e(sn)\u200e","code":"sn"},{"lang":"Somali \u200e(so)\u200e","code":"so"},{"lang":"Albanian \u200e(sq)\u200e","code":"sq"},{"lang":"Serbian \u200e(sr)\u200e","code":"sr"},{"lang":"Swati \u200e(ss)\u200e","code":"ss"},{"lang":"Sotho, Southern \u200e(st)\u200e","code":"st"},{"lang":"Sundanese \u200e(su)\u200e","code":"su"},{"lang":"Swedish \u200e(sv)\u200e","code":"sv"},{"lang":"Swahili \u200e(sw)\u200e","code":"sw"},{"lang":"Tamil \u200e(ta)\u200e","code":"ta"},{"lang":"Telugu \u200e(te)\u200e","code":"te"},{"lang":"Tajik \u200e(tg)\u200e","code":"tg"},{"lang":"Thai \u200e(th)\u200e","code":"th"},{"lang":"Tigrinya \u200e(ti)\u200e","code":"ti"},{"lang":"Turkmen \u200e(tk)\u200e","code":"tk"},{"lang":"Tagalog \u200e(tl)\u200e","code":"tl"},{"lang":"Tswana \u200e(tn)\u200e","code":"tn"},{"lang":"Tonga (Tonga Islands) \u200e(to)\u200e","code":"to"},{"lang":"Turkish \u200e(tr)\u200e","code":"tr"},{"lang":"Tsonga \u200e(ts)\u200e","code":"ts"},{"lang":"Tatar \u200e(tt)\u200e","code":"tt"},{"lang":"Twi \u200e(tw)\u200e","code":"tw"},{"lang":"Tahitian \u200e(ty)\u200e","code":"ty"},{"lang":"Uighur; Uyghur \u200e(ug)\u200e","code":"ug"},{"lang":"Ukrainian \u200e(uk)\u200e","code":"uk"},{"lang":"Urdu \u200e(ur)\u200e","code":"ur"},{"lang":"Uzbek \u200e(uz)\u200e","code":"uz"},{"lang":"Venda \u200e(ve)\u200e","code":"ve"},{"lang":"Vietnamese \u200e(vi)\u200e","code":"vi"},{"lang":"Volap\u00fck \u200e(vo)\u200e","code":"vo"},{"lang":"Walloon \u200e(wa)\u200e","code":"wa"},{"lang":"Wolof \u200e(wo)\u200e","code":"wo"},{"lang":"Xhosa \u200e(xh)\u200e","code":"xh"},{"lang":"Yiddish \u200e(yi)\u200e","code":"yi"},{"lang":"Yoruba \u200e(yo)\u200e","code":"yo"},{"lang":"Zhuang; Chuang \u200e(za)\u200e","code":"za"},{"lang":"Chinese \u200e(zh)\u200e","code":"zh"},{"lang":"Zulu \u200e(zu)\u200e","code":"zu"}]},"help":{"addsource":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;It is recommended that an alternative media source is provided, since desktop and mobile browsers vary in which file formats they support.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Add alternative source\" aria-label=\"Help with Add alternative source\"><\/i>\n<\/a>","tracks":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Subtitles, captions, chapters and descriptions can be added via a WebVTT (Web Video Text Tracks) format file. Track labels will be shown in the selection drop-down menu. For each type of track, any track set as default will be pre-selected at the start of the video.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Subtitles and captions\" aria-label=\"Help with Subtitles and captions\"><\/i>\n<\/a>","subtitles":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Subtitles may be used to provide a transcription or translation of the dialogue.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Subtitles\" aria-label=\"Help with Subtitles\"><\/i>\n<\/a>","captions":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Captions may be used to describe everything happening in the track, including non-verbal sounds such as a phone ringing.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Captions\" aria-label=\"Help with Captions\"><\/i>\n<\/a>","descriptions":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Audio descriptions may be used to provide a narration which explains visual details not apparent from the audio alone.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Descriptions\" aria-label=\"Help with Descriptions\"><\/i>\n<\/a>","chapters":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Chapter titles may be provided for use in navigating the media resource.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Chapters\" aria-label=\"Help with Chapters\"><\/i>\n<\/a>","metadata":"<a class=\"btn btn-link p-0\" role=\"button\"\n    data-container=\"body\" data-toggle=\"popover\"\n    data-placement=\"right\" data-content=\"&lt;div class=&quot;no-overflow&quot;&gt;&lt;p&gt;Metadata tracks, for use from a script, may be used only if the player supports metadata.&lt;\/p&gt;\n&lt;\/div&gt; \"\n    data-html=\"true\" tabindex=\"0\" data-trigger=\"focus\">\n  <i class=\"icon fa fa-question-circle text-info fa-fw \"  title=\"Help with Metadata\" aria-label=\"Help with Metadata\"><\/i>\n<\/a>"}},{"name":"recordrtc","params":[],"contextid":52,"sesskey":"DyXZbRiLhr","allowedtypes":"both","audiobitrate":"128000","videobitrate":"2500000","timelimit":"120","audiortcicon":"i\/audiortc","videortcicon":"i\/videortc","maxrecsize":1073741824},{"name":"managefiles","params":[],"disabled":false,"area":{"context":52,"areamaxbytes":-1,"maxbytes":0,"subdirs":0,"return_types":15,"removeorphaneddrafts":false},"usercontext":5}]},{"group":"style2","plugins":[{"name":"underline","params":[]},{"name":"strike","params":[]},{"name":"subscript","params":[]},{"name":"superscript","params":[]}]},{"group":"align","plugins":[{"name":"align","params":[]}]},{"group":"indent","plugins":[{"name":"indent","params":[]}]},{"group":"insert","plugins":[{"name":"equation","params":[],"texfilteractive":true,"contextid":52,"library":{"group1":{"groupname":"librarygroup1","elements":"\n\\cdot\n\\times\n\\ast\n\\div\n\\diamond\n\\pm\n\\mp\n\\oplus\n\\ominus\n\\otimes\n\\oslash\n\\odot\n\\circ\n\\bullet\n\\asymp\n\\equiv\n\\subseteq\n\\supseteq\n\\leq\n\\geq\n\\preceq\n\\succeq\n\\sim\n\\simeq\n\\approx\n\\subset\n\\supset\n\\ll\n\\gg\n\\prec\n\\succ\n\\infty\n\\in\n\\ni\n\\forall\n\\exists\n\\neq\n"},"group2":{"groupname":"librarygroup2","elements":"\n\\leftarrow\n\\rightarrow\n\\uparrow\n\\downarrow\n\\leftrightarrow\n\\nearrow\n\\searrow\n\\swarrow\n\\nwarrow\n\\Leftarrow\n\\Rightarrow\n\\Uparrow\n\\Downarrow\n\\Leftrightarrow\n"},"group3":{"groupname":"librarygroup3","elements":"\n\\alpha\n\\beta\n\\gamma\n\\delta\n\\epsilon\n\\zeta\n\\eta\n\\theta\n\\iota\n\\kappa\n\\lambda\n\\mu\n\\nu\n\\xi\n\\pi\n\\rho\n\\sigma\n\\tau\n\\upsilon\n\\phi\n\\chi\n\\psi\n\\omega\n\\Gamma\n\\Delta\n\\Theta\n\\Lambda\n\\Xi\n\\Pi\n\\Sigma\n\\Upsilon\n\\Phi\n\\Psi\n\\Omega\n"},"group4":{"groupname":"librarygroup4","elements":"\n\\sum{a,b}\n\\sqrt[a]{b+c}\n\\int_{a}^{b}{c}\n\\iint_{a}^{b}{c}\n\\iiint_{a}^{b}{c}\n\\oint{a}\n(a)\n[a]\n\\lbrace{a}\\rbrace\n\\left| \\begin{matrix} a_1 & a_2 \\ a_3 & a_4 \\end{matrix} \\right|\n\\frac{a}{b+c}\n\\vec{a}\n\\binom {a} {b}\n{a \\brack b}\n{a \\brace b}\n"}},"texdocsurl":"http:\/\/docs.moodle.org\/37\/en\/Using_TeX_Notation"},{"name":"charmap","params":[]},{"name":"table","params":[],"allowBorders":false,"allowWidth":false,"allowBackgroundColour":false},{"name":"clear","params":[]}]},{"group":"undo","plugins":[{"name":"undo","params":[]}]},{"group":"accessibility","plugins":[{"name":"accessibilitychecker","params":[]},{"name":"accessibilityhelper","params":[]}]},{"group":"other","plugins":[{"name":"html","params":[]}]}],"pageHash":"2a434d39b60e9093356609ce9c50e1f359b65473"});
});
 M.util.js_pending('random5dc8028a67caa21'); Y.on('domready', function() {  M.util.js_pending('random5dc8028a67caa21'); Y.use('mform', function(Y) { M.form.initFormDependencies(Y, "mform1_HhDcviUoF7VpRrw", {"timeopen[enabled]":{"notchecked":{"1":[["timeopen[day]","timeopen[month]","timeopen[year]","timeopen[hour]","timeopen[minute]","timeopen[calendar]"]]}},"timeclose[enabled]":{"notchecked":{"1":[["timeclose[day]","timeclose[month]","timeclose[year]","timeclose[hour]","timeclose[minute]","timeclose[calendar]","attemptclosed","correctnessclosed","marksclosed","specificfeedbackclosed","generalfeedbackclosed","rightanswerclosed","overallfeedbackclosed"]]}},"timelimit[enabled]":{"notchecked":{"1":[["timelimit[number]","timelimit[timeunit]"]]}},"graceperiod[enabled]":{"notchecked":{"1":[["graceperiod[number]","graceperiod[timeunit]"]]}},"grade[modgrade_rescalegrades]":{"eq":{"":[["grade[modgrade_point]","grade[modgrade_point]","grade[modgrade_point]"]]}},"attemptimmediately":{"notchecked":{"1":[["correctnessimmediately","specificfeedbackimmediately","generalfeedbackimmediately","rightanswerimmediately"]]}},"attemptopen":{"notchecked":{"1":[["correctnessopen","specificfeedbackopen","generalfeedbackopen","rightansweropen"]]}},"attemptclosed":{"notchecked":{"1":[["correctnessclosed","specificfeedbackclosed","generalfeedbackclosed","rightanswerclosed"]]}},"preferredbehaviour":{"eq":{"deferredfeedback":[["correctnessduring","marksduring","specificfeedbackduring","generalfeedbackduring","rightanswerduring"],["canredoquestions"]],"deferredcbm":[["correctnessduring","marksduring","specificfeedbackduring","generalfeedbackduring","rightanswerduring"],["canredoquestions"]],"adaptive":{"1":["canredoquestions"]},"adaptivenopenalty":{"1":["canredoquestions"]}},"neq":{"wontmatch":[["attemptduring","overallfeedbackduring"]]}},"delay1[enabled]":{"notchecked":{"1":[["delay1[number]","delay1[timeunit]"]]}},"delay2[enabled]":{"notchecked":{"1":[["delay2[number]","delay2[timeunit]"]]}},"timelimit[number]":{"neq":[[["allowofflineattempts"]]]},"subnet":{"neq":{"":[["allowofflineattempts"]]}},"grade":{"eq":[[["feedbackboundaries[0]","feedbacktext[1]"]]]},"completionusegrade":{"notchecked":{"1":[["completionpass"]]}},"completionpass":{"notchecked":{"1":[["completionattemptsexhausted"]]}},"completionexpected[enabled]":{"notchecked":{"1":[["completionexpected[day]","completionexpected[month]","completionexpected[year]","completionexpected[hour]","completionexpected[minute]","completionexpected[calendar]"]]}},"overduehandling":{"neq":{"graceperiod":{"1":["graceperiod[number]","graceperiod[timeunit]","graceperiod[enabled]","graceperiod"]}}},"grade[modgrade_type]":{"neq":{"scale":{"1":["grade[modgrade_scale]"]},"point":{"1":["grade[modgrade_point]","grade[modgrade_rescalegrades]"]}},"eq":{"none":{"1":["gradecat","gradepass"]}}},"attempts":{"eq":{"1":{"1":["grademethod","attemptonlast","delay1[number]","delay1[timeunit]","delay1[enabled]","delay1","delay2[number]","delay2[timeunit]","delay2[enabled]","delay2"]},"2":{"1":["delay2[number]","delay2[timeunit]","delay2[enabled]","delay2"]}}},"completion":{"ne":{"2":{"1":["completionview","completionusegrade","completionpass","completionattemptsexhausted","completionpassgroup"]}},"eq":[{"1":["completionexpected[day]","completionexpected[month]","completionexpected[year]","completionexpected[hour]","completionexpected[minute]","completionexpected[calendar]","completionexpected[enabled]","completionexpected"]}]},"groupmode":{"eq":[{"1":["groupingid"]}]}});  M.util.js_complete('random5dc8028a67caa21'); });  M.util.js_complete('random5dc8028a67caa21'); });
 M.util.js_pending('random5dc8028a67caa22'); Y.on('domready', function() { M.util.js_complete("init");  M.util.js_complete('random5dc8028a67caa22'); });
})();
//]]>
</script>

        </div>
            
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
                ga('create', 'UA-65124233-5', 'auto', 'mcglobal');
                ga('create', 'UA-65124233-2', 'auto', 'mcregion');
    
                ga('mcglobal.set', 'dimension1', 'free.2');
                ga('mcregion.set', 'dimension1', 'free.2');
    
                ga('mcglobal.send', 'pageview');
                ga('mcregion.send', 'pageview');
            </script>
   
                </div>  
            </footer>
        </div><div id="yui3-css-stamp" style="position: absolute !important; visibility: hidden !important" class=""></div>


    </body>
        </html>