<%-- 
    Document   : login
    Created on : Nov 4, 2019, 6:04:54 PM
    Author     : OS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="user" class="model.Lecture" scope="session"/>
<!DOCTYPE html>
<html dir="ltr" lang="vi" xml:lang="vi" class="yui3-js-enabled">
    <head>
        <title>Đăng Kí</title>
        <link rel="shortcut icon" href="https://cacbank.moodlecloud.com/theme/image.php/boost/theme/1569995285/favicon">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="keywords" content="moodle, My new Moodle site: Đăng nhập vào trang">
        <link rel="stylesheet" type="text/css" href="https://cacbank.moodlecloud.com/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.css"><script async="" src="https://www.google-analytics.com/analytics.js"></script><script charset="utf-8" id="yui_3_17_2_1_1572865364677_8" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?m/1569995285/core/event/event-min.js&amp;m/1569995285/filter_mathjaxloader/loader/loader-min.js" async=""></script><script charset="utf-8" id="yui_3_17_2_1_1572865364677_19" src="https://cacbank.moodlecloud.com/theme/yui_combo.php?3.17.2/event-mousewheel/event-mousewheel-min.js&amp;3.17.2/event-resize/event-resize-min.js&amp;3.17.2/event-hover/event-hover-min.js&amp;3.17.2/event-touch/event-touch-min.js&amp;3.17.2/event-move/event-move-min.js&amp;3.17.2/event-flick/event-flick-min.js&amp;3.17.2/event-valuechange/event-valuechange-min.js&amp;3.17.2/event-tap/event-tap-min.js" async=""></script><script id="firstthemesheet" type="text/css">/** Required in order to fix style inclusion problems in IE with YUI **/</script><link rel="stylesheet" type="text/css" href="https://cacbank.moodlecloud.com/theme/styles.php/boost/1569995285_1569995291/all"> 
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body id="page-login-index" class="format-site path-login chrome dir-ltr lang-vi yui-skin-sam yui3-skin-sam cacbank-moodlecloud-com pagelayout-login course-1 context-1 notloggedin jsenabled eupopup">

        <div id="page-wrapper">
            <div id="page" class="container-fluid mt-0">
                <div id="page-content" class="row">
                    <div id="region-main-box" class="col-12">
                        <section id="region-main" class="col-12" aria-label="Nội dung">
                            <span class="notifications" id="user-notifications"></span>
                            <div role="main"><span id="maincontent"></span><div class="my-1 my-sm-5"></div>
                                <div class="row justify-content-center">
                                    <div class="col-xl-6 col-sm-8 ">
                                        <div class="card">
                                            <div class="card-block">
                                                <h2 class="card-header text-center">Đăng kí</h2>
                                                <div class="card-body">
                                                    <div class="row justify-content-md-center">
                                                        <div class="col-md-5">
                                                            <form class="mt-3" action="register" method="post" id="login">   
                                                                <div class="form-group">
                                                                    <label for="username">
                                                                        Mã đăng nhập:
                                                                    </label>
                                                                    <label>${code}</label>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="password" class="sr-only">Full Name</label>
                                                                    <input type="text" name="name" id="name" value="" class="form-control" placeholder="Full Name" autocomplete="current-password">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="password" class="sr-only">Email</label>
                                                                    <input type="text" name="email" id="email" value="" class="form-control" placeholder="Email(abc@gmail.com)" autocomplete="current-password">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="password" class="sr-only">Mật khẩu</label>
                                                                    <input type="password" name="password" id="password" value="" class="form-control" placeholder="Mật khẩu" autocomplete="current-password">
                                                                </div>
                                                                <button type="submit" class="btn btn-primary btn-block mt-3" id="loginbtn">Đăng Kí</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div></div>
                        </section>
                    </div>
                </div>
            </div>
            <footer id="page-footer" class="py-3 bg-dark text-light">
                <div class="container">
                    <div id="course-footer"></div>
                    <div class="logininfo">Bạn chưa đăng nhập.</div>
                    <div class="homelink"><a href="#">Trang chủ</a></div>
                    <div class="tool_dataprivacy"><a href="">Contact</a></div><a href="#">Get the mobile app</a><div class="policiesfooter"><a href="">Policies</a></div>
                </div>
            </footer>
    </body>
</html>