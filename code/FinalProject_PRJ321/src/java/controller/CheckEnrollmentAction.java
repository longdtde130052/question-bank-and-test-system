/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map;
//import static jdk.management.resource.internal.SimpleResourceContext.get;
import model.*;

/**
 *
 * @author OS
 */
public class CheckEnrollmentAction {

    public CheckEnrollmentAction() {
    }

    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Student s = (Student) map.get("USER");
        Competition co = (Competition) map.get("compe");
        StudentCompetition sc = StudentCompetition.getResult(s.getCode(), co.getComCode());
        if (sc == null) {
            return "enroll";
        } else {

            ArrayList<String> qCodes = TestBank.getQuestionByTcode(co.gettCode());
            ArrayList<Question> lq = QuestionBank.getQuestionsInTest(qCodes);

            String testVersion = sc.getTestVersion();

            ArrayList<String> ANS = new ArrayList<>();
            int dem = 0;
            for (int i1 = 0; i1 <= testVersion.length() - 1; i1++) {
                if (testVersion.charAt(i1) == ' ') {
                    String S = testVersion.substring(dem, i1);
                    dem = i1 + 1;
                    for (int j = 0; j < S.length() - 1; j++) {
                        if (S.charAt(j) == 'A') {
                            String Q = S.substring(0, j);
                            String A = S.substring(j, S.length());
                            for(Question q:lq){
                                if(q.getqCode().equals(Q)){
                                    q.setStuAnswer(A);
                                }
                            }
                        }
                    }
                }
            }

            int k = 1;
            for (Question q5 : lq) {
                q5.setIndex(k);
                k++;
            }
            k = 0;
            int currentPage = 0, i = 0;
            int size = 0;
            if (lq.size() % 5 != 0) {
                size = lq.size() / 5;
            } else {
                size = lq.size() / 5 - 1;
            }
            ArrayList<String> codesOfq = new ArrayList<>();
            ArrayList<Question> lqSmall = new ArrayList<>();
            for (Question q : lq) {
                lqSmall.add(q);
                if (lqSmall.size() == 5) {
                    break;
                }
            }

            for (Question q1 : lqSmall) {
                codesOfq.add(q1.getqCode());
            }

            ArrayList<Answer> la = Answer.getAnswersInTest(codesOfq);
            for (Question q : lqSmall) {
                int j = 0;
                for (Answer a : la) {
                    if (a.getqCode().equals(q.getqCode())) {
                        char c = (char) ((char) 65 + j);
                        a.setSign(String.valueOf(c));
                        j++;
                    }
                }
            }

            map.put("listsmall", lqSmall);
            map.put("curPage", currentPage);
            map.put("size", size);
            map.put("lquestion", lq);
            map.put("lanswer", la);
            //xu ly dong do khi user log out vao lai
            String remainingTime = Timer.getRemainingTime(sc.getStartTime(), Timestamp.valueOf(LocalDateTime.now()), co.getComTime());
            if (remainingTime == null) {
                return "result";
            }
            String mins = remainingTime.substring(0, 2);
            String secs = remainingTime.substring(3, 5);
            if (map.get("notRefresh") == null) {
                Timer t = new Timer(mins, secs);
                t.start();
                map.put("timer", t);
                map.put("notRefresh", "hihi");
            }
//          map.remove("notRefresh");
            return "open";
        }
    }

}
