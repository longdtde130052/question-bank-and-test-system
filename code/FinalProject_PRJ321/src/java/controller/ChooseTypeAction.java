/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;

/**
 *
 * @author OS
 */
public class ChooseTypeAction extends ActionSupport{
    
    
    public ChooseTypeAction() {
    }
    
    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        String subCode = (String) map.get("subName");
        map.put("errorMessage2", "cantdo");
        if(subCode==null||map.get("chap")==null){
            return ERROR;
        }
        if(map.get("errorMessage2")!=null){
            map.remove("errorMessage2");
        }
        return SUCCESS;
    }
    
}
