/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import model.*;

/**
 *
 * @author OS
 */
public class EditquestionAction extends ActionSupport {

    private String qid;

    public EditquestionAction() {
    }

    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Question q = Question.getQuestion(qid);
        ArrayList<Answer> la = Answer.getAnswers(qid);
        q.setLecModName(Lecture.getLecture(q.getLecModify()).getName());
        q.setLecName(Lecture.getLecture(q.getLecCode()).getName());
        map.put("QUESTION", q);
        map.put("ANSWER", la);
        return SUCCESS;
    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

}
