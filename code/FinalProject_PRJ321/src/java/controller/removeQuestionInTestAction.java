/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
//import com.sun.xml.internal.messaging.saaj.packaging.mime.util.QDecoderStream;
import java.util.ArrayList;
import java.util.Map;
import model.Question;
import model.TestTemp;

/**
 *
 * @author MyPC
 */
public class removeQuestionInTestAction {

    private String qcode;

    public removeQuestionInTestAction() {
    }

    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        ArrayList<Question> lq = (ArrayList<Question>) map.get("lquestemp");
        ArrayList<Question> lques = (ArrayList<Question>) map.get("listQuestion");
        for (Question q : lques) {
            if ((q.getqCode().trim()).equals(qcode)) {
                q.setStatus(0);
                System.out.println("hehe");
            }
        }
        map.put("listQuestion", lques);
        for (Question q : lq) {
            if ((q.getqCode().trim()).equals(qcode)) {
                lq.remove(q);
                return "success";
            }
        }
        
        map.put("lquestemp", lq);
        return null;
    }

    public String getQcode() {
        return qcode;
    }

    public void setQcode(String qcode) {
        this.qcode = qcode;
    }

}
