/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import model.*;

/**
 *
 * @author OS
 */
public class searchQuestion extends ActionSupport {

    private String subject;

    private int chapter;

    public searchQuestion() {
    }

    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        QuestionBank q = new QuestionBank();
        // lấy tất cả câu hỏi by subCode
        ArrayList<Question> lq = q.getQuestionBySub(subject);
        //lấy tên của giảng viên đã tạo câu hỏi bằng Leccode
        for (Question q1 : lq) {
            q1.setLecModName(Lecture.getLecture(q1.getLecModify()).getName());
            q1.setLecName(Lecture.getLecture(q1.getLecCode()).getName());
        }
        //get tất cả các chapter của môn học  
        ArrayList<Chapter> lc = Chapter.getAllChap(subject);
        
        ArrayList<Test1> lt = Test1.getTestBySub(subject);
        TestTemp tt = new TestTemp();
        map.put("testtemp", tt);
        map.put("listTest", lt);
        //put vào session
        map.put("listChap", lc);
        map.put("subName", getSubject());
        map.put("listQuestion", lq);
        return SUCCESS;
    }

    public String searchChap() {
        Map<String, Object> map = ActionContext.getContext().getSession();
        String s = (String) map.get("subName");
        QuestionBank q = new QuestionBank();
        map.put("chap", chapter);
        if (chapter == -1) {
            ArrayList<Question> lq = q.getQuestionBySub(s);
            for (Question q1 : lq) {
                q1.setLecModName(Lecture.getLecture(q1.getLecModify()).getName());
                q1.setLecName(Lecture.getLecture(q1.getLecCode()).getName());
            }
            map.put("listQuestion", lq);
        } else {
            ArrayList<Question> lq = q.getQuesgionByChap(chapter, s);
            for (Question q1 : lq) {
                q1.setLecModName(Lecture.getLecture(q1.getLecModify()).getName());
                q1.setLecName(Lecture.getLecture(q1.getLecCode()).getName());
            }
            map.put("listQuestion", lq);
        }
        return SUCCESS;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getChapter() {
        return chapter;
    }

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }

}
