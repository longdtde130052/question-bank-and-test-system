/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import java.util.ArrayList;
import java.util.Map;
import model.Question;
import model.*;

/**
 *
 * @author MyPC
 */
public class saveQuestionAction {

    public saveQuestionAction() {
    }

    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Test1 t =(Test1) map.get("testcurrent");
        ArrayList<Question> lq = (ArrayList<Question>) map.get("lquestemp");
        Test1.saveQuestionToTest(lq, t);
        return "success";
    }

}
