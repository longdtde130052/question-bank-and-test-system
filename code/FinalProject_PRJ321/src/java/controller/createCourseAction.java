/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import java.util.Map;
import model.Admin;
import model.Course;

/**
 *
 * @author ASUS
 */
public class createCourseAction {
    private String subject;
    private String fullname;
    private String lecture;
    private String enrollkey;
    public createCourseAction() {
    }
    
    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Admin a = (Admin)map.get("USER");
        Course c = new Course(fullname, lecture, subject, enrollkey, a.getCode());
        if(Course.createCourse(c)>0){
            return "success";
        }
        return "error";
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getLecture() {
        return lecture;
    }

    public void setLecture(String lecture) {
        this.lecture = lecture;
    }

    public String getEnrollkey() {
        return enrollkey;
    }

    public void setEnrollkey(String enrollkey) {
        this.enrollkey = enrollkey;
    }
    
}
