/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import model.Chapter;
import model.ClassList;
import model.Competition;
import model.Course;
import model.Lecture;
import model.Student;

/**
 *
 * @author MyPC
 */
public class CourseAction extends ActionSupport {

    private int cCode;

    public CourseAction() {
    }

    public String execute() throws Exception {

        Map<String, Object> map = ActionContext.getContext().getSession();
        Student s = (Student) map.get("USER");

        Course c = new Course(cCode);
        c.setLecName(Lecture.getLecture(c.getLecCode()).getName());
        ArrayList<Chapter> lchap = Chapter.getAllChap(c.getSubCode());
        Competition com = new Competition();
        map.put("COM", com);
        map.put("listchap", lchap);
        map.put("CCODE", c.getcCode());
        map.put("course", c);
        if (ClassList.checkEnroll(s.getCode(), cCode)) {
            return "enroll";
        }
        return SUCCESS;
    }

    public int getCCode() {
        return cCode;
    }

    public void setCCode(int cCode) {
        this.cCode = cCode;
    }

}
