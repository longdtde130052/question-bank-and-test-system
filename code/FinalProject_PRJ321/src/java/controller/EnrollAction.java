/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import model.ClassList;
import model.Course;
import model.Student;

/**
 *
 * @author MyPC
 */
public class EnrollAction extends ActionSupport{
    private String enrollKey;
    public EnrollAction() {
    }
    
    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Course c = (Course) map.get("course");
        Student s = (Student) map.get("USER");
        if(Course.checkKey(c.getcCode(), enrollKey)){
            ClassList cl = new ClassList("SE1304", s.getCode(), c.getcCode());
            if(!ClassList.checkEnroll(s.getCode(), c.getcCode())){
                ClassList.addList(cl);
            }
            ArrayList<ClassList> lcl = ClassList.getAL(s.getCode());
            for(ClassList l:lcl){
                l.setcName(Course.getC(l.getcCode()).getcName());
            }
            map.put("listCourseEnroll", lcl);
            map.remove("SEK");
            return SUCCESS;                   
        }
        else {
            String msg = "Vui lòng nhập lại";
            map.put("SEK", msg);
            return ERROR;
        }
    }

    public String getEnrollKey() {
        return enrollKey;
    }

    public void setEnrollKey(String enrollKey) {
        this.enrollKey = enrollKey;
    }
    
}
