/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;

/**
 *
 * @author MACBOOK
 */
public class CreateCompetitionAction extends ActionSupport{
    private int chapCode;

    public int getChapCode() {
        return chapCode;
    }

    public void setChapCode(int chapCode) {
        this.chapCode = chapCode;
    }
    
    public CreateCompetitionAction() {
    }
    
    public String execute() throws Exception {
         Map<String, Object> map = ActionContext.getContext().getSession();
         map.put("chap", chapCode);
        return SUCCESS;
    }
    
}
