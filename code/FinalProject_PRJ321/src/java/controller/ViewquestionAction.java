/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import model.*;

/**
 *
 * @author OS
 */
public class ViewquestionAction extends ActionSupport{
    
    private String qid;
    
    public ViewquestionAction() {
    }
    
    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Question q = Question.getQuestion(qid);
        ArrayList<Answer> la = Answer.getAnswers(qid);
        int i = 0;
        for(Answer a : la){
            char c = (char) ((char) 65 +i);
            a.setSign(String.valueOf(c));
            i++;
        }
        map.put("QUESTION", q);
        map.put("ANSWER", la);
        return SUCCESS;
    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }
    
    
}
