/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import model.Competition;
import model.Student;
import model.StudentCompetition;

/**
 *
 * @author MACBOOK
 */
public class ViewComLecAction extends ActionSupport {

    private int comCode;

    public int getComCode() {
        return comCode;
    }

    public void setComCode(int comCode) {
        this.comCode = comCode;
    }

    public ViewComLecAction() {
    }

    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();

        ArrayList<StudentCompetition> s = StudentCompetition.getAllByComCode(comCode);
        if (s.size() == 0) {
            return ERROR;
        }
        ArrayList<Student> stu = new ArrayList<Student>();
        double highestgrade = s.get(0).getGrade();
        for (StudentCompetition s1 : s) {
            stu.add(new Student(s1.getStuCode()));
            if (s1.getGrade() > highestgrade) {
                highestgrade = s1.getGrade();
            }

        }
        Competition com = Competition.getComByCode(comCode);
        map.put("compe", com);
        map.put("highestgrade", highestgrade);
        map.put("listComLec", s);
        map.put("listComStu", stu);
        return SUCCESS;
    }

}
