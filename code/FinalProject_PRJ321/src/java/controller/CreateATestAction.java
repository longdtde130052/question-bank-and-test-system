/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;
import model.*;

/**
 *
 * @author MyPC
 */
public class CreateATestAction {
    private String tname;
    private String subcode;
    public CreateATestAction() {
    }
    
    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Lecture l = (Lecture) map.get("USER");
        String subCode = subcode.substring(0, 6);
        System.out.println(tname);
        System.out.println(subCode);
        System.out.println(l.getCode());
        Test1 t = new Test1(tname, subCode, l.getCode(), LocalDate.now());
        Test1.createTest(t);
        ArrayList<Test1> lt = Test1.getTestBySub(subCode);
        map.put("listTest", lt);
        return "success";
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getSubcode() {
        return subcode;
    }

    public void setSubcode(String subcode) {
        this.subcode = subcode;
    }
    
}
