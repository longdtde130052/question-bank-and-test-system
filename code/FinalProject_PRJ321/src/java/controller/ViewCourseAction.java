/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import model.Chapter;
import model.Competition;
import model.Course;
import model.Lecture;

/**
 *
 * @author MyPC
 */
public class ViewCourseAction extends ActionSupport{
    private int cCode;
    public ViewCourseAction() {
    }
    
    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Course c = new Course(cCode);
        ArrayList<Chapter> lchap = Chapter.getAllChap(c.getSubCode());
        c.setLecName(Lecture.getLecture(c.getLecCode()).getName());
        Competition com = new Competition();
        map.put("COM", com);
        map.put("course", c);
        map.put("CCODE", c.getcCode());
        map.put("listchap", lchap);
        return SUCCESS;
        
    }

    public int getCCode() {
        return cCode;
    }

    public void setCCode(int cCode) {
        this.cCode = cCode;
    }
    
}
