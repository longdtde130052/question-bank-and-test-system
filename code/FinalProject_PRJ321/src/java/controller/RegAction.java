/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import model.Functional;
import model.Lecture;

/**
 *
 * @author MyPC
 */
public class RegAction extends ActionSupport{
    private String name;
    private String email;
    private String password; 
    public RegAction() {
    }
    
    public String execute() throws Exception {
        Map<String,Object> session = ActionContext.getContext().getSession();
        String code = (String) session.get("code");
        System.out.println(code);
        if(code.contains("LT")){
            Functional.addUser(name, email, password, "Lecture");
            return SUCCESS;
        }
        else{
            Functional.addUser(name, email, password, "Student");
            return SUCCESS;
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
