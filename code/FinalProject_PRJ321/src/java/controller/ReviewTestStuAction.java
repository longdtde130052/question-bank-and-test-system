/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import model.Answer;
import model.Competition;
import model.Lecture;
import model.Question;
import model.QuestionBank;
import model.Student;
import model.StudentCompetition;
import model.TestBank;

/**
 *
 * @author MACBOOK
 */
public class ReviewTestStuAction extends ActionSupport {

    private String stuCode;
    private int qid;

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public int getQid() {
        return qid;
    }

    public void setQid(int qid) {
        this.qid = qid;
    }

    public ReviewTestStuAction() {
    }

    public String execute() throws Exception {
            Map<String, Object> map = ActionContext.getContext().getSession();
        ///////////////////////////////////////////
            Competition co = (Competition) map.get("compe");
            Student stu;

            if (stuCode == null) {
                stu = (Student) map.get("USER");
            } else {
                stu = new Student(stuCode);
            }
    
            StudentCompetition s = new StudentCompetition(co.getComCode(), stu.getCode());
            String testVision = s.getTestVersion();
            ArrayList<String> qCodes = new ArrayList<String>();
            //Lay Cau tra loi cua Student
            ArrayList<String> ANS = new ArrayList<>();
            int dem = 0;
            for (int i1 = 0; i1 <= testVision.length() - 1; i1++) {
                if (testVision.charAt(i1) == ' ') {
                    String S = testVision.substring(dem, i1);
                    if ( !S.contains("A")){
                        String Q = S;
                        qCodes.add(Q);
                        dem = i1 + 1;
                        
                    } else {

                    for (int j = 0; j < S.length() - 1; j++) {
                        if (S.charAt(j) == 'A') {
                            String Q = S.substring(0, j);
                            qCodes.add(Q);
                            String A = S.substring(j, S.length());
                             dem = i1 + 1;
                             break;
                        }
                    }
                    }

                }

            }
        
        ArrayList<Question> lq = QuestionBank.getQuestionsInTest(qCodes);
        int k = 1;
        for (Question q5 : lq) {
            q5.setIndex(k);
            k++;
        }
        k = 0;
        int currentPage = 0, i = 0;
        int size = 0;
        if (lq.size() % 5 != 0) {
            size = lq.size() / 5;
        } else {
            size = lq.size() / 5 - 1;
        }
        ArrayList<String> codesOfq = new ArrayList<>();
        ArrayList<Question> lqSmall = new ArrayList<>();
        for (Question q : lq) {
            lqSmall.add(q);
            if (lqSmall.size() == 5) {
                break;
            }
        }

        for (Question q1 : lqSmall) {
            codesOfq.add(q1.getqCode());
        }

        ArrayList<Answer> la = Answer.getAnswersInTest(codesOfq);
        for (Question q : lqSmall) {
            int j = 0;
            for (Answer a : la) {
                if (a.getqCode().equals(q.getqCode())) {
                    char c = (char) ((char) 65 + j);
                    a.setSign(String.valueOf(c));
                    j++;
                }
            }
        }
        map.put("listsmall", lqSmall);
        map.put("curPage", currentPage);
        map.put("size", size);
        map.put("lquestion", lq);
        map.put("lanswer", la);

        //Lấy câu hỏi trong Test
        ArrayList<String> lqCode = new ArrayList<>();

        for (Question q : lq) {
            lqCode.add(q.getqCode());
        }
        ArrayList<Answer> a = Answer.getAnswersInTest(lqCode);
        //Cau tra loi lay trong database
        ArrayList<Answer> ans = new ArrayList<Answer>();
        for (Answer aa : a) {
            if (aa.getValue().equals("T")) {
                ans.add(aa);
            }
        }
        map.put("ans", ans);
        System.out.println(ans);
            // Xac dinh review Test tu Lecture hay Student
        dem =0;
           for (int i1 = 0; i1 <= testVision.length() - 1; i1++) {
                if (testVision.charAt(i1) == ' ') {
                    String S = testVision.substring(dem, i1);
                    
                    dem = i1 + 1;
                    for (int j = 0; j < S.length() - 1; j++) {
                        if (S.charAt(j) == 'A') {

                            String A = S.substring(j, S.length());
                            for (Answer a1 : ans) {
                                if (a1.getaCode().equals(A)) {
                                    A = A + "T"; //Neu la cau tra loi dung thi tra ve dạng: A?-?T
                                    break;
                                }

                            }
                            if (A.charAt(A.length() - 1) == 'T') {
                                ANS.add(A);
                            } else {
                                ANS.add(A + "F");//Neu la cau tra loi dung thi tra ve dạng: A?-?F
                            }

                        }
                    }

                }

            }
          
            map.put("ansStu", ANS);
            map.put("reviewtest", s);
            
        
        return SUCCESS;
    }

}
