/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.*;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author OS
 */
public class NextPrevConfirmTestAction extends ActionSupport {

    private String finish;
    private String nextPage;
    private String prevPage;

    public NextPrevConfirmTestAction() {
    }

    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
        Competition co = (Competition) map.get("compe");
        Student s = (Student) map.get("USER");
        if (finish != null) {
            //cập nhật câu hỏi trả lời trong list nhỏ ở trang trước
            ArrayList<Question> lqSmall = (ArrayList) map.get("listsmall");
            lqSmall.stream().forEach((q) -> {
                String code = q.getqCode().trim();
                if (request.getParameter(code) != null) {
                    q.setStuAnswer(request.getParameter(code));
                }
            });
            //cập nhật câu trả lời từ list nhỏ vào list lớn
            ArrayList<Question> lq = (ArrayList) map.get("lquestion");
            for (Question q1 : lq) {
                for (Question q2 : lqSmall) {
                    if (q1.getqCode().trim().equals(q2.getqCode().trim()) && q2.getStuAnswer() != null) {
                        q1.setStuAnswer(q2.getStuAnswer());
                    }
                }
            }
            //lấy testversion của sinh viên từ list lớn và lưu tạm thời
            ArrayList<String> qCodes = new ArrayList<>();
            String testVersion = "";
            for (Question q : lq) {
                if (q.getStuAnswer() != null) {
                    qCodes.add(q.getqCode());
                    testVersion = testVersion + q.getqCode() + q.getStuAnswer() + " ";
                }
            }
            
            
            //cập nhật testversion vào database
            StudentCompetition sc = StudentCompetition.getResult(s.getCode(), co.getComCode());
            sc.setTestVersion(testVersion);
            StudentCompetition.updateStudentCompe(sc);
            sc = StudentCompetition.getResult(s.getCode(), co.getComCode());

            map.put("studentResult", sc);
            map.put("lquestion", lq);
            map.put("listsmall", lqSmall);
            return "finish";
        }
        if (nextPage != null) {
            //cập nhật câu trả lời từ list nhỏ ở trang trước
            ArrayList<Question> lqSmall = (ArrayList) map.get("listsmall");
            for (Question q : lqSmall) {
                String code = q.getqCode().trim();
                if (request.getParameter(code) != null) {
                    q.setStuAnswer(request.getParameter(code));
                }
            }
            //cập nhật câu trả lời từ list nhỏ vào list lớn
            ArrayList<Question> lq = (ArrayList) map.get("lquestion");
            for (Question q1 : lq) {
                for (Question q2 : lqSmall) {
                    if (q1.getqCode().trim().equals(q2.getqCode().trim()) && q2.getStuAnswer() != null) {
                        q1.setStuAnswer(q2.getStuAnswer());
                    }
                }
            }
            //lấy testversion của sinh viên từ list lớn và lưu tạm thời
            ArrayList<String> qCodes = new ArrayList<>();
            String testVersion = "";
            for (Question q : lq) {
                if (q.getStuAnswer() != null) {
                    qCodes.add(q.getqCode());
                    testVersion = testVersion + q.getqCode() + q.getStuAnswer() + " ";
                }
            }
            //cập nhật testversion vào database
            StudentCompetition sc = StudentCompetition.getResult(s.getCode(), co.getComCode());
            sc.setTestVersion(testVersion);
            StudentCompetition.updateStudentCompe(sc);
            sc = StudentCompetition.getResult(s.getCode(), co.getComCode());
            int currentPage = (int) map.get("curPage");
            //lấy list nhỏ mới từ list lớn mới được cập nhật
            ArrayList<Question> lqSmall2 = new ArrayList<>();
            currentPage = currentPage + 1;
            int k = 1;
            for (Question q3 : lq) {
                if (k > currentPage * 5) {
                    lqSmall2.add(q3);
                }
                k++;
                if (lqSmall2.size() == 5) {
                    break;
                }
            }
            //lấy mã câu hỏi từ list nhỏ mới cập nhật
            ArrayList<String> codesOfq = new ArrayList<>();
            for (Question q4 : lqSmall2) {
                codesOfq.add(q4.getqCode());
            }
            //lấy mã của các câu trả lời của list nhỏ mới
            ArrayList<Answer> la = Answer.getAnswersInTest(codesOfq);
            for (Question q5 : lqSmall2) {
                int j = 0;
                for (Answer a : la) {
                    if (a.getqCode().equals(q5.getqCode())) {
                        char c = (char) ((char) 65 + j);
                        a.setSign(String.valueOf(c));
                        j++;
                    }
                }
            }
            map.put("studentResult", sc);
            map.put("lquestion", lq);
            map.put("curPage", currentPage);
            map.put("listsmall", lqSmall2);
            map.put("lanswer", la);
            return "next";
        }
        if (prevPage != null) {
            //cập nhật câu trả lời từ list nhỏ ở trang trước
            ArrayList<Question> lqSmall = (ArrayList) map.get("listsmall");
            for (Question q : lqSmall) {
                String code = q.getqCode().trim();
                if (request.getParameter(code) != null) {
                    q.setStuAnswer(request.getParameter(code));
                }
            }
            //cập nhật câu trả lời từ list nhỏ vào list lớn
            ArrayList<Question> lq = (ArrayList) map.get("lquestion");
            for (Question q1 : lq) {
                for (Question q2 : lqSmall) {
                    if (q1.getqCode().trim().equals(q2.getqCode().trim()) && q2.getStuAnswer() != null) {
                        q1.setStuAnswer(q2.getStuAnswer());
                    }
                }
            }
            //lấy testversion của sinh viên từ list lớn và lưu tạm thời
            ArrayList<String> qCodes = new ArrayList<>();
            String testVersion = "";
            for (Question q : lq) {
                if (q.getStuAnswer() != null) {
                    qCodes.add(q.getqCode());
                    testVersion = testVersion + q.getqCode() + q.getStuAnswer() + " ";
                }
            }
            //cập nhật testversion vào database
            StudentCompetition sc = StudentCompetition.getResult(s.getCode(), co.getComCode());
            sc.setTestVersion(testVersion);
            StudentCompetition.updateStudentCompe(sc);
            sc = StudentCompetition.getResult(s.getCode(), co.getComCode());
            int currentPage = (int) map.get("curPage");
            //lấy list nhỏ mới từ list lớn mới được cập nhật
            ArrayList<Question> lqSmall2 = new ArrayList<>();
            currentPage = currentPage - 1;
            int k = 1;
            for (Question q3 : lq) {
                if (k > currentPage * 5) {
                    lqSmall2.add(q3);
                }
                k++;
                if (lqSmall2.size() == 5) {
                    break;
                }
            }
            //lấy mã câu hỏi từ list nhỏ mới cập nhật
            ArrayList<String> codesOfq = new ArrayList<>();
            for (Question q4 : lqSmall2) {
                codesOfq.add(q4.getqCode());
            }
            //lấy mã của các câu trả lời của list nhỏ mới
            ArrayList<Answer> la = Answer.getAnswersInTest(codesOfq);
            for (Question q5 : lqSmall2) {
                int j = 0;
                for (Answer a : la) {
                    if (a.getqCode().equals(q5.getqCode())) {
                        char c = (char) ((char) 65 + j);
                        a.setSign(String.valueOf(c));
                        j++;
                    }
                }
            }
            map.put("studentResult", sc);
            map.put("lquestion", lq);
            map.put("curPage", currentPage);
            map.put("listsmall", lqSmall2);
            map.put("lanswer", la);
            return "prev";
        }
        return null;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }

    public String getPrevPage() {
        return prevPage;
    }

    public void setPrevPage(String prevPage) {
        this.prevPage = prevPage;
    }

}
