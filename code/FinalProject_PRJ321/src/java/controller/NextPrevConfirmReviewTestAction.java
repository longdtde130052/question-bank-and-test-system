/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.Answer;
import model.Question;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author MACBOOK
 */
public class NextPrevConfirmReviewTestAction {
      private String nextPage;
    private String prevPage;

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }

    public String getPrevPage() {
        return prevPage;
    }

    public void setPrevPage(String prevPage) {
        this.prevPage = prevPage;
    }

    public NextPrevConfirmReviewTestAction() {
    }
    
    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);


       if (nextPage != null) {
            ArrayList<Question> lqSmall = (ArrayList) map.get("listsmall");
            for (Question q : lqSmall) {
                String code = q.getqCode().trim();
                if (request.getParameter(code) != null) {
                    q.setStuAnswer(request.getParameter(code));
                }
            }
            //cap nhat
            ArrayList<Question> lq = (ArrayList) map.get("lquestion");
            for (Question q1 : lq) {
                for (Question q2 : lqSmall) {
                    if (q1.getqCode().trim().equals(q2.getqCode().trim()) && q2.getStuAnswer() != null) {
                        q1.setStuAnswer(q2.getStuAnswer());
                    }
                }
            }

            int currentPage = (int) map.get("curPage");

            ArrayList<Question> lqSmall2 = new ArrayList<>();
            currentPage = currentPage + 1;
            int k = 1;
            for (Question q3 : lq) {
                if (k > currentPage * 5) {
                    lqSmall2.add(q3);
                }
                k++;
                if (lqSmall2.size() == 5) {
                    break;
                }
            }
            ArrayList<String> codesOfq = new ArrayList<>();
            for (Question q4 : lqSmall2) {
                codesOfq.add(q4.getqCode());
            }

            ArrayList<Answer> la = Answer.getAnswersInTest(codesOfq);
            for (Question q5 : lqSmall2) {
                int j = 0;
                for (Answer a : la) {
                    if (a.getqCode().equals(q5.getqCode())) {
                        char c = (char) ((char) 65 + j);
                        a.setSign(String.valueOf(c));
                        j++;
                    }
                }
            }
            map.put("lquestion", lq);
            map.put("curPage", currentPage);
            map.put("listsmall", lqSmall2);
            map.put("lanswer", la);
            return "next";
        }
        if (prevPage != null) {
            ArrayList<Question> lqSmall = (ArrayList) map.get("listsmall");
            for (Question q : lqSmall) {
                String code = q.getqCode().trim();
                if (request.getParameter(code) != null) {
                    q.setStuAnswer(request.getParameter(code));
                }
            }
            //cap nhat
            ArrayList<Question> lq = (ArrayList) map.get("lquestion");
            for (Question q1 : lq) {
                for (Question q2 : lqSmall) {
                    if (q1.getqCode().trim().equals(q2.getqCode().trim()) && q2.getStuAnswer() != null) {
                        q1.setStuAnswer(q2.getStuAnswer());
                    }
                }
            }

            int currentPage = (int) map.get("curPage");

            ArrayList<Question> lqSmall2 = new ArrayList<>();
            currentPage = currentPage - 1;
            int k = 1;
            for (Question q3 : lq) {
                if (k > currentPage * 5) {
                    lqSmall2.add(q3);
                }
                k++;
                if (lqSmall2.size() == 5) {
                    break;
                }
            }
            ArrayList<String> codesOfq = new ArrayList<>();
            for (Question q4 : lqSmall2) {
                codesOfq.add(q4.getqCode());
            }

            ArrayList<Answer> la = Answer.getAnswersInTest(codesOfq);
            for (Question q5 : lqSmall2) {
                int j = 0;
                for (Answer a : la) {
                    if (a.getqCode().equals(q5.getqCode())) {
                        char c = (char) ((char) 65 + j);
                        a.setSign(String.valueOf(c));
                        j++;
                    }
                }
            }
            map.put("lquestion", lq);
            map.put("curPage", currentPage);
            map.put("listsmall", lqSmall2);
            map.put("lanswer", la);
            return "prev";
        }
        return null;
    }
    
}
