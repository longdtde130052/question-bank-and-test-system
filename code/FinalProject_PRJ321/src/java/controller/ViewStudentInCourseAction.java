/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import java.util.ArrayList;
import java.util.Map;
import model.Course;
import model.Student;
import model.StudentCourse;

/**
 *
 * @author Tr_hieu
 */
public class ViewStudentInCourseAction {
    
    public ViewStudentInCourseAction() {
    }
    
    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Course c =  (Course)map.get("course");
        ArrayList<Student> ls = StudentCourse.getAllStudentInACourses(c.getcCode());
        map.put("listStudent", ls);
        return SUCCESS;
    }

}
