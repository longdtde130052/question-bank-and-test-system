/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import model.*;

/**
 *
 * @author OS
 */
public class LoginAction extends ActionSupport {

    private String username;
    private String password;

    public LoginAction() {

    }

    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        if (username.contains("LT")) {
            Lecture l = Lecture.loginAsLecture(username, password);
            if (l != null) {
                map.put("USER", l);
                ArrayList<Course> lc = Course.getCourse(l.getCode());
                for(Course c:lc){
                   c.setSubName(new Subject(c.getSubCode()).getSubName());
                } 
                map.put("listCourse", lc);
                map.put("HomePage", "homeLec_1.jsp");
                return "lecture";
            } else {
                map.put("errorMessage", "Mật khẩu không chính xác vui lòng nhập lại!");
            }
        } else if (username.contains("ST")) {
            Student s = Student.loginAsStudent(username, password);
            if (s != null) {
                map.put("USER", s);
                ArrayList<Course> lc = Course.viewCourses();
                for (Course c : lc) {
                    c.setLecName(Lecture.getLecture(c.getLecCode()).getName());
                }
                ArrayList<ClassList> lcl = ClassList.getAL(s.getCode());
                for (ClassList cl : lcl) {
                    cl.setcName(Course.getC(cl.getcCode()).getcName());
                }
                map.put("listCourseEnroll", lcl);
                map.put("listCourse", lc);
                return "student";
            } else {
                map.put("errorMessage", "Mật khẩu không chính xác vui lòng nhập lại!");
            }
        } else if(username.contains("AD")){
            Admin a = Admin.loginAsAdmin(username, password);
            if(a!=null){
                map.put("USER", a);
                return "admin";
            }else {
                map.put("errorMessage", "Mật khẩu không chính xác vui lòng nhập lại!");
            }
        }
        else {
            map.put("errorMessage", "Mật khẩu không chính xác vui lòng nhập lại!");
        }
        return ERROR;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String logout() {
        Map<String, Object> session = ActionContext.getContext().getSession();
        if (session.containsKey("USER")) {
            session.remove("USER");
            ((org.apache.struts2.dispatcher.SessionMap) session).invalidate();
        }
        return SUCCESS;
    }

}
