/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import model.Lecture;
import model.Student;

/**
 *
 * @author MACBOOK
 */
public class EditProfileAction extends ActionSupport{
    private String name,email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public EditProfileAction() {
    }
    
    public String execute() throws Exception {
        
        Map<String, Object> sess = ActionContext.getContext().getSession();
        try{
        Student s = (Student)sess.get("USER");
         s.setName(name); s.setEmail(email);
       Student.updateStudent(s);
         sess.put("USER", s);
          return SUCCESS;
        }catch (Exception e){
            Lecture l = (Lecture)sess.get("USER");
            l.setName(name); l.setEmail(email);
        Lecture.updateLecture(l);
         sess.put("USER", l);
          return SUCCESS;
        }
        
         
          
       
      
      
    }
}
