/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import static javafx.scene.input.KeyCode.A;
import model.*;

/**
 *
 * @author OS
 */
public class UpdateQuestionAction extends ActionSupport {

    private String questionContent;
    private String[] answerContent;
    private String[] values;

    public UpdateQuestionAction() {
    }

    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Question q = (Question) map.get("QUESTION");
        ArrayList<Answer> la = (ArrayList) map.get("ANSWER");
        Lecture l = (Lecture) map.get("USER");
        q.setLecModify(l.getCode());
        q.setqContent(questionContent);
        Question.updateQuestion(q);
        int i = 0;
        for (Answer a : la) {
            a.setaContent(answerContent[i]);
            a.setValue(values[i]);
            Answer.updateAnswer(a);
            i++;
        }
        //cap nhat lich su chinh sua
        QuestionBank qb = new QuestionBank();
        String s = (String) map.get("subName");
        ArrayList<Question> lq = qb.getQuestionBySub(s);
        for (Question q1 : lq) {
            q1.setLecModName(Lecture.getLecture(q1.getLecModify()).getName());
            q1.setLecName(Lecture.getLecture(q1.getLecCode()).getName());
        }
        map.put("listQuestion", lq);
        return SUCCESS;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String[] getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String[] answerContent) {
        this.answerContent = answerContent;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

}
