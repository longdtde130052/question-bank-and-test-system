/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import model.Competition;
import model.Course;

/**
 *
 * @author MACBOOK
 */
public class CreateTestAction extends ActionSupport {

    private String name;
    private String timeopenday, timeopenmonth, timeopenyear, timeopenhour, timeopenminute;
    private String timecloseday, timeclosemonth, timecloseyear, timeclosehour, timecloseminute;
    private String timelimitnumber, timelimittimeunit;
    private String password, code;

    public CreateTestAction() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeopenday() {
        return timeopenday;
    }

    public void setTimeopenday(String timeopenday) {
        this.timeopenday = timeopenday;
    }

    public String getTimeopenmonth() {
        return timeopenmonth;
    }

    public void setTimeopenmonth(String timeopenmonth) {
        this.timeopenmonth = timeopenmonth;
    }

    public String getTimeopenyear() {
        return timeopenyear;
    }

    public void setTimeopenyear(String timeopenyear) {
        this.timeopenyear = timeopenyear;
    }

    public String getTimeopenhour() {
        return timeopenhour;
    }

    public void setTimeopenhour(String timeopenhour) {
        this.timeopenhour = timeopenhour;
    }

    public String getTimeopenminute() {
        return timeopenminute;
    }

    public void setTimeopenminute(String timeopenminute) {
        this.timeopenminute = timeopenminute;
    }

    public String getTimecloseday() {
        return timecloseday;
    }

    public void setTimecloseday(String timecloseday) {
        this.timecloseday = timecloseday;
    }

    public String getTimeclosemonth() {
        return timeclosemonth;
    }

    public void setTimeclosemonth(String timeclosemonth) {
        this.timeclosemonth = timeclosemonth;
    }

    public String getTimecloseyear() {
        return timecloseyear;
    }

    public void setTimecloseyear(String timecloseyear) {
        this.timecloseyear = timecloseyear;
    }

    public String getTimeclosehour() {
        return timeclosehour;
    }

    public void setTimeclosehour(String timeclosehour) {
        this.timeclosehour = timeclosehour;
    }

    public String getTimecloseminute() {
        return timecloseminute;
    }

    public void setTimecloseminute(String timecloseminute) {
        this.timecloseminute = timecloseminute;
    }

    public String getTimelimitnumber() {
        return timelimitnumber;
    }

    public void setTimelimitnumber(String timelimitnumber) {
        this.timelimitnumber = timelimitnumber;
    }

    public String getTimelimittimeunit() {
        return timelimittimeunit;
    }

    public void setTimelimittimeunit(String timelimittimeunit) {
        this.timelimittimeunit = timelimittimeunit;
    }

    public String execute() throws Exception {
        Map<String,Object> sess = ActionContext.getContext().getSession();
        int chapCode =(Integer) sess.get("chap");
        Course c = (Course )sess.get("course");
        String startTime = "", endTime = "";
        int time = Integer.parseInt(timelimitnumber);
        startTime = timeopenyear + "-" + timeopenmonth + "-" + timeopenday + " " + timeopenhour + ":" + timeopenminute;
        endTime = timecloseyear + "-" + timeclosemonth + "-" + timecloseday + " " + timeclosehour + ":" + timecloseminute;
        int tcode = Integer.parseInt(code);
        int i = Competition.addCom(tcode, name, password, startTime, endTime, time,chapCode,c.getcCode());
        System.out.println(i);
        if (i > 0) {
            return SUCCESS;
        } else {
            sess.put("errorMessage3", "Please, check input data 1 more time");
            return ERROR;
        }
    }

}
