/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import model.*;

/**
 *
 * @author OS
 */
public class DeleteQuestionAction extends ActionSupport{
    
    private String qid;
    
    public DeleteQuestionAction() {
    }
    
    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        System.out.println(Answer.deleteAnswer(qid));
        System.out.println(Question.deleteQuestionFromTest(qid));
        System.out.println(Question.deleteQuestion(qid));
        QuestionBank qb = new QuestionBank();
        String s = (String) map.get("subName");
        ArrayList<Question> lq = qb.getQuestionBySub(s);
        for (Question q1 : lq) {
            q1.setLecModName(Lecture.getLecture(q1.getLecModify()).getName());
            q1.setLecName(Lecture.getLecture(q1.getLecCode()).getName());
        }
        map.put("listQuestion", lq);
        return SUCCESS;
    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }
    
    
}
