/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import model.Lecture;
import model.Student;

/**
 *
 * @author MACBOOK
 */
public class ChangePassAction extends ActionSupport{
    private String password,newpassword1;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewpassword1() {
        return newpassword1;
    }

    public void setNewpassword1(String newpassword1) {
        this.newpassword1 = newpassword1;
    }
    
    public ChangePassAction() {
    }
    
    public String execute() throws Exception {
       Map<String,Object> sess = ActionContext.getContext().getSession();
       try{
       Student s = (Student) sess.get("USER");
       if (s.getPassword().equals(getPassword())) {
           s.setPassword(getNewpassword1());
           Student.updateStudent(s);
           sess.put("USER", s);
           return SUCCESS;
       }
       else {
           String msg ="Vui lòng nhập lại mật khẩu";
           sess.put("SMK", msg);
           return ERROR;         
       }
       } catch(Exception e){
           Lecture l = (Lecture)sess.get("USER");
       if (l.getPassword().equals(getPassword())) {
           l.setPassword(getNewpassword1());
           Lecture.updateLecture(l);
           sess.put("USER", l);
           return SUCCESS;
       }
       else {
           String msg ="Vui lòng nhập lại mật khẩu";
           sess.put("SMK", msg);
           return ERROR;         
       }
       }
    }
    
}
