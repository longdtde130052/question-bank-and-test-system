/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import java.util.ArrayList;
import java.util.Map;
import model.*;

/**
 *
 * @author OS
 */
public class QuestionBankAction {
    
    public QuestionBankAction() {
    }
    
    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Lecture l = (Lecture) map.get("USER");
        ArrayList<String> ls = Subject.getAllSubByTeacher(l.getCode());
        ArrayList<Subject> listSub = Subject.getAll(ls);
        map.put("ListSub", listSub);
        return "success";
    }
    
}
