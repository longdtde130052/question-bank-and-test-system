/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import model.Functional;
import model.Lecture;

/**
 *
 * @author MyPC
 */
public class uRightAction extends ActionSupport{
    private String userright; 
    public uRightAction() {
    }
    
    public String execute() throws Exception {
        Map<String,Object> session = ActionContext.getContext().getSession();
        if(userright.equals("Lecture")){            
            String code = Functional.getMaxID("Lecture");
            session.put("code", code);
            return SUCCESS;
        }
        else{
            String code = Functional.getMaxID("Student");
            session.put("code", code);
            return ERROR;
        }
    }

    public String getUserright() {
        return userright;
    }

    public void setUserright(String userright) {
        this.userright = userright;
    }
    
}
