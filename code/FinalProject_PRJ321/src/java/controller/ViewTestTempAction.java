/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import java.util.ArrayList;
import java.util.Map;
import model.Answer;
import model.Question;
import model.*;
import model.TestTemp;

/**
 *
 * @author MyPC
 */
public class ViewTestTempAction {
    
    public ViewTestTempAction() {
    }
    
    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        TestTemp tt = (TestTemp) map.get("testtemp");
        if((String) map.get("testcode")==null){
            return "error";
        }
        if(tt==null){
            return "error";
        }
        int s = Integer.parseInt((String) map.get("testcode"));
        Test1 t = Test1.getTest1(s);
        ArrayList<Question> lq = tt.getQuesTest();
        ArrayList<String> qCode = new ArrayList<>();
        for(Question q :tt.getQuesTest()){
            qCode.add(q.getqCode().trim());
            q.setqCode(q.getqCode().trim());
        }
        ArrayList<Answer> la = Answer.getAnswersInTest(qCode);
        for (Question q : lq) {
            int i = 0;
            for (Answer a : la) {
                a.setqCode(a.getqCode().trim());
                if (a.getqCode().equals(q.getqCode())) {
                    char c = (char) ((char) 65 + i);
                    a.setSign(String.valueOf(c));
                    i++;
                }
            }
        }
        map.put("testcurrent", t);
        map.put("testtemp", tt);
        map.put("lquestemp", lq);
        map.put("lanstemp", la);
        return "success";
    }
    
}
