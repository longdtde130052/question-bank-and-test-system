/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import java.util.ArrayList;
import java.util.Map;
import model.Question;
import model.TestTemp;

/**
 *
 * @author MyPC
 */
public class AddQuestionToTestAction {

    private String[] questionCode;
    private String testcode;

    public AddQuestionToTestAction() {
    }

    public String execute() throws Exception {
        if(questionCode==null){
            return "error";
        }
        Map<String, Object> map = ActionContext.getContext().getSession();
        TestTemp tt = (TestTemp) map.get("testtemp");
        map.put("testcode", testcode);
        ArrayList<Question> listq = (ArrayList<Question>) map.get("listQuestion");
        for (int i = 0; i < questionCode.length; i++) {
            Question q = Question.getQuestion(questionCode[i]);
            for (Question ques : listq) {
                if(ques.getqCode().equals(q.getqCode())){
                    ques.setStatus(1);
                }
            }
            tt.getQuesTest().add(q);
        }
        map.put("testtemp", tt);
        map.put("listQuestion", listq);
        return "success";
    }

    public String getTestcode() {
        return testcode;
    }

    public void setTestcode(String testcode) {
        this.testcode = testcode;
    }

    public String[] getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(String[] questionCode) {
        this.questionCode = questionCode;
    }

}
