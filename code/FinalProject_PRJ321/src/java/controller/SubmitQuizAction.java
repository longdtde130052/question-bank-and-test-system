/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map;
import model.*;

/**
 *
 * @author OS
 */
public class SubmitQuizAction {

    public SubmitQuizAction() {
    }

    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Student s = (Student) map.get("USER");
        Competition co = (Competition) map.get("compe");
        StudentCompetition sc = StudentCompetition.getResult(s.getCode(), co.getComCode());
        ArrayList<Question> lq = (ArrayList) map.get("lquestion");
        ArrayList<String> qCodes = new ArrayList<>();
        String testVersion = "";
        int numOfTrueQ = 0;
        for (Question q : lq) {
            if (q.getStuAnswer() != null) {
                qCodes.add(q.getqCode());
                testVersion = testVersion + q.getqCode() + q.getStuAnswer() + " ";
            } else {
                 testVersion = testVersion+ q.getqCode()+" ";
            }
        }
        // Thêm nữa câu không có câu trả lời vào TestVersion
           
    
        ArrayList<String> lTrueAnswer = Answer.getCorrectAnswer(qCodes);
        for (Question q : lq) {
            for (String a : lTrueAnswer) {
                if (q.getStuAnswer() != null) {
                    if (q.getStuAnswer().equals(a)) {
                        numOfTrueQ++;
                    
                }
            }
        }
        }
        //xu ly diem
        double averageGrade = (double) 10/lq.size();
        double grade = numOfTrueQ * averageGrade;
        grade = (double) Math.round(grade*100)/100;  
        Timestamp endTime = Timestamp.valueOf(LocalDateTime.now());
        String doneTime = Timer.getDoneTime(sc.getStartTime(), endTime);      
        sc.setGrade(grade);
        sc.setEndTime(endTime);
        sc.setDoneTime(doneTime);
        sc.setTestVersion(testVersion);   
        sc.setNumberOfRightAns(numOfTrueQ);
        StudentCompetition.updateStudentCompe(sc);
        System.out.println(Test1.getTestByCode(co.gettCode()));
        int num = Test1.getTestByCode(co.gettCode()).getNumberOfQues();
        map.put("numberOfQues", num);
        map.put("studentResult", sc);
        return "success";
    }
}
