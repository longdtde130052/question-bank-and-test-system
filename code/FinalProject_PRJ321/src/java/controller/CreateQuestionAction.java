/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import model.*;

/**
 *
 * @author OS
 */
public class CreateQuestionAction extends ActionSupport {

    private String questionContent;
    private int qlevel;
    private String[] answerContent;
    private String[] values;

    public CreateQuestionAction(){
    }

    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        if(Question.QuestionExist(questionContent.trim())){
            map.put("WARNING", "Question exists in Question Bank");
            return "error";
        }
        for(int i = 0; i<answerContent.length;i++){
            for(int j = i+1; j<=answerContent.length-1;j++){
                if(answerContent[i].equals(answerContent[j])){
                    map.put("WARNING", "Answer is duplicated!");
                    return "error";
                }
            }
        }
        int check = 0;
        for(int i = 0; i<values.length;i++){
            for(int j = i+1; j<=values.length-1;j++){
                if(!values[i].equals(values[j])){
                    check++;
                }
            }
        }
        if(check==0){
            map.put("WARNING", "At least one answer has true value!");
            return "error";
        }
        ArrayList<Answer> la = new ArrayList<>();
        String subCode = (String) map.get("subName");
        int chap = (int) map.get("chap");
        String type = (String) map.get("TYPEQUESTION");
        Lecture l = (Lecture) map.get("USER");
        int numOfAns = (int) map.get("NUMOFANS");
        if(qlevel==0){
            qlevel=1;
        }
        Question q = new Question(QuestionBank.getNextQcode(), subCode, chap ,type ,questionContent , qlevel, l.getCode());
        String[] acodes = Answer.getNextACodes(numOfAns);
        String qcode = Question.addQuestion(q);
        for (int i = 0;i<numOfAns;i++) {
            la.add(new Answer(acodes[i], qcode, answerContent[i], values[i]));
        }
        Answer.addAnswers(qcode, la);
        //cap nhat lai danh sach cau hoi
        QuestionBank qb = new QuestionBank();
        String s = (String) map.get("subName");
        System.out.println("subName");
        ArrayList<Question> lq = qb.getQuestionBySub(s);
        for (Question q1 : lq) {
            q1.setLecModName(Lecture.getLecture(q1.getLecModify()).getName());
            q1.setLecName(Lecture.getLecture(q1.getLecCode()).getName());
        }
        map.put("listQuestion", lq);
        return SUCCESS;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String[] getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String[] answerContent) {
        this.answerContent = answerContent;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    public int getQlevel() {
        return qlevel;
    }

    public void setQlevel(int qlevel) {
        this.qlevel = qlevel;
    }

    
    
}
