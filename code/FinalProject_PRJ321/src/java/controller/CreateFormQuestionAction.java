/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;

/**
 *
 * @author OS
 */
public class CreateFormQuestionAction extends ActionSupport{
    
    private String typeQuestion;
    private int numOfAns;
    
    public CreateFormQuestionAction() {
    }
    
    public String execute() throws Exception {
        Map<String,Object> map = ActionContext.getContext().getSession();
        map.put("TYPEQUESTION", typeQuestion);
        if(numOfAns!=0 && typeQuestion.equals("MultiChoice")){
            map.put("NUMOFANS", numOfAns);            
        }else{
            numOfAns = 2;
            map.put("NUMOFANS", numOfAns);
        }
        return SUCCESS;
    }

    public String getTypeQuestion() {
        return typeQuestion;
    }

    public void setTypeQuestion(String typeQuestion) {
        this.typeQuestion = typeQuestion;
    }

    public int getNumOfAns() {
        return numOfAns;
    }

    public void setNumOfAns(int numOfAns) {
        this.numOfAns = numOfAns;
    }
    
    
}
