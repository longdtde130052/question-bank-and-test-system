
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;
import model.*;

/**
 *
 * @author OS
 */
public class AttemptQuizAction {

    private int qid;

    public AttemptQuizAction() {
    }

    public String execute() throws Exception {
        Map<String, Object> map = ActionContext.getContext().getSession();
        Competition c = Competition.getComByCode(qid);
        Student s = (Student) map.get("USER");
        StudentCompetition sc = StudentCompetition.getResult(s.getCode(), c.getComCode());
        if (sc != null) {
            if (sc.getDoneTime() != null) {
                System.out.println(c.gettCode());
                System.out.println(Test1.getTestByCode(c.gettCode()));
                int num = Test1.getTestByCode(c.gettCode()).getNumberOfQues();
                map.put("numberOfQues", num);
                map.put("compe", c);
                map.put("studentResult", sc);
                return "review";
            }
        }
        Timestamp now = Timestamp.valueOf(LocalDateTime.now());
        if (now.compareTo(c.getStartTime()) > 0 && now.compareTo(c.getEndTime()) < 0) {
            map.put("compe", c);
            return "open";
        } else if (now.compareTo(c.getStartTime()) < 0) {
            map.put("compe", c);
            return "notyet";
        } else {
            map.put("compe", c);
            return "close";
        }   
    }

    public int getQid() {
        return qid;
    }

    public void setQid(int qid) {
        this.qid = qid;
    }

}
