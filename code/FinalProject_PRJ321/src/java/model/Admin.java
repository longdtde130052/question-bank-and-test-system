/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.DatabaseInfo.dbURL;
import static model.DatabaseInfo.driveName;
import static model.DatabaseInfo.passDB;
import static model.DatabaseInfo.userDB;

/**
 *
 * @author ASUS
 */
public class Admin {
    private String code;
    private String name;
    private String email;
    private String password;

    public Admin() {
    }

    public Admin(String code, String name, String email, String password) {
        this.code = code;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
     public static Admin getAdmin(String aCode) {
         Admin a = null;
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("Select AdCode,AdName,AdMail,Password from Admin where AdCode='"+aCode+"'");
            if(rs.next()){
                a = new Admin(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
            con.close();
            stmt.close();
            return a;
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
     }
    public static Admin loginAsAdmin(String aCode, String pass) {
        Admin a = getAdmin(aCode);
        if (a != null) {
            if (a.getPassword().equals(pass)) {
                return a;
            }
        }
        return null;
    }
    public static void main(String[] args) {
        System.out.println(loginAsAdmin("AD0001", "123"));
    }

    @Override
    public String toString() {
        return "Admin{" + "code=" + code + ", name=" + name + ", email=" + email + ", password=" + password + '}';
    }
    
}
