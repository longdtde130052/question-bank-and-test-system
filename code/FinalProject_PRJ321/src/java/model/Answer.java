/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.DatabaseInfo.dbURL;
import static model.DatabaseInfo.driveName;
import static model.DatabaseInfo.passDB;
import static model.DatabaseInfo.userDB;

/**
 *
 * @author OS
 */
public class Answer implements Serializable, DatabaseInfo {

    private String aCode;
    private String qCode;
    private String aContent;
    private String value;
    private String sign;

    //hehehehehe


    // abc xyz 
    //absdhbasjbdasd 


    //asdadfasdjkfasndfkngf,a,sfbsdaf
    // abc xyz 
    //absdhbasjbdasd 


    public Answer() {
    }

    public Answer(String aCode, String qCode, String aContent, String value) {
        this.aCode = aCode;
        this.qCode = qCode;
        this.aContent = aContent;
        this.value = value;
    }

    public Answer(String aCode, String qCode, String aContent, String value, String sign) {
        this.aCode = aCode;
        this.qCode = qCode;
        this.aContent = aContent;
        this.value = value;
        this.sign = sign;
    }

    
    
    public String getaCode() {
        return aCode;
    }

    public void setaCode(String aCode) {
        this.aCode = aCode;
    }

    public String getqCode() {
        return qCode;
    }

    public void setqCode(String qCode) {
        this.qCode = qCode;
    }

    public String getaContent() {
        return aContent;
    }

    public void setaContent(String aContent) {
        this.aContent = aContent;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public static ArrayList<Answer> getAnswers(String qCode) {
        ArrayList<Answer> lq = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select ACode, QCode, AContent, Value from Answer where QCode = '" + qCode + "'");
            while (rs.next()) {
                lq.add(new Answer(rs.getString(1).trim(), rs.getString(2).trim(), rs.getString(3), rs.getString(4)));
            }
            con.close();
            return lq;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Answer{" + "aCode=" + aCode + ", qCode=" + qCode + ", aContent=" + aContent + ", value=" + value + ", sign=" + sign + '}';
    }

    public static Boolean updateAnswer(Answer a) {
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            PreparedStatement stmt = con.prepareStatement("update Answer set AContent = ?, Value = ? where ACode = ?");
            stmt.setString(1, a.getaContent());
            stmt.setString(2, a.getValue());
            stmt.setString(3, a.getaCode());
            int r = stmt.executeUpdate();
            con.close();
            return r == 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean deleteAnswer(String qCode) {
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            PreparedStatement stmt = con.prepareStatement("delete from Answer where QCode = ?");
            stmt.setString(1, qCode);
            int r = stmt.executeUpdate();
            con.close();
            return r == 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String[] getNextACodes(int numOfAnswer) {
        String[] aCodes = new String[numOfAnswer];
        String s = QuestionBank.getNextQcode();
        s = s.substring(1);
        int k = 0;
        for (int i = 1; i <= numOfAnswer; i++) {
            aCodes[k] = "A" + s + "-" + i;
            k++;
        }
        return aCodes;
    }

    public static void addAnswers(String qCode, ArrayList<Answer> la) {
        int[] n = null;
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            con.setAutoCommit(false);
            PreparedStatement stmt = null;
            stmt = con.prepareStatement("insert into Answer(ACode, QCode, AContent,Value) values(?,?,?,?)");
            for (Answer a : la) {
                stmt.setString(1, a.getaCode());
                stmt.setString(2, a.getqCode());
                stmt.setString(3, a.getaContent());
                stmt.setString(4, a.getValue());
                stmt.addBatch();
            }
            n = stmt.executeBatch();
            if (n.length == la.size()) {
                con.commit();
            } else {
                con.rollback();
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Answer> getAnswersInTest(ArrayList<String> lqCode) {
        ArrayList<Answer> la = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = null;
            ResultSet rs = null;
            for (String qCode : lqCode) {
                stmt = con.createStatement();
                rs = stmt.executeQuery("select ACode, QCode, AContent, Value from Answer where QCode = '" + qCode + "'");
                while (rs.next()) {
                    la.add(new Answer(rs.getString(1).trim(), rs.getString(2).trim(), rs.getString(3), rs.getString(4)));
                }
            }
            con.close();
            return la;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static ArrayList<String> getCorrectAnswer(ArrayList<String> qCodes){
        ArrayList<String> la = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = null;
            ResultSet rs = null;
            for (String qCode : qCodes) {
                stmt = con.createStatement();
                rs = stmt.executeQuery("select ACode from Answer where QCode = '" + qCode + "' and Value='T'");
                while (rs.next()) {
                    la.add(rs.getString(1).trim());
                }
            }
            con.close();
            return la;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {

    }

}
