/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OS
 */
public class QuestionBank implements DatabaseInfo, Serializable{
    private ArrayList<Question> listQuestion = new ArrayList<>();

    public QuestionBank() {
    }

    public ArrayList<Question> getLq() {
        return listQuestion;
    }

    public void setLq(ArrayList<Question> listQuestion) {
        this.listQuestion = listQuestion;
    }
    
    public ArrayList<Question> getQuestionBySub(String subCode){
        ArrayList<Question> lq = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select QCode, SubCode, ChapCode, QType, QContent, QLevel, LecCode, CreatedDate, LecModify, ModifiedDate from Question where SubCode = '"+subCode+"'");
            while(rs.next()){
                LocalDate modDate = null;
                if(rs.getDate(8)!=null){
                    modDate = rs.getDate(8).toLocalDate();
                }
               lq.add(new Question(rs.getString(1),rs.getString(2),rs.getInt(3), rs.getString(4),rs.getString(5), rs.getInt(6),rs.getString(7),rs.getDate(8).toLocalDate(),rs.getString(9),modDate));
            }
            con.close();
            return lq;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public ArrayList<Question> getQuesgionByChap(int chapCode, String subCode){
        ArrayList<Question> list = getQuestionBySub(subCode);
        ArrayList<Question> l = new ArrayList<>();
        for(Question q : list){
            if(q.getChapCode()==chapCode){
                l.add(q);
            }
        }
        return l;
    }
    
    public static ArrayList<String> getAllQuestion(){
        ArrayList<String> lq = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select QCode from Question");
            while(rs.next()){
                lq.add(rs.getString(1).trim());
            }
            con.close();
            return lq;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public static String getNextQcode(){
        ArrayList<String> lq = getAllQuestion();
        String q = "";
        int i = 0, j=0;
        for(String s : lq){
            i = Integer.parseInt(s.substring(1));
            if(i>j){
                j = i;
            }
        }
        j = j+1;
        q = "Q"+j;
        return q;
    }
    
    public static ArrayList<Question> getQuestionsInTest(ArrayList<String> lqCode){
        ArrayList<Question> lq = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
            Statement stmt = null;
            ResultSet rs = null;
            for(String qCode : lqCode){
                stmt = con.createStatement();
                rs = stmt.executeQuery("select QCode, QContent from Question where QCode = '"+qCode+"'");
                if(rs.next()){
                    lq.add(new Question(rs.getString(1).trim(), rs.getString(2)));
                }
            }
            con.close();
            return lq;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public ArrayList<Question> getQuestionByPage(ArrayList<Question> lq,int start,int end){
        ArrayList<Question> list = new ArrayList<>();
        for(int i=start;i<end;i++){
            list.add(lq.get(i));
        }
        return list;
    }
    
    public static void main(String[] args) {
        
    }
    
}
