/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OS
 */
public class Course implements Serializable, DatabaseInfo{
    private int cCode;
    private String cName,lecCode,subCode,enrollKey,lecName,subName,adCode;

    public Course() {
    }
    
    public Course(int cCode){
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
            PreparedStatement stmt =con.prepareCall("Select CName,LecCode,SubCode,EnrollKey from Course where CCode=?");
            stmt.setInt(1, cCode);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                this.cCode=cCode;
                this.cName=rs.getString(1);
                this.lecCode=rs.getString(2);
                this.subCode=rs.getString(3);
                this.enrollKey=rs.getString(4);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Course(int cCode, String cName, String lecCode, String subCode, String enrollKey) {
        this.cCode = cCode;
        this.cName = cName;
        this.lecCode = lecCode;
        this.subCode = subCode;
        this.enrollKey = enrollKey;
    }

    public Course(String cName, String lecCode, String subCode, String enrollKey, String adCode) {
        this.cName = cName;
        this.lecCode = lecCode;
        this.subCode = subCode;
        this.enrollKey = enrollKey;
        this.adCode = adCode;
    }

    public static Course getC(int cCode){
        Course c = null;
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
             Statement stmt= con.createStatement();
             ResultSet rs = stmt.executeQuery("select CName,LecCode,SubCode,EnrollKey from Course where CCode='"+cCode+"'");
             if(rs.next()){
                 int ccode= cCode;
                 String name=rs.getString(1);
                String leccode=rs.getString(2);
                String subcode=rs.getString(3);
                String enrollkey=rs.getString(4);
                c = new Course(cCode, name, leccode, subcode, enrollkey);
             }
             con.close();
             return c;
        } catch (SQLException ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public static boolean checkKey(int cCode,String key){
        Course c = getC(cCode);
        if(c.getEnrollKey().equals(key)){
            return true;
        }else{
            return false;
        }
    }
    public static ArrayList<Course> getCourse(String lecCode){
        ArrayList<Course> lc = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con =  DriverManager.getConnection(dbURL, userDB, passDB)){
            PreparedStatement stmt = con.prepareStatement("Select CCode,CName,LecCode,SubCode,EnrollKey from Course where LecCode='"+lecCode+"'");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                lc.add(new Course(rs.getInt(1), rs.getString(2), lecCode,rs.getString(4),rs.getString(5)));
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lc;
    }
    public static ArrayList<Course> viewCourses(){
        ArrayList<Course> lc = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con =  DriverManager.getConnection(dbURL, userDB, passDB)){
            PreparedStatement stmt = con.prepareStatement("Select CCode,CName,LecCode,SubCode,EnrollKey from Course");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                lc.add(new Course(rs.getInt(1), rs.getString(2), rs.getString(3),rs.getString(4),rs.getString(5)));
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lc;
    }
     public ArrayList<Course> getAll(){
        ArrayList<Course> lc = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con =  DriverManager.getConnection(dbURL, userDB, passDB)){
            PreparedStatement stmt = con.prepareStatement("Select CCode,CName,LecCode,SubCode,EnrollKey from Course");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                lc.add(new Course(rs.getInt(1), rs.getString(2), rs.getString(3),rs.getString(4),rs.getString(5)));
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lc;
    }
    public int getcCode() {
        return cCode;
    }

    public void setcCode(int cCode) {
        this.cCode = cCode;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getLecCode() {
        return lecCode;
    }

    public void setLecCode(String lecCode) {
        this.lecCode = lecCode;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getEnrollKey() {
        return enrollKey;
    }

    public void setEnrollKey(String enrollKey) {
        this.enrollKey = enrollKey;
    }

    public String getLecName() {
        return lecName;
    }

    public void setLecName(String lecName) {
        this.lecName = lecName;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getAdCode() {
        return adCode;
    }

    public void setAdCode(String adCode) {
        this.adCode = adCode;
    }

    
    
    @Override
    public String toString() {
        return "Course{" + "cCode=" + cCode + ", cName=" + cName + ", lecCode=" + lecCode + ", subCode=" + subCode + ", enrollKey=" + enrollKey + '}';
    }
    public static int createCourse(Course c){
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
            con.setAutoCommit(false);
            PreparedStatement stmt = con.prepareStatement("insert into Course(CName,LecCode,SubCode,EnrollKey,AdCode) output inserted.CCode values(?,?,?,?,?)");
            stmt.setString(1, c.getcName());
            stmt.setString(2, c.getLecCode());
            stmt.setString(3, c.getSubCode());
            stmt.setString(4, c.getEnrollKey());
            stmt.setString(5, c.getAdCode());
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int n = rs.getInt(1);
            if (n > 0) {
                con.commit();
            } else {
                con.rollback();
            }
            con.close();
            return n;
        } catch (SQLException ex) {
            Logger.getLogger(Course.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
   
}
class test{
    public static void main(String[] args) {
        Course c = new Course("SSG101-Spring-2020", "LT0002", "SSG101", "SSG101", "AD0001");
        System.out.println(Course.createCourse(c));
    }
}