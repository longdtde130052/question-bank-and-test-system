/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.DatabaseInfo.dbURL;
import static model.DatabaseInfo.driveName;
import static model.DatabaseInfo.passDB;
import static model.DatabaseInfo.userDB;

/**
 *
 * @author OS
 */
public class Subject implements DatabaseInfo, Serializable {

    private String subCode, subName;

    public Subject() {
    }

    public Subject(String subCode) {
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            PreparedStatement stmt = con.prepareCall("select SubName from Subject where subCode=?");
            stmt.setString(1, subCode);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                this.subCode = subCode;
                this.subName = rs.getString(1);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Subject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Subject(String subCode, String subName) {
        this.subCode = subCode;
        this.subName = subName;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public static ArrayList<Subject> getAll(ArrayList<String> lsc) {
        ArrayList<Subject> ls = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            for (String s : lsc) {
                ResultSet rs = stmt.executeQuery("select SubCode, SubName from Subject where SubCode ='"+s+"'");
                if (rs.next()) {
                    ls.add(new Subject(rs.getString(1), rs.getString(2)));
                }
            }
            con.close();
            return ls;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public ArrayList<Subject> getAllSub(){
        ArrayList<Subject> ls = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("Select SubCode,SubName from Subject");
            while(rs.next()){
                ls.add(new Subject(rs.getString(1)));
            }
            con.close();
            return ls;
        } catch (SQLException ex) {
            Logger.getLogger(Subject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public static ArrayList<String> getAllSubByTeacher(String lecCode) {
        ArrayList<String> ls = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select SubCode from Course where LecCode ='" + lecCode + "'");
            while (rs.next()) {
                ls.add(rs.getString(1));
            }
            con.close();
            return ls;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Subject{" + "subCode=" + subCode + ", subName=" + subName + '}';
    }

    public static void main(String[] args) {
        Subject s = new Subject();
        System.out.println(s.getAllSub());
    }
}
