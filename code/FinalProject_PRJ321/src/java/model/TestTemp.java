/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author MyPC
 */
public class TestTemp implements Serializable{

    private ArrayList<Question> quesTest=new ArrayList<>();;

    public TestTemp() {
        quesTest = new ArrayList<>();
    }
    public TestTemp(ArrayList<Question> quesTest) {
        this.quesTest = quesTest;
    }

    public ArrayList<Question> getQuesTest() {
        return quesTest;
    }

    public void setQuesTest(ArrayList<Question> quesTest) {
        this.quesTest = quesTest;
    }
}
