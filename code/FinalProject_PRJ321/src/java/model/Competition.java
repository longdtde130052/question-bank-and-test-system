/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.*;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MACBOOK
 */
public class Competition implements DatabaseInfo {

    private int comCode;
    private String comName;
    private int tCode;
    private int comTime;
    private Timestamp startTime;
    private Timestamp endTime;
    private String password;
    private int chapCode;
    private int cCode;
    private String dateStartFormat;
    private String dateEndFormat;
    public Competition() {
    }

    public Competition(int comCode, String comName, int tCode, int comTime, Timestamp startTime, Timestamp endTime, String password, int chapCode, int cCode) {
        this.comCode = comCode;
        this.comName = comName;
        this.tCode = tCode;
        this.comTime = comTime;
        this.startTime = startTime;
        this.endTime = endTime;
        this.password = password;
        this.chapCode = chapCode;
        this.cCode = cCode;
    }
    public int getChapCode() {
        return chapCode;
    }

    public void setChapCode(int chapCode) {
        this.chapCode = chapCode;
    }


    public int getComCode() {
        return comCode;
    }

    public void setComCode(int comCode) {
        this.comCode = comCode;
    }

    public String getComName() {
        return comName;
    }

    public void setComName(String comName) {
        this.comName = comName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getComTime() {
        return comTime;
    }

    public void setComTime(int comTime) {
        this.comTime = comTime;
    }

    public int gettCode() {
        return tCode;
    }

    public void settCode(int tCode) {
        this.tCode = tCode;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public int getcCode() {
        return cCode;
    }

    public void setcCode(int cCode) {
        this.cCode = cCode;
    }

    public String getDateStartFormat() {
        String s = startTime.toString();
        String date = s.substring(8, 10);
        String month = s.substring(5,7);
        switch(month){
            case "01":{
                month = "January";
                break;
            }
            case "02":{
                month = "February";
                break;
            }
            case "03":{
                month = "March";
                break;
            }
            case "04":{
                month = "April";
                break;
            }
            case "05":{
                month = "May";
                break;
            }
            case "06":{
                month = "June";
                break;
            }
            case "07":{
                month = "July";
                break;
            }
            case "08":{
                month = "August";
                break;
            }
            case "09":{
                month = "September";
                break;
            }
            case "10":{
                month = "October";
                break;
            }
            case "11":{
                month = "November";
                break;
            }
            case "12":{
                month = "December";
                break;
            }
        }
        String year = s.substring(0,4);
        String hour = s.substring(11,13);
        String minute = s.substring(14,16);
        return date+" "+month+" "+year+", "+hour+":"+minute;
    }

    public void setDateStartFormat(String dateStartFormat) {
        this.dateStartFormat = dateStartFormat;
    }

    public String getDateEndFormat() {
        String s = endTime.toString();
        String date = s.substring(8, 10);
        String month = s.substring(5,7);
        switch(month){
            case "01":{
                month = "January";
                break;
            }
            case "02":{
                month = "February";
                break;
            }
            case "03":{
                month = "March";
                break;
            }
            case "04":{
                month = "April";
                break;
            }
            case "05":{
                month = "May";
                break;
            }
            case "06":{
                month = "June";
                break;
            }
            case "07":{
                month = "July";
                break;
            }
            case "08":{
                month = "August";
                break;
            }
            case "09":{
                month = "September";
                break;
            }
            case "10":{
                month = "October";
                break;
            }
            case "11":{
                month = "November";
                break;
            }
            case "12":{
                month = "December";
                break;
            }
        }
        String year = s.substring(0,4);
        
        String hour = s.substring(11,13);
        String minute = s.substring(14,16);
        return date+" "+month+" "+year+", "+hour+":"+minute;
    }

    public void setDateEndFormat(String dateEndFormat) {
        this.dateEndFormat = dateEndFormat;
    }

    

    
    
    @Override
    public String toString() {
        return "Competition{" + "comCode=" + comCode + ", comName=" + comName + ", tCode=" + tCode + ", comTime=" + comTime + ", startTime=" + startTime + ", endTime=" + endTime + ", password=" + password + '}';
    }

    
    
    public static int addCom(int tcode, String name, String pass, String start, String end, int time,int chapCode,int cCode) {
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            con.setAutoCommit(false);
            Class.forName(driveName);
            Statement stmt = con.createStatement();
            String sql = "insert into Competition(ComName,TCode,ComTime,StartTime,EndTime,Password,chapCode,cCode)"
                    + "values('" + name + "'," + tcode + "," + time + ",'" + start + "','" + end + "','" + pass + "',"+chapCode+","+cCode+")";

            int i = stmt.executeUpdate(sql);
            if(i==1) con.commit();
            else con.rollback();
            con.close();
            return i;
        } catch (Exception e) {
            Logger.getLogger(Student.class.getName()).log(Level.SEVERE, null, e);
            return -1;
        }
    }

    public  ArrayList<Competition> getAll(){
        ArrayList<Competition> lc = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select ComCode, ComName, TCode, ComTime, StartTime, EndTime, Password,chapCode,cCode from Competition");
            while (rs.next()) {
                lc.add(new Competition(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getString(7),rs.getInt(8),rs.getInt(9)));
            }
            con.close();
            stmt.close();
            return lc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static Competition getComByCode(int code){
        Competition c = null;
        try {
            Class.forName(driveName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select ComCode, ComName, TCode, ComTime, StartTime, EndTime, Password,chapCode,cCode from Competition where ComCode='"+code+"'");
            if (rs.next()) {
                c = new Competition(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getString(7),rs.getInt(8),rs.getInt(9));
            }
            con.close();
            stmt.close();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static void main(String[] args) {
        Competition c = getComByCode(13);
        System.out.println(c);
    }
}
