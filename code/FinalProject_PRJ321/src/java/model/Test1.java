/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.DatabaseInfo.dbURL;
import static model.DatabaseInfo.driveName;
import static model.DatabaseInfo.passDB;
import static model.DatabaseInfo.userDB;

/**
 *
 * @author OS
 */
public class Test1 implements DatabaseInfo, Serializable {

    private int tCode;
    private String tName;
    private String subCode;
    private String lecCode;
    private LocalDate createdDate;
    private int numberOfQues;

    public Test1() {
    }

    public Test1(String tName, String subCode, String lecCode, LocalDate createdDate) {
        this.tName = tName;
        this.subCode = subCode;
        this.lecCode = lecCode;
        this.createdDate = createdDate;
    }

    public Test1(int tCode, String tName, String subCode, String lecCode, LocalDate createdDate, int numOfQues) {
        this.tCode = tCode;
        this.tName = tName;
        this.subCode = subCode;
        this.lecCode = lecCode;
        this.createdDate = createdDate;
        this.numberOfQues = numOfQues;
    }

    public int gettCode() {
        return tCode;
    }

    public void settCode(int tCode) {
        this.tCode = tCode;
    }

    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getLecCode() {
        return lecCode;
    }

    public void setLecCode(String lecCode) {
        this.lecCode = lecCode;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public int getNumberOfQues() {
        return numberOfQues;
    }

    public void setNumberOfQues(int numOfQues) {
        this.numberOfQues = numOfQues;
    }

    public static int createTest(Test1 t) {
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            con.setAutoCommit(false);
            PreparedStatement stmt = con.prepareStatement("insert into Test(Tname,SubCode,LecCode,CreatedDate)  output inserted.TCode values(?,?,?,?)");
            stmt.setString(1, t.gettName());
            stmt.setString(2, t.getSubCode());
            stmt.setString(3, t.getLecCode());
            stmt.setDate(4, Date.valueOf(t.getCreatedDate()));
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int n = rs.getInt(1);
            if (n > 0) {
                con.commit();
            } else {
                con.rollback();
            }
            con.close();
            return n;
        } catch (SQLException ex) {
            Logger.getLogger(Test1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public static void saveQuestionToTest(ArrayList<Question> lq, Test1 t) {
        int n[] = null;
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            con.setAutoCommit(false);
            PreparedStatement stmt = con.prepareCall("Update Test set NumberOfQues=? where TCode=?");
            stmt.setInt(1, lq.size());
            stmt.setInt(2, t.gettCode());
            stmt.executeUpdate();
            stmt = con.prepareStatement("insert into TestBank(TCode,QCode) values(?,?)");
            for (Question q : lq) {
                stmt.setInt(1, t.gettCode());
                stmt.setString(2, q.getqCode());
                stmt.addBatch();
            }
            n = stmt.executeBatch();

            if (n.length == lq.size()) {
                con.commit();
            } else {
                con.rollback();
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Test1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Test1 getTest1(int tCode) {
        Test1 t = null;
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select TCode,TName,SubCode,LecCode,CreatedDate,NumberOfQues from Test where TCode='" + tCode + "'");
            if (rs.next()) {
                t = new Test1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDate(5).toLocalDate(), rs.getInt(6));
            }
            con.close();
            return t;
        } catch (SQLException ex) {
            Logger.getLogger(Test1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static ArrayList<Test1> getAll() {
        ArrayList<Test1> lt = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select TCode, TName, SubCode, LecCode, CreatedDate from Test");
            while (rs.next()) {
                lt.add(new Test1());
            }
            con.close();
            return lt;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<Test1> getTestBySub(String subcode) {
        ArrayList<Test1> lt = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select TCode, TName, SubCode, LecCode, CreatedDate,NumberOfQues from Test where SubCode='" + subcode + "'");
            while (rs.next()) {
                lt.add(new Test1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDate(5).toLocalDate(), rs.getInt(6)));
            }
            con.close();
            return lt;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Test1 getTestByCode(int tcode) {
        Test1 t = null;
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select TCode, TName, SubCode, LecCode, CreatedDate,NumberOfQues from Test where TCode='" + tcode + "'");
            while (rs.next()) {
                t = new Test1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDate(5).toLocalDate(), rs.getInt(6));
            }
            con.close();
            return t;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
