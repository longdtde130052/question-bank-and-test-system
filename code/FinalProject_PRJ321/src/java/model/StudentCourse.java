/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tr_hieu
 */
public class StudentCourse implements DatabaseInfo, Serializable {

    private int sCCode;
    private String sCName;
    private String stuCode;
    private int cCode;

    public StudentCourse() {
    }

    public StudentCourse(int sCCode, String sCName, String stuCode, int cCode) {
        this.sCCode = sCCode;
        this.sCName = sCName;
        this.stuCode = stuCode;
        this.cCode = cCode;
    }

    public int getsCCode() {
        return sCCode;
    }

    public void setsCCode(int sCCode) {
        this.sCCode = sCCode;
    }

    public String getsCName() {
        return sCName;
    }

    public void setsCName(String sCName) {
        this.sCName = sCName;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public int getcCode() {
        return cCode;
    }

    public void setcCode(int cCode) {
        this.cCode = cCode;
    }

    public static  ArrayList<Student> getAllStudentInACourses(int cCode){
        ArrayList<Student> ls = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("Select StuCode from SCourse where CCode = '"+cCode+"'");
            while(rs.next()){
                ls.add(new Student(rs.getString(1)));
            }
            con.close();
            return ls;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public static void main(String[] args) {
        StudentCourse o = new StudentCourse();
        System.out.println(o.getAllStudentInACourses(1));
    }
}
