/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.*;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author MyPC
 */
public class Functional implements DatabaseInfo{
    public static String addUser(String name, String email, String pass, String userRight) {
        try {
            Class.forName(driveName);
        } catch (Exception ex) {
            Logger.getLogger(Lecture.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            PreparedStatement stmt = con.prepareStatement("insert into " + userRight + " values(?,?,?,?)");
            stmt.setString(1,getMaxID(userRight));
            stmt.setString(2,name);
            stmt.setString(3,email);
            stmt.setString(4,pass);
            String rc = getMaxID(userRight);
            stmt.execute();
            con.close();
            return rc;
        } catch (SQLException ex) {
            Logger.getLogger(Lecture.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String getMax(String userRight) {
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Lecture.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            
            Statement stmt = con.createStatement();
            if (userRight.equals("Lecture")) {
                ResultSet rs = stmt.executeQuery("Select Max(LecCode) from Lecture");
                if (rs.next()) {
                    String maxID = rs.getString(1);
                    return maxID;
                }
            } else {
                ResultSet rs = stmt.executeQuery("Select Max(StuCode) from Student");
                if (rs.next()) {
                    String maxID = rs.getString(1);
                    return maxID;
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(Lecture.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public static String getMaxID(String userRight) {
        String next = getMax(userRight);
        String sub = next.substring(2);
        int i = Integer.parseInt(sub);
        i++;
        if (userRight.equals("Lecture")) {
            if (i <= 9) {
                String result = "LT000" + Integer.toString(i);
                return result;
            } else if (i <= 99) {
                String result = "LT00" + Integer.toString(i);
                return result;
            } else if (i <= 999) {
                String result = "LT0" + Integer.toString(i);
                return result;
            } else {
                String result = "LT" + Integer.toString(i);
                return result;
            }
        } else {
            if (i <= 9) {
                String result = "ST000" + Integer.toString(i);
                return result;
            } else if (i <= 99) {
                String result = "ST00" + Integer.toString(i);
                return result;
            } else if (i <= 999) {
                String result = "ST0" + Integer.toString(i);
                return result;
            } else {
                String result = "ST" + Integer.toString(i);
                return result;
            }
        }
    }
}
