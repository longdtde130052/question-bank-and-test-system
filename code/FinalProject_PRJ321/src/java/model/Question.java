/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.*;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.DatabaseInfo.dbURL;
import static model.DatabaseInfo.driveName;
import static model.DatabaseInfo.passDB;
import static model.DatabaseInfo.userDB;

/**
 *
 * @author OS
 */
public class Question implements Serializable, DatabaseInfo {

    private String qCode;
    private String subCode;
    private int chapCode;
    private String qType;
    private String qContent;
    private int qLevel;
    private String lecCode;
    private LocalDate date;
    private String lecModify;
    private LocalDate modifiedDate;
    private String LecName;
    private String lecModName;
    private String stuAnswer;
    private int index;
    private int status;

    public Question() {
    }

    public Question(String qCode, String subCode, int chapCode, String qType, String qContent, int qLevel, String lecCode, LocalDate date, String lecModify, LocalDate modifiedDate) {
        this.qCode = qCode;
        this.subCode = subCode;
        this.chapCode = chapCode;
        this.qType = qType;
        this.qContent = qContent;
        this.qLevel = qLevel;
        this.lecCode = lecCode;
        this.date = date;
        this.lecModify = lecModify;
        this.modifiedDate = modifiedDate;
    }

    public Question(String qCode, String qContent) {
        this.qCode = qCode;
        this.qContent = qContent;
    }

    public Question(String qCode, String subCode, int chapCode, String qType, String qContent, int qLevel, String lecCode) {
        this.qCode = qCode;
        this.subCode = subCode;
        this.chapCode = chapCode;
        this.qType = qType;
        this.qContent = qContent;
        this.qLevel = qLevel;
        this.lecCode = lecCode;
    }

    public String getqCode() {
        return qCode;
    }

    public void setqCode(String qCode) {
        this.qCode = qCode;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public int getChapCode() {
        return chapCode;
    }

    public void setChapCode(int chapCode) {
        this.chapCode = chapCode;
    }

    public String getqType() {
        return qType;
    }

    public void setqType(String qType) {
        this.qType = qType;
    }

    public String getqContent() {
        return qContent;
    }

    public void setqContent(String qContent) {
        this.qContent = qContent;
    }

    public int getqLevel() {
        return qLevel;
    }

    public void setqLevel(int qLevel) {
        this.qLevel = qLevel;
    }

    public String getLecCode() {
        return lecCode;
    }

    public void setLecCode(String lecCode) {
        this.lecCode = lecCode;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Question{" + "qCode=" + qCode + ", qContent=" + qContent;
    }

    public String getLecName() {
        return LecName;
    }

    public void setLecName(String LecName) {
        this.LecName = LecName;
    }

    public String getLecModify() {
        return lecModify;
    }

    public void setLecModify(String lecModify) {
        this.lecModify = lecModify;
    }

    public LocalDate getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(LocalDate modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getLecModName() {
        return lecModName;
    }

    public void setLecModName(String lecModName) {
        this.lecModName = lecModName;
    }

    public String getStuAnswer() {
        return stuAnswer;
    }

    public void setStuAnswer(String stuAnswer) {
        this.stuAnswer = stuAnswer;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public static Question getQuestion(String qCode) {
        Question q = null;
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select QCode, SubCode, ChapCode, QType, QContent, QLevel, LecCode, CreatedDate, LecModify, ModifiedDate from Question where QCode = '" + qCode + "'");
            while (rs.next()) {
                q = new Question(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7), rs.getDate(8).toLocalDate(), rs.getString(9), rs.getDate(10).toLocalDate());
            }
            con.close();
            return q;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateQuestion(Question q) {
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            con.setAutoCommit(false);
            PreparedStatement stmt = con.prepareStatement("update Question set QContent = ?, LecModify = ?, ModifiedDate = ? where QCode = ?");
            stmt.setString(1, q.getqContent());
            stmt.setString(2, q.getLecModify());
            stmt.setDate(3, Date.valueOf(LocalDate.now()));
            stmt.setString(4, q.getqCode());
            int r = stmt.executeUpdate();
            if (r == 1) {
                con.commit();
            } else {
                con.rollback();
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Boolean deleteQuestion(String qCode) {
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            PreparedStatement stmt = con.prepareStatement("delete from Question where QCode = ?");
            stmt.setString(1, qCode);
            int r = stmt.executeUpdate();
            con.close();
            return r == 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean deleteQuestionFromTest(String qCode) {
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            PreparedStatement stmt = con.prepareStatement("delete from TestBank where QCode = ?");
            stmt.setString(1, qCode);
            int r = stmt.executeUpdate();
            con.close();
            return r == 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String addQuestion(Question q) {
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            con.setAutoCommit(false);
            PreparedStatement stmt = con.prepareStatement("insert into Question(QCode, SubCode, ChapCode, QType, QContent, QLevel, LecCode, CreatedDate, LecModify, ModifiedDate) output inserted.QCode values(?,?,?,?,?,?,?,?,?,?)");
            stmt.setString(1, q.getqCode());
            stmt.setString(2, q.getSubCode());
            stmt.setInt(3, q.getChapCode());
            stmt.setString(4, q.getqType());
            stmt.setString(5, q.getqContent());
            stmt.setInt(6, q.getqLevel());
            stmt.setString(7, q.getLecCode());
            stmt.setDate(8, Date.valueOf(LocalDate.now()));
            stmt.setString(9, q.getLecCode());
            stmt.setDate(10, Date.valueOf(LocalDate.now()));
            ResultSet rs = stmt.executeQuery();
            rs.next();
            String qcode = rs.getString(1);
            if (qcode != null) {
                con.commit();
            } else {
                con.rollback();
            }
            con.close();
            return qcode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static boolean QuestionExist(String content){
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Question where QContent ='"+content+"'");
            if(rs.next()){
                return true;
            }
            con.close();
            return false;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public static void main(String[] args) {

    }
}
