/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.*;

/**
 *
 * @author OS
 */
public class Student implements DatabaseInfo, Serializable {

    private String code;
    private String name;
    private String email;
    private String password;

    public Student() {
    }

    public Student(String sCode) {
        Student s = getStudent(sCode);
        this.code = s.code;
        this.name = s.name;
        this.email = s.email;
        this.password = s.password;
    }

    public Student(String code, String name, String email, String password) {
        this.code = code;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static Student getStudent(String sCode) {
        Student s = null;
        try {
            Class.forName(driveName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select StuCode, StuName, StuEmail, Password from Student where StuCode = '" + sCode + "'");
            if (rs.next()) {
                s = new Student(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
            con.close();
            stmt.close();
            return s;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Student loginAsStudent(String sCode, String pass) {
        Student s = getStudent(sCode);
        if (s != null) {
            if (s.getPassword().equals(pass)) {
                return s;
            }
        }
        return null;
    }

    public static boolean updateStudent(Student s) {
        try {
            Class.forName(driveName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Update Student set StuName=?,StuEmail=?, Password=? where StuCode=?");
            stmt.setString(1, s.getName());
            stmt.setString(2, s.getEmail());
            stmt.setString(3, s.getPassword());
            stmt.setString(4, s.getCode());

            int rc = stmt.executeUpdate();
            con.close();
            return rc == 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
