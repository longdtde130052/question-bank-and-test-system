/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OS
 */
public class ClassList implements DatabaseInfo {

    private String  scName, stuCode,cName;
    private int scCode,cCode;

    public ClassList() {
    }

    public ClassList(String scName, String stuCode, int cCode) {
        this.scName = scName;
        this.stuCode = stuCode;
        this.cCode = cCode;
    }

    public ClassList(int scCode, String scName, String stuCode, int cCode) {
        this.scCode = scCode;
        this.scName = scName;
        this.stuCode = stuCode;
        this.cCode = cCode;
    }

    public static void addList(ClassList cl) {
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClassList.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            PreparedStatement stmt = con.prepareStatement("insert into SCourse(SCName,StuCode,CCode) values(?,?,?)");
            stmt.setString(1, cl.getScName());
            stmt.setString(2, cl.getStuCode());
            stmt.setInt(3, cl.getcCode());
            stmt.execute();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(ClassList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getScCode() {
        return scCode;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public static ArrayList<ClassList> getAL(String stuCode) {
         ArrayList<ClassList> lcl = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClassList.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            PreparedStatement stmt = con.prepareCall("select SCCode,SCName,StuCode,CCode from SCourse where StuCode='"+stuCode+"'");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                ClassList cl = new ClassList(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
                lcl.add(cl);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClassList.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lcl;
    }
    public static boolean checkEnroll(String stuCode,int cCode){
         ArrayList<ClassList> lcl = getAL(stuCode);
         for(ClassList cl:lcl){
             if(cl.getStuCode().equals(stuCode)&&cl.getcCode()==cCode){
                 return true;
             }
         }
         return false;
    }
    public void setScCode(int scCode) {
        this.scCode = scCode;
    }

    public String getScName() {
        return scName;
    }

    public void setScName(String scName) {
        this.scName = scName;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public int getcCode() {
        return cCode;
    }

    public void setcCode(int cCode) {
        this.cCode = cCode;
    }
    public static void main(String[] args) {
        ArrayList<ClassList> lcl = getAL("ST0001");
        System.out.println(checkEnroll("ST0001", 1));
    }
}
