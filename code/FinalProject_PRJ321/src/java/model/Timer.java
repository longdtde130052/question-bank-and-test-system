/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OS
 */
public class Timer extends Thread implements Serializable {

    private String minute;
    private String second;

    public Timer() {
    }

    public Timer(String minute, String second) {
        this.minute = minute;
        this.second = second;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public void getTimer() {
        int secs = 0, mins = 0;
        while (true) {
            secs = Integer.parseInt(second);
            mins = Integer.parseInt(minute);
            if (--secs == -1) {
                secs = 59;
                --mins;
            }
            if (secs < 10) {
                second = "0" + secs;
            } else {
                second = secs + "";
            }
            if (mins < 10) {
                minute = "0" + mins;
            } else {
                minute = mins + "";
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Timer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Timer.class.getName()).log(Level.SEVERE, null, ex);
        }
        getTimer();
    }

    public static String getRemainingTime(Timestamp start, Timestamp now, long period) {
        long a = (now.getTime() - start.getTime()) / 1000;
        long minute = a / 60;
        long second = a % 60;
        String mins = null, secs=null;
        if (minute == 0) {
            second = 60 - second;
            minute = period - 1;
            if (minute < 10) {
                mins = "0" + minute;
            } else {
                mins = minute + "";
            }
            if (second < 10) {
                secs = "0" + second;
            } else {
                secs = second + "";
            }
            return mins + ":" + secs;
        } else {
            second = 60 - second;
            minute = period - minute - 1;
            if (minute < 0) {
                return "timeout";
            }
            if (minute < 10) {
                mins = "0" + minute;
            } else {
                mins = minute + "";
            }
            if (second < 10) {
                secs = "0" + second;
            } else {
                secs = second + "";
            }
            return mins + ":" + secs;
        }
    }

    public static String getDoneTime(Timestamp start, Timestamp now) {
        long a = (now.getTime() - start.getTime()) / 1000;
        long minute = a / 60;
        long second = a % 60;
        String mins = "", secs = "";
        if (minute < 10) {
            mins = "0" + minute;
        } else {
            mins = minute + "";
        }
        if (second < 10) {
            secs = "0" + second;
        } else {
            secs = second + "";
        }
        return mins + ":" + secs;
    }

    public static void main(String[] args) {
        Timestamp a = Timestamp.valueOf("2019-11-13 01:42:17.543");
        Timestamp b = Timestamp.valueOf("2019-11-13 02:42:16.543");
//        String c = getRemainingTime(a, b, 65);
//        String d = c.substring(0, 2);
//        String e = c.substring(3, 5);
//        System.out.println(Timestamp.valueOf(LocalDateTime.now()));
//        System.out.println(getRemainingTime(a, b, 60));

    }
}
