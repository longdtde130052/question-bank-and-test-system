/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.DatabaseInfo.dbURL;
import static model.DatabaseInfo.driveName;
import static model.DatabaseInfo.passDB;
import static model.DatabaseInfo.userDB;

/**
 *
 * @author OS
 */
public class Chapter implements DatabaseInfo, Serializable{
    private int chapCode;
    private String subCode;
    private int chapNumber;
    private String chapName;

    public Chapter() {
    }

    public Chapter(int chapCode, String subCode, int chapNumber, String chapName) {
        this.chapCode = chapCode;
        this.subCode = subCode;
        this.chapNumber = chapNumber;
        this.chapName = chapName;
    }

    public int getChapCode() {
        return chapCode;
    }

    public void setChapCode(int chapCode) {
        this.chapCode = chapCode;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public int getChapNumber() {
        return chapNumber;
    }

    public void setChapNumber(int chapNumber) {
        this.chapNumber = chapNumber;
    }

    public String getChapName() {
        return chapName;
    }

    public void setChapName(String chapName) {
        this.chapName = chapName;
    }
    
    public static ArrayList<Chapter> getAllChap(String subCode){
        ArrayList<Chapter> lc = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select ChapCode, SubCode, ChapNumber, ChapName from Chapter where SubCode = '"+subCode+"'");
            while(rs.next()){
               lc.add(new Chapter(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getString(4)));
            }
            con.close();
            return lc;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
