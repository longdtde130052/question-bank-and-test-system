/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author OS
 */
public class StudentCompetition implements DatabaseInfo, Serializable{
    private int sComCode;
    private String stuCode;
    private int comCode;
    private double grade;
    private Timestamp startTime;
    private Timestamp endTime;
    private String doneTime;
    private String testVersion;
    private String dateEndFormat;
    private int numberOfRightAns;
    private String dateStartFormat;
    public StudentCompetition() {
    }

    public StudentCompetition(String stuCode, int comCode, Timestamp startTime) {
        this.stuCode = stuCode;
        this.comCode = comCode;
        this.startTime = startTime;
    }

    public StudentCompetition(int sComCode, String stuCode, int comCode, double grade, Timestamp startTime, Timestamp endTime, String doneTime, String testVersion,int numberOfRightAns) {
        this.sComCode = sComCode;
        this.stuCode = stuCode;
        this.comCode = comCode;
        this.grade = grade;
        this.startTime = startTime;
        this.endTime = endTime;
        this.doneTime = doneTime;
        this.testVersion = testVersion;
        this.numberOfRightAns = numberOfRightAns;
    }

    public int getsComCode() {
        return sComCode;
    }

    public void setsComCode(int sComCode) {
        this.sComCode = sComCode;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public int getComCode() {
        return comCode;
    }

    public void setComCode(int comCode) {
        this.comCode = comCode;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public String getDoneTime() {
        return doneTime;
    }

    public void setDoneTime(String doneTime) {
        this.doneTime = doneTime;
    }

    public String getTestVersion() {
        return testVersion;
    }

    public void setTestVersion(String testVersion) {
        this.testVersion = testVersion;
    }

    public int getNumberOfRightAns() {
        return numberOfRightAns;
    }

    public void setNumberOfRightAns(int numberOfRightAns) {
        this.numberOfRightAns = numberOfRightAns;
    }

    
    
    public String getDateEndFormat() {
        String s = endTime.toString();
        String date = s.substring(8, 10);
        String month = s.substring(5,7);
        switch(month){
            case "01":{
                month = "January";
                break;
            }
            case "02":{
                month = "February";
                break;
            }
            case "03":{
                month = "March";
                break;
            }
            case "04":{
                month = "April";
                break;
            }
            case "05":{
                month = "May";
                break;
            }
            case "06":{
                month = "June";
                break;
            }
            case "07":{
                month = "July";
                break;
            }
            case "08":{
                month = "August";
                break;
            }
            case "09":{
                month = "September";
                break;
            }
            case "10":{
                month = "October";
                break;
            }
            case "11":{
                month = "November";
                break;
            }
            case "12":{
                month = "December";
                break;
            }
        }
        String year = s.substring(0,4);
        
        String hour = s.substring(11,13);
        String minute = s.substring(14,16);
        return date+" "+month+" "+year+", "+hour+":"+minute;
    }
    
    public void setEndStartFormat(String dateStartFormat) {
        this.dateEndFormat = dateStartFormat;
    }

    public String getDateStartFormat() {
        String s = startTime.toString();
        String date = s.substring(8, 10);
        String month = s.substring(5, 7);
        switch (month) {
            case "01": {
                month = "January";
                break;
            }
            case "02": {
                month = "February";
                break;
            }
            case "03": {
                month = "March";
                break;
            }
            case "04": {
                month = "April";
                break;
            }
            case "05": {
                month = "May";
                break;
            }
            case "06": {
                month = "June";
                break;
            }
            case "07": {
                month = "July";
                break;
            }
            case "08": {
                month = "August";
                break;
            }
            case "09": {
                month = "September";
                break;
            }
            case "10": {
                month = "October";
                break;
            }
            case "11": {
                month = "November";
                break;
            }
            case "12": {
                month = "December";
                break;
            }
        }
        String year = s.substring(0, 4);

        String hour = s.substring(11, 13);
        String minute = s.substring(14, 16);
        return date + " " + month + " " + year + ", " + hour + ":" + minute;
    }

    public void setDateStartFormat(String dateStartFormat) {
        this.dateStartFormat = dateStartFormat;
    }
    
    @Override
    public String toString() {
        return "StudentCompetition{" + "sComCode=" + sComCode + ", stuCode=" + stuCode + ", comCode=" + comCode + ", grade=" + grade + ", startTime=" + startTime + ", endTime=" + endTime + ", doneTime=" + doneTime + ", testVersion=" + testVersion + '}';
    }
    
    public static StudentCompetition getResult(String stuCode, int comCode){
        StudentCompetition c = null;
        try {
            Class.forName(driveName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select SComCode, StuCode, ComCode, Grade, StartTime, EndTime, DoneTime, TestVersion, NumberOfRightAns from SCompetition where StuCode ='"+stuCode+"' and ComCode='"+comCode+"'");
            if(rs.next()){
                c = new StudentCompetition(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getString(7), rs.getString(8), rs.getInt(9));
            }
            con.close();
            return c;
        }catch(Exception e ){
            e.printStackTrace();
        }
        return null;
    }
    
    public static Boolean addNewStudentCompe(StudentCompetition sc){
        try {
            Class.forName(driveName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
            con.setAutoCommit(false);
            PreparedStatement stmt = con.prepareStatement("insert into SCompetition(StuCode, ComCode, StartTime) values(?,?,?)");
            stmt.setString(1, sc.getStuCode());
            stmt.setInt(2, sc.getComCode());
            stmt.setTimestamp(3, sc.getStartTime());
            int r = stmt.executeUpdate();
            if(r==1) con.commit();
            else con.rollback();
            con.close();
            return r==1;
        }catch(Exception e ){
            e.printStackTrace();
        }
        return false;
    }
    
    
    public static Boolean updateStudentCompe(StudentCompetition sc){
        try {
            Class.forName(driveName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
            con.setAutoCommit(false);
            PreparedStatement stmt = con.prepareStatement("update SCompetition set Grade=?, EndTime=?, DoneTime=?, TestVersion=?, NumberOfRightAns= ? where SComCode=?");
            stmt.setDouble(1, sc.getGrade());
            stmt.setTimestamp(2, sc.getEndTime());
            stmt.setString(3, sc.getDoneTime());
            stmt.setString(4, sc.getTestVersion());
            stmt.setInt(5, sc.getNumberOfRightAns());
            stmt.setInt(6, sc.getsComCode());    
            int r = stmt.executeUpdate();
            if(r==1) con.commit();
            else con.rollback();
            con.close();
            return r==1;
        }catch(Exception e ){
            e.printStackTrace();
        }
        return false;
    }
    
    /// Cho Student
    public StudentCompetition(int ComCode1, String Stu){
        StudentCompetition s = null;
        try {
            Class.forName(driveName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select SComCode, StuCode, ComCode, Grade,StartTime, EndTime,DoneTime,TestVersion from SCompetition where ComCode = '" + ComCode1 + "' and StuCode='"+Stu+"'");
            if (rs.next()) {
                sComCode=rs.getInt(1);
                stuCode=rs.getString(2);
                comCode=rs.getInt(3);
                grade=rs.getDouble(4);
                startTime=rs.getTimestamp(5);
                endTime=rs.getTimestamp(6);
                doneTime=rs.getString(7);
                testVersion=rs.getString(8);
            }
            con.close();
            stmt.close();
           
        } catch (Exception e) {
            e.printStackTrace();
        }
       
    }
    // Cho Lecture
     public static ArrayList<StudentCompetition> getAllByComCode(int ComCode1){
        ArrayList<StudentCompetition> s = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select SComCode, StuCode, ComCode, Grade,StartTime, EndTime,DoneTime,TestVersion,NumberOfRightAns from SCompetition where ComCode = '" + ComCode1 +"'");
            while (rs.next()) {
               s.add(new StudentCompetition(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getDouble(4),rs.getTimestamp(5),rs.getTimestamp(6),rs.getString(7),rs.getString(8),rs.getInt(9)));
            }
            con.close();
            stmt.close();
           return s;
        } catch (Exception e) {
            e.printStackTrace();
        }
       return null;
    }
    
    public static void main(String[] args) {
    }
    
}
