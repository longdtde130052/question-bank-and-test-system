/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OS
 */
public class Lecture implements DatabaseInfo {

    private String code;
    private String name;
    private String email;
    private String password;

    public Lecture() {
    }

    public Lecture(String code, String name, String mail, String password) {
        this.code = code;
        this.name = name;
        this.email = mail;
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Lecture{" + "code=" + code + ", name=" + name + ", email=" + email + ", password=" + password + '}';
    }

    public static Lecture getLecture(String lCode) {
        Lecture l = null;
        try {
            Class.forName(driveName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select LecCode, LecName, LecEmail, Password from Lecture where LecCode = '" + lCode + "'");
            if (rs.next()) {
                l = new Lecture(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
            con.close();
            stmt.close();
            return l;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<Lecture> getAll() {
        ArrayList<Lecture> ll = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Lecture.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection con = DriverManager.getConnection(dbURL, userDB, passDB)) {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("Select * from Lecture");
            while(rs.next()){
                ll.add(new Lecture(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
            con.close();
            return ll;
        } catch (SQLException ex) {
            Logger.getLogger(Lecture.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public static void main(String[] args) {
        Lecture l = new Lecture();
        System.out.println(l.getAll());
    }
    public static Lecture loginAsLecture(String lCode, String pass) {
        Lecture s = getLecture(lCode);
        if (s != null) {
            if (s.getPassword().equals(pass)) {
                return s;
            }
        }
        return null;
    }

    public static boolean updateLecture(Lecture l) {
        try {
            Class.forName(driveName);
            Connection con = DriverManager.getConnection(dbURL, userDB, passDB);
            PreparedStatement stmt = con.prepareStatement("Update Lecture set LecName=?,LecEmail=?, Password=? where LecCode=?");
            stmt.setString(1, l.getName());
            stmt.setString(2, l.getEmail());
            stmt.setString(3, l.getPassword());
            stmt.setString(4, l.getCode());

            int rc = stmt.executeUpdate();
            con.close();
            return rc == 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
