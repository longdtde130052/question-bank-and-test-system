/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OS
 */
public class TestBank implements DatabaseInfo, Serializable{
    private int tBCode;
    private int tCode;
    private String qCode;

    public TestBank() {
    }

    public TestBank(int tBCode, int tCode, String qCode) {
        this.tBCode = tBCode;
        this.tCode = tCode;
        this.qCode = qCode;
    }

    public int gettBCode() {
        return tBCode;
    }

    public void settBCode(int tBCode) {
        this.tBCode = tBCode;
    }

    public int gettCode() {
        return tCode;
    }

    public void settCode(int tCode) {
        this.tCode = tCode;
    }

    public String getqCode() {
        return qCode;
    }

    public void setqCode(String qCode) {
        this.qCode = qCode;
    }

    @Override
    public String toString() {
        return "TestBank{" + "tBCode=" + tBCode + ", tCode=" + tCode + ", qCode=" + qCode + '}';
    }
    
    public static ArrayList<String> getQuestionByTcode(int tCode){
        ArrayList<String> lq = new ArrayList<>();
        try {
            Class.forName(driveName);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuestionBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        try(Connection con = DriverManager.getConnection(dbURL, userDB, passDB)){
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select QCode from TestBank where TCode = '"+tCode+"'");
            while(rs.next()){
               lq.add(rs.getString(1));
            }
            con.close();
            return lq;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    public static void main(String[] args) {
        System.out.println(getQuestionByTcode(1));
    }
}
